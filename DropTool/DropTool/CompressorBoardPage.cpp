// BoardCompressorPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "CompressorBoardPage.h"

#include "CanFrame.h"


// ���������� ���� CMCompressorBoardPage

IMPLEMENT_DYNCREATE(CMCompressorBoardPage, CMToolPage)

CMCompressorBoardPage::CMCompressorBoardPage()
	: CMToolPage(CMCompressorBoardPage::IDD)
{
	m_NodeAddress = 0x9;

	m_IsSensorsStateChecked = false;
	m_IsMotorOn = false;
}

CMCompressorBoardPage::~CMCompressorBoardPage()
{
}

void CMCompressorBoardPage::DoDataExchange(CDataExchange* pDX)
{
	CMToolPage::DoDataExchange(pDX);
}

BOOL CMCompressorBoardPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();
	CSliderCtrl* sliderCtrl;
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_VOLTAGE);
	sliderCtrl->SetLineSize(1);
	sliderCtrl->SetPageSize(10);
	sliderCtrl->SetRange(0, 200);
	sliderCtrl->SetPos(50);
	GetDlgItem(IDC_EDIT_MOTOR_VOLTAGE)->SetWindowText(_T("50"));

	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_CURRENT);
	sliderCtrl->SetLineSize(1);
	sliderCtrl->SetPageSize(10);
	sliderCtrl->SetRange(0, 200);
	sliderCtrl->SetPos(50);
	GetDlgItem(IDC_EDIT_MOTOR_CURRENT)->SetWindowText(_T("50"));

	((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(true);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMCompressorBoardPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	// ��� "����� ����" � ��������� CanKingdom 105M
	if(canFrame->m_ExtendedId == (m_NodeAddress + 16) )
	{
		if(!m_IsFirmwareFor105M)
		{
			m_IsFirmwareFor105M = true;
			m_VersionString.Format(_T("v. %d.%d (%d/%d/%d)"),
				canFrame->m_Data.Byte[2],
				canFrame->m_Data.Byte[3],
				canFrame->m_Data.Byte[6],
				canFrame->m_Data.Byte[5],
				canFrame->m_Data.Byte[4]+2000
				);
			Invalidate(true);
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		int page = m_IsFirmwareFor105M ? canFrame->m_Data.Byte[1] : canFrame->m_Data.Byte[0];
		// ������������ ����� �� ������ ������ ������.
		switch(page)
		{
		case 1:
			m_IsFirmwareFor105M = false;
			CreateVersionString(canFrame);
			break;
		case 'H':
			{
				int sensorsState = canFrame->m_Data.Byte[2];
				int counter = canFrame->m_Data.Int16[3];
				CString text;
				const int c_controlID[] = {
					IDC_RADIO_HALL_1, IDC_RADIO_HALL_2, IDC_RADIO_HALL_3,
					0
				};

				for(int i = 0; c_controlID[i] != 0; i++)
				{
					CButton* radioButton = (CButton*)GetDlgItem(c_controlID[i]);
					radioButton->SetCheck((sensorsState & 0x1) != 0);
					sensorsState >>= 1;
				}
				text.Format(_T("%d"), counter);
				GetDlgItem(IDC_EDIT_COUNTER)->SetWindowText(text);
			}
			break;
		case 'M':
			{
				int speed = canFrame->m_Data.Int16[3];
				CString text;
				text.Format(_T("%d"), speed);
				GetDlgItem(IDC_EDIT_MOTOR_SPEED)->SetWindowText(text);
			}
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		if(m_IsFirmwareFor105M)
		{
			ShowStatus105M(canFrame);
		}
		else
		{
			ShowStatus205(canFrame);
		}
	}
	else
	{
		int type = canFrame->m_ExtendedId; // & ((m_NodeAddress << 4)
	}
}

void CMCompressorBoardPage::ShowStatus205(const CGCanFrame* canFrame)
{
	// ���������� ��������� �������.
	CSCompressorFrame* compressorStatus = (CSCompressorFrame*)canFrame->m_Data.Byte;
	int sensor1, sensor2;
	int motorState;
	CButton* radioButton;

	motorState = compressorStatus->DeviceState1;
	sensor1 = motorState & COMPR_SENSOR_MIN;
	sensor2 = motorState & COMPR_SENSOR_MAX;

	radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_MIN);
	radioButton->SetCheck(sensor1 != 0);
	radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_MAX);
	radioButton->SetCheck(sensor2 != 0);


	bool isMotorOn = ((motorState & (COMPR_MOTOR_FORWARD | COMPR_MOTOR_BACKWARD)) != 0);
	if(!isMotorOn && m_IsMotorOn)
	{
		radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
		radioButton->SetCheck(false);
		GetDlgItem(IDC_RADIO_FORWARD)->EnableWindow(true);
		GetDlgItem(IDC_RADIO_BACKWARD)->EnableWindow(true);
	}
	m_IsMotorOn = isMotorOn;

	motorState = compressorStatus->DeviceState2;
	sensor1 = motorState & COMPR_CASSETTE_1;
	sensor2 = motorState & COMPR_CASSETTE_2;

	radioButton = (CButton*)GetDlgItem(IDC_RADIO_SENSOR_CASSETTE_1);
	radioButton->SetCheck(sensor1 != 0);
	radioButton = (CButton*)GetDlgItem(IDC_RADIO_SENSOR_CASSETTE_2);
	radioButton->SetCheck(sensor2 != 0);

	int pressure = compressorStatus->PressureValue;
	CString text;
	if( (compressorStatus->DeviceState2 & COMPR_PRESSURE_NEGATIVE) != 0 )
	{
		pressure = -pressure;
	}
	text.Format(_T("%d"), pressure);
	GetDlgItem(IDC_EDIT_PRESSURE)->SetWindowText(text);

	int errorCode = compressorStatus->ErrorCode;
	if(errorCode != 0)
	{
		ShowErrorMessage(IDS_ERROR_COMPRESSOR, IDS_ERROR_COMPRESSOR, errorCode);
	}
}

void CMCompressorBoardPage::ShowStatus105M(const CGCanFrame* canFrame)
{
	// ���������� ��������� �������.
	CSCompressorFrame105M* compressorStatus = (CSCompressorFrame105M*)canFrame->m_Data.Byte;
	int sensor1, sensor2;
	int motorState;
	CButton* radioButton;

	motorState = compressorStatus->DeviceState1;
	sensor1 = motorState & COMPR_M_SENSOR_MIN;
	sensor2 = motorState & COMPR_M_SENSOR_MAX;

	radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_MIN);
	radioButton->SetCheck(sensor1 != 0);
	radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_MAX);
	radioButton->SetCheck(sensor2 != 0);


	bool isMotorOn = ((motorState & (COMPR_M_MOTOR_FORWARD | COMPR_M_MOTOR_BACKWARD)) != 0);
	if(!isMotorOn && m_IsMotorOn)
	{
		radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
		radioButton->SetCheck(false);
		GetDlgItem(IDC_RADIO_FORWARD)->EnableWindow(true);
		GetDlgItem(IDC_RADIO_BACKWARD)->EnableWindow(true);
	}
	m_IsMotorOn = isMotorOn;


	int pressure = compressorStatus->PressureValue;
	CString text;
	text.Format(_T("%d"), pressure);
	GetDlgItem(IDC_EDIT_PRESSURE)->SetWindowText(text);

	int errorCode = compressorStatus->ErrorCode;
	if(errorCode != 0)
	{
		ShowErrorMessage(IDS_ERROR_COMPRESSOR, IDS_ERROR_COMPRESSOR, errorCode);
	}
}

BEGIN_MESSAGE_MAP(CMCompressorBoardPage, CMToolPage)
	ON_BN_CLICKED(IDC_CHECK_START_MOTOR, &CMCompressorBoardPage::OnBnClickedCheckStartMotor)
	ON_BN_CLICKED(IDC_CHECK_VALVE_1, &CMCompressorBoardPage::OnBnClickedCheckValve1)
	ON_BN_CLICKED(IDC_CHECK_VALVE_2, &CMCompressorBoardPage::OnBnClickedCheckValve2)
	ON_BN_CLICKED(IDC_CHECK_HALL_SENSORS, &CMCompressorBoardPage::OnBnClickedCheckHallSensors)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


void CMCompressorBoardPage::OnBnClickedCheckStartMotor()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
	CSliderCtrl* sliderCtrl;

	CGCanFrame canFrame;
	int voltage;
	int current;
	bool start = (checkButton->GetCheck() != 0);
	
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_VOLTAGE);
	voltage = sliderCtrl->GetPos();
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_CURRENT);
	current = sliderCtrl->GetPos();

	canFrame.m_Data.Byte[0] = 'M';
	if(start)
	{
		checkButton = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		canFrame.m_Data.Byte[1] = BYTE(checkButton->GetCheck() ? 1 : 0xFF);
		canFrame.m_Data.Byte[2] = BYTE(voltage);
		canFrame.m_Data.Byte[3] = BYTE(current);
	}
	else
	{
		// ��������� ���� ��������.
	}

	SendWriteCommand(&canFrame);
	GetDlgItem(IDC_RADIO_FORWARD)->EnableWindow(!start);
	GetDlgItem(IDC_RADIO_BACKWARD)->EnableWindow(!start);

	m_IsMotorOn = true;
}

void CMCompressorBoardPage::OnBnClickedCheckValve1()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_VALVE_1);
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = BYTE(checkButton->GetCheck() ? 1 : 0);
	canFrame.m_Data.Byte[2] = 0;
	SendWriteCommand(&canFrame);
}

void CMCompressorBoardPage::OnBnClickedCheckValve2()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_VALVE_2);
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = BYTE(checkButton->GetCheck() ? 1 : 0);
	canFrame.m_Data.Byte[2] = 1;
	SendWriteCommand(&canFrame);
}

void CMCompressorBoardPage::OnBnClickedCheckHallSensors()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_HALL_SENSORS);
	m_IsSensorsStateChecked = (checkButton->GetCheck() != 0);
}

void CMCompressorBoardPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl* sliderCtrl = (CSliderCtrl*)pScrollBar;
	int sliderID = sliderCtrl->GetDlgCtrlID();

	switch(nSBCode)
	{
	case SB_ENDSCROLL:
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		{
			int editId = 0;
			CString text;
			nPos = sliderCtrl->GetPos();
			switch(sliderID)
			{
			case IDC_SLIDER_MOTOR_VOLTAGE:
				editId = IDC_EDIT_MOTOR_VOLTAGE;
				SetMotorVoltage();
				break;
			case IDC_SLIDER_MOTOR_CURRENT:
				editId = IDC_EDIT_MOTOR_CURRENT;
				SetMotorVoltage();
				break;
			}
			text.Format(_T("%d"), nPos);
			GetDlgItem(editId)->SetWindowText(text);
		}
		break;
	}

	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMCompressorBoardPage::OnTimer()
{
	if(m_IsSensorsStateChecked)
	{
		SendReadCommand('H');
		SendReadCommand('M');
	}
}

void CMCompressorBoardPage::SetMotorVoltage()
{
	CSliderCtrl* sliderCtrl;
	int voltage;
	int current;
	CGCanFrame canFrame;

	if(m_IsMotorOn)
	{
		sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_VOLTAGE);
		voltage = sliderCtrl->GetPos();
		sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_MOTOR_CURRENT);
		current = sliderCtrl->GetPos();

		canFrame.m_Data.Byte[0] = 'b';
		canFrame.m_Data.Byte[1] = (BYTE)voltage;
		canFrame.m_Data.Byte[2] = (BYTE)current;
		SendWriteCommand(&canFrame);
	}
}

