#include "stdafx.h"
#include "resource.h"
#include "MotorsBoardPage.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

enum
{
	OUTPUT_LIFT,
	INPUT_LIFT,
};

IMPLEMENT_DYNCREATE(CMMotorsBoardPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMMotorsBoardPage property page

CMMotorsBoardPage::CMMotorsBoardPage() : CMToolPage(CMMotorsBoardPage::IDD)
{
	// ����� ����������� ��������.
	m_NodeAddress = 0x06;
	//m_VersionString = _T("Version");

	m_IsLiftMotorOn = false;
	m_IsStepMotorOn = false;
	m_IsSensorsStateChecked = false;

	m_SelectedLiftMotor = 0;
	m_SelectedStepMotor = 0;
}

CMMotorsBoardPage::~CMMotorsBoardPage()
{
}

void CMMotorsBoardPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

BOOL CMMotorsBoardPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_LIFT_MOTOR);
	CString text;

	for(int i = 0; i < 2; i++)
	{
		text.LoadString(IDS_CONTROL_MOTORS_LIFT_0+i);
		comboBox->AddString(text);
	}
	comboBox->SetCurSel(0);
	
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_STEP_MOTOR);
	for(int i = 0; i < 6; i++)
	{
		text.LoadString(IDS_CONTROL_MOTORS_STEP_0+i);
		comboBox->AddString(text);
	}
	comboBox->SetCurSel(0);
	
	CSliderCtrl* sliderCtrl;
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_VOLTAGE);
	sliderCtrl->SetLineSize(1);
	sliderCtrl->SetPageSize(10);
	sliderCtrl->SetRange(0, 200);
	sliderCtrl->SetPos(50);
	GetDlgItem(IDC_EDIT_LIFT_MOTOR_VOLTAGE)->SetWindowText(_T("50"));

	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_CURRENT);
	sliderCtrl->SetLineSize(1);
	sliderCtrl->SetPageSize(10);
	sliderCtrl->SetRange(0, 200);
	sliderCtrl->SetPos(50);
	GetDlgItem(IDC_EDIT_LIFT_MOTOR_CURRENT)->SetWindowText(_T("50"));

	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_FREQUENCY);
	sliderCtrl->SetLineSize(10);
	sliderCtrl->SetPageSize(100);
	sliderCtrl->SetRange(100, 1000);
	sliderCtrl->SetPos(200);
	GetDlgItem(IDC_EDIT_FREQUENCY)->SetWindowText(_T("200"));

	GetDlgItem(IDC_EDIT_LIFT_MOTOR_R)->SetWindowText(_T("2"));
	GetDlgItem(IDC_EDIT_STEPS)->SetWindowText(_T("200"));

	((CButton*)GetDlgItem(IDC_RADIO_LIFT_FORWARD))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_STEP_FORWARD))->SetCheck(true);

	return result;
}

void CMMotorsBoardPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			CreateVersionString(canFrame);
			break;
		case 'H':
			{
				int sensorsState = (m_SelectedLiftMotor == INPUT_LIFT ? canFrame->m_Data.Byte[1] : canFrame->m_Data.Byte[2]);
				const int c_controlID[] = {
					IDC_RADIO_HALL_1, IDC_RADIO_HALL_2, IDC_RADIO_HALL_3, 0, 
					0, 0,
					IDC_RADIO_OPTO_SENSOR_LIFT_1, IDC_RADIO_OPTO_SENSOR_LIFT_2
					
				};

				for(int i = 0; i < 8; i++)
				{
					if(c_controlID[i] != 0)
					{
						CButton* radioButton = (CButton*)GetDlgItem(c_controlID[i]);
						radioButton->SetCheck((sensorsState & 0x1) != 0);
					}
					sensorsState >>= 1;
				}
			}
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		// ��������� ��������� ������� ������� ����������.
		int sensor1, sensor2;
		int motorState = -1;
		CSCarouselFrame* carouselStatus = (CSCarouselFrame*)canFrame->m_Data.Byte;
		CSMonochromatorFrame* monoStatus = (CSMonochromatorFrame*)canFrame->m_Data.Byte;
		CButton* radioButton;

		switch(m_SelectedStepMotor)
		{
		case 0:
			if( (monoStatus->Mode & MOTORS_MONOCHROMATOR) == MOTORS_MONOCHROMATOR &&
				(monoStatus->Mode & MOTORS_MONO_2) == MOTORS_MONO_2 )
			{
				motorState = monoStatus->State;
				sensor1 = motorState & MS_SENSOR_MIN;
				sensor2 = motorState & MS_SENSOR_ROT;
			}
			break;
		case 1:
			if( (monoStatus->Mode & MOTORS_MONOCHROMATOR) == MOTORS_MONOCHROMATOR &&
				(monoStatus->Mode & MOTORS_MONO_2) == 0 )
			{
				motorState = monoStatus->State;
				sensor1 = motorState & MS_SENSOR_MIN;
				sensor2 = motorState & MS_SENSOR_ROT;
			}
			break;
		case 2:
			if( (carouselStatus->Mode & MOTORS_AUTOSAMPLER) == MOTORS_AUTOSAMPLER &&
				(carouselStatus->Mode & MOTORS_OUTPUT_CAROUSEL) == 0 )
			{
				motorState = carouselStatus->State;
				sensor1 = motorState & CS_SENSOR;
				sensor2 = motorState & CS_SENSOR_2;

				// ��������� ���������� � ������� ������
				radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_BUTTON_LEFT);
				radioButton->SetCheck((carouselStatus->CurrentPosition & BS_LEFT_PRESSED) != 0);
				radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_BUTTON_RIGHT);
				radioButton->SetCheck((carouselStatus->CurrentPosition & BS_RIGHT_PRESSED) != 0);
			}
			break;
		case 3:
			if( (carouselStatus->Mode & MOTORS_AUTOSAMPLER) == MOTORS_AUTOSAMPLER &&
				(carouselStatus->Mode & MOTORS_OUTPUT_CAROUSEL) == MOTORS_OUTPUT_CAROUSEL )
			{
				motorState = carouselStatus->State;
				sensor1 = motorState & CS_SENSOR;
				sensor2 = motorState & CS_SENSOR_2;
			}
			break;
		case 4:
			if( (carouselStatus->Mode & MOTORS_AUTOSAMPLER) != 0 )
			{
				motorState = carouselStatus->MoverState;
				sensor1 = motorState & OMS_SENSOR_1;
				sensor2 = motorState & OMS_SENSOR_2;
			}
			break;
		case 5:
			if( (carouselStatus->Mode & MOTORS_AUTOSAMPLER) != 0 )
			{
				motorState  = carouselStatus->OpenerState;
				sensor1 = motorState & OMS_SENSOR_1;
				sensor2 = motorState & OMS_SENSOR_2;
			}
			break;
		}

		// �������� ����� ���������.
		if(motorState >= 0)
		{
			radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_STEP_1);
			radioButton->SetCheck(sensor1 != 0);
			radioButton = (CButton*)GetDlgItem(IDC_RADIO_OPTO_SENSOR_STEP_2);
			radioButton->SetCheck(sensor2 != 0);

			bool isStepMotorOn = ((motorState & (MS_FORWARD | MS_BACKWARD)) != 0);
			if(!isStepMotorOn && m_IsStepMotorOn)
			{
				radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_STEP_MOTOR);
				radioButton->SetCheck(false);
				GetDlgItem(IDC_COMBO_STEP_MOTOR)->EnableWindow(true);
			}
			m_IsStepMotorOn = isStepMotorOn;
		}

		bool isLiftMotorOn = ((carouselStatus->LiftState & (LS_GO_DOWN | LS_GO_UP)) == 0);
		if( (carouselStatus->Mode & (MOTORS_INPUT_CAROUSEL | MOTORS_AUTOSAMPLER))
			== (MOTORS_INPUT_CAROUSEL | MOTORS_AUTOSAMPLER) )
		{
			if(m_SelectedLiftMotor == INPUT_LIFT && 
				!isLiftMotorOn && m_IsLiftMotorOn)
			{
				radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_LIFT_MOTOR);
				radioButton->SetCheck(false);
				GetDlgItem(IDC_COMBO_LIFT_MOTOR)->EnableWindow(true);
			}
		}
		if( (carouselStatus->Mode & (MOTORS_OUTPUT_CAROUSEL | MOTORS_AUTOSAMPLER))
			== (MOTORS_OUTPUT_CAROUSEL | MOTORS_AUTOSAMPLER) )
		{
			if(m_SelectedLiftMotor == OUTPUT_LIFT &&
				!isLiftMotorOn && m_IsLiftMotorOn)
			{
				radioButton = (CButton*)GetDlgItem(IDC_CHECK_START_LIFT_MOTOR);
				radioButton->SetCheck(false);
				GetDlgItem(IDC_COMBO_LIFT_MOTOR)->EnableWindow(true);
			}
		}
		m_IsLiftMotorOn = isLiftMotorOn;

		return;
	}
}

BEGIN_MESSAGE_MAP(CMMotorsBoardPage, CMToolPage)
	ON_BN_CLICKED(IDC_CHECK_START_LIFT_MOTOR, &CMMotorsBoardPage::OnBnClickedStartLiftMotor)
	ON_BN_CLICKED(IDC_CHECK_HALL_SENSORS, &CMMotorsBoardPage::OnBnClickedCheckHallSensors)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_CHECK_START_STEP_MOTOR, &CMMotorsBoardPage::OnBnClickedStartStepMotor)
	ON_CBN_SELCHANGE(IDC_COMBO_LIFT_MOTOR, &CMMotorsBoardPage::OnSelectLiftMotor)
	ON_CBN_SELCHANGE(IDC_COMBO_STEP_MOTOR, &CMMotorsBoardPage::OnSelectStepMotor)
END_MESSAGE_MAP()


void CMMotorsBoardPage::OnBnClickedStartLiftMotor()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_LIFT_MOTOR);
	CSliderCtrl* sliderCtrl;

	CGCanFrame canFrame;
	int voltage;
	int current;
	int steps = GetIntValue(IDC_EDIT_LIFT_MOTOR_R)*24;
	bool start = (checkButton->GetCheck() != 0);

	if(((CButton*)GetDlgItem(IDC_RADIO_LIFT_BACKWARD))->GetCheck() != 0)
	{
		steps = -steps;
	}

	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_VOLTAGE);
	voltage = sliderCtrl->GetPos();
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_CURRENT);
	current = sliderCtrl->GetPos();

	canFrame.m_Data.Byte[0] = 'b';
	canFrame.m_Data.Byte[1] = (BYTE)(1-m_SelectedLiftMotor);
	if(start)
	{
		canFrame.m_Data.Int16[1] = (INT16)steps;
		canFrame.m_Data.Byte[4] = (BYTE)voltage;
		canFrame.m_Data.Byte[5] = (BYTE)current;
	}
	else
	{
		// ��������� ���� ��������.
	}

	SendWriteCommand(&canFrame);
	GetDlgItem(IDC_COMBO_LIFT_MOTOR)->EnableWindow(!start);

	m_IsLiftMotorOn = true;
}

void CMMotorsBoardPage::OnBnClickedCheckHallSensors()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_HALL_SENSORS);
	m_IsSensorsStateChecked = (checkButton->GetCheck() != 0);
}


void CMMotorsBoardPage::OnBnClickedStartStepMotor()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_STEP_MOTOR);
	CSliderCtrl* sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_FREQUENCY);
	bool start = (checkButton->GetCheck() != 0);

	CGCanFrame canFrame;
	int frequency = sliderCtrl->GetPos();
	int steps = GetIntValue(IDC_EDIT_STEPS);

	if(((CButton*)GetDlgItem(IDC_RADIO_STEP_BACKWARD))->GetCheck() != 0)
	{
		steps = -steps;
	}

	canFrame.m_Data.Byte[0] = 's';
	canFrame.m_Data.Byte[1] = (BYTE)m_SelectedStepMotor;
	if(start)
	{
		canFrame.m_Data.Int16[1] = (INT16)steps;
		canFrame.m_Data.Uint16[2] = (UINT16)frequency;
	}
	else
	{
		// ��������� ���� ��������.
	}
	SendWriteCommand(&canFrame);
	GetDlgItem(IDC_COMBO_STEP_MOTOR)->EnableWindow(!start);

	m_IsStepMotorOn = true;
}

void CMMotorsBoardPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl* sliderCtrl = (CSliderCtrl*)pScrollBar;
	int sliderID = sliderCtrl->GetDlgCtrlID();

	switch(nSBCode)
	{
	case SB_ENDSCROLL:
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		{
			int editId = 0;
			CString text;
			nPos = sliderCtrl->GetPos();
			switch(sliderID)
			{
			case IDC_SLIDER_LIFT_MOTOR_VOLTAGE:
				editId = IDC_EDIT_LIFT_MOTOR_VOLTAGE;
				SetLiftMotorVoltage();
				break;
			case IDC_SLIDER_LIFT_MOTOR_CURRENT:
				editId = IDC_EDIT_LIFT_MOTOR_CURRENT;
				SetLiftMotorVoltage();
				break;
			case IDC_SLIDER_FREQUENCY:
				editId = IDC_EDIT_FREQUENCY;
				SetStepMotorFrequency(nPos);
				break;
			}
			text.Format(_T("%d"), nPos);
			GetDlgItem(editId)->SetWindowText(text);
		}
		break;
	}

	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMMotorsBoardPage::SetLiftMotorVoltage()
{
	CSliderCtrl* sliderCtrl;
	int voltage;
	int current;
	CGCanFrame canFrame;

	if(m_IsLiftMotorOn)
	{
		sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_VOLTAGE);
		voltage = sliderCtrl->GetPos();
		sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_LIFT_MOTOR_CURRENT);
		current = sliderCtrl->GetPos();

		canFrame.m_Data.Byte[0] = 'b';
		canFrame.m_Data.Byte[1] = (BYTE)(1-m_SelectedLiftMotor);
		canFrame.m_Data.Byte[4] = (BYTE)voltage;
		canFrame.m_Data.Byte[5] = (BYTE)current;
		SendWriteCommand(&canFrame);
	}
}

void CMMotorsBoardPage::SetStepMotorFrequency(int frequency)
{
	CGCanFrame canFrame;

	if(m_IsStepMotorOn)
	{
		canFrame.m_Data.Byte[0] = 's';
		canFrame.m_Data.Byte[1] = (BYTE)m_SelectedStepMotor;
		canFrame.m_Data.Byte[4] = (BYTE)frequency;
		SendWriteCommand(&canFrame);
	}
}

void CMMotorsBoardPage::OnSelectLiftMotor()
{
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_LIFT_MOTOR);
	m_SelectedLiftMotor = comboBox->GetCurSel();
}

void CMMotorsBoardPage::OnSelectStepMotor()
{
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_STEP_MOTOR);
	m_SelectedStepMotor = comboBox->GetCurSel();
}

void CMMotorsBoardPage::OnTimer()
{
	if(m_IsSensorsStateChecked)
	{
		SendReadCommand('H');
	}
}

