// ConnectionPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "ConnectionPage.h"

#include "MainPropertySheet.h"

// ���������� ���� CMConnectionPage

IMPLEMENT_DYNAMIC(CMConnectionPage, CMToolPage)

CMConnectionPage::CMConnectionPage()
	: CMToolPage(CMConnectionPage::IDD)
{
}

CMConnectionPage::~CMConnectionPage()
{
}

void CMConnectionPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}

void CMConnectionPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
}

BEGIN_MESSAGE_MAP(CMConnectionPage, CMToolPage)
	ON_BN_CLICKED(IDC_RADIO_USB_CAN, &CMConnectionPage::OnBnClickedRadioButtonUsbCan)
	ON_BN_CLICKED(IDC_RADIO_MAIN_CONTROLLER_205, &CMConnectionPage::OnBnClickedRadioButtonCapel205)
	ON_BN_CLICKED(IDC_RADIO_MAIN_CONTROLLER_115, &CMConnectionPage::OnBnClickedRadioButtonCapel115)
END_MESSAGE_MAP()

// ����������� ��������� CMConnectionPage
BOOL CMConnectionPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();

	((CButton*)GetDlgItem(IDC_RADIO_USB_CAN))->SetCheck(true);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMConnectionPage::OnBnClickedRadioButtonUsbCan()
{
	bool isChecked = ((CButton*)GetDlgItem(IDC_RADIO_USB_CAN))->GetCheck() != 0;
	if(isChecked)
	{
		((CMMainPropertySheet*)GetParent())->SetUsbCanAdapter(c_AdapterUsbCan);
	}
}

void CMConnectionPage::OnBnClickedRadioButtonCapel205()
{
	bool isChecked = ((CButton*)GetDlgItem(IDC_RADIO_MAIN_CONTROLLER_205))->GetCheck() != 0;
	if(isChecked)
	{
		((CMMainPropertySheet*)GetParent())->SetUsbCanAdapter(c_AdapterCapel205);
	}
}

void CMConnectionPage::OnBnClickedRadioButtonCapel115()
{
	bool isChecked = ((CButton*)GetDlgItem(IDC_RADIO_MAIN_CONTROLLER_115))->GetCheck() != 0;
	if(isChecked)
	{
		((CMMainPropertySheet*)GetParent())->SetUsbCanAdapter(c_AdapterCapel115);
	}
}

