#include "stdafx.h"
#include "resource.h"
#include "MainPropertySheet.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

//const unsigned short c_UsbVendorID = 0x03EB;
//const unsigned short c_UsbProductID = 0x6201;

//USB\VID_090C& PID_1000
const unsigned short c_UsbVendorID = 0x090C;
const unsigned short c_UsbCanProductID = 0x1000;	// USB

//const unsigned short c_UsbVendorID = 0x0D8C;	
//const unsigned short c_UsbCanProductID = 0x013C;	// USB
// 
//const unsigned short c_UsbVendorID = 0x16E0;	// Lumex
//const unsigned short c_UsbCanProductID = 0x0380;	// USB-CAN adapter

#if 1
const unsigned short c_Capel205_ProductID  = 0x0301;	// Capel205
const unsigned short c_Capel115_ProductID  = 0x0302;	// Capel205
// ������������� ������� � ����������.
const BYTE NET_ADDR_CAPEL   = 190;
#else
const unsigned short c_CapelProductID  = 0x0305;	// Panorama
const BYTE NET_ADDR_CAPEL   = 150;
#endif


/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet

IMPLEMENT_DYNAMIC(CMMainPropertySheet, CMFCPropertySheet)

CMMainPropertySheet::CMMainPropertySheet()
	 : CMFCPropertySheet(IDS_MAIN_CAPTION, NULL)
{
	m_psh.dwFlags |= PSH_NOAPPLYNOW; // | PSH_MODELESS;
	m_psh.dwFlags &= ~PSH_HASHELP;


	SetLook(PropSheetLook_Tree, 250);
	SetIconsList(IDB_TREE_ICONS, 16);
	EnablePageHeader(30);

	CMFCPropertySheetCategoryInfo* �ategoryNode = AddTreeCategory (
		_T("����"), 0, 1);
	AddPageToTree (�ategoryNode, &m_AutosamplerPage, -1, 2);
	AddPageToTree (�ategoryNode, &m_MonochromatorPage, -1, 2);
	AddPageToTree (�ategoryNode, &m_CompressorPage, -1, 2);
	AddPageToTree (�ategoryNode, &m_ThermostatPage, -1, 2);

	CMFCPropertySheetCategoryInfo* �ategoryBoard = AddTreeCategory (
		_T("�����"), 0, 1);

	AddPageToTree (�ategoryBoard, &m_MotorsBoardPage, -1, 2);
	AddPageToTree (�ategoryBoard, &m_CompressorBoardPage, -1, 2);
	AddPageToTree (�ategoryBoard, &m_PhotometricBoardPage, -1, 2);
	AddPageToTree (�ategoryBoard, &m_LampsBoardPage, -1, 2);

	CMFCPropertySheetCategoryInfo* �ategorySetup = AddTreeCategory (
		_T("���������"), 0, 1);
	AddPageToTree (�ategorySetup, &m_ConnectionPage, -1, 2);

	m_TimeStamp = 0;
	m_LastCheckTime = 0;
	m_LastStatusFrameTime = 0;

	m_UseUsbCanAdapter = true;
	m_IsCanConnected = false;
	m_UsbProductID = c_UsbCanProductID;
}

CMMainPropertySheet::~CMMainPropertySheet()
{
	// ����� ����������� � DestroyWindow.
}


UINT WM_DATA_RECEIVED = ::RegisterWindowMessage(_T("Message Data Received"));
const int idTree = 101;

BEGIN_MESSAGE_MAP(CMMainPropertySheet, CMFCPropertySheet)
	ON_WM_TIMER()
//	ON_WM_CLOSE()
	ON_WM_DEVICECHANGE()
	ON_REGISTERED_MESSAGE(WM_DATA_RECEIVED, OnDataReceived)
	//ON_BN_CLICKED(IDOK, OnOk)
	ON_NOTIFY(TVN_SELCHANGEDW, idTree, &CMMainPropertySheet::OnSelectTree)
END_MESSAGE_MAP()

// ������� ������� ���, ����� ������ ���������� ��c������.
void CMMainPropertySheet::OnSelectTree(NMHDR* pNMHDR, LRESULT* pResult)
{
	CMFCPropertySheet::OnSelectTree(pNMHDR, pResult);

	HTREEITEM firstItem = m_wndTree.GetRootItem();
	m_wndTree.Expand(firstItem, TVE_EXPAND);
	HTREEITEM secondItem = m_wndTree.GetNextSiblingItem(firstItem);
	m_wndTree.Expand(secondItem, TVE_EXPAND);
}

/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet message handlers
BOOL CMMainPropertySheet::OnInitDialog()
{
	CString text;
	CMFCPropertySheet::OnInitDialog();
	GetDlgItem(IDHELP)->ShowWindow(false);
	GetDlgItem(IDOK)->ShowWindow(false);
	text.LoadString(IDS_CONTROL_EXIT);
	GetDlgItem(IDCANCEL)->SetWindowText(text);
	//GetDlgItem(IDCANCEL)->ShowWindow(false);

	CreateDataChannel();
	StartDataExchange();

	// ��������� ������ ��� �������� ����� � ���������� ��������.
	m_TimerID = SetTimer(1, 100, 0);
	if(m_TimerID == 0)
	{
		AfxMessageBox(IDS_ERROR_STARTING_TIMER);		// "Can't start timer"
		return -1;
	}
	
	CRect rect;
	int width;
	GetDlgItem(IDOK)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	width = rect.Width();
	rect.left = rect.Height();
	rect.right = rect.left + width*4;
	m_StatusInfo.Create(NULL, WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE, rect, this);
	m_StatusInfo.SetFont(GetDlgItem(IDOK)->GetFont());

	ShowUsbState();

	/*
	// ������������� "��������" ������ �������� � ���� CMFCPropertySheet::OnSelectTree
	// � �� ������� �� �����. ���� 
	LONG style = ::GetWindowLong(m_wndTree.GetSafeHwnd(), GWL_STYLE);
	style |= TVS_SINGLEEXPAND;
	style |= TVS_HASLINES;
	::SetWindowLong(m_wndTree.GetSafeHwnd(), GWL_STYLE, style);
	*/

	SetActivePage(&m_ConnectionPage);

	HTREEITEM firstItem = m_wndTree.GetRootItem();
	m_wndTree.Expand(firstItem, TVE_EXPAND);
	HTREEITEM secondItem = m_wndTree.GetNextSiblingItem(firstItem);
	m_wndTree.Expand(secondItem, TVE_EXPAND);

	return TRUE;
}

void CMMainPropertySheet::OnTimer(UINT nIDEvent)
{
	if(nIDEvent != m_TimerID)
		return;

	if(m_DataChannel.IsOpened())
	{
		CString text;
		int time = GetTickCount();
		if(time - m_LastCheckTime > 1000)
		{
			SendCanReadVersion();
			m_LastCheckTime = time;
		}
		if(time - m_TimeStamp > 2000)
		{
			if(m_IsCanConnected)
			{
				for(int i = 0; i < GetPageCount(); i++)
				{
					((CMToolPage*)GetPage(i))->ClearVersionString();
				}

			}

			// ���������� ��������� �� ���������� �����.
			text.LoadString(IDS_CONTROL_CAN_NOT_CONNECTED);
			m_StatusInfo.SetWindowText(text);
			m_IsCanConnected = false;
		}
		else
		{
			if(!m_IsCanConnected)
			{
				SendCanFrameEnableStatus();
			}
			// ������ ������.
			m_StatusInfo.SetWindowText(text);
			m_IsCanConnected = true;
		}

		((CMToolPage*)GetActivePage())->OnTimer();
	}
}

BOOL CMMainPropertySheet::OnDeviceChange(UINT nEventType, DWORD_PTR dwData)
{
	// ������ ��������� ��� �������
	if(m_DataChannel.OnDeviceNotificationMessage(nEventType, (UINT*)dwData))
	{
		int numberOfDevices = m_DataChannel.EnumerateDevices(c_UsbVendorID, m_UsbProductID);
		if(numberOfDevices > m_NumberOfDevices)
		{
		//	AfxMessageBox(_T("Device is connected"));
			if(m_NumberOfDevices == 0)
				StartDataExchange();
		}
		else if(numberOfDevices == m_NumberOfDevices)
		{
		//	AfxMessageBox(_T("Device is restarted"));
			if(numberOfDevices != 0)
			{
				StopDataExchange();
				StartDataExchange();
			}
		}
		else
		{
		//	AfxMessageBox(_T("Device is disconnected"));
			if(numberOfDevices == 0)
				StopDataExchange();
		}
		m_NumberOfDevices = numberOfDevices;
		// �������� ������ ��������� �����.
		ShowUsbState();

		// ���������� ������� ������ ����� ���������� �������.
		m_TimeStamp = 0;
	}
	return true;
}

void CMMainPropertySheet::OnOk()
{
	int i = 2;
	return;
}

// OnClose() ������� �� ����������, ������� ������ ���.
BOOL CMMainPropertySheet::DestroyWindow()
{
	if(m_DataChannel.IsOpened())
	{
		if(!m_UseUsbCanAdapter)
		{
			// ��������� ����������� ������� ������� � ������� �� ������ (0x9)
			// ��� ���� ����� 0xFF.
			EnableCanTracing(false);
			::Sleep(100);
		}
	}
	StopDataExchange();

	return CMFCPropertySheet::DestroyWindow();
}


void CMMainPropertySheet::OnDrawPageHeader (CDC* pDC, int nPage, CRect rectHeader)
{
	CWnd* page = GetActivePage();
	CString text;

	page->GetWindowText(text);

	rectHeader.top += 2;
	rectHeader.right -= 2;
	rectHeader.bottom -= 2;

	pDC->FillRect (rectHeader, &afxGlobalData.brBtnFace);
	pDC->Draw3dRect (rectHeader, afxGlobalData.clrBtnShadow, afxGlobalData.clrBtnShadow);

	CDrawingManager dm (*pDC);
	dm.DrawShadow (rectHeader, 2);	

	//strText.Format (_T("Page %d description..."), nPage + 1);

	CRect rectText = rectHeader;
	rectText.DeflateRect (10, 0);

	CFont* pOldFont = pDC->SelectObject (&afxGlobalData.fontBold);
	pDC->SetBkMode (TRANSPARENT);
	pDC->SetTextColor (afxGlobalData.clrBtnText);

	CString versionString = ((CMToolPage*)GetActivePage())->GetVersionString();
	if(!versionString.IsEmpty())
	{
		text += _T("    ");
		text += versionString;
	}

	pDC->DrawText (text, rectText, DT_SINGLELINE | DT_VCENTER);

	pDC->SelectObject (pOldFont);
}

////////////////////////////////////////////////
// ������ � ������� ������

void CMMainPropertySheet::SetUsbCanAdapter(int usedAdapter)
{
	const UINT16 usbProductID[] =
	{
		c_UsbCanProductID,
		c_Capel205_ProductID,
		c_Capel115_ProductID
	};

	m_UseUsbCanAdapter = (usedAdapter == c_AdapterUsbCan);

	StopDataExchange();
	m_UsbProductID = usbProductID[usedAdapter];

	StartDataExchange();
	// �������� ������ ��������� �����.
	ShowUsbState();
}

void CMMainPropertySheet::CreateDataChannel()
{
	if( !m_DataChannel.Create("", DCT_USB_HID, 100, 0) )
	{
		return;
	}

	// ����������� � ����������� USB-���������
	m_DataChannel.SetDeviceNotificationWindow(GetSafeHwnd());

	// ����������� � ��������� ������
	m_DataChannel.SetNotification(GetSafeHwnd(), WM_DATA_RECEIVED, 0);
}

void CMMainPropertySheet::StartDataExchange()
{
	m_NumberOfDevices = m_DataChannel.EnumerateDevices(c_UsbVendorID, m_UsbProductID);
	if(m_NumberOfDevices > 0)
	{
		const int c_bufferLength = 256;
		wchar_t buffer[c_bufferLength];
		CString string;

		m_DataChannel.GetNextDeviceName(true, buffer, c_bufferLength);
		string = CString(buffer);

		m_DataChannel.SelectDevice(0);
		m_DataChannel.StartDataExchange(DCP_HID_1, 100, 100, 0);
	}
}

void CMMainPropertySheet::StopDataExchange()
{
	m_DataChannel.StopDataExchange();
	m_DataChannel.Close();
}

void CMMainPropertySheet::ShowUsbState()
{
	CString text;
	if(m_NumberOfDevices == 0)
	{
		text.LoadString(m_UseUsbCanAdapter ?
				IDS_CONTROL_USB_CAN_NOT_FOUND : IDS_CONTROL_CAPEL_NOT_FOUND);
	}
	else
	{
		if(!m_DataChannel.IsOpened())
		{
			text.LoadString(IDS_CONTROL_USB_NOT_CONNECTED);
		}
	}
	m_StatusInfo.SetWindowText(text);
}

// ������� �������� �����������.
const BYTE GET_CAN_NODE_INFO  = 32;
const BYTE SET_CAN_NODE_INFO  = 33;
const BYTE ENABLE_CAN_TRACING = 34;
const BYTE CAN_NODE_PACKET    = 48;

// �����, ����������� ������������ (���������� � COM-����)
// ���������� � ������������ ������� CAN.
enum
{
  TRACE_STATUS_FLAG    = 0x01,
  TRACE_SET_FLAG       = 0x02,
  TRACE_GET_FLAG       = 0x04,
  TRACE_RESULT_FLAG    = 0x08,
  DISABLE_TRACING_FLAG = 0x80,
};

void CMMainPropertySheet::SendCanFrame(CGCanFrame* canFrame)
{
	if(m_NumberOfDevices == 0)
		return;

	if(m_UseUsbCanAdapter)
	{
		m_DataChannel.SendData((BYTE*)canFrame, sizeof(CGCanFrame));
	}
	else
	{
		UINT32 data[3];
		BYTE* message = (BYTE*)data;
		BYTE command = (canFrame->m_ExtendedId & CAN_ID_READ_CMD) ? GET_CAN_NODE_INFO : SET_CAN_NODE_INFO;
		BYTE nodeAddress = (canFrame->m_ExtendedId >> 4);
		message[0] = NET_ADDR_CAPEL;
		message[1] = command;

		if(command == GET_CAN_NODE_INFO)
		{
			message[2] = nodeAddress;
			message[3] = canFrame->m_Data.Byte[0];
			data[1] = 0;
			data[2] = 0;
		}
		else
		{
			message[2] = nodeAddress;
			message[3] = 0;
			data[1] = canFrame->m_Data.Uint32[0];
			data[2] = canFrame->m_Data.Uint32[1];
		}
		m_DataChannel.SendData(message, 12);
	}
}

void CMMainPropertySheet::EnableCanTracing(bool enable)
{
	BYTE mask = TRACE_STATUS_FLAG | TRACE_RESULT_FLAG;
	// ��������� ����������� ������� ������� � ������� �� ������
	// ��� ���� ����� 0xFF.
	BYTE message[] = { NET_ADDR_CAPEL, ENABLE_CAN_TRACING, 0xFF, 0 };
	if(!enable)
	{
		mask |= DISABLE_TRACING_FLAG;
	}

	message[3] = mask;
	m_DataChannel.SendData(message, 4);
}

void CMMainPropertySheet::SendCanFrameEnableStatus()
{
	if(m_UseUsbCanAdapter)
	{
		CGCanFrame canFrame;
		// ����������������� ������, �������������� ����� �������������
		// ������������� ���������� �� �� ������.
		canFrame.m_ExtendedId = 0;
		canFrame.m_Data.Byte[0] = 0;
		canFrame.m_Data.Byte[3] = 3;
		SendCanFrame(&canFrame);
	}
	else
	{
		EnableCanTracing(true);
	}
}

void CMMainPropertySheet::SendCanReadVersion()
{
	CGCanFrame canFrame;
	// ����������������� ������, �������������� ����� �������������
	// ������������� ���������� �� �� ������.
	canFrame.m_ExtendedId = 0;
	// ������� - ��� ����.
	canFrame.m_Data.Byte[0] = 0;
	// ������ ������ ������.
	canFrame.m_Data.Byte[1] = 1;
	SendCanFrame(&canFrame);
}

LRESULT CMMainPropertySheet::OnDataReceived(WPARAM wParam, LPARAM lParam)
{
	const int cBufferSize = 65;
	BYTE buffer[cBufferSize];
	int size;

	while(m_DataChannel.GetReceivedData(buffer, cBufferSize, &size))
	{
		TRACE("Received: %2x %2x %2x %2x ", buffer[14], buffer[15], buffer[16], buffer[17]);
		CGCanFrame* canFrame = (CGCanFrame*)buffer;

		// ��������� ����������� ���������� �� ��������� �������� �����������.
		if(!m_UseUsbCanAdapter && buffer[0] == NET_ADDR_CAPEL && buffer[1] == CAN_NODE_PACKET)
		{
			UINT16* data = (UINT16*)(buffer+2);
			//int type     = buffer[2+2];
			CGCanFrame cf;
			cf.m_ExtendedId = data[0];
			cf.m_Dlc        = buffer[2+3];
			cf.m_Flags      = 0;
			cf.m_Data.Uint16[0] = data[2];
			cf.m_Data.Uint16[1] = data[3];
			cf.m_Data.Uint16[2] = data[4];
			cf.m_Data.Uint16[3] = data[5];

			canFrame = &cf;
		}
		if((canFrame->m_Flags & CAN_FLAG_ECHO_FRAME) == 0)
		{
			// ��� ����� �� �����, � �� "���" �����������.
			CMToolPage* activePage = (CMToolPage*)GetActivePage();
			activePage->ProcessStatusFrame(canFrame);

			m_TimeStamp = GetTickCount();

			if((canFrame->m_ExtendedId & 0xF) == CAN_ID_STATUS_FRAME)
			{
				m_LastStatusFrameTime = m_TimeStamp;
			}
			else
			{
				if(m_TimeStamp - m_LastStatusFrameTime > 2000)
				{
					SendCanFrameEnableStatus();
					m_LastStatusFrameTime = m_TimeStamp;
				}
			}
		}


		/*
		CString logString;
		bool isValid = (canFrame->m_ExtendedId & m_FilterMask) == (m_FilterValue & m_FilterMask);

		logString = GetLogString(*canFrame);

		TRACE("Received: %s", logString);
		isValid = isValid || !m_UseFilterForFile;
		if(m_LogFile.m_pStream != 0 && isValid)
		{
			m_LogFile.WriteString(logString);
		}
		*/
	}

	return 0;
}

