// LiftCurrentDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "LiftCurrentDialog.h"

#include "ToolPage.h"
#include "CanFrame.h"

// ���������� ���� CMLiftCurrentDialog

IMPLEMENT_DYNAMIC(CMLiftCurrentDialog, CDialog)

CMLiftCurrentDialog::CMLiftCurrentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMLiftCurrentDialog::IDD, pParent)
	, m_PressDown(_T(""))
	, m_Movement(_T(""))
{
	m_ParentPage = 0;
}

CMLiftCurrentDialog::~CMLiftCurrentDialog()
{
}

void CMLiftCurrentDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PRESS_UP, m_PressUp);
	DDX_Text(pDX, IDC_EDIT_PRESS_DOWN, m_PressDown);
	DDX_Text(pDX, IDC_EDIT_MOVEMENT, m_Movement);
}


BEGIN_MESSAGE_MAP(CMLiftCurrentDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_READ, &CMLiftCurrentDialog::OnBnClickedButtonRead)
	ON_BN_CLICKED(IDC_BUTTON_WRITE, &CMLiftCurrentDialog::OnBnClickedButtonWrite)
END_MESSAGE_MAP()

int CMLiftCurrentDialog::GetIntValue(int controlID)
{
	const int MAX_SIZE = 6;
	TCHAR text[MAX_SIZE];

	GetDlgItem(controlID)->GetWindowText(text, MAX_SIZE);
	return _ttoi(text);
}


// ����������� ��������� CMLiftCurrentDialog

void CMLiftCurrentDialog::OnBnClickedButtonRead()
{
	// TODO: �������� ���� ��� ����������� �����������
}

void CMLiftCurrentDialog::OnBnClickedButtonWrite()
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'l';

	canFrame.m_Data.Byte[1] = BYTE(GetIntValue(IDC_EDIT_PRESS_UP));
	canFrame.m_Data.Byte[2] = BYTE(GetIntValue(IDC_EDIT_PRESS_DOWN));
	canFrame.m_Data.Byte[3] = BYTE(GetIntValue(IDC_EDIT_MOVEMENT));

	m_ParentPage->SendWriteCommand(&canFrame);
}
