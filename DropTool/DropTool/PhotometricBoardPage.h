#pragma once

#include "ToolPage.h"

// ���������� ���� CMPhotometricBoardPage

class CMPhotometricBoardPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMPhotometricBoardPage)

public:
	CMPhotometricBoardPage();
	virtual ~CMPhotometricBoardPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// ������ ����������� ����
	enum { IDD = IDD_PAGE_BOARD_PHOTO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();

public:
	afx_msg void OnBnClickedCheckFlowPotential();
	afx_msg void OnBnClickedButtonInitialization();
	afx_msg void OnBnClickedButtonSetMode();
	afx_msg void OnBnClickedCheckSyncOn();

	DECLARE_MESSAGE_MAP()
};
