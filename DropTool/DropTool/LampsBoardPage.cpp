// LampsBoardPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "LampsBoardPage.h"

#include "CanFrame.h"

// ���������� ���� CMLampsBoardPage

IMPLEMENT_DYNAMIC(CMLampsBoardPage, CMToolPage)

CMLampsBoardPage::CMLampsBoardPage()
	: CMToolPage(CMLampsBoardPage::IDD)
{
	m_NodeAddress = 0x8;

}

CMLampsBoardPage::~CMLampsBoardPage()
{
}

void CMLampsBoardPage::DoDataExchange(CDataExchange* pDX)
{
	CMToolPage::DoDataExchange(pDX);
}


BOOL CMLampsBoardPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();

	CSliderCtrl* sliderCtrl;
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_HEATING);
	sliderCtrl->SetLineSize(100);
	sliderCtrl->SetPageSize(1000);
	sliderCtrl->SetRange(0, 12000);
	sliderCtrl->SetPos(0);
	GetDlgItem(IDC_EDIT_HEATING)->SetWindowText(_T("0"));

	m_LampsModeBitmask.SubclassDlgItem(IDC_LIST_MODE_BITS, this);
	m_LampsModeBitmask.Initialize(IDS_BITS_LAMPS_MODE, 6);
	m_LampsModeBitmask.EnableWindow(false);

	m_LampsStateBitmask.SubclassDlgItem(IDC_LIST_STATUS_BITS, this);
	m_LampsStateBitmask.Initialize(IDS_BITS_LAMPS_STATUS, 6);
	m_LampsStateBitmask.EnableWindow(false);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMLampsBoardPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			CreateVersionString(canFrame);
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		CSLampsFrame* lampsStatus = (CSLampsFrame*)canFrame->m_Data.Byte;
		CString text;
		text.Format(_T("%d"), lampsStatus->SupplyVoltage);
		GetDlgItem(IDC_EDIT_VOLTAGE_SUPPLY)->SetWindowText(text);
		text.Format(_T("%d"), lampsStatus->CathodeVoltage);
		GetDlgItem(IDC_EDIT_VOLTAGE_HEATING)->SetWindowText(text);
		text.Format(_T("%.1f"), lampsStatus->AnodeCurrent/10.0);
		GetDlgItem(IDC_EDIT_CURRENT_ANODE)->SetWindowText(text);
		
		// �������� ����� ��������� �� ��������.
		BYTE deviceState = (lampsStatus->DeviceMode & 0xC) >> 2;
		deviceState |= (lampsStatus->DeviceState & 0xF);
		m_LampsModeBitmask.ShowBitmask(lampsStatus->DeviceMode);
		m_LampsStateBitmask.ShowBitmask(deviceState);

		bool lampIsOn = ((lampsStatus->DeviceMode & (LAMPS_MODE_D2_LAMP_ON | LAMPS_MODE_D2_LAMP_INIT)) != 0);
		((CButton*)GetDlgItem(IDC_CHECK_D2_LAMP))->SetCheck(lampIsOn);

		if((lampsStatus->DeviceMode & LAMPS_MODE_ERROR) != 0)
		{
			((CButton*)GetDlgItem(IDC_CHECK_D2_LAMP))->SetCheck(false);
		}

		int errorCode = lampsStatus->DeviceState >> 4;
		if(errorCode != 0)
		{
			ShowErrorMessage(IDS_ERROR_LAMPS, IDS_ERROR_LAMPS, errorCode);
		}
		return;
	}
}

BEGIN_MESSAGE_MAP(CMLampsBoardPage, CMToolPage)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_CHECK_MANUAL, &CMLampsBoardPage::OnBnClickedCheckManual)
	ON_BN_CLICKED(IDC_CHECK_D2_LAMP, &CMLampsBoardPage::OnBnClickedCheckD2Lamp)
	ON_BN_CLICKED(IDC_CHECK_HEATING, &CMLampsBoardPage::OnBnClickedCheckHeating)
	ON_BN_CLICKED(IDC_CHECK_ANODE, &CMLampsBoardPage::OnBnClickedCheckAnode)
	ON_BN_CLICKED(IDC_BUTTON_IGNITE, &CMLampsBoardPage::OnBnClickedButtonIgnite)
	ON_BN_CLICKED(IDC_CHECK_HALOGEN_LAMP, &CMLampsBoardPage::OnBnClickedCheckHalogenLamp)
	ON_BN_CLICKED(IDC_CHECK_RED_LED, &CMLampsBoardPage::OnBnClickedCheckRedLed)
END_MESSAGE_MAP()


// ����������� ��������� CMLampsBoardPage

void CMLampsBoardPage::OnBnClickedCheckManual()
{
	const int c_manualControlID[] = 
	{
		IDC_CHECK_HEATING,
		IDC_EDIT_HEATING,
		IDC_STATIC_MV,
		IDC_SLIDER_HEATING,
		IDC_CHECK_ANODE,
		IDC_BUTTON_IGNITE,
		0
	};
	const int c_autoControlID[] = 
	{
		IDC_CHECK_D2_LAMP,
		0
	};

	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_MANUAL);
	bool isManual = (checkButton->GetCheck() != 0);

	if(isManual)
	{
		checkButton = (CButton*)GetDlgItem(IDC_CHECK_D2_LAMP);
		if(checkButton->GetCheck() != 0)
		{
			checkButton->SetCheck(false);
			OnBnClickedCheckD2Lamp();
		}
		checkButton = (CButton*)GetDlgItem(IDC_CHECK_HEATING);
		checkButton->SetCheck(true);
		EnableControls(c_manualControlID, true);
		EnableControls(c_autoControlID, false);
	}
	else
	{
		checkButton = (CButton*)GetDlgItem(IDC_CHECK_HEATING);
		checkButton->SetCheck(false);
		OnBnClickedCheckHeating();

		checkButton = (CButton*)GetDlgItem(IDC_CHECK_ANODE);
		if(checkButton->GetCheck() != 0)
		{
			checkButton->SetCheck(false);
			OnBnClickedCheckAnode();
		}

		EnableControls(c_manualControlID, false);
		EnableControls(c_autoControlID, true);
	}
}

void CMLampsBoardPage::OnBnClickedCheckD2Lamp()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_D2_LAMP);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'D';
	canFrame.m_Data.Byte[1] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMLampsBoardPage::OnBnClickedCheckHeating()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_HEATING);
	bool isOn = (checkButton->GetCheck() != 0);
	CSliderCtrl* sliderCtrl;
	sliderCtrl = (CSliderCtrl*)GetDlgItem(IDC_SLIDER_HEATING);

	if(isOn)
	{
		sliderCtrl->EnableWindow(true);
	}
	else
	{
		sliderCtrl->SetPos(0);
		sliderCtrl->EnableWindow(false);
		GetDlgItem(IDC_EDIT_HEATING)->SetWindowText(_T("0"));
		SetHeatingVoltage(0);
	}
}

void CMLampsBoardPage::OnBnClickedCheckAnode()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_ANODE);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 1;
	canFrame.m_Data.Uint16[1] = UINT16(isOn);

	SendWriteCommand(&canFrame);
}

void CMLampsBoardPage::OnBnClickedButtonIgnite()
{
	CGCanFrame canFrame;
	
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 2;

	SendWriteCommand(&canFrame);
}

void CMLampsBoardPage::SetHeatingVoltage(int voltage)
{
	CGCanFrame canFrame;
	
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Uint16[1] = UINT16(voltage);

	SendWriteCommand(&canFrame);
}

void CMLampsBoardPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl* sliderCtrl = (CSliderCtrl*)pScrollBar;
	int sliderID = sliderCtrl->GetDlgCtrlID();

	switch(nSBCode)
	{
	case SB_ENDSCROLL:
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		{
			CString text;
			nPos = sliderCtrl->GetPos();
			SetHeatingVoltage(nPos);
			text.Format(_T("%d"), nPos);
			GetDlgItem(IDC_EDIT_HEATING)->SetWindowText(text);
		}
		break;
	}

	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMLampsBoardPage::OnBnClickedCheckHalogenLamp()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_HALOGEN_LAMP);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'H';
	canFrame.m_Data.Byte[1] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMLampsBoardPage::OnBnClickedCheckRedLed()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_RED_LED);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'R';
	canFrame.m_Data.Byte[1] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}
