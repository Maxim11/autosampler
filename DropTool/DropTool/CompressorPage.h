#pragma once

#include "ToolPage.h"
#include "Controls/BitmaskListBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMCompressorPage dialog

class CMCompressorPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMCompressorPage)

// Construction
public:
	CMCompressorPage();
	~CMCompressorPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

	// ���������� ������������ ��������� ������� �������� ����.
	virtual void OnTimer();

// Dialog Data
	enum { IDD = IDD_PAGE_COMPRESSOR };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
protected:
	void StartMotor(int voltage, int current, bool forward);
	void UpdateDirection(int stateFlags);

	void ShowStatus205(const CGCanFrame* canFrame);
	void ShowStatus105M(const CGCanFrame* canFrame);
	void ShowHermTestState(int pressure, bool isReady);

	void EnableControls(bool enable);

public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStartHermTest();
	afx_msg void OnBnClickedCheckInitialization();
	afx_msg void OnBnClickedCheckStartMotor();
	afx_msg void OnBnClickedCheckStartTest();
	afx_msg void OnBnClickedCheckValve1();
	afx_msg void OnBnClickedCheckValve2();

protected:
	CMBitmaskListBox m_CompressorStateBitmask;

	bool m_IsHermTestStarted;

	bool m_IsMotorOn;
	bool m_IsMotorTestOn;
	int m_MotorDirection;
	int m_MotorStartDirection;
	int m_Voltage;
	int m_Current;

	unsigned int m_StartTime;

	// ��� ����� �������������.
	int m_TotalTime;
	int m_TimeCounter;
	int m_InitialPressure;

	BYTE m_CompressorState;

	DECLARE_MESSAGE_MAP()
};
