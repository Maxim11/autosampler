#pragma once

struct CGCanFrame;

// CMToolPage

class CMToolPage : public CMFCPropertyPage
{
	DECLARE_DYNAMIC(CMToolPage)

public:
	CMToolPage(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CMToolPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame) = 0;
	void SendWriteCommand(CGCanFrame* canFrame);
	void SendReadCommand(int page);
	void SendReadCommand105M(int page);

	void CreateVersionString(const CGCanFrame* canFrame);
	void ClearVersionString();
	CString GetVersionString() { return m_VersionString; }

	// ���������� ������������ ��������� ������� �������� ����.
	virtual void OnTimer() { };

protected:
	DECLARE_MESSAGE_MAP()

protected:
	int GetIntValue(int controlID);
	int GetCheckedRadioButton(const int buttonID[]);

	void EnableControls(const int controlID[], const int enable);

	void ShowErrorMessage(const int textID, const int errorTextID, const int errorCode);

protected:
	// ����� ���� �� ���� CAN.
	BYTE m_NodeAddress;

	CString m_VersionString;
	bool m_IsFirmwareFor105M;

	//bool m_IsErrorShowing;
	class CMErrorDialog* m_ErrorDialog;
};

// ���� �������, ������������ ��� �������� ���������������.
enum
{
  CAN_ID_WRITE_CMD    = 0,
  CAN_ID_READ_CMD     = 2,
  CAN_ID_STATUS_FRAME = 4,
  CAN_ID_WRITE_REPLY  = 5,
  CAN_ID_READ_REPLY   = 6,
};

// �������� ������ ������� ��������� �� ����������� ��������.
// ������������ ������������ �������� �������.
enum
{
  MOTORS_AUTOSAMPLER     = 0x80,
  MOTORS_INPUT_CAROUSEL  = 0x00,
  MOTORS_OUTPUT_CAROUSEL = 0x20,
  MOTORS_MONOCHROMATOR   = 0x40,
  MOTORS_MONO_1          = 0x00,
  MOTORS_MONO_2          = 0x20,
  MOTORS_UNIT_MASK       = 0xE0,
};

// ���� ��������� ��������, ����������� � ������� ����������.
enum
{
  CS_FORWARD     = 0x01,      // �������� "������"
  CS_BACKWARD    = 0x02,      // �������� "�����"
  CS_SENSOR      = 0x04,      // �������� ���������
  CS_SENSOR_2    = 0x08,      // ������ ������
  CS_INIT        = 0x10,      // �������������
  CS_HARD_ERROR  = 0x40,      // ������ �������� ��
  CS_FATAL_ERROR = 0x80,      // ����� ��������
};

// ���� ��������� "����������" � �����������, ����������� � ������� ����������.
enum
{
  OMS_FORWARD      = 0x01,      // �������� "������"
  OMS_BACKWARD     = 0x02,      // �������� "�����"
  OMS_SENSOR_1     = 0x04,      // ��������� �������� 1
  OMS_SENSOR_2     = 0x08,      // ��������� �������� 2
  OMS_INIT         = 0x10,      // ����� �������������
  OMS_HARD_ERROR   = 0x40,      // ������ �������� ��
  OMS_FATAL_ERROR  = 0x80,      // ����� ��������� ��� ������������� ��������
};

enum
{
  BS_LEFT_PRESSED  = 0x80,      // ������ �����
  BS_RIGHT_PRESSED = 0x40,      // ������ ������
  AS_POSITION_MASK = 0x3F,      // ����� ������ ��������
};

// ������ ������ ������� ��������.
struct CSCarouselFrame
{
  BYTE Mode;
  BYTE State;
  BYTE LiftState;
  BYTE OpenerState;
  BYTE MoverState;
  BYTE MoverPosition;
  BYTE ErrorCode;
  BYTE CurrentPosition;
};

// ���� ���������, �������������, ����������� � ������� ����������.
enum
{
  MS_FORWARD     = 0x01,      // ���������� ���������� ��������
  MS_BACKWARD    = 0x02,      // �������� "�����"
  MS_SENSOR_MIN  = 0x04,      // ��������� �� ��������� MIN
  MS_SENSOR_ROT  = 0x08,      // �������� ���������
  MS_SENSOR_MAX  = 0x10,      // ��������� �� ��������� MAX
  MS_HARD_ERROR  = 0x40,      // ������ �������� ��
  MS_FATAL_ERROR = 0x80,      // �����
};

// ���� ��������� �����, ����������� � ������� ����������
enum
{
  LS_GO_UP       = 0x01,      // �������� ����
  LS_GO_DOWN     = 0x02,      // �������� �����
  LS_LOWER_POS   = 0x04,      // ������ ���������
  LS_UPPER_POS   = 0x08,      // ������� ���������
  LS_STOPPED     = 0x10,      // ���� ���������� (������������� ���������)
  LS_MANUAL      = 0x20,      // "������" ���������� �������
  LS_HARD_ERROR  = 0x40,      // ������ �������� ������
  LS_FATAL_ERROR = 0x80,      // ������ - �� ����� ��������� ������.
};


// �������� ������� �����, ����������� �� CAN.
struct CSMonochromatorFrame
{
  BYTE Mode;
  BYTE State;
  BYTE ErrorCode;
  BYTE Unused;
  INT16 TargetStep;
  INT16 CurrentStep;
};


