#include "stdafx.h"
#include "resource.h"
#include "AutosamplerPage.h"

#include "LiftCurrentDialog.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/*
�� ���������:
���� ������� ������� ����� � ��������� ����������, �� ��� ������� ������ ���������
����� ������ ��������� � 31, � � ���� �������� (��������� ��� ������������)
���������� � �� �� 06 �� 11.
���� 36-� ������� � ���� ����������, �� ��� �������� ������ ��������� �����
��������� � 53, � � ���� �������� � � 40 �� � 43.
*/
// ��������� ��������.
enum
{
	INPUT_MAX_POSITION       = 36,
	OUTPUT_MIN_POSITION      = INPUT_MAX_POSITION,
	// ������� ��� ����������� ��������. � ��� ���� ��������� ������� �������,
	// ����� ������� ������� ��������� � ������ �����.
	INPUT_MOVEMENT_TO_WORK   = 31,
	INPUT_MOVEMENT_TO_LOAD   = -6,
	OUTPUT_MAX_POSITION      = 24,
	// "�����" ��� ���������� ����������� ������� ����������� �������� ��� �����������.
	OUTPUT_MOVEMENT_TO_WORK  = 17,
	OUTPUT_MOVEMENT_TO_LOAD  = -4,
};

enum
{
  MOVER_STATE_INNER = 1,      // ����������� �� ���������� ��������
  MOVER_STATE_OUTER,          // ����������� �� ������� ��������
  MOVER_STATE_OUTER_OPEN,     // ����������� �� ������� �������� ��� ����������
};


IMPLEMENT_DYNCREATE(CMAutosamplerPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMAutosamplerPage property page

CMAutosamplerPage::CMAutosamplerPage() : CMToolPage(CMAutosamplerPage::IDD)
{
	// ����� ����������� ��������.
	m_NodeAddress = 0x06;
	m_CurrentInputPosition = 0;
	m_CurrentOutputPosition = 0;
}

CMAutosamplerPage::~CMAutosamplerPage()
{
}

void CMAutosamplerPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

int CMAutosamplerPage::NormalizeVialNumber(int vialNumber, const int maxNumber)
{
	while(vialNumber < 0)
	{
		vialNumber += maxNumber;
	}
	while(vialNumber >= maxNumber)
	{
		vialNumber -= maxNumber;
	}

	return vialNumber;
}

void CMAutosamplerPage::ShowInputPosition()
{
	CString text;
	int vialNumber = m_CurrentInputPosition;

	text.Format(_T("%d"), vialNumber);
	GetDlgItem(IDC_STATIC_WORK_INPUT)->SetWindowText(text);

	vialNumber -= INPUT_MOVEMENT_TO_WORK;
	vialNumber = NormalizeVialNumber(vialNumber, INPUT_MAX_POSITION);
	text.Format(_T("%d"), vialNumber);
	GetDlgItem(IDC_STATIC_OPEN_INPUT)->SetWindowText(text);

	vialNumber -= INPUT_MOVEMENT_TO_LOAD;
	vialNumber = NormalizeVialNumber(vialNumber, INPUT_MAX_POSITION);
	text.Format(_T("%d"), vialNumber);
	GetDlgItem(IDC_STATIC_LOAD_INPUT)->SetWindowText(text);
}

void CMAutosamplerPage::ShowOutputPosition()
{
	CString text;
	int vialNumber = m_CurrentOutputPosition;

	text.Format(_T("%d"), vialNumber+OUTPUT_MIN_POSITION);
	GetDlgItem(IDC_STATIC_WORK_OUTPUT)->SetWindowText(text);

	vialNumber -= OUTPUT_MOVEMENT_TO_WORK;
	vialNumber = NormalizeVialNumber(vialNumber, OUTPUT_MAX_POSITION);
	text.Format(_T("%d"), vialNumber+OUTPUT_MIN_POSITION);
	GetDlgItem(IDC_STATIC_OPEN_OUTPUT)->SetWindowText(text);

	vialNumber -= OUTPUT_MOVEMENT_TO_LOAD;
	vialNumber = NormalizeVialNumber(vialNumber, OUTPUT_MAX_POSITION);
	text.Format(_T("%d"), vialNumber+OUTPUT_MIN_POSITION);
	GetDlgItem(IDC_STATIC_LOAD_OUTPUT)->SetWindowText(text);
}

void CMAutosamplerPage::ShowInputLiftPosition(int liftState)
{
	bool isUp = ((liftState & LS_UPPER_POS) != 0);
	bool isDown = ((liftState & LS_LOWER_POS) != 0);
	((CButton*)GetDlgItem(IDC_RADIO_LIFT_INPUT_UP))->SetCheck(isUp);
	((CButton*)GetDlgItem(IDC_RADIO_LIFT_INPUT_DOWN))->SetCheck(isDown);
}

void CMAutosamplerPage::ShowOutputLiftPosition(int liftState)
{
	bool isUp = ((liftState & LS_UPPER_POS) != 0);
	bool isDown = ((liftState & LS_LOWER_POS) != 0);
	((CButton*)GetDlgItem(IDC_RADIO_LIFT_OUTPUT_UP))->SetCheck(isUp);
	((CButton*)GetDlgItem(IDC_RADIO_LIFT_OUTPUT_DOWN))->SetCheck(isDown);
}

void CMAutosamplerPage::ShowMoverPosition(int moverState)
{
	bool isInner = (moverState == MOVER_STATE_INNER);
	bool isOuter = (moverState == MOVER_STATE_OUTER);
	bool isOuterOpen = (moverState == MOVER_STATE_OUTER_OPEN);

	((CButton*)GetDlgItem(IDC_RADIO_MOVER_INT))->SetCheck(isInner);
	((CButton*)GetDlgItem(IDC_RADIO_MOVER_EXT))->SetCheck(isOuter);
	((CButton*)GetDlgItem(IDC_RADIO_MOVER_EXT_OPEN))->SetCheck(isOuterOpen);
}

void CMAutosamplerPage::ShowSensorsState(int sensorsState)
{
	m_SensorsBitmask.ShowBitmask(sensorsState);
}

void CMAutosamplerPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			CreateVersionString(canFrame);
			break;
		case 'l':
			// ��������� ������ ����� ���������� ������.
			ShowLiftCurrentsDialog(
				canFrame->m_Data.Byte[1],
				canFrame->m_Data.Byte[2],
				canFrame->m_Data.Byte[3]
				);
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId != ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		// "�����" �����.
		return;
	}

	CSCarouselFrame* statusFrame = (CSCarouselFrame*)canFrame->m_Data.Byte;
	int errorBase;
	if((statusFrame->Mode & MOTORS_AUTOSAMPLER) == 0)
	{
		// ��� �� ������ ������������.
		return;
	}
	if((statusFrame->Mode & MOTORS_OUTPUT_CAROUSEL) == 0)
	{
		m_CurrentInputPosition = statusFrame->CurrentPosition & AS_POSITION_MASK;
		m_InputCarouselState   = statusFrame->State;
		ShowInputPosition();
		ShowInputLiftPosition(statusFrame->LiftState);
		errorBase = IDS_ERROR_CAROUSEL_EXT;
	}
	else
	{
		m_CurrentOutputPosition = statusFrame->CurrentPosition & AS_POSITION_MASK;
		m_OutputCarouselState   = statusFrame->State;
		ShowOutputPosition();
		ShowOutputLiftPosition(statusFrame->LiftState);
		errorBase = IDS_ERROR_CAROUSEL_INT;
	}

	ShowMoverPosition(statusFrame->MoverPosition & 0x0F);

	int sensorsState = 0;
	if((m_InputCarouselState & CS_SENSOR) != 0)
	{
		sensorsState |= 0x1;
	}
	if((m_OutputCarouselState & CS_SENSOR) != 0)
	{
		sensorsState |= 0x2;
	}
	if((statusFrame->MoverState & OMS_SENSOR_2) != 0)
	{
		sensorsState |= 0x4;
	}
	if((statusFrame->MoverState & OMS_SENSOR_1) != 0)
	{
		sensorsState |= 0x8;
	}
	TRACE("Opener %x\n", statusFrame->OpenerState);
	if((statusFrame->OpenerState & OMS_SENSOR_1) != 0)
	{
		sensorsState |= 0x10;
	}

	ShowSensorsState(sensorsState);

	int errorCode = statusFrame->ErrorCode & 0x3F;
	if(errorCode != 0)
	{
		ShowErrorMessage(errorBase, IDS_ERROR_AUTOSAMPLER, errorCode);
	}
}

BEGIN_MESSAGE_MAP(CMAutosamplerPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_INIT_START, &CMAutosamplerPage::OnBnClickedButtonInitStart)
	ON_BN_CLICKED(IDC_BUTTON_LIFT_CURRENTS, &CMAutosamplerPage::OnBnClickedLiftCurrents)
	ON_BN_CLICKED(IDC_RADIO_LIFT_INPUT_UP, &CMAutosamplerPage::OnBnClickedRadioLiftInputUp)
	ON_BN_CLICKED(IDC_RADIO_LIFT_INPUT_DOWN, &CMAutosamplerPage::OnBnClickedRadioLiftInputDown)
	ON_BN_CLICKED(IDC_RADIO_LIFT_OUTPUT_UP, &CMAutosamplerPage::OnBnClickedRadioLiftOutputUp)
	ON_BN_CLICKED(IDC_RADIO_LIFT_OUTPUT_DOWN, &CMAutosamplerPage::OnBnClickedRadioLiftOutputDown)
	ON_BN_CLICKED(IDC_RADIO_MOVER_EXT_OPEN, &CMAutosamplerPage::OnBnClickedRadioMoverExtOpen)
	ON_BN_CLICKED(IDC_RADIO_MOVER_EXT, &CMAutosamplerPage::OnBnClickedRadioMoverExt)
	ON_BN_CLICKED(IDC_RADIO_MOVER_INT, &CMAutosamplerPage::OnBnClickedRadioMoverInt)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_CAP, &CMAutosamplerPage::OnBnClickedButtonOpenCap)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_REVERSE, &CMAutosamplerPage::OnBnClickedButtonOpenReverse)
	ON_BN_CLICKED(IDC_BUTTON_SET_INPUT, &CMAutosamplerPage::OnBnClickedButtonSetInput)
	ON_BN_CLICKED(IDC_BUTTON_SET_OUTPUT, &CMAutosamplerPage::OnBnClickedButtonSetOutput)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MOVE_INPUT, &CMAutosamplerPage::OnDeltaposSpinMoveInput)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MOVE_OUTPUT, &CMAutosamplerPage::OnDeltaposSpinMoveOutput)
	ON_BN_CLICKED(IDC_CHECK_MOVER_TEST, &CMAutosamplerPage::OnBnClickedCheckMoverTest)
END_MESSAGE_MAP()

BOOL CMAutosamplerPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();
	((CButton*)GetDlgItem(IDC_RADIO_WORK_INPUT))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_WORK_OUTPUT))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_INIT_FULL))->SetCheck(true);

	m_SensorsBitmask.SubclassDlgItem(IDC_LIST_SENSORS, this);
	m_SensorsBitmask.Initialize(IDS_BITS_AUTOSAMPLER_SENSORS, 7);
	m_SensorsBitmask.EnableWindow(false);
	
	return result;
}

void CMAutosamplerPage::OnBnClickedButtonInitStart()
{
	CGCanFrame canFrame;
	const int c_radioButtonID[] =
	{
		IDC_RADIO_INIT_FULL,
		IDC_RADIO_INIT_CAROUSEL_INPUT,
		IDC_RADIO_INIT_CAROUSEL_OUTPUT,
		IDC_RADIO_INIT_LIFT_INPUT,
		IDC_RADIO_INIT_LIFT_OUTPUT,
		IDC_RADIO_INIT_MOVER,
		IDC_RADIO_INIT_OPENER,
		0,
	};
	int selectedMode = GetCheckedRadioButton(c_radioButtonID);

	canFrame.m_Data.Byte[0] = 'I';
	switch(selectedMode)
	{
	case 0:				// IDC_RADIO_INIT_FULL
		canFrame.m_Data.Byte[1] = 0;
		break;
	case 1:				// IDC_RADIO_INIT_CAROUSEL_INPUT
		canFrame.m_Data.Byte[1] = 2;
		canFrame.m_Data.Byte[2] = 0x1;
		canFrame.m_Data.Byte[3] = 31;
		break;
	case 2:				// IDC_RADIO_INIT_CAROUSEL_OUTPUT
		canFrame.m_Data.Byte[1] = 2;
		canFrame.m_Data.Byte[2] = 0x2;
		break;
	case 3:				// IDC_RADIO_INIT_LIFT_INPUT
		canFrame.m_Data.Byte[1] = 3;
		canFrame.m_Data.Byte[2] = 0x1;
		break;
	case 4:				// IDC_RADIO_INIT_LIFT_OUTPUT
		canFrame.m_Data.Byte[1] = 3;
		canFrame.m_Data.Byte[2] = 0x2;
		break;
	case 5:				// IDC_RADIO_INIT_MOVER
		canFrame.m_Data.Byte[1] = 4;
		break;
	case 6:				// IDC_RADIO_INIT_OPENER
		canFrame.m_Data.Byte[1] = 5;
		break;
	}

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedRadioLiftInputUp()
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 0;		// �������
	canFrame.m_Data.Byte[2] = 1;		// Up

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::ShowLiftCurrentsDialog(const int upCurrent, const int downCurrent, const int movementCurrent)
{
	CString text;
	CMLiftCurrentDialog dialog;
	dialog.SetParentPage(this);

	text.Format(_T("%d"), upCurrent);
	dialog.m_PressUp = text;
	text.Format(_T("%d"), downCurrent);
	dialog.m_PressDown = text;
	text.Format(_T("%d"), movementCurrent);
	dialog.m_Movement = text;

	dialog.DoModal();
}

void CMAutosamplerPage::OnBnClickedLiftCurrents()
{
	SendReadCommand('l');
	// ShowLiftCurrentsDialog(80, 30, 60);
}

void CMAutosamplerPage::OnBnClickedRadioLiftInputDown()
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 0;		// �������
	canFrame.m_Data.Byte[2] = 0;		// Down

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedRadioLiftOutputUp()
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 1;		// ��������
	canFrame.m_Data.Byte[2] = 1;		// Up

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedRadioLiftOutputDown()
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 1;		// ��������
	canFrame.m_Data.Byte[2] = 0;		// Down

	SendWriteCommand(&canFrame);
}

// ������� ���������, ���������� � ������� �����������.
enum
{
  MOVER_TARGET_INNER = 1,
  MOVER_TARGET_OUTER,         // �������, �������� ��������
  MOVER_TARGET_OUTER_OPEN,    // �������, ���������� ������
};

void CMAutosamplerPage::OnBnClickedRadioMoverExtOpen()
{
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = MOVER_TARGET_OUTER_OPEN;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedRadioMoverExt()
{
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = MOVER_TARGET_OUTER;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedRadioMoverInt()
{
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = MOVER_TARGET_INNER;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedButtonOpenCap()
{
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'O';
	canFrame.m_Data.Byte[1] = 0;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedButtonOpenReverse()
{
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'O';
	canFrame.m_Data.Byte[1] = 1;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedButtonSetInput()
{
	CGCanFrame canFrame;
	const int c_radioButtonID[] =
	{
		IDC_RADIO_WORK_INPUT,
		IDC_RADIO_OPEN_INPUT,
		IDC_RADIO_LOAD_INPUT,
		0,
	};
	const int c_shifts[] =
	{
		0,
		INPUT_MOVEMENT_TO_WORK,
		INPUT_MOVEMENT_TO_WORK+INPUT_MOVEMENT_TO_LOAD,
	};
	int selectedMode = GetCheckedRadioButton(c_radioButtonID);
	int vialNumber = GetIntValue(IDC_EDIT_INPUT) + c_shifts[selectedMode];

	vialNumber = NormalizeVialNumber(vialNumber, INPUT_MAX_POSITION);

	canFrame.m_Data.Byte[0] = 'S';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Byte[2] = (BYTE)vialNumber;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedButtonSetOutput()
{
	CGCanFrame canFrame;
	const int c_radioButtonID[] =
	{
		IDC_RADIO_WORK_OUTPUT,
		IDC_RADIO_OPEN_OUTPUT,
		IDC_RADIO_LOAD_OUTPUT,
		0,
	};
	const int c_shifts[] =
	{
		0,
		OUTPUT_MOVEMENT_TO_WORK,
		OUTPUT_MOVEMENT_TO_WORK+OUTPUT_MOVEMENT_TO_LOAD,
	};
	int selectedMode = GetCheckedRadioButton(c_radioButtonID);
	int vialNumber = GetIntValue(IDC_EDIT_OUTPUT);

	if(vialNumber >= OUTPUT_MIN_POSITION)
	{
		vialNumber -= OUTPUT_MIN_POSITION;
	}
	vialNumber += c_shifts[selectedMode];
	vialNumber = NormalizeVialNumber(vialNumber, OUTPUT_MAX_POSITION);

	canFrame.m_Data.Byte[0] = 'S';
	canFrame.m_Data.Byte[1] = 1;
	canFrame.m_Data.Byte[2] = (BYTE)vialNumber;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnDeltaposSpinMoveInput(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	CGCanFrame canFrame;
	int delta = -pNMUpDown->iDelta;
	int vialNumber = m_CurrentInputPosition+delta;

	vialNumber = NormalizeVialNumber(vialNumber, INPUT_MAX_POSITION);

	canFrame.m_Data.Byte[0] = 'S';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Byte[2] = (BYTE)vialNumber;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnDeltaposSpinMoveOutput(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	CGCanFrame canFrame;
	int delta = -pNMUpDown->iDelta;
	int vialNumber = m_CurrentOutputPosition+delta;

	vialNumber = NormalizeVialNumber(vialNumber, OUTPUT_MAX_POSITION);

	canFrame.m_Data.Byte[0] = 'S';
	canFrame.m_Data.Byte[1] = 1;
	canFrame.m_Data.Byte[2] = (BYTE)vialNumber;

	SendWriteCommand(&canFrame);
}

void CMAutosamplerPage::OnBnClickedCheckMoverTest()
{
	bool isStarting = ((CButton*)GetDlgItem(IDC_CHECK_MOVER_TEST))->GetCheck() != 0;
	CGCanFrame canFrame;

	canFrame.m_Data.Byte[0] = 'V';
	// ������� ������������ �����.
	canFrame.m_Data.Int16[2] = 1;
	if(isStarting)
	{
		if(((CButton*)GetDlgItem(IDC_RADIO_MOVER_INT))->GetCheck() != 0)
		{
			// �������� �� ����������� ���������.
			canFrame.m_Data.Int16[1] = 60;
		}
		else
		{
			canFrame.m_Data.Int16[1] = -60;
		}
	}
	else
	{
		// ����� ��������, ����� 0 � 1 ������������� ����.
		canFrame.m_Data.Int16[2] = 10;
		canFrame.m_Data.Int16[1] = 10;
	}

	SendWriteCommand(&canFrame);
}
