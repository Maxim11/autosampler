#pragma once

#include "ToolPage.h"
#include "Controls/BitmaskListBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMMonochromatorPage dialog

class CMMonochromatorPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMMonochromatorPage)

// Construction
public:
	CMMonochromatorPage();
	~CMMonochromatorPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// Dialog Data
	enum { IDD = IDD_PAGE_MONOCHROMATOR };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
public:
	afx_msg void OnBnClickedButtonSetStep();
	afx_msg void OnBnClickedButtonInitialization();
	afx_msg void OnBnClickedButtonStartTest();
	afx_msg void OnEditStep();
	afx_msg void OnDeltaposSpinStep(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	DECLARE_MESSAGE_MAP()

protected:
	CMBitmaskListBox m_Mono1StateBitmask;

	int m_CurrentStep;
	int m_TargetStep;
};
