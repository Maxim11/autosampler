#pragma once

#include "ToolPage.h"
#include "Controls/BitmaskListBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMThermostatPage dialog

class CMThermostatPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMThermostatPage)

// Construction
public:
	CMThermostatPage();
	~CMThermostatPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// Dialog Data
	enum { IDD = IDD_PAGE_THERMOSTAT };

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

public:
	afx_msg void OnBnClickedRadioRegulator();
	afx_msg void OnBnClickedRadioCooling();
	afx_msg void OnBnClickedRadioHeating();
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonInitStart();
	afx_msg void OnBnClickedCheckPumpWork();
	afx_msg void OnBnClickedCheckPumpReverse();
	afx_msg void OnBnClickedCheckWhite();
	afx_msg void OnBnClickedCheckGreen();
	afx_msg void OnBnClickedCheckRed();

protected:
// Implementation
	void ShowStatus205(const CGCanFrame* canFrame);
	void ShowStatus105M(const CGCanFrame* canFrame);

protected:
	CMBitmaskListBox m_ThermostatStateBitmask;

	bool m_IsStarted;
	bool m_AskCurrent;

	DECLARE_MESSAGE_MAP()
};

// ������ ������, ���������� �� CAN.
struct CSThermostatFrame
{
	BYTE   DeviceMode;            // ������� ����� ������
	BYTE   ErrorCode;             // ��� ������
	UINT16 DeviceState;           // ��������� ���������
	UINT16 WorkTemperature;       // ����������� ����
	UINT16 AirTemperature;        // ����������� �������
};

// ���� Thermostat.DeviceMode.
enum
{
	THERMO_MODE_ERROR         = 0x01,
	THERMO_MODE_WORK          = 0x02,
	THERMO_MODE_STABILIZATION = 0x04,
	THERMO_MODE_AUTOTEST      = 0x08,
};

//  ���� ��������� ����������.
enum
{
  THERMO_PELTIER_HEATING     = 0x01,
  THERMO_PELTIER_COOLING     = 0x02,
  THERMO_PELTIER_REGULATOR   = 0x04,
  THERMO_PUMP_NORMAL         = 0x10,
  THERMO_PUMP_REVERSE        = 0x20,
  THERMO_MIN_LEVEL_SENSOR_ON = 0x40,
  THERMO_MAX_LEVEL_SENSOR_ON = 0x80,

  THERMO_STREAM_SENSOR_ON    = 0x0100,
  THERMO_FAN_ERROR           = 0x1000,
  THERMO_STREAM_ERROR        = 0x2000,
  THERMO_PELTIER_ERROR       = 0x4000,
};

struct CSThermostatFrame105M
{
	BYTE   DeviceMode;            // ������� ����� ������
	BYTE   ErrorCode;             // ��� ������ (��� �������� �������� �������)
	BYTE   DeviceState;           // ��������� ���������
	BYTE   PumpEmf;               // �������� ��� ���������
	UINT16 WorkTemperature;       // ����������� ����
	UINT16 AirTemperature;        // ����������� �������
};

// ���� Thermostat.DeviceMode ��������� 105�.
enum
{
  THERMO_M_MODE_ERROR         = 0x80,
  THERMO_M_MODE_WORK          = 0x20,
  THERMO_M_MODE_STABILIZATION = 0x00,
  THERMO_M_MODE_AUTOTEST      = 0x02,
  THERMO_M_MODE_WAIT          = 0x10,
};

// ���� Thermostat.DeviceState ��������� 105�.
enum
{
  THERMO_M_PELTIER_ON          = 0x01,
  THERMO_M_PELTIER_PID         = 0x02,
  THERMO_M_PUMP_ON             = 0x04,
  THERMO_M_TEMP_SENSOR_ERROR   = 0x08,
  THERMO_M_STREAM_ERROR        = 0x10,
  THERMO_M_PELTIER_ERROR       = 0x20,
  THERMO_M_FAN_ERROR           = 0x40,
};
