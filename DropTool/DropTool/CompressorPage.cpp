#include "stdafx.h"
#include "resource.h"
#include "CompressorPage.h"
#include "CompressorBoardPage.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMCompressorPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMCompressorPage property page

CMCompressorPage::CMCompressorPage() : CMToolPage(CMCompressorPage::IDD)
{
	m_NodeAddress = 0x9;

	m_IsHermTestStarted = false;
	m_IsMotorTestOn = false;
	m_IsMotorOn = false;
	m_MotorDirection = 0;
	m_MotorStartDirection = 0;
	m_Voltage = 0;

	m_StartTime = 0;

	m_CompressorState = 0;
}

CMCompressorPage::~CMCompressorPage()
{
}

void CMCompressorPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}


BOOL CMCompressorPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();

	m_CompressorStateBitmask.SubclassDlgItem(IDC_LIST_STATUS_BITS, this);
	m_CompressorStateBitmask.Initialize(IDS_BITS_COMPRESSOR_STATUS, 8);
	m_CompressorStateBitmask.EnableWindow(false);

	GetDlgItem(IDC_EDIT_TARGET_VALUE)->SetWindowText(_T("1000"));
	GetDlgItem(IDC_EDIT_TARGET_VALUE_HT)->SetWindowText(_T("1000"));
	GetDlgItem(IDC_EDIT_TARGET_TIME)->SetWindowText(_T("60"));
	GetDlgItem(IDC_EDIT_MOTOR_VOLTAGE)->SetWindowText(_T("100"));
	GetDlgItem(IDC_EDIT_MOTOR_CURRENT)->SetWindowText(_T("100"));
	((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(true);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMCompressorPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	// ��� "����� ����" � ��������� CanKingdom 105M
	if(canFrame->m_ExtendedId == (m_NodeAddress + 16) )
	{
		if(!m_IsFirmwareFor105M)
		{
			m_IsFirmwareFor105M = true;
			m_VersionString.Format(_T("v. %d.%d (%d/%d/%d)"),
				canFrame->m_Data.Byte[2],
				canFrame->m_Data.Byte[3],
				canFrame->m_Data.Byte[6],
				canFrame->m_Data.Byte[5],
				canFrame->m_Data.Byte[4]+2000
				);
			Invalidate(true);
		}
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			m_IsFirmwareFor105M = false;
			CreateVersionString(canFrame);
			break;
		case 'H':
			{
				int counter = canFrame->m_Data.Int16[3];
				CString text;
				text.Format(_T("%d"), counter);
				GetDlgItem(IDC_EDIT_COUNTER)->SetWindowText(text);
			}
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		if(m_IsFirmwareFor105M)
		{
			ShowStatus105M(canFrame);
		}
		else
		{
			ShowStatus205(canFrame);
		}
	}
}

void CMCompressorPage::OnTimer()
{
	SendReadCommand('H');
}

void CMCompressorPage::ShowStatus205(const CGCanFrame* canFrame)
{
	// ���� DeviceMode.
	enum
	{
	  DEVICE_MODE_ERROR          = 0x01,
	  DEVICE_MODE_WORK           = 0x02,
	  DEVICE_MODE_STABILIZATION  = 0x04,
	  DEVICE_MODE_AUTOTEST       = 0x08,
	};

	// ���������� ��������� �������.
	int sensor1, sensor2;
	CSCompressorFrame* compressorStatus = (CSCompressorFrame*)canFrame->m_Data.Byte;
	int motorState;

	motorState = compressorStatus->DeviceState1;
	sensor1 = motorState & COMPR_SENSOR_MIN;
	sensor2 = motorState & COMPR_SENSOR_MAX;

	m_CompressorStateBitmask.ShowBitmask(motorState);
	m_CompressorState = motorState;

	int motorDirection = 0;
	if((motorState & COMPR_MOTOR_FORWARD) != 0)
		motorDirection = 1;
	if((motorState & COMPR_MOTOR_BACKWARD) != 0)
		motorDirection = -1;
	if(motorDirection == 0 && m_MotorDirection != 0)
	{
		if(m_IsMotorTestOn)
		{
			StartMotor(m_Voltage, m_Current, m_MotorDirection < 0);
			// ��� ����������� �� ���������
			if(m_MotorDirection < 0)
			{
				((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(true);
				((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(false);
			}
			else
			{
				((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(false);
				((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(true);
			}

		}
		else
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
			if(startButton->GetCheck() != 0)
			{
				CString text;
				int editID = m_MotorStartDirection < 0 ? IDC_EDIT_TIME_BACKWARD : IDC_EDIT_TIME_FORWARD;
					 
				text.Format(_T("%.1f"), (GetTickCount()-m_StartTime)/1000.0);
				GetDlgItem(editID)->SetWindowText(text);

				startButton->SetCheck(false);
				OnBnClickedCheckStartMotor();
				UpdateDirection(motorState);
			}
		}
	}
	m_MotorDirection = motorDirection;


	int pressure = compressorStatus->PressureValue;
	CString text;
	if( (compressorStatus->DeviceState2 & COMPR_PRESSURE_NEGATIVE) != 0 )
	{
		pressure = -pressure;
	}
	text.Format(_T("%.1f"), pressure/10.0);
	GetDlgItem(IDC_EDIT_PRESSURE)->SetWindowText(text);
	text.Format(_T("%d"), compressorStatus->MotorPower);
	GetDlgItem(IDC_EDIT_POWER)->SetWindowText(text);

	CButton* initButton = (CButton*)GetDlgItem(IDC_CHECK_INITIALIZATION);
	if(initButton->GetCheck() != 0)
	{
		if((compressorStatus->DeviceMode & DEVICE_MODE_AUTOTEST) == 0)
			initButton->SetCheck(false);
	}

	if(m_IsHermTestStarted)
	{
		// ���������� ������������ � ��� ���� ��������� ��������.
		bool isReady = ((compressorStatus->DeviceMode & DEVICE_MODE_STABILIZATION) != 0
			&& (motorState & (COMPR_MOTOR_FORWARD | COMPR_MOTOR_BACKWARD)) == 0);
		ShowHermTestState((pressure+5)/10, isReady);
	}

	int errorCode = compressorStatus->ErrorCode;
	if(errorCode != 0)
	{
		if(errorCode <= 10)
		{
			ShowErrorMessage(IDS_ERROR_COMPRESSOR, IDS_ERROR_COMPRESSOR, errorCode);
		}
		else
		{
			ShowErrorMessage(IDS_ERROR_COMPRESSOR_AT, IDS_ERROR_COMPRESSOR_AT, errorCode-10);
		}
	}
}

void CMCompressorPage::ShowStatus105M(const CGCanFrame* canFrame)
{
	// ���� DeviceMode.
	enum
	{
	  DEVICE_MODE_M_ERROR          = 0x80,
	  DEVICE_MODE_M_WORK           = 0x20,
	  DEVICE_MODE_M_STABILIZATION  = 0x00,
	  DEVICE_MODE_M_AUTOTEST       = 0x02,
	  DEVICE_MODE_M_TEST           = 0x10,
	};

	// ���������� ��������� �������.
	int sensor1, sensor2;
	CSCompressorFrame105M* compressorStatus = (CSCompressorFrame105M*)canFrame->m_Data.Byte;
	int motorState;

	motorState = compressorStatus->DeviceState1;
	sensor1 = motorState & COMPR_M_SENSOR_MIN;
	sensor2 = motorState & COMPR_M_SENSOR_MAX;

	int stateFlags = 0;
	if(motorState & COMPR_M_SENSOR_MIN)
	{
		stateFlags |= COMPR_SENSOR_MIN;
	}
	if(motorState & COMPR_M_SENSOR_MAX)
	{
		stateFlags |= COMPR_SENSOR_MAX;
	}
	if(motorState & COMPR_M_MOTOR_BACKWARD)
	{
		stateFlags |= COMPR_MOTOR_BACKWARD;
	}
	if(motorState & COMPR_M_MOTOR_FORWARD)
	{
		stateFlags |= COMPR_MOTOR_FORWARD;
	}
	if(motorState & COMPR_M_VALVE_1_ON)
	{
		stateFlags |= COMPR_VALVE_1_ON;
	}

	m_CompressorStateBitmask.ShowBitmask(stateFlags);
	m_CompressorState = stateFlags;

	int motorDirection = 0;
	if((motorState & COMPR_M_MOTOR_FORWARD) != 0)
		motorDirection = 1;
	if((motorState & COMPR_M_MOTOR_BACKWARD) != 0)
		motorDirection = -1;
	if(motorDirection == 0 && m_MotorDirection != 0)
	{
		if(m_IsMotorTestOn)
		{
			StartMotor(m_Voltage, m_Current, m_MotorDirection < 0);
			// ��� ����������� �� ���������
			if(m_MotorDirection < 0)
			{
				((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(true);
				((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(false);
			}
			else
			{
				((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(false);
				((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(true);
			}

		}
		else
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
			if(startButton->GetCheck() != 0)
			{
				CString text;
				int editID = m_MotorStartDirection < 0 ? IDC_EDIT_TIME_BACKWARD : IDC_EDIT_TIME_FORWARD;
					 
				text.Format(_T("%.1f"), (GetTickCount()-m_StartTime)/1000.0);
				GetDlgItem(editID)->SetWindowText(text);

				startButton->SetCheck(false);
				EnableControls(true);

				UpdateDirection(stateFlags);
			}
		}
	}
	m_MotorDirection = motorDirection;


	int pressure = compressorStatus->PressureValue;
	CString text;
	text.Format(_T("%d"), pressure);
	GetDlgItem(IDC_EDIT_PRESSURE)->SetWindowText(text);

	CButton* initButton = (CButton*)GetDlgItem(IDC_CHECK_INITIALIZATION);
	if(initButton->GetCheck() != 0)
	{
		if((compressorStatus->DeviceMode & DEVICE_MODE_M_AUTOTEST) == 0)
			initButton->SetCheck(false);
	}


	if(m_IsHermTestStarted)
	{
		// ��������� ��������.
		bool isReady = ((motorState
			 & (COMPR_M_MOTOR_FORWARD | COMPR_M_MOTOR_BACKWARD | COMPR_M_SENSOR_MIN)) == 0);
		ShowHermTestState(pressure, isReady);
	}

	int errorCode = compressorStatus->ErrorCode;
	if(errorCode != 0)
	{
		if((compressorStatus->DeviceMode & DEVICE_MODE_M_AUTOTEST) == 0)
		{
			ShowErrorMessage(IDS_ERROR_COMPRESSOR, IDS_ERROR_COMPRESSOR, errorCode);
		}
		else
		{
			ShowErrorMessage(IDS_ERROR_COMPRESSOR_AT, IDS_ERROR_COMPRESSOR_AT, errorCode);
		}
	}
}

void CMCompressorPage::ShowHermTestState(int pressure, bool isReady)
{
	if(m_InitialPressure == 0)
	{
		// ���������� ������������ � ��� ���� ��������� ��������.
		if(isReady)
		{
			TRACE("Herm pres %d %d\n", m_TimeCounter, pressure);
			m_TimeCounter = m_TotalTime;
			m_InitialPressure = pressure;
		}
		else
		{
			m_TimeCounter--;
			// �������� �� ������������.
			if(m_TimeCounter < -25)
			{
				// ��������� ����, �������� ������� ������.
				CButton* checkButton = (CButton*)GetDlgItem(IDC_BUTTON_START_HT);
				checkButton->SetCheck(false);
				OnBnClickedButtonStartHermTest();
				GetDlgItem(IDC_EDIT_RESULT)->SetWindowText(_T("???"));
				return;
			}
		}
	}

	int delta = m_InitialPressure-pressure;
	CString time;
	CString result;
	if(m_TimeCounter >= 0)
	{
		if(m_TimeCounter == 0)
		{
			// ����� �����������.
			CButton* checkButton = (CButton*)GetDlgItem(IDC_BUTTON_START_HT);
			checkButton->SetCheck(false);
			OnBnClickedButtonStartHermTest();
		}
		time.Format(_T("%d"), m_TimeCounter);
		result.Format(_T("%d"), delta);
		m_TimeCounter--;
	}

	GetDlgItem(IDC_EDIT_TIME)->SetWindowText(time);
	GetDlgItem(IDC_EDIT_RESULT)->SetWindowText(result);
}

BEGIN_MESSAGE_MAP(CMCompressorPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_START, &CMCompressorPage::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_CHECK_INITIALIZATION, &CMCompressorPage::OnBnClickedCheckInitialization)
	ON_BN_CLICKED(IDC_BUTTON_START_HT, &CMCompressorPage::OnBnClickedButtonStartHermTest)
	ON_BN_CLICKED(IDC_CHECK_START_MOTOR, &CMCompressorPage::OnBnClickedCheckStartMotor)
	ON_BN_CLICKED(IDC_CHECK_START_TEST, &CMCompressorPage::OnBnClickedCheckStartTest)
	ON_BN_CLICKED(IDC_CHECK_VALVE_C1, &CMCompressorPage::OnBnClickedCheckValve1)
	ON_BN_CLICKED(IDC_CHECK_VALVE_C2, &CMCompressorPage::OnBnClickedCheckValve2)
END_MESSAGE_MAP()

void CMCompressorPage::OnBnClickedButtonStart()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_BUTTON_START);

	CGCanFrame canFrame;
	int pressure;
	bool start = (checkButton->GetCheck() != 0);
	
	pressure = GetIntValue(IDC_EDIT_TARGET_VALUE);

	canFrame.m_Data.Byte[0] = 'S';
	if(start)
	{
		canFrame.m_Data.Int16[1] = INT16(pressure);
	}
	else
	{
		// ��������� ���� ��������.
	}

	SendWriteCommand(&canFrame);
	GetDlgItem(IDC_EDIT_TARGET_VALUE)->EnableWindow(!start);
	GetDlgItem(IDC_EDIT_TARGET_VALUE_HT)->EnableWindow(!start);
	GetDlgItem(IDC_EDIT_TARGET_TIME)->EnableWindow(!start);
	GetDlgItem(IDC_BUTTON_START_HT)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_INITIALIZATION)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_START_MOTOR)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_START_TEST)->EnableWindow(!start);
}

void CMCompressorPage::OnBnClickedCheckInitialization()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_INITIALIZATION);

	CGCanFrame canFrame;
	bool start = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'T';
	if(start)
	{
		canFrame.m_Data.Uint16[1] = 1;
	}
	else
	{
		// ��������� ���� ��������.
	}

	SendWriteCommand(&canFrame);
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(!start);
	//GetDlgItem(IDC_CHECK_INITIALIZATION)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_START_TEST)->EnableWindow(!start);
}

void CMCompressorPage::OnBnClickedButtonStartHermTest()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_BUTTON_START_HT);

	CGCanFrame canFrame;
	int pressure;
	bool start = (checkButton->GetCheck() != 0);
	
	pressure = GetIntValue(IDC_EDIT_TARGET_VALUE_HT);

	canFrame.m_Data.Byte[0] = 'S';
	if(start)
	{
		canFrame.m_Data.Int16[1] = INT16(pressure);
		canFrame.m_Data.Int16[2] = 0;  // �����
		canFrame.m_Data.Int16[3] = 1;  // ����� ����� �������������

		// ������������� �������� ������� ����� ������ ����� ��������� ��������.
		m_TimeCounter = -1;
		m_TotalTime = GetIntValue(IDC_EDIT_TARGET_TIME);
		GetDlgItem(IDC_EDIT_TIME)->SetWindowText(_T(""));
		GetDlgItem(IDC_EDIT_RESULT)->SetWindowText(_T(""));
	}
	else
	{
		// ��������� ���� ��������.
	}

	SendWriteCommand(&canFrame);

	m_IsHermTestStarted = start;
	m_InitialPressure = 0;

	GetDlgItem(IDC_EDIT_TARGET_VALUE)->EnableWindow(!start);
	GetDlgItem(IDC_EDIT_TARGET_VALUE_HT)->EnableWindow(!start);
	GetDlgItem(IDC_EDIT_TARGET_TIME)->EnableWindow(!start);
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_INITIALIZATION)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_START_MOTOR)->EnableWindow(!start);
	GetDlgItem(IDC_CHECK_START_TEST)->EnableWindow(!start);
}

void CMCompressorPage::StartMotor(int voltage, int current, bool forward)
{
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = BYTE(forward ? 1 : 0xFF);
	canFrame.m_Data.Byte[2] = BYTE(voltage);
	canFrame.m_Data.Byte[3] = BYTE(current);
	SendWriteCommand(&canFrame);

	m_StartTime = GetTickCount();
}

void CMCompressorPage::OnBnClickedCheckStartMotor()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
	bool start = (checkButton->GetCheck() != 0);


	m_Voltage = GetIntValue(IDC_EDIT_MOTOR_VOLTAGE);
	m_Current = GetIntValue(IDC_EDIT_MOTOR_CURRENT);

	if(start)
	{
		bool moveForward;
		checkButton = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		moveForward = (checkButton->GetCheck() != 0);
		StartMotor(m_Voltage, m_Current, moveForward);

		m_IsMotorOn = true;
		m_MotorStartDirection = (moveForward ? 1 : -1);
	}
	else
	{
		CGCanFrame canFrame;
		canFrame.m_Data.Byte[0] = 'M';
		// ��������� ���� ��������.
		SendWriteCommand(&canFrame);
	}

	EnableControls(!start);
}

void CMCompressorPage::EnableControls(bool enable)
{
	GetDlgItem(IDC_EDIT_MOTOR_VOLTAGE)->EnableWindow(enable);
	GetDlgItem(IDC_EDIT_MOTOR_CURRENT)->EnableWindow(enable);
	GetDlgItem(IDC_RADIO_FORWARD)->EnableWindow(enable);
	GetDlgItem(IDC_RADIO_BACKWARD)->EnableWindow(enable);
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_INITIALIZATION)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_START_TEST)->EnableWindow(enable);
}

void CMCompressorPage::OnBnClickedCheckStartTest()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_TEST);
	m_IsMotorTestOn = (checkButton->GetCheck() != 0);

	UpdateDirection(m_CompressorState);

	CButton* startMotorButton = (CButton*)GetDlgItem(IDC_CHECK_START_MOTOR);
	// �������� ����� c ������������� ��������� ������ ������������.
	startMotorButton->SetCheck(m_IsMotorTestOn);
	OnBnClickedCheckStartMotor();

	// ����������� � OnBnClickedCheckStartMotor()
	GetDlgItem(IDC_CHECK_START_TEST)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_START_MOTOR)->EnableWindow(!m_IsMotorTestOn);
}

void CMCompressorPage::UpdateDirection(int stateFlags)
{
	if(stateFlags & COMPR_SENSOR_MIN)
	{
		((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(true);
		((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(false);
	}
	if(stateFlags & COMPR_SENSOR_MAX)
	{
		((CButton*)GetDlgItem(IDC_RADIO_FORWARD))->SetCheck(false);
		((CButton*)GetDlgItem(IDC_RADIO_BACKWARD))->SetCheck(true);
	}
}

void CMCompressorPage::OnBnClickedCheckValve1()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_VALVE_C1);
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = BYTE(checkButton->GetCheck() ? 1 : 0);
	canFrame.m_Data.Byte[2] = 0;
	SendWriteCommand(&canFrame);
}

void CMCompressorPage::OnBnClickedCheckValve2()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_VALVE_C2);
	CGCanFrame canFrame;
	canFrame.m_Data.Byte[0] = 'V';
	canFrame.m_Data.Byte[1] = BYTE(checkButton->GetCheck() ? 1 : 0);
	canFrame.m_Data.Byte[2] = 1;
	SendWriteCommand(&canFrame);
}

