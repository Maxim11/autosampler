#include "stdafx.h"
#include "resource.h"
#include "MonochromatorPage.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMMonochromatorPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMMonochromatorPage property page

const int c_InvalidStep = 0xFFFF;

CMMonochromatorPage::CMMonochromatorPage() : CMToolPage(CMMonochromatorPage::IDD)
{
	// ����� ����������� ��������.
	m_NodeAddress = 0x06;

	m_CurrentStep = c_InvalidStep;
}

CMMonochromatorPage::~CMMonochromatorPage()
{
}

void CMMonochromatorPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

// ���� ������ ������ Mode.
enum
{
  MONO_MODE_ERROR = 0x01,
  MONO_MODE_TEST  = 0x04,
  MONO_MODE_INIT  = 0x08,
};

void CMMonochromatorPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			CreateVersionString(canFrame);
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		CSMonochromatorFrame* monoStatus = (CSMonochromatorFrame*)canFrame->m_Data.Byte;

		if( (monoStatus->Mode & MOTORS_MONOCHROMATOR) == MOTORS_MONOCHROMATOR &&
			(monoStatus->Mode & MOTORS_MONO_2) == 0 )
		{
			m_CurrentStep = monoStatus->CurrentStep;

			CWnd* stepBox = GetDlgItem(IDC_EDIT_STEP);
			if(GetFocus()->GetSafeHwnd() != stepBox->GetSafeHwnd())
			{
				CString text;
				text.Format(_T("%d"), monoStatus->CurrentStep);
				stepBox->SetWindowText(text);
				m_TargetStep = m_CurrentStep;
			}

			m_Mono1StateBitmask.ShowBitmask(monoStatus->State);
			if((monoStatus->Mode & MONO_MODE_INIT) == 0)
			{
				((CButton*)GetDlgItem(IDC_CHECK_INITIALIZATION))->SetCheck(false);
			}
			if((monoStatus->Mode & MONO_MODE_TEST) == 0)
			{
				((CButton*)GetDlgItem(IDC_CHECK_START_TEST))->SetCheck(false);
			}
			if((monoStatus->State & (MS_FORWARD | MS_BACKWARD)) == 0)
			{
				GetDlgItem(IDC_EDIT_STEP)->EnableWindow(true);
				GetDlgItem(IDC_BUTTON_SET_STEP)->EnableWindow(true);
				GetDlgItem(IDC_SPIN_STEP)->EnableWindow(true);
			}

			int errorCode = monoStatus->ErrorCode;
			if(errorCode != 0)
			{
				ShowErrorMessage(IDS_ERROR_MONO_ONE, IDS_ERROR_MONO, errorCode);
			}
		}
	}
}

BEGIN_MESSAGE_MAP(CMMonochromatorPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_SET_STEP, &CMMonochromatorPage::OnBnClickedButtonSetStep)
	ON_BN_CLICKED(IDC_CHECK_INITIALIZATION, &CMMonochromatorPage::OnBnClickedButtonInitialization)
	ON_BN_CLICKED(IDC_CHECK_START_TEST, &CMMonochromatorPage::OnBnClickedButtonStartTest)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_STEP, &CMMonochromatorPage::OnDeltaposSpinStep)
	ON_EN_CHANGE(IDC_EDIT_STEP, OnEditStep)
END_MESSAGE_MAP()

BOOL CMMonochromatorPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();

	m_Mono1StateBitmask.SubclassDlgItem(IDC_LIST_MONO1_STATUS_BITS, this);
	m_Mono1StateBitmask.Initialize(IDS_BITS_MONO1_STATUS, 8);
	m_Mono1StateBitmask.EnableWindow(false);
	
	return result;
}

void CMMonochromatorPage::OnEditStep()
{
	m_TargetStep = GetIntValue(IDC_EDIT_STEP);
}

void CMMonochromatorPage::OnBnClickedButtonSetStep()
{
	if(m_CurrentStep == c_InvalidStep)
		return;

	CGCanFrame canFrame;
	//int newStep = GetIntValue(IDC_EDIT_STEP);

	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Int16[1] = (INT16)m_TargetStep;

	SendWriteCommand(&canFrame);

	GetDlgItem(IDC_EDIT_STEP)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_SET_STEP)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_STEP)->EnableWindow(false);
}

void CMMonochromatorPage::OnBnClickedButtonInitialization()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_INITIALIZATION);
	bool start = (checkButton->GetCheck() != 0);
	CGCanFrame canFrame;

	if(start)
	{
		canFrame.m_Data.Byte[0] = 'I';
		// ������������� ��������������.
		canFrame.m_Data.Byte[1] = 1;
		// ����� (�����) �������������.
		canFrame.m_Data.Byte[2] = 0x1;
	}
	else
	{
		canFrame.m_Data.Byte[0] = 'M';
		canFrame.m_Data.Byte[1] = 0;
		// ������� �������� �������������.
		canFrame.m_Data.Byte[4] = 1;
	}

	SendWriteCommand(&canFrame);
}

void CMMonochromatorPage::OnBnClickedButtonStartTest()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_START_TEST);
	bool start = (checkButton->GetCheck() != 0);
	CGCanFrame canFrame;

	if(start)
	{
		canFrame.m_Data.Byte[0] = 'M';
		canFrame.m_Data.Byte[1] = 0;
		// ������� ������������ �������������.
		canFrame.m_Data.Byte[4] = 2;
	}
	else
	{
		canFrame.m_Data.Byte[0] = 'M';
		canFrame.m_Data.Byte[1] = 0;
		// ������� �������� �������������.
		canFrame.m_Data.Byte[4] = 1;
	}

	SendWriteCommand(&canFrame);
}

void CMMonochromatorPage::OnDeltaposSpinStep(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: �������� ���� ��� ����������� �����������
	*pResult = 0;

	if(m_CurrentStep == c_InvalidStep)
		return;

	CGCanFrame canFrame;
	int delta = -pNMUpDown->iDelta;
	int newStep = m_CurrentStep+delta;

	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Int16[1] = (INT16)newStep;

	SendWriteCommand(&canFrame);
}
