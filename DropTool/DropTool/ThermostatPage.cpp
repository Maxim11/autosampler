#include "stdafx.h"
#include "resource.h"
#include "ThermostatPage.h"

#include "CanFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMThermostatPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMThermostatPage property page

CMThermostatPage::CMThermostatPage() : CMToolPage(CMThermostatPage::IDD)
{
	m_NodeAddress = 0x5;

	m_IsStarted = false;
	m_AskCurrent = false;
}

CMThermostatPage::~CMThermostatPage()
{
}

void CMThermostatPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

BOOL CMThermostatPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();

	m_ThermostatStateBitmask.SubclassDlgItem(IDC_LIST_STATUS_BITS, this);
	m_ThermostatStateBitmask.Initialize(IDS_BITS_THERMOSTAT_STATUS, 12);
	m_ThermostatStateBitmask.EnableWindow(false);

	((CButton*)GetDlgItem(IDC_RADIO_REGULATOR))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_TEST_FULL))->SetCheck(true);

	GetDlgItem(IDC_EDIT_TARGET_VALUE)->SetWindowText(_T("20"));

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}


void CMThermostatPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	// ��� "����� ����" � ��������� CanKingdom 105M
	if(canFrame->m_ExtendedId == (m_NodeAddress + 16) )
	{
		if(!m_IsFirmwareFor105M)
		{
			m_IsFirmwareFor105M = true;
			m_VersionString.Format(_T("v. %d.%d (%d/%d/%d)"),
				canFrame->m_Data.Byte[2],
				canFrame->m_Data.Byte[3],
				canFrame->m_Data.Byte[6],
				canFrame->m_Data.Byte[5],
				canFrame->m_Data.Byte[4]+2000
				);
			Invalidate(true);
		}
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		CString text;
		int page = m_IsFirmwareFor105M ? canFrame->m_Data.Byte[1] : canFrame->m_Data.Byte[0];

		// ������������ ����� �� ������ ������ ������.
		switch(page)
		{
		case 1:
			m_IsFirmwareFor105M = false;
			CreateVersionString(canFrame);
			break;
		case 'H':
			text.Format(_T("%d"), canFrame->m_Data.Int16[2]);
			GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(text);
			break;
		case 'c':
			text.Format(_T("%d"), canFrame->m_Data.Int16[1]);
			GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(text);
			break;
		case 'W':
			TRACE(_T("\nSensor Off=%d On=%d Sp=%d\n"),
				canFrame->m_Data.Int16[1],
				canFrame->m_Data.Int16[2],
				canFrame->m_Data.Int16[3]
			);
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		if(m_IsFirmwareFor105M)
		{
			ShowStatus105M(canFrame);
		}
		else
		{
			ShowStatus205(canFrame);
		}
	}
}

void CMThermostatPage::ShowStatus205(const CGCanFrame* canFrame)
{
	CSThermostatFrame* thermostatStatus = (CSThermostatFrame*)canFrame->m_Data.Byte;
	CString text;
	int i, d;
	i = thermostatStatus->WorkTemperature/100;
	d = thermostatStatus->WorkTemperature%100;
	text.Format(_T("%d.%d"), i, d);
	GetDlgItem(IDC_EDIT_WATER)->SetWindowText(text);
	i = thermostatStatus->AirTemperature/100;
	d = thermostatStatus->AirTemperature%100;
	text.Format(_T("%d.%d"), i, d);
	GetDlgItem(IDC_EDIT_AIR)->SetWindowText(text);

	int state = thermostatStatus->DeviceState;
	// ������� ��� ����������������� ����
	state = (state & 0x1FF) | ((state & 0xF000) >> 3);

	m_ThermostatStateBitmask.ShowBitmask(state);
	if(m_IsStarted)
	{
		if((state & (THERMO_PELTIER_HEATING | THERMO_PELTIER_COOLING | THERMO_PELTIER_REGULATOR | THERMO_PUMP_NORMAL)) == 0)
		{
			((CButton*)GetDlgItem(IDC_CHECK_START))->SetCheck(false);
			OnBnClickedButtonStart();
		}
	}

	if((thermostatStatus->DeviceMode & THERMO_MODE_AUTOTEST) == 0)
	{
		((CButton*)GetDlgItem(IDC_CHECK_INIT_START))->SetCheck(false);
	}
	if(m_AskCurrent)
	{
		SendReadCommand('H');
	}
	SendReadCommand('W');

	int errorCode = thermostatStatus->ErrorCode;
	if(errorCode != 0)
	{
		if(errorCode <= 10)
		{
			ShowErrorMessage(IDS_ERROR_THERMOSTAT, IDS_ERROR_THERMOSTAT, errorCode);
		}
		else
		{
			ShowErrorMessage(IDS_ERROR_THERMOSTAT_AT, IDS_ERROR_THERMOSTAT_AT, errorCode-10);
		}
	}
}

void CMThermostatPage::ShowStatus105M(const CGCanFrame* canFrame)
{
	CSThermostatFrame105M* thermostatStatus = (CSThermostatFrame105M*)canFrame->m_Data.Byte;
	CString text;
	int i, d;
	i = thermostatStatus->WorkTemperature/100;
	d = thermostatStatus->WorkTemperature%100;
	text.Format(_T("%d.%d"), i, d);
	GetDlgItem(IDC_EDIT_WATER)->SetWindowText(text);
	i = thermostatStatus->AirTemperature/100;
	d = thermostatStatus->AirTemperature%100;
	text.Format(_T("%d.%d"), i, d);
	GetDlgItem(IDC_EDIT_AIR)->SetWindowText(text);

	int deviceState = thermostatStatus->DeviceState;
	int stateFlags = 0;
	if(deviceState & THERMO_M_PELTIER_ON)
	{
		stateFlags |= (THERMO_PELTIER_HEATING | THERMO_PELTIER_COOLING);
	}
	if(deviceState & THERMO_M_PELTIER_PID)
	{
		stateFlags |= THERMO_PELTIER_REGULATOR;
	}
	if(deviceState & THERMO_M_PUMP_ON)
	{
		stateFlags |= THERMO_PUMP_NORMAL;
	}

	m_ThermostatStateBitmask.ShowBitmask(stateFlags);
	if(m_IsStarted)
	{
		if((stateFlags & (THERMO_PELTIER_HEATING | THERMO_PELTIER_COOLING | THERMO_PELTIER_REGULATOR | THERMO_PUMP_NORMAL)) == 0)
		{
			((CButton*)GetDlgItem(IDC_CHECK_START))->SetCheck(false);
			OnBnClickedButtonStart();
		}
	}

	if((thermostatStatus->DeviceMode & THERMO_M_MODE_AUTOTEST) == 0)
	{
		((CButton*)GetDlgItem(IDC_CHECK_INIT_START))->SetCheck(false);
	}
	if(m_AskCurrent)
	{
		SendReadCommand('c');
	}

	int errorCode = thermostatStatus->ErrorCode;
	// ��� ���������� ������ � thermostatStatus->ErrorCode ���������� �������� ������, ������� ��������� � thermostatStatus->PumpEmf
	if(errorCode != 0 && errorCode != thermostatStatus->PumpEmf)
	{
		if((thermostatStatus->DeviceMode & THERMO_MODE_AUTOTEST) == 0)
		{
			ShowErrorMessage(IDS_ERROR_THERMOSTAT, IDS_ERROR_THERMOSTAT, errorCode);
		}
		else
		{
			ShowErrorMessage(IDS_ERROR_THERMOSTAT_AT, IDS_ERROR_THERMOSTAT_AT, errorCode);
		}
	}
}

BEGIN_MESSAGE_MAP(CMThermostatPage, CMToolPage)
	ON_BN_CLICKED(IDC_RADIO_REGULATOR, &CMThermostatPage::OnBnClickedRadioRegulator)
	ON_BN_CLICKED(IDC_RADIO_COOLING, &CMThermostatPage::OnBnClickedRadioCooling)
	ON_BN_CLICKED(IDC_RADIO_HEATING, &CMThermostatPage::OnBnClickedRadioHeating)
	ON_BN_CLICKED(IDC_CHECK_START, &CMThermostatPage::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_CHECK_INIT_START, &CMThermostatPage::OnBnClickedButtonInitStart)
	ON_BN_CLICKED(IDC_CHECK_PUMP_WORK, &CMThermostatPage::OnBnClickedCheckPumpWork)
	ON_BN_CLICKED(IDC_CHECK_PUMP_REVERSE, &CMThermostatPage::OnBnClickedCheckPumpReverse)
	ON_BN_CLICKED(IDC_CHECK_WHITE, &CMThermostatPage::OnBnClickedCheckWhite)
	ON_BN_CLICKED(IDC_CHECK_GREEN, &CMThermostatPage::OnBnClickedCheckGreen)
	ON_BN_CLICKED(IDC_CHECK_RED, &CMThermostatPage::OnBnClickedCheckRed)
END_MESSAGE_MAP()

void CMThermostatPage::OnBnClickedRadioRegulator()
{
	//GetDlgItem(IDC_STATIC_PWM)->ShowWindow(false);
	GetDlgItem(IDC_STATIC_TEMPERATURE)->ShowWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_VALUE)->ShowWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_PWM)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->SetWindowText(_T(""));
}

void CMThermostatPage::OnBnClickedRadioCooling()
{
	//GetDlgItem(IDC_STATIC_PWM)->ShowWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_PWM)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(_T("400"));
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->SetWindowText(_T("400"));
	GetDlgItem(IDC_STATIC_TEMPERATURE)->ShowWindow(false);
	GetDlgItem(IDC_EDIT_TARGET_VALUE)->ShowWindow(false);
}

void CMThermostatPage::OnBnClickedRadioHeating()
{
	GetDlgItem(IDC_EDIT_TARGET_PWM)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(_T("400"));
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->SetWindowText(_T("400"));
	GetDlgItem(IDC_STATIC_TEMPERATURE)->ShowWindow(false);
	GetDlgItem(IDC_EDIT_TARGET_VALUE)->ShowWindow(false);
}

void CMThermostatPage::OnBnClickedButtonStart()
{
	CGCanFrame canFrame;

	m_IsStarted = ((CButton*)GetDlgItem(IDC_CHECK_START))->GetCheck() != 0;
	if(m_IsStarted)
	{
		if(((CButton*)GetDlgItem(IDC_RADIO_REGULATOR))->GetCheck() != 0)
		{
			canFrame.m_Data.Byte[0] = 'S';
			// ��������� �������� ��� �������� ������� ����.
			canFrame.m_Data.Byte[1] = 1;
			// � ����� �������.
			canFrame.m_Data.Uint16[1] = UINT16(GetIntValue(IDC_EDIT_TARGET_VALUE))*100;

			m_AskCurrent = true;
			GetDlgItem(IDC_EDIT_TARGET_VALUE)->EnableWindow(false);
		}
		else
		{
			canFrame.m_Data.Byte[0] = 'P';
			if(((CButton*)GetDlgItem(IDC_RADIO_HEATING))->GetCheck() != 0)
			{
				canFrame.m_Data.Byte[1] = 1;
			}
			else
			{
				canFrame.m_Data.Byte[1] = 0;
			}
			canFrame.m_Data.Uint16[1] = UINT16(GetIntValue(IDC_EDIT_TARGET_PWM));
			canFrame.m_Data.Uint16[3] = UINT16(GetIntValue(IDC_EDIT_TARGET_VOLTAGE));

			GetDlgItem(IDC_EDIT_TARGET_PWM)->EnableWindow(false);
			GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->EnableWindow(false);
		}
		GetDlgItem(IDC_RADIO_REGULATOR)->EnableWindow(false);
		GetDlgItem(IDC_RADIO_COOLING)->EnableWindow(false);
		GetDlgItem(IDC_RADIO_HEATING)->EnableWindow(false);
	}
	else
	{
		canFrame.m_Data.Byte[0] = 'S';
		canFrame.m_Data.Uint16[1] = 0;
		
		if(m_AskCurrent)
		{
			GetDlgItem(IDC_EDIT_TARGET_VALUE)->EnableWindow(true);
			GetDlgItem(IDC_EDIT_TARGET_PWM)->SetWindowText(_T(""));
			m_AskCurrent = false;
		}
		else
		{
			GetDlgItem(IDC_EDIT_TARGET_PWM)->EnableWindow(true);
			GetDlgItem(IDC_EDIT_TARGET_VOLTAGE)->EnableWindow(true);
		}
		GetDlgItem(IDC_RADIO_REGULATOR)->EnableWindow(true);
		GetDlgItem(IDC_RADIO_COOLING)->EnableWindow(true);
		GetDlgItem(IDC_RADIO_HEATING)->EnableWindow(true);
	}

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedButtonInitStart()
{
	bool isStarting = ((CButton*)GetDlgItem(IDC_CHECK_INIT_START))->GetCheck() != 0;
	CGCanFrame canFrame;
	const int c_radioButtonID[] =
	{
		IDC_RADIO_TEST_FULL,
		IDC_RADIO_TEST_SENSORS,
		IDC_RADIO_TEST_PUMPS,
		IDC_RADIO_TEST_HEATING,
		IDC_RADIO_TEST_COOLING,
		0,
	};
	const UINT16 c_masks[] =
	{
		0xF,	// ������
		0x1,
		0x2,
		0x4,
		0x8,
	};
	// ����������� ������ ����������������
	enum
	{
	  AT_CMD_WITH_CASSETTE = 0x01,
	  AT_CMD_USE_MASK      = 0x02,
	  AT_CMD_FUNCTIONAL    = 0x04,
	  AT_CMD_STOP          = 0x10,
	};

	int selectedMode = GetCheckedRadioButton(c_radioButtonID);

	canFrame.m_Data.Byte[0] = 'T';
	if(isStarting)
	{
		canFrame.m_Data.Byte[1] = AT_CMD_USE_MASK | AT_CMD_WITH_CASSETTE;	// ���������� ������� �����
		canFrame.m_Data.Uint16[1] = c_masks[selectedMode];
	}
	else
	{
		canFrame.m_Data.Byte[1] = AT_CMD_STOP;
		canFrame.m_Data.Uint16[1] = 0;
	}

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedCheckPumpWork()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_PUMP_WORK);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Byte[2] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedCheckPumpReverse()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_PUMP_REVERSE);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'M';
	canFrame.m_Data.Byte[1] = 1;
	canFrame.m_Data.Byte[2] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedCheckWhite()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_WHITE);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 0;
	canFrame.m_Data.Byte[2] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedCheckGreen()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_GREEN);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 1;
	canFrame.m_Data.Byte[2] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMThermostatPage::OnBnClickedCheckRed()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_RED);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'L';
	canFrame.m_Data.Byte[1] = 2;
	canFrame.m_Data.Byte[2] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}
