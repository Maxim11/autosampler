#pragma once
#include "ToolPage.h"

// ���������� ���� CMCompressorBoardPage

class CMCompressorBoardPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMCompressorBoardPage)

public:
	CMCompressorBoardPage();   // ����������� �����������
	virtual ~CMCompressorBoardPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

	// ���������� ������������ ��������� ������� �������� ����.
	virtual void OnTimer();

// ������ ����������� ����
	enum { IDD = IDD_PAGE_BOARD_COMPRESSOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();

	void SetMotorVoltage();

	void ShowStatus205(const CGCanFrame* canFrame);
	void ShowStatus105M(const CGCanFrame* canFrame);

public:
	afx_msg void OnBnClickedCheckStartMotor();
	afx_msg void OnBnClickedCheckValve1();
	afx_msg void OnBnClickedCheckValve2();
	afx_msg void OnBnClickedCheckHallSensors();

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

protected:
	DECLARE_MESSAGE_MAP()

protected:
	bool m_IsSensorsStateChecked;

	bool m_IsMotorOn;
};

// ������ ������, ���������� �� CAN.
struct CSCompressorFrame
{
  BYTE  DeviceMode;             // ������� ����� ������
  BYTE  DeviceState1;           // ��������� ���������
  BYTE  DeviceState2;           // ��������� ���������
  BYTE  ErrorCode;              // ��� ������
  UINT16 PressureValue;         // �������� �������� � ������� �����
  INT16  MotorPower;            // �������� ���������� �� ���������
};

//  ������ ���� ��������� ����������.
enum
{
  COMPR_MOTOR_FORWARD  = 0x01,      // �������� "������"
  COMPR_MOTOR_BACKWARD = 0x02,      // �������� "�����"
  COMPR_SENSOR_MIN     = 0x04,      // ������� �� ��������� MIN
  COMPR_SENSOR_MAX     = 0x08,      // ������� �� ��������� MAX
  COMPR_VALVE_1_ON     = 0x10,      // ������ ������
  COMPR_VALVE_2_ON     = 0x20,      // ������ ������
  COMPR_HARD_ERROR     = 0x40,      // ������ �������� ������
  COMPR_FATAL_ERROR    = 0x80,      // ������ - ����� �� ����� ��������� ������.
};

//  ������ ���� ��������� ����������.
enum
{
  COMPR_PRESSURE_NEGATIVE  = 0x01,  // ���������� �������� ������������
  COMPR_PRESSURE_OK        = 0x02,  // �������� ������������� ��������
  COMPR_CASSETTE_1         = 0x40,  // ������ �������
  COMPR_CASSETTE_2         = 0x80,  // ������ �������
};


// ������ ������, ���������� �� CAN.
struct CSCompressorFrame105M
{
  BYTE  DeviceMode;             // ������� ����� ������
  BYTE  ErrorCode;              // ��� ������
  BYTE  DeviceState1;           // ��������� ���������
  BYTE  DeviceState2;           // ��������� ���������
  INT16 PressureValue;         // �������� �������� � ����
  INT16 MotorPower;            // �������� ���������� �� ���������
};

//  ������ ���� ��������� ����������.
enum
{
  COMPR_M_SENSOR_MIN     = 0x01,      // ������� �� ��������� MIN
  COMPR_M_SENSOR_MAX     = 0x02,      // ������� �� ��������� MAX
  COMPR_M_MOTOR_FORWARD  = 0x04,      // �������� "������"
  COMPR_M_MOTOR_BACKWARD = 0x08,      // �������� "�����"
  COMPR_M_VALVE_1_ON     = 0x10,      // ������ ������
  COMPR_M_STABILIZATION  = 0x20,      // �������� ������������� ��������
  COMPR_M_MOTOR_ON       = 0x40,      // ���������� ���������� ��������
  COMPR_M_FATAL_ERROR    = 0x80,      // ������ - ����� �� ����� ��������� ������.
};

//  ������ ���� ��������� ����������.
enum
{
  COMPR_M_HARD_ERROR     = 0x02,      // ������ �������� ������
  COMPR_M_VALVE_ERROR    = 0x04,      // ������ - ����� �� ����� ��������� ������.
};
