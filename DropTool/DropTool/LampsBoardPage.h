#pragma once

#include "ToolPage.h"
#include "Controls/BitmaskListBox.h"

// ���������� ���� CMLampsBoardPage

class CMLampsBoardPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMLampsBoardPage)

public:
	CMLampsBoardPage();
	virtual ~CMLampsBoardPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// ������ ����������� ����
	enum { IDD = IDD_PAGE_BOARD_LAMPS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();

public:
	afx_msg void OnBnClickedCheckManual();
	afx_msg void OnBnClickedCheckD2Lamp();
	afx_msg void OnBnClickedCheckHeating();
	afx_msg void OnBnClickedCheckAnode();
	afx_msg void OnBnClickedButtonIgnite();
	afx_msg void OnBnClickedCheckHalogenLamp();
	afx_msg void OnBnClickedCheckRedLed();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

protected:
	void SetHeatingVoltage(int voltage);

protected:
	CMBitmaskListBox m_LampsModeBitmask;
	CMBitmaskListBox m_LampsStateBitmask;

	DECLARE_MESSAGE_MAP()
};

// ������ ������, ���������� �� CAN.
struct CSLampsFrame
{
	BYTE   DeviceMode;            // ������� ����� ������
	BYTE   DeviceState;           // ��������� ���������
	UINT16 SupplyVoltage;         // ���������� ������� (��)
	UINT16 CathodeVoltage;        // ���������� ������ (��)
	UINT16 AnodeCurrent;          // ������� ��� (������� �����������)
};

// ���� DeviceMode.
enum
{
	LAMPS_MODE_ERROR            = 0x01,
	LAMPS_MODE_D2_LAMP_ON       = 0x02,
	LAMPS_MODE_D2_LAMP_INIT     = 0x04,
	LAMPS_MODE_TEST             = 0x10,
	LAMPS_MODE_MANUAL           = 0x20,
	// ������� ��� ���� ���������, �� � ����� "����������" ������ ����������
	// � ����� ������.
	LS_HALOGEN_LAMP_ON           = 0x40,
	LS_RED_LED_ON                = 0x80,
};

// ���� ��������� ����� D2.
enum
{
	LS_CATHODE_VOLTAGE_ON    = 0x01,
	LS_ANODE_VOLTAGE_ON      = 0x02,
	LS_IGNITION_VOLTAGE_ON   = 0x04,
	LS_ANODE_CURRENT_OK      = 0x08,
	// ������� ���� �������� ��� ������.
	LS_ERROR_MASK            = 0xF000,
};

// ��� ������ ���������� � ������� 4-� ����� ����� ���������.
enum
{
	ERROR_LAMPS_SUPPLY_VOLTAGE_LOW = 1,
	ERROR_LAMPS_SUPPLY_VOLTAGE_HIGH,
	ERROR_LAMPS_WARMING_UP_VOLTAGE_LOW,
	ERROR_LAMPS_WARMING_UP_VOLTAGE_HIGH,
	ERROR_LAMPS_WORK_VOLTAGE_LOW,
	ERROR_LAMPS_WORK_VOLTAGE_HIGH,
	ERROR_LAMPS_IGNITION_FAILURE,
	ERROR_LAMPS_D2_LAMP_IS_GO_OUT,
};
