#if !defined(AFX_BITMASKLISTBOX_H__05952731_2FB2_4204_99B1_9790CF44B774__INCLUDED_)
#define AFX_BITMASKLISTBOX_H__05952731_2FB2_4204_99B1_9790CF44B774__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmaskListBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMBitmaskListBox window

class CMBitmaskListBox : public CCheckListBox
{
// Construction
public:
	CMBitmaskListBox();

// Attributes
public:

	void Initialize(int firstStringID, int size);
	void ShowBitmask(short bitmask, const int* order = 0);
	short GetBitmask();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMBitmaskListBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMBitmaskListBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMBitmaskListBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMASKLISTBOX_H__05952731_2FB2_4204_99B1_9790CF44B774__INCLUDED_)
