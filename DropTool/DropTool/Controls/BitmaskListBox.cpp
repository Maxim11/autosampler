// BitmaskListBox.cpp : implementation file
//

#include "stdafx.h"
//#include "..\Stend.h"
#include "BitmaskListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMBitmaskListBox

CMBitmaskListBox::CMBitmaskListBox()
{
}

CMBitmaskListBox::~CMBitmaskListBox()
{
}


BEGIN_MESSAGE_MAP(CMBitmaskListBox, CCheckListBox)
	//{{AFX_MSG_MAP(CMBitmaskListBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMBitmaskListBox message handlers

void CMBitmaskListBox::Initialize(int firstStringID, int size)
{
	CString text;

	ResetContent();
	for(int i = 0; i < size; i++)
	{
		text.LoadString(firstStringID+i);
		AddString(text);
	}
}

void CMBitmaskListBox::ShowBitmask(short bitmask, const int* order)
{
	int size = GetCount();
	for(int i = 0; i < size; i++)
	{
		if(order == 0)
			SetCheck(i, (bitmask & 1));
		else
			SetCheck(order[i], (bitmask & 1));
		bitmask >>= 1;
	}
}

short CMBitmaskListBox::GetBitmask()
{
	short result = 0;;
	int size = GetCount();
	for(int i = 0; i < size; i++)
	{
		bool isChecked = (GetCheck(i) != 0);
		result |= (isChecked << i);
	}
	return result;
}

