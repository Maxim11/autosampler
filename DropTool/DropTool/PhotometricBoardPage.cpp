// PhotometricBoardPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "PhotometricBoardPage.h"

#include "CanFrame.h"


// ���������� ���� CMPhotometricBoardPage

IMPLEMENT_DYNAMIC(CMPhotometricBoardPage, CMToolPage)

CMPhotometricBoardPage::CMPhotometricBoardPage()
	: CMToolPage(CMPhotometricBoardPage::IDD)
{
	m_NodeAddress = 0xB;
}

CMPhotometricBoardPage::~CMPhotometricBoardPage()
{
}

void CMPhotometricBoardPage::DoDataExchange(CDataExchange* pDX)
{
	CMToolPage::DoDataExchange(pDX);
}


BOOL CMPhotometricBoardPage::OnInitDialog()
{
	CMToolPage::OnInitDialog();

	GetDlgItem(IDC_EDIT_FREQUENCY)->SetWindowText(_T("10"));
	GetDlgItem(IDC_EDIT_TURBO)->SetWindowText(_T("0"));

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

double ConvertInt32ToVolts(long value)
{
	const long c_MaxInt32Value = 0x00800000;		// �������� ��������� 24-������� ���
	const double c_MaxVolts = 6.0;					// ������������ ���������� �������������
	return double(value)*c_MaxVolts/double(c_MaxInt32Value);
}

void CMPhotometricBoardPage::ProcessStatusFrame(const CGCanFrame* canFrame)
{
	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_READ_REPLY) )
	{
		// ������������ ����� �� ������ ������ ������.
		switch(canFrame->m_Data.Byte[0])
		{
		case 1:
			CreateVersionString(canFrame);
			break;
		}
		return;
	}

	if(canFrame->m_ExtendedId == ((m_NodeAddress << 4) | CAN_ID_STATUS_FRAME) )
	{
		int errorDelta = 0;
		// ��������� ������ ������ ������������� �������� ���������� �� ���������,
		// �� �� ��������� ����������� ���������� ���������� �������� �����.
		int adcResultPhoto     = canFrame->m_Data.Int32[0] >> 8;
		int adcResultReference = canFrame->m_Data.Int32[1] >> 8;
		int adcStatePhoto      = canFrame->m_Data.Byte[0];
		int adcStateReference  = canFrame->m_Data.Byte[4];

		if((adcStatePhoto & 0x80) == 0)
		{
			// ������ �� ��� ����������.
			CString text;
			int photoControlID = IDC_EDIT_SIGNAL_PHOTO_1;
			int referenceControlID = IDC_EDIT_SIGNAL_REFERENCE_1;
			if((adcStatePhoto & 0x40) != 0)
			{
				errorDelta = 1;
				photoControlID = IDC_EDIT_SIGNAL_PHOTO_2;
				referenceControlID = IDC_EDIT_SIGNAL_REFERENCE_2;
			}
			else
			{
				TRACE("Signal p %.6f r %.6f Time %d\n", ConvertInt32ToVolts(adcResultPhoto), ConvertInt32ToVolts(adcResultReference),
					GetTickCount());
			}
			text.Format(_T("%.6f"), ConvertInt32ToVolts(adcResultPhoto));
			GetDlgItem(photoControlID)->SetWindowText(text);
			text.Format(_T("%.6f"), ConvertInt32ToVolts(adcResultReference));
			GetDlgItem(referenceControlID)->SetWindowText(text);

			int errorCode = adcStatePhoto & 0xF;
			if(errorCode != 0)
			{
				ShowErrorMessage(IDS_ERROR_ADC_PHOTO+errorDelta, IDS_ERROR_ADC_ADS, errorCode);
			}
			errorCode = adcStateReference & 0xF;
			if(errorCode != 0)
			{
				ShowErrorMessage(IDS_ERROR_ADC_REF+errorDelta, IDS_ERROR_ADC_ADS, errorCode);
			}
		}
		else
		{
			// ������ �� ��� ��.
			CString text;
			text.Format(_T("%.6f"), ConvertInt32ToVolts(adcResultPhoto));
			GetDlgItem(IDC_EDIT_SIGNAL_FP)->SetWindowText(text);

			int errorCode = adcStatePhoto & 0xF;
			if(errorCode != 0)
			{
				ShowErrorMessage(IDS_ERROR_ADC_FP, IDS_ERROR_ADC_MCP, errorCode);
			}
		}

		return;
	}
}

BEGIN_MESSAGE_MAP(CMPhotometricBoardPage, CMToolPage)
	ON_BN_CLICKED(IDC_CHECK_FLOW_POTENTIAL, &CMPhotometricBoardPage::OnBnClickedCheckFlowPotential)
	ON_BN_CLICKED(IDC_BUTTON_INITIALIZATION, &CMPhotometricBoardPage::OnBnClickedButtonInitialization)
	ON_BN_CLICKED(IDC_CHECK_SYNC_ON, &CMPhotometricBoardPage::OnBnClickedCheckSyncOn)
	ON_BN_CLICKED(IDC_BUTTON_SET_MODE, &CMPhotometricBoardPage::OnBnClickedButtonSetMode)
END_MESSAGE_MAP()


// ����������� ��������� CMPhotometricBoardPage

void CMPhotometricBoardPage::OnBnClickedCheckFlowPotential()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_ANODE);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'F';
	canFrame.m_Data.Byte[1] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}

void CMPhotometricBoardPage::OnBnClickedButtonInitialization()
{
	CGCanFrame canFrame;
	
	canFrame.m_Data.Byte[0] = 'I';

	SendWriteCommand(&canFrame);
}

void CMPhotometricBoardPage::OnBnClickedButtonSetMode()
{
	CGCanFrame canFrame;
	int frequency = GetIntValue(IDC_EDIT_FREQUENCY);
	int turbo = GetIntValue(IDC_EDIT_TURBO);
	
	canFrame.m_Data.Byte[0] = 'S';
	canFrame.m_Data.Byte[1] = BYTE(frequency);
	canFrame.m_Data.Byte[2] = BYTE(turbo);

	SendWriteCommand(&canFrame);
}

void CMPhotometricBoardPage::OnBnClickedCheckSyncOn()
{
	CButton* checkButton = (CButton*)GetDlgItem(IDC_CHECK_ANODE);
	CGCanFrame canFrame;
	bool isOn = (checkButton->GetCheck() != 0);
	
	canFrame.m_Data.Byte[0] = 'Y';
	canFrame.m_Data.Byte[1] = BYTE(isOn);

	SendWriteCommand(&canFrame);
}
