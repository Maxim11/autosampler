#pragma once

#include "ToolPage.h"

/////////////////////////////////////////////////////////////////////////////
// CMMotorsBoardPage dialog

class CMMotorsBoardPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMMotorsBoardPage)

// Construction
public:
	CMMotorsBoardPage();
	~CMMotorsBoardPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

	// ���������� ������������ ��������� ������� �������� ����.
	virtual void OnTimer();

// Dialog Data
	enum { IDD = IDD_PAGE_BOARD_MOTORS };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
public:
	afx_msg void OnSelectLiftMotor();
	afx_msg void OnBnClickedStartLiftMotor();
	afx_msg void OnBnClickedCheckHallSensors();
	afx_msg void OnSelectStepMotor();
	afx_msg void OnBnClickedStartStepMotor();

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
protected:
	DECLARE_MESSAGE_MAP()

protected:
	void SetLiftMotorVoltage();
	void SetStepMotorFrequency(int frequency);

protected:
	int m_SelectedLiftMotor;
	int m_SelectedStepMotor;

	bool m_IsLiftMotorOn;
	bool m_IsStepMotorOn;
	bool m_IsSensorsStateChecked;
};
