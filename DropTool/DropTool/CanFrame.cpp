#include "StdAfx.h"
#include "CanFrame.h"

CString GetLogString(const CGCanFrame& canFrame)
{
	CString string;
	//CString item;

	string += GetIdString(canFrame)+_T(' ');
	string += GetDlcString(canFrame)+_T(' ');
	string += GetDataString(canFrame)+_T(' ');
	string += GetTimeString(canFrame)+_T(' ');
	string += GetFlagsString(canFrame)+_T(' ');

/*
	if((canFrame->m_ExtendedId & CAN_FLAG_EXTENDED_ID) != 0)
	{
		item.Format(_T("%08x  "), (canFrame->m_ExtendedId & ~CAN_FLAG_EXTENDED_ID));
	}
	else
	{
		item.Format(_T("%04x  "), canFrame->m_ExtendedId);
		item = CString(_T("    "))+item;
	}
	string += item;

	item.Format(_T("%2d    "), canFrame->m_Dlc);
	string += item;

	for(int i = 0; i < 8; i++)
	{
		item.Format(_T("%02x "), canFrame->m_Data[i]);
		string += item;
	}

	//item.Format(_T("%10x  "), canFrame->m_TimeStamp);
	item.Format(_T("%10d  "), canFrame->m_TimeStamp);
	string += item;

	item.Format(_T("%1d "), (canFrame->m_Flags & CAN_FLAG_ECHO_FRAME) != 0);
	string += item;
*/
	string += _T('\n');

	return string;
}

CString GetHeaderString()
{
	return CString(_T("#### ID    DLC            DATA              TIME  ####\n"));
}

bool SetFromString(CGCanFrame* canFrame, CString& string)
{
	CString item;
	int start = 0;

	canFrame->m_Flags = 0;

	if(string[0] == _T('#'))
	{
		// ��� ������ �����������
		return false;
	}

	item = string.Tokenize(_T(" ,"), start);
	if(item.IsEmpty())
		return false;
	
	canFrame->m_ExtendedId = HexStringToInt(item);
	if(item.GetLength() > 4)
	{
		canFrame->m_ExtendedId |= CAN_FLAG_EXTENDED_ID;
	}

	item = string.Tokenize(_T(" ,"), start);
	if(item.IsEmpty())
		return false;
	
	canFrame->m_Dlc = HexStringToInt(item);

	for(int i = 0; i < 8; i++)
	{
		item = string.Tokenize(_T(" ,"), start);
		if(item.IsEmpty())
			return false;
		
		canFrame->m_Data.Byte[i] = HexStringToInt(item);
	}

	item = string.Tokenize(_T(" ,"), start);
	if(!item.IsEmpty())
	{
		canFrame->m_TimeStamp = HexStringToInt(item);
	}
	else
	{
		canFrame->m_TimeStamp = 0;
	}

	return true;
}

UINT HexStringToInt(CString& string)
{
	const CString digits = _T("0123456789ABCDEF");
	int result = 0;

	string.MakeUpper();
	for (int i = 0; i < string.GetLength(); i++)
	{
		int index = digits.Find(string[i]);
		if(index < 0)
			break;
		result = result*16+index;
	}

	return result;
}

// �������� ���������� � ��������� ����
CString GetIdString(const CGCanFrame& canFrame)
{
	CString string;
	if((canFrame.m_ExtendedId & CAN_FLAG_EXTENDED_ID) != 0)
	{
		string.Format(_T("%08x  "), (canFrame.m_ExtendedId & ~CAN_FLAG_EXTENDED_ID));
	}
	else
	{
		string.Format(_T("%04x  "), canFrame.m_ExtendedId);
		string = CString(_T("    "))+string;
	}
	return string;
}

CString GetDlcString(const CGCanFrame& canFrame)
{
	CString string;
	string.Format(_T("%2d "), canFrame.m_Dlc);
	return string;
}

CString GetDataString(const CGCanFrame& canFrame)
{
	CString string;
	CString item;
	int i;
	for( i = 0; i < canFrame.m_Dlc; i++)
	{
		item.Format(_T("%02x "), canFrame.m_Data.Byte[i]);
		string += item;
	}
	i = 8-canFrame.m_Dlc;
	if(i > 0)
	{
		string += CString(_T(' '), i*3);
	}

	return string;
}

CString GetTimeString(const CGCanFrame& canFrame)
{
	CString string;
	string.Format(_T("%10d "), canFrame.m_TimeStamp);
	return string;
}

CString GetFlagsString(const CGCanFrame& canFrame)
{
	CString string;
	if( (canFrame.m_Flags & CAN_FLAG_ECHO_FRAME) != 0)
	{
		string = _T('*');
	}
	return string;
}

