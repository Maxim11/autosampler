// ErrorDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "ErrorDialog.h"


// ���������� ���� CMErrorDialog

IMPLEMENT_DYNAMIC(CMErrorDialog, CDialog)

CMErrorDialog::CMErrorDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMErrorDialog::IDD, pParent)
{
	m_IgnoredErrorsArray.SetSize(0, 10);
}

CMErrorDialog::~CMErrorDialog()
{
}

void CMErrorDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMErrorDialog, CDialog)
END_MESSAGE_MAP()

// ������ ����� ������ �������
bool CMErrorDialog::SetErrorCode(const int groupTextID, const int errorTextID, const int errorCode)
{
	int errorID = errorTextID+errorCode;

	for(int i = 0; i < m_IgnoredErrorsArray.GetSize(); i++)
	{
		TRACE("Ignored err %d %d\n", i, m_IgnoredErrorsArray[i]);
		if(errorID == m_IgnoredErrorsArray[i])
			return false;
	}

	m_ErrorCode   = errorCode;
	m_ErrorTextID = errorID;
	m_GroupTextID = groupTextID;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CMErrorDialog message handlers

BOOL CMErrorDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect dialogRect;
	GetWindowRect(&dialogRect);
	m_InitialSize.cx = dialogRect.Width();
	m_InitialSize.cy = dialogRect.Height();

	Update();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMErrorDialog::Update()
{
	CString errorMessage;
	CString errorDescription1;
	CString errorDescription2;

	errorMessage.Format(m_GroupTextID, m_ErrorCode);
	errorDescription1.LoadString(m_ErrorTextID);

// ����� ��������� �� ������ ����� ���� �� ���� �����
	int newLine = errorDescription1.Find('\n');
	if(newLine >= 0)
	{
		errorDescription2 = errorDescription1.Mid(newLine+1);
		errorDescription1 = errorDescription1.Left(newLine);
	}

// ���������� ������� ������
	CClientDC clientDC(this);
	clientDC.SelectObject(GetFont());
	CSize size0 = clientDC.GetTextExtent(errorMessage);
	CSize size1 = clientDC.GetTextExtent(errorDescription1);
	CSize size2 = clientDC.GetTextExtent(errorDescription2);
	int maxWidth = max(size0.cx, max(size1.cx, size2.cx))+4;
	
	CWnd* messageStatic = GetDlgItem(IDC_STATIC_PROMPT);
	CWnd* descriptionStatic1 = GetDlgItem(IDC_STATIC_TEXT);
	CWnd* descriptionStatic2 = GetDlgItem(IDC_STATIC_TEXT_2);
	CWnd* ignoreCheckBox = GetDlgItem(IDC_CHECK_IGNORE_ERROR);
	CWnd* buttonOk = GetDlgItem(IDOK);

// ��������� ������� � ��������� ��������� � ����������� �� ����� ������
	CRect dialogRect;
	CRect buttonRect;
	CRect checkBoxRect;
	CRect rect0;
	CRect rect1;
	CRect rect2;
	GetWindowRect(&dialogRect);
	messageStatic->GetWindowRect(&rect0);
	descriptionStatic1->GetWindowRect(&rect1);
	descriptionStatic2->GetWindowRect(&rect2);
	ignoreCheckBox->GetWindowRect(&checkBoxRect);
	buttonOk->GetWindowRect(&buttonRect);
	ScreenToClient(&checkBoxRect);
	ScreenToClient(&buttonRect);

// ����� ��������� ������� ��������� ������ ��������� ����������
	int dialogWidth = rect0.left-dialogRect.left+maxWidth+size0.cy*2;
//	dialogRect.right += maxWidth - rect1.Width();
	dialogWidth = max(dialogWidth, m_InitialSize.cx);
	dialogRect.right = dialogRect.left+dialogWidth;
	buttonRect.left = (dialogRect.Width()-buttonRect.Width())/2;
	
	messageStatic->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	descriptionStatic1->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	descriptionStatic2->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

// ����� ��������� ������� �� ����� ������
	if(errorDescription2.IsEmpty())
	{
		if(dialogRect.Height() == m_InitialSize.cy)
		{
			int height = rect2.top-rect1.top;
			buttonRect.top -= height;
			checkBoxRect.top -= height;
			dialogRect.bottom -= height;

			descriptionStatic2->ShowWindow(false);
		}
	}
	else
	{
		if(dialogRect.Height() != m_InitialSize.cy)
		{
			dialogRect.bottom = dialogRect.top+m_InitialSize.cy;
			descriptionStatic2->ShowWindow(true);
			checkBoxRect.top += rect2.top-rect1.top;
			buttonRect.top += rect2.top-rect1.top;
		}
	}

	ignoreCheckBox->SetWindowPos(NULL, checkBoxRect.left, checkBoxRect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	buttonOk->SetWindowPos(NULL, buttonRect.left, buttonRect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
	SetWindowPos(NULL, 0, 0, dialogRect.Width(), dialogRect.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	CenterWindow();
	buttonOk->Invalidate();

// ������� ����� ���������. ������ ��� � ����� �����, ����� ��������� ����� �����������.
	messageStatic->SetWindowText(errorMessage);
	descriptionStatic1->SetWindowText(errorDescription1);
	descriptionStatic2->SetWindowText(errorDescription2);
}

// ���� ������ ������������, ��� �����������, ���������� ������� ���� �� ��� ��������
void CMErrorDialog::OnCancel()
{
	CButton* ignoreButton = (CButton*)GetDlgItem(IDC_CHECK_IGNORE_ERROR);
	if(ignoreButton != 0 && ignoreButton->GetCheck() != 0)
	{
		m_IgnoredErrorsArray.Add(m_ErrorTextID);
	}

	DestroyWindow();
}

void CMErrorDialog::OnOK()
{
	OnCancel();
}
