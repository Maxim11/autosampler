// ToolPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "DropTool.h"
#include "ToolPage.h"

#include "MainPropertySheet.h"
#include "CanFrame.h"

#include "ErrorDialog.h"

// CMToolPage

IMPLEMENT_DYNAMIC(CMToolPage, CMFCPropertyPage)

CMToolPage::CMToolPage(UINT nIDTemplate, UINT nIDCaption)
	: CMFCPropertyPage(nIDTemplate, nIDCaption)
{
	//m_IsErrorShowing = false;
	m_ErrorDialog = 0;
	m_IsFirmwareFor105M = false;
}

CMToolPage::~CMToolPage()
{
	if(m_ErrorDialog != 0)
		delete m_ErrorDialog;
}

void CMToolPage::SendWriteCommand(CGCanFrame* canFrame)
{
	canFrame->m_Dlc = 8;
	canFrame->m_Flags = 0;
	canFrame->m_ExtendedId = (m_NodeAddress << 4) | CAN_ID_WRITE_CMD;
	((CMMainPropertySheet*)GetParent())->SendCanFrame(canFrame);
}

void CMToolPage::SendReadCommand(int page)
{
	if(m_IsFirmwareFor105M)
	{
		SendReadCommand105M(page);
		return;
	}

	CGCanFrame canFrame;
	canFrame.m_Flags = 0;
	canFrame.m_Dlc = 8;
	canFrame.m_ExtendedId = (m_NodeAddress << 4) | CAN_ID_READ_CMD;
	canFrame.m_Data.Byte[0] = BYTE(page);

	((CMMainPropertySheet*)GetParent())->SendCanFrame(&canFrame);
}

void CMToolPage::SendReadCommand105M(int page)
{
	CGCanFrame canFrame;
	canFrame.m_Flags = 0;
	canFrame.m_Dlc = 8;
	canFrame.m_ExtendedId = (m_NodeAddress << 4) | CAN_ID_READ_CMD;
	canFrame.m_Data.Byte[0] = BYTE(0);
	canFrame.m_Data.Byte[1] = BYTE(page);

	((CMMainPropertySheet*)GetParent())->SendCanFrame(&canFrame);
}

void CMToolPage::CreateVersionString(const CGCanFrame* canFrame)
{
	CString string;
	string.Format(_T("v. %d.%d (%d/%d/%d)"),
		canFrame->m_Data.Byte[1],
		canFrame->m_Data.Byte[2],
		canFrame->m_Data.Byte[5],
		canFrame->m_Data.Byte[4],
		canFrame->m_Data.Byte[3]+2000
	);

	if(string != m_VersionString)
	{
		m_VersionString = string;
		// ����� ������������ ��������� ���������� ���������� �� ��������� ����.
		Invalidate(true);
	}
}

void CMToolPage::ClearVersionString()
{
	if(!m_VersionString.IsEmpty())
	{
		m_IsFirmwareFor105M = false;
		m_VersionString.Empty();
		Invalidate(true);
	}
}


int CMToolPage::GetIntValue(int controlID)
{
	const int MAX_SIZE = 6;
	TCHAR text[MAX_SIZE];

	GetDlgItem(controlID)->GetWindowText(text, MAX_SIZE);
	return _ttoi(text);
}

int CMToolPage::GetCheckedRadioButton(const int buttonID[])
{
	for(int i = 0; buttonID[i] != 0; i++)
	{
		CButton* radioButton = (CButton*)GetDlgItem(buttonID[i]);
		if(radioButton->GetCheck() != 0)
		{
			return i;
		}
	}
	return 0;
}

void CMToolPage::EnableControls(const int controlID[], const int enable)
{
	for(int i = 0; controlID[i] != 0; i++)
	{
		CWnd* control = GetDlgItem(controlID[i]);
		control->EnableWindow(enable);
	}
}

void CMToolPage::ShowErrorMessage(const int textID, const int errorTextID, const int errorCode)
{
	if(m_ErrorDialog == 0)
		m_ErrorDialog = new CMErrorDialog;

	if(!m_ErrorDialog->SetErrorCode(textID, errorTextID, errorCode))
	{
		// ������������ ����� ������������ ��������� �� ������
		return;
	}

	if(m_ErrorDialog->GetSafeHwnd() == 0)
	{
		m_ErrorDialog->Create(IDD_DIALOG_ERROR_MESSAGE, this);
	}
	else
	{
		m_ErrorDialog->Update();
	}


/*
	if(!m_IsErrorShowing)
	{
		CString text;
		CString errorText;
		text.Format(textID, errorCode);
		errorText.LoadString(errorTextID+errorCode);
		text += CString(_T('\n')) + errorText;

		m_IsErrorShowing = true;
		AfxMessageBox(text);
		m_IsErrorShowing = false;
	}
*/
}

BEGIN_MESSAGE_MAP(CMToolPage, CMFCPropertyPage)
END_MESSAGE_MAP()



// ����������� ��������� CMToolPage


