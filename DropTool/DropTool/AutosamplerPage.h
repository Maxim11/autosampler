#pragma once

#include "ToolPage.h"
#include "Controls/BitmaskListBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMAutosamplerPage dialog

class CMAutosamplerPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMAutosamplerPage)

// Construction
public:
	CMAutosamplerPage();
	~CMAutosamplerPage();

	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// Dialog Data
	enum { IDD = IDD_PAGE_AUTOSAMPLER };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
protected:
	int NormalizeVialNumber(int vialNumber, const int maxNumber);

	void ShowInputPosition();
	void ShowOutputPosition();

	void ShowInputLiftPosition(int liftState);
	void ShowOutputLiftPosition(int liftState);

	void ShowMoverPosition(int moverState);

	void ShowSensorsState(int sensorsState);

	void ShowLiftCurrentsDialog(const int upCurrent, const int downCurrent, const int movementCurrent);

	void InitSensorsList();

public:
	afx_msg void OnBnClickedButtonInitStart();
	afx_msg void OnBnClickedLiftCurrents();
	afx_msg void OnBnClickedRadioLiftInputUp();
	afx_msg void OnBnClickedRadioLiftInputDown();
	afx_msg void OnBnClickedRadioLiftOutputUp();
	afx_msg void OnBnClickedRadioLiftOutputDown();
	afx_msg void OnBnClickedRadioMoverExtOpen();
	afx_msg void OnBnClickedRadioMoverExt();
	afx_msg void OnBnClickedRadioMoverInt();
	afx_msg void OnBnClickedButtonOpenCap();
	afx_msg void OnBnClickedButtonOpenReverse();
	afx_msg void OnBnClickedButtonSetInput();
	afx_msg void OnBnClickedButtonSetOutput();

	afx_msg void OnBnClickedCheckMoverTest();
	afx_msg void OnDeltaposSpinMoveInput(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMoveOutput(NMHDR *pNMHDR, LRESULT *pResult);

protected:
	DECLARE_MESSAGE_MAP()

protected:
	CMBitmaskListBox m_SensorsBitmask;

	int m_CurrentInputPosition;
	int m_CurrentOutputPosition;
	int m_InputCarouselState;
	int m_OutputCarouselState;
};
