#pragma once

#include "DChannel/DChannel.h"

#include "AutosamplerPage.h"
#include "MonochromatorPage.h"
#include "CompressorPage.h"
#include "ThermostatPage.h"
#include "MotorsBoardPage.h"
#include "CompressorBoardPage.h"
#include "PhotometricBoardPage.h"
#include "LampsBoardPage.h"
#include "ConnectionPage.h"

/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet

class CMMainPropertySheet : public CMFCPropertySheet
{
	DECLARE_DYNAMIC(CMMainPropertySheet)

// Construction
public:
	CMMainPropertySheet();

// Attributes
// Operations
public:

// Overrides
	virtual void OnDrawPageHeader (CDC* pDC, int nPage, CRect rectHeader);
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();

// Implementation
public:
	virtual ~CMMainPropertySheet();

	void SendCanFrame(CGCanFrame* canFrame);
	void SetUsbCanAdapter(int usedAdapter);

protected:
	void CreateDataChannel();
	void StartDataExchange();
	void StopDataExchange();

	void ShowUsbState();

	void SendCanFrameEnableStatus();
	void SendCanReadVersion();

 	bool OpenLogFile();

	void EnableCanTracing(bool enable);

	afx_msg void OnOk();

	//afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnDataReceived(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData);
	afx_msg void OnSelectTree(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()

protected:
	CMAutosamplerPage   m_AutosamplerPage;
	CMMonochromatorPage m_MonochromatorPage;
	CMCompressorPage    m_CompressorPage;
	CMThermostatPage    m_ThermostatPage;

	CMMotorsBoardPage      m_MotorsBoardPage;
	CMCompressorBoardPage  m_CompressorBoardPage;
	CMPhotometricBoardPage m_PhotometricBoardPage;
	CMLampsBoardPage       m_LampsBoardPage;

	CMConnectionPage       m_ConnectionPage;

protected:
	// ����� ���������, ��������� ��� ��������� �������������
	int m_NumberOfDevices;

// ����� ������������ ��� ������ ������� ����� �������� � �����������
	CGDataChannel m_DataChannel;

	int m_TimerID;
	// ����� ������� ���������� CAN-������
	unsigned int m_TimeStamp;
	// ����� ���������� ������� ���������� � �����
	unsigned int m_LastCheckTime;
	// ����� ������ ���������� ������ �������
	unsigned int m_LastStatusFrameTime;

	bool m_UseUsbCanAdapter;
	UINT16 m_UsbProductID;

	bool m_IsCanConnected;
	CStatic m_StatusInfo;
};

enum
{
	c_AdapterUsbCan,
	c_AdapterCapel205,
	c_AdapterCapel115,
};