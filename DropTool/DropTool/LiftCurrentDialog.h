#pragma once
#include "afxwin.h"

class CMToolPage;
// ���������� ���� CMLiftCurrentDialog

class CMLiftCurrentDialog : public CDialog
{
	DECLARE_DYNAMIC(CMLiftCurrentDialog)

public:
	CMLiftCurrentDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~CMLiftCurrentDialog();

// ������ ����������� ����
	enum { IDD = IDD_DIALOG_LIFT_CURRENT };

	void SetParentPage(CMToolPage* page) { m_ParentPage = page; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_PressUp;
	CString m_PressDown;
	CString m_Movement;
	afx_msg void OnBnClickedButtonRead();
	afx_msg void OnBnClickedButtonWrite();

protected:
	int GetIntValue(int controlID);

protected:
	CMToolPage* m_ParentPage;
};
