object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Radiohlam USB HID Application'
  ClientHeight = 551
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 498
    TabOrder = 0
    object Label1: TLabel
      Left = 143
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Select one:'
    end
    object Label2: TLabel
      Left = 143
      Top = 31
      Width = 92
      Height = 13
      Caption = 'Hex string to send:'
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 184
      Width = 615
      Height = 314
      Caption = ' Log '
      TabOrder = 0
      object RichEdit1: TRichEdit
        Left = 7
        Top = 16
        Width = 601
        Height = 289
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object Button2: TButton
      Left = 504
      Top = 1
      Width = 105
      Height = 25
      Caption = 'Connect'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button1: TButton
      Left = 8
      Top = 1
      Width = 129
      Height = 25
      Caption = 'Find USB HID Devces'
      TabOrder = 2
      OnClick = Button1Click
    end
    object ComboBox1: TComboBox
      Left = 242
      Top = 3
      Width = 256
      Height = 21
      TabOrder = 3
      Text = '<None>'
      OnChange = ComboBox1Change
    end
    object Button3: TButton
      Left = 504
      Top = 25
      Width = 105
      Height = 25
      Caption = 'Disconnect'
      TabOrder = 4
      OnClick = Button3Click
    end
    object Edit1: TEdit
      Left = 242
      Top = 27
      Width = 256
      Height = 21
      TabOrder = 5
      OnKeyDown = Edit1KeyDown
      OnKeyPress = Edit1KeyPress
    end
    object Button5: TButton
      Left = 8
      Top = 78
      Width = 601
      Height = 25
      Caption = 
        'Send Output Report via WriteFile (interrupt transfer if posible,' +
        ' otherwise control transfer)'
      TabOrder = 6
      OnClick = Button5Click
    end
    object Button4: TButton
      Left = 8
      Top = 27
      Width = 129
      Height = 25
      Caption = 'Clear log'
      TabOrder = 7
      OnClick = Button4Click
    end
    object Button6: TButton
      Left = 8
      Top = 54
      Width = 490
      Height = 25
      Caption = 
        'Start Reading Input Reports from buffer via ReadFile (by the tim' +
        'er, with interrupt transfer)'
      TabOrder = 8
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 504
      Top = 54
      Width = 105
      Height = 25
      Caption = 'Stop Timer'
      TabOrder = 9
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 304
      Top = 150
      Width = 305
      Height = 25
      Caption = 'Send Feature Report via HidD_SetFeature'
      TabOrder = 10
      OnClick = Button8Click
    end
    object Button9: TButton
      Left = 8
      Top = 102
      Width = 601
      Height = 25
      Caption = 
        'Read Input Report via HidD_GetInputReport (directly, bypassing t' +
        'he buffer, with control transfer)'
      TabOrder = 11
      OnClick = Button9Click
    end
    object Button10: TButton
      Left = 8
      Top = 126
      Width = 601
      Height = 25
      Caption = 'Send Output Report via HidD_SetOutputReport (control transfer)'
      TabOrder = 12
      OnClick = Button10Click
    end
    object Button11: TButton
      Left = 8
      Top = 150
      Width = 297
      Height = 25
      Caption = 'Read Featute Report via HidD_GetFeature'
      TabOrder = 13
      OnClick = Button11Click
    end
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 504
    object File1: TMenuItem
      Caption = 'File'
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 56
    Top = 504
  end
end
