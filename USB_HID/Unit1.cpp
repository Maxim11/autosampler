/* --- https://radiohlam.ru --- */
//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

extern "C" {
#include "hidsdi.h"
#include <setupapi.h>
}
#pragma comment(lib,"HID-BCB.LIB")

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
/*
 *  HID Initialization
 *    Parameters:      None
 *    Return Value:    None
 */

void HID_Init()
{	int  i;

	DevCount = 0;																// �������� ���������� ��������� ���������
	DevSelected = -1;															// ���������� ��������� ����������
	for(i = 0; i < DEV_NUM; i++)												// ���������� ���� ������ � ����������� �� ���������, �������� ���� � �����������
	{	DevDetailData[i] = NULL;												// �������� ��������� ��������� �� ���������, �������� ���� � ����������
	}
}
//---------------------------------------------------------------------------
/*
 *  HID Uninitialization
 *    Parameters:      None
 *    Return Value:    None
 */

void HID_UnInit()
{	int  i;

	for(i = 0; i < DEV_NUM; i++)												// ���������� ���� ������ � ����������� �� ���������, �������� ���� � �����������
	{	if(DevDetailData[i])	free(DevDetailData[i]);							// ������� ������ ���������, �������� ���� � ����������
	}
}
//---------------------------------------------------------------------------
/*
 *  HID Find Devices
 *    Parameters:      None
 *    Return Value:    Number of Devices found
 */
int HID_FindDevices()
{	GUID								HidGuid;								// ������������� ������ HID-���������
	HANDLE								DevHandle;								// ��������� �� �������� ������
	HDEVINFO							DevInfo;								// ��������� �� ���������� ����� (������) HID-���������
	SP_DEVICE_INTERFACE_DATA			DevData;								// ��������� � ������� �� ���������� ���������� ����������
	PSP_DEVICE_INTERFACE_DETAIL_DATA	DevDetail;								// ��������� � ���������� ������� �� ���������� ���������� ���������� (�������� ���� � ����������)
	PSP_DEVICE_INTERFACE_DETAIL_DATA	DevDetailSelected;						// ��������� � ���������� ������� �� ���������� ���������� ���������� (�������� ���� � ����������)
	PHIDP_PREPARSED_DATA				PreparsedData;							// ���� ������ ��� ��������� ��������
	HIDD_ATTRIBUTES						Attributes;								// ��������� ��� �������� ��������� ����������
	HIDP_CAPS							Capabilities;							// ����������� �� ����������� �������
	ULONG								Length;									// ������ ������ ��� ��������� ��������� ������ �� ���������� ���������� ����������
	int									Index;									// ����� ���������� ���������������� ����������
	int									Sz;										// ������ ����
	BOOL								ok;										// ������� ���������� ������ ��������� �������

	wchar_t								Manufacturer[128];						// ���� ����� �������� �������������
	wchar_t								Product[128];	  						// ���� - �������� ��������
	wchar_t 							Serial[128];	  						// � ���� - �������� �����

	// �������� GUID ������ ��������� USB HID
	HidD_GetHidGuid(&HidGuid);
	Form1->RichEdit1->Lines->Add("HID GUID: " + Sysutils::GUIDToString(HidGuid)); // ����� ���������� GUID � Log

	// �������� ������ ���� HID-��������� (�� ���� ��������� � ��������� GUID), ������������������ � �������
	DevInfo = SetupDiGetClassDevs(	&HidGuid,
									NULL,
									NULL,
									(DIGCF_PRESENT | DIGCF_DEVICEINTERFACE)
								 );
	Form1->RichEdit1->Lines->Add("Ok! ������ ������������������ � ������� HID-��������� �������");	// ��������, ��� ������ �������
	DevData.cbSize = sizeof(DevData);											// ������ ���������, ���������� ������ �� ����������

	DevDetail = NULL;															// ���������� ��������� �� ��������� � ���������� ������� �� ����������
	if (DevSelected != -1)
	{	/* �������� ��������� ������ (����) �������� ���������� ������� �� ��������� ���������� (���������),
			����� ��� ����� ���� ����� �� ����� �������������� ������ */
		DevDetailSelected = (PSP_DEVICE_INTERFACE_DETAIL_DATA) malloc(DevDetailDataSz[DevSelected]);
		memcpy(DevDetailSelected, DevDetailData[DevSelected], DevDetailDataSz[DevSelected]);
	}
	else
	{	DevDetailSelected = NULL;		  										// ���� �������� ���������� ������� ��� - ���������� ���������
	}

	// ���������� ��� ��������� �� ��������� ������ � ������
	for (Index = 0; Index < DEV_NUM; Index++)
	{	if (DevDetailData[Index])
		{	free(DevDetailData[Index]);											// ������� ��������� (����������� ������)
			DevDetailData[Index] = NULL;										// ���������� ���������
		}
	}

	/* Scan all Devices */
	// ������� ComboBox � ����������� ������ ������
	Form1->ComboBox1->Items->Clear();											// ������� ������������ ������ ���������
	//Form1->ComboBox1->Items->Add(UnicodeString("<None>"));						// ������ ������ � ������ - None

	Index = -1;																	// �������������� ������� ��� �������� ���� ��������� hid-���������
	DevCount = 0;																// �������������� ������� ����������� � ������ ���������
	DevHandle = INVALID_HANDLE_VALUE;											// ���������� ��������� �� �������� ������
	do
	{   Index++;																// ����������� ������� ��� ��������� ���� ��������� hid-���������
		if(DevHandle != INVALID_HANDLE_VALUE)	CloseHandle(DevHandle); 		// ��������� �������� ����� ������, ���� ������-�� �� ������� �� �����

		ok = SetupDiEnumDeviceInterfaces(DevInfo,0,&HidGuid,Index,&DevData);	// �������� ���������� �� ��������� ����������
		if(!ok)	break;															// ������ ������ ��� ���������, - ��������� ���� do

		// ������� ����� ������ ������� ����� ����� ��� ��������� ���� � ����������
		ok = SetupDiGetDeviceInterfaceDetail(DevInfo,&DevData,NULL,0,&Length,NULL);

		/* �������� ������ ��� ����� � ���������� ������� (����) */
		if(DevDetail)	free(DevDetail);										// ���� � ��� ��� ���� �����, ���������� ���� - ������� ���
		DevDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA) malloc(Length);          // ������ ����� ����� ������� ������� � ���� ��������� PSP_DEVICE_INTERFACE_DETAIL_DATA
		Sz = Length;															// ���������� ������ ��������� ���������
		DevDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);			// ������������� ������ ��������� ���������

		// �������� ��������� ���������� (���� � ����������)
		ok = SetupDiGetDeviceInterfaceDetail(DevInfo,&DevData,DevDetail,Length,NULL,NULL);
		if(!ok)	continue;														// ��������� ������� �������� � ��������� � ���������� ����������
		// ������� ���������� ���� � ���������� � log
		Form1->RichEdit1->Lines->Add("Device_" + IntToStr(Index) + ":");
		Form1->RichEdit1->Lines->Add("   |-> path: " + UnicodeString(DevDetail->DevicePath));

		/* ��������� ����������� ����������, ������� ������� ��� � ���������� dwDesiredAccess = 0 */
		DevHandle = CreateFile(	DevDetail->DevicePath,
								0,                                      		/* dwDesiredAccess */
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								(LPSECURITY_ATTRIBUTES)NULL,
								OPEN_EXISTING,
								0,
								NULL
							  );
		if(DevHandle == INVALID_HANDLE_VALUE)	continue;						// ���� ������ �������� �� ������� - ��������� ��������, ��������� � ���������� ����������
		// ��������, ��� ������ ��������
		Form1->RichEdit1->Lines->Add("   |-> ���������� ��������");

		Attributes.Size = sizeof(Attributes);	 								// ���������� ������ ��������� ��� �������� ��������� ����������
		ok = HidD_GetAttributes(DevHandle,&Attributes);							// �������� �������� ����������
		if(!ok) continue; 														// ��������� ������� �������� � ��������� � ���������� ����������
		// ������� � ��� �������� �������
		Form1->RichEdit1->Lines->Add("   |-> VID = " + IntToHex(Attributes.VendorID,4) +
									" & PID = " + IntToHex(Attributes.ProductID,4));

		ok = HidD_GetManufacturerString(DevHandle, Manufacturer, 128);	 		// �������� �������������
		if(!ok)
		{	Form1->RichEdit1->Lines->Add("   |-> Manufacturer: Unavailable");	// ��������, ��� ������������� ����������
			wcscpy(Manufacturer, IntToHex(Attributes.VendorID,4).w_str());		// ����������� ������ ������������� VID
		}
		else	Form1->RichEdit1->Lines->Add("   |-> Manufacturer: " + UnicodeString(Manufacturer));	// ������� � ��� �������������

		ok = HidD_GetProductString(DevHandle, Product, 128);	  				// �������� ��� ��������
		if(!ok)
		{	Form1->RichEdit1->Lines->Add("   |-> Product: Unavailable"); 		// ��������, ��� ��� �������� ����������
			wcscpy(Product, IntToHex(Attributes.ProductID,4).w_str());			// ����������� ������ ����� �������� PID
		}
		else	Form1->RichEdit1->Lines->Add("   |-> Product: " + UnicodeString(Product));	// ������� � ��� ��� ��������

		ok = HidD_GetSerialNumberString(DevHandle, Serial, 128);				// �������� �������� ����� ��������
		if(!ok) Form1->RichEdit1->Lines->Add("   |-> Serial: Unavailable");    				// ��������, ��� ����� ����������
		else	Form1->RichEdit1->Lines->Add("   |-> Serial: " + UnicodeString(Serial));	// �������� �������� ����� ��������

		ok = HidD_GetPreparsedData(DevHandle, &PreparsedData);					// ����������� ���� ������ ��� ��������� ��������
		if(!ok)	continue;														// ��������� ������� �������� � ��������� � ���������� ����������

		ok = HidP_GetCaps(PreparsedData, &Capabilities);   						// �������� ����������� �� ����������� �������
		if(!ok) continue;								   						// ��������� ������� �������� � ��������� � ���������� ����������
		HidD_FreePreparsedData(PreparsedData);									// ����������� �������, ������� ������� ������� ������ HID ��� �������� ����� ������ � ��������
		/* ������� ����������� (USage, UsagePage, ������� �������� IN, OUT, FEATURE) � ���*/
		Form1->RichEdit1->Lines->Add("   |-> Usage = " + IntToHex(Capabilities.Usage,4));
		Form1->RichEdit1->Lines->Add("   |-> Usage Page = " + IntToHex(Capabilities.UsagePage,4));
		Form1->RichEdit1->Lines->Add("   |-> InputReportByteLength = " + IntToHex(Capabilities.InputReportByteLength,4));
		Form1->RichEdit1->Lines->Add("   |-> OutputReportByteLength = " + IntToHex(Capabilities.OutputReportByteLength,4));
		Form1->RichEdit1->Lines->Add("   |-> FeatureReportByteLength = " + IntToHex(Capabilities.FeatureReportByteLength,4));

		/* ���������� � ������ ��������� ���������� (���� � ����������) � �������������� �������� */
		//if((Capabilities.UsagePage == 0xFF00) && (Capabilities.Usage == 0x0001))	// ������������� ���������� �� Usage � UsagePage
		//{   // ��������� ��������� ������ � ComboBox
			// ���� � ������� ������ ��� �� �����, �� �������������, �� ������ ��� �� ��������� VID/PID
			Form1->ComboBox1->Items->Add(UnicodeString(Manufacturer) + UnicodeString(" - ") + UnicodeString(Product));
			DevDetailData[DevCount] = DevDetail;								// ��������� ���� � ����������
			DevDetailDataSz[DevCount] = Sz;										// ��������� ������ ����
			// ������� ���� - ��� ������ report ID, ������� � ��� ������ ����� �������� �� 1 ���� ������, ��� ���������� �������� ����
			DevInputReportSz[DevCount] = Capabilities.InputReportByteLength;	// ��������� ������ ������� IN ����������
			DevOutputReportSz[DevCount] = Capabilities.OutputReportByteLength;	// ��������� ������ ������� OUT ����������
			DevFeatureReportSz[DevCount] = Capabilities.FeatureReportByteLength;// ��������� ������ ������� Feature ����������
			if (DevDetailSelected)												// ���� ���� �����-�� ��������� ������
			{	// ���� ������ ����� (��������� ��������� = 0)
				if (!strcmp(DevDetailSelected->DevicePath, DevDetailData[DevCount]->DevicePath))
				{	DevSelected = DevCount;										// ���������� ������� ������ ��� ���������
					Form1->ComboBox1->ItemIndex = DevCount;						// �������� ��� � ComboBox
				}
			}
			DevCount++;															// ����������� �������
			DevDetail = NULL;													// ���������� ��������� �� ��������� � ���������� �������
		//}
		CloseHandle (DevHandle); 												// ��������� �������� ����� ������
		DevHandle = INVALID_HANDLE_VALUE;										// ���������� ���������
	} while(DevCount < DEV_NUM);												// ������������ ���������� ��������� ����������� � ������

  if (DevDetail)			free(DevDetail);	   								// ������� �����, ���������� ���� � ���������� ���������� ����������
  if (DevDetailSelected)	free(DevDetailSelected);							// ������� �����, � ������� ���������� ���� � ���������� ����������
  SetupDiDestroyDeviceInfoList(DevInfo);										// ������� �����, ���������� ������ HID-���������

  return (DevCount);															// ���������� ���������� ��������� ���������
}
//---------------------------------------------------------------------------
/*
 *  HID Open
 *    Parameters:      num:   Device number (�� ������, ��������������� ��� ������)
 *    Return Value:    TRUE - Success, FALSE - Error
 */
BOOL HID_Open(int Num)
{	ULONG	NumberBuffers;														// ������ ������

	if (DevDetailData[Num] == NULL) return (FALSE);								// ���� � ������ ����� � ����������� ��� ���� � ���������� ������� - ������� � ���������� false

	/* ��������� ���������� ��� ������/������ � ������ ������ ������� */
	DevHandle = CreateFile(	DevDetailData[Num]->DevicePath,
							GENERIC_READ | GENERIC_WRITE,
							FILE_SHARE_READ | FILE_SHARE_WRITE,
							(LPSECURITY_ATTRIBUTES)NULL,
							OPEN_EXISTING,
							FILE_FLAG_OVERLAPPED,
							NULL
						  );
	if(DevHandle == INVALID_HANDLE_VALUE) return (FALSE);	   					// ���� �� ���������� ������� - ������� � ���������� false
	DevSelected = Num;															// ��������� ��������� ������ � �������� ����������
	memset(&DevWriteOverlapped, 0, sizeof(OVERLAPPED));	   						// �������� ������ � ��������� OVERLAPPED ��� ������
	memset(&DevReadOverlapped, 0, sizeof(OVERLAPPED));	   						// �������� ������ � ��������� OVERLAPPED ��� ������
	// ����� ������ �������� ������
	if(HidD_GetNumInputBuffers(DevHandle, &NumberBuffers))						// �������� ������ ������ ������
	{	Form1->RichEdit1->Lines->Add("������ �������� ������: " + IntToStr((int)NumberBuffers));	// ������� � ��� ���������� ��������� ���������
	}
	else
	{	Form1->RichEdit1->Lines->Add("�� ������� ������ ������ �������� ������!");	// ������� � ��� ���������� ��������� ���������
	}
	return (TRUE);
}
//---------------------------------------------------------------------------
/*
 *  HID Close
 *    Parameters:      None
 *    Return Value:    None
 */
void HID_Close()
{	DevSelected = -1;															// �������� ����� ����������
	CancelIo(DevHandle);														// �������� ��� ����������� �������� ��� ����������
	DevReadPending  = FALSE;

	if(DevHandle != INVALID_HANDLE_VALUE)
	{	CloseHandle(DevHandle);
		DevHandle = INVALID_HANDLE_VALUE;
	}
}
//---------------------------------------------------------------------------
/*
 *  HID Write
 *    Parameters:      buf:   Pointer to buffer with data to write
 *                     sz:    Number of bytes to write
 *                     cnt:   Pointer to number of bytes written
 *    Return Value:    TRUE - Success, FALSE - Error
 */

BOOL HID_Write(BYTE *Buf, DWORD Sz, DWORD *Cnt)
{	WriteFile(DevHandle, Buf, Sz, Cnt, &DevWriteOverlapped);					// ���������� ���������� ����� � ����������
	WaitForSingleObject(DevWriteOverlapped.hEvent,2000);        				// ������������� ����� �� 2 � ��� ���� �� ���������� �������� ������ � ����
	if(GetOverlappedResult(DevHandle,&DevWriteOverlapped,Cnt,true))	return true;// ���� �������� ����������� ������� - ������� ������
	else                                                                        // � ��������� ������ -
	{	CancelIo(DevHandle);													// �������� ����������� �������� �����/������
		return false; 															// ���������� ����
	}
}
//---------------------------------------------------------------------------
/*
 *  HID Read
 *    Parameters:      buf:   Pointer to buffer that receives data
 *                     sz:    Number of bytes to read
 *                     cnt:   Pointer to number of bytes read
 *    Return Value:    TRUE - Success, FALSE - Error
 */

BOOL HID_Read(BYTE *Buf, DWORD Sz, DWORD *Cnt)
{	int last_err;																// ���� ����� �������� ��� ������

	if (!DevReadPending)														// ���� ���� �������� ���������� �������� ������ �������
	{	if (ReadFile(DevHandle, Buf, Sz, Cnt, &DevReadOverlapped))				// �������� ���������� ��������� �� ���������� IN ������
		{	return (true);														// ���� ���������� - �������, ���������� true, Cnt = ���������� ����������� ����
		}
		DevReadPending = true;													// ������������� ���� �������� ���������� �������� ������
	}
	else																		// ���� ���� �������� ���������� �������� ������ ����������
	{	if (GetOverlappedResult(DevHandle, &DevReadOverlapped, Cnt, false))		// ���������, ����������� �� �������� ������
		{	DevReadPending  = false;											// ���������� ���� �������� ���������� �������� ������
																				// Cnt �������� ���������� �������� ����
			return (true);														// �������, ���������� true
		}
	}
	// ���� �������� ���� ���� ������ ��� ���������� ���� ��������, ���� ���� ���� ���������� �����,
	// �� �������� ��������, ��� �������� ������ ������ ��� �� ����������� (������ ��� �� �������)
	last_err = GetLastError();													// ����� ��� ��������� ������
	if((last_err != ERROR_IO_INCOMPLETE) && (last_err != ERROR_IO_PENDING))		// ���� ������ ����������� �� � ���, ��� ������ ������ �� ����� �� �������
	{	DevReadPending  = false;												// ���������� ���� �������� ���������� �������� ������
		CancelIo(DevHandle);													// �������� ��� ����������� �������� �����/������ (�� ������ �� ���������)
		return (false);															// �������, ���������� false
	}
	return (true);																// ���� ������  ������ ��� �� ������� �� ����� - �������, ���������� true
}
//---------------------------------------------------------------------------
//--- ����������� ��� ������� ����������� ������ ----------------------------
void OnInput()
{	UnicodeString	ComposedStr;												// ����� ����� ����������� ������ ������

	ComposedStr = "";															// ������� ������ ������
	for(int i=1; i<InReportSz; i++)												// ���������� ��� �������� ����� �������
	{	ComposedStr = ComposedStr + " " + IntToHex(InReport[i],2);				// ����������� ��������� ���� � ��������� ������������� hex
	}
	Form1->RichEdit1->Lines->Add("Recieved data (input):" + ComposedStr);		//  ������� �������������� ������ � ���
}
//---------------------------------------------------------------------------
//--- ����������� ��� ������ �� ����� ������ / ������ ������ ----------------
void OnError()
{	Running = FALSE;															// ���������� ���� ��������� ����������
	Form1->Timer1->Enabled = false;												// ��������� ������
	Form1->RichEdit1->Lines->Add("������������� ����� ����������");	  			// �������� ��� ������������� ����� ���������� (������ �� ����������)
	HID_Close();																// ��������� �������� ����������
	Form1->RichEdit1->Lines->Add("���������� �������");							// �������� ��� ���������� �������
}
//---------------------------------------------------------------------------
//***************************************************************************
//--- FORM FUNCTIONS --------------------------------------------------------
//--- ����������� ����� -----------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{   Form1->AutoSize = true;	 													// ������������� ���������� �����
	Form1->Panel1->AutoSize = true;	  											// ������������� ���������� ����� � � ������� ������
	// ������������� USB
	Running       = FALSE;														// ��� ������������ ���������
	InReport      = NULL;														// ��������� �� ������ ��� �����/�������� ��������
	OutReport     = NULL;
	FeatureReport = NULL;
	HID_Init();																	// �������������� ���������� � ��������� ��� ������ HID-���������
	Form1->RichEdit1->Lines->Add("����������� ����� HID-���������...");			// �������� � ������ ������ HID-���������
	DevCount = HID_FindDevices();   	                                        // ��������� ����� HID-���������
	Form1->RichEdit1->Lines->Add("������� ���������: "+IntToStr(DevCount));		// ������� � ��� ���������� ��������� ���������
}
//---------------------------------------------------------------------------
//--- ���� -> exit ----------------------------------------------------------
void __fastcall TForm1::Exit1Click(TObject *Sender)
{   if (Running)
	{	Running = FALSE;														// ������� ���� ������� ������������� ����������
		HID_Close();															// �������� ����������� �������� I/O � ��������� ����������
	}
	if (InReport)      free(InReport);											// ������� ������ ��������
	if (OutReport)     free(OutReport);
	if (FeatureReport) free(FeatureReport);
	HID_UnInit();																// ������� ���������, ���������� ���� � �����������
	Close();																	// ��������� ����������
}
//---------------------------------------------------------------------------
//--- �������� ��������� ������ ---------------------------------------------
void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{	Form1->RichEdit1->Lines->Add("������� ����������: "+IntToStr(Form1->ComboBox1->ItemIndex));
}
//---------------------------------------------------------------------------
//--- ������ "Clear log" ----------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{	Form1->RichEdit1->Clear();													// ������� ���
}
//---------------------------------------------------------------------------
//--- ������ Find USB HID Devices -------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{   Form1->RichEdit1->Lines->Add(""); 											// ������� �������
	Form1->RichEdit1->Lines->Add("����������� ����� HID-���������...");			// �������� � ������ ������ HID-���������
	DevCount = HID_FindDevices();												// ��������� ����� HID-���������
	Form1->RichEdit1->Lines->Add("������� ���������: "+IntToStr(DevCount));		// ������� � ��� ���������� ��������� ���������
}
//---------------------------------------------------------------------------
//--- ������ Connect --------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{	if(Running)																	// ���� ���� �������� ������ ��������, ��� ��� ����� �������
	{   Form1->RichEdit1->Lines->Add("������� ����� ������� �������� ����������");
	}
	else																		// ���� ��������� ������� ���
	{	if(Form1->ComboBox1->ItemIndex != -1)									// ���� ���-�� �������
		{   // ������� ������� ��������� ����������
			DevSelected = Form1->ComboBox1->ItemIndex;							// �������� ������ ���������� � ������
			if (HID_Open(Form1->ComboBox1->ItemIndex))							// ���� ���������
			{   // ������� � ��� ���� � ��������� ����������
				Form1->RichEdit1->Lines->Add("������� ���������� "+UnicodeString(DevDetailData[DevSelected]->DevicePath));
				// ��������� ������� �������� �� ����������� ��� ������ ��������
				InReportSz		= DevInputReportSz[DevSelected];
				OutReportSz     = DevOutputReportSz[DevSelected];
				FeatureReportSz = DevFeatureReportSz[DevSelected];
				// ������� ������ ��������
				if (InReport)      free(InReport);
				if (OutReport)     free(OutReport);
				if (FeatureReport) free(FeatureReport);
				// ������� ������ ��� ����� ������ �������� � ������������ � ���������� ���������
				InReport		= (BYTE *)calloc(InReportSz, 1);
				OutReport       = (BYTE *)calloc(OutReportSz, 1);
				FeatureReport   = (BYTE *)calloc(FeatureReportSz, 1);
				Running          = true;										// ������������� ���� ������� ��������� ����������
			}
			else
			{	// ��������, ��� �� ������� ������� ������ ����������
				Form1->RichEdit1->Lines->Add("�� ������� ������� ���������� "+UnicodeString(DevDetailData[DevSelected]->DevicePath));
				DevSelected = -1;												// ���������� ������
			}
		}
		else                                                                    // ���� ������ �� �������
		{	Form1->RichEdit1->Lines->Add("������� ����� ������� ����������");	// �������� ��� ������� ����� �������
		}
	}
}
//---------------------------------------------------------------------------
//--- ������ Disconnect -----------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{	if(Running)																	// ���� ���� �������� ������ - ��������� ���
	{	Running = false;														// ���������� ���� ������� ��������� ����������
		Form1->Timer1->Enabled = false;											// ��������� ������
		Form1->RichEdit1->Lines->Add("������������� ����� ����������");	  		// �������� ��� ������������� ����� ���������� (������ �� ����������)
		HID_Close();                                                            // ��������� �������� ����������
		Form1->RichEdit1->Lines->Add("���������� �������");						// �������� ��� ���������� �������
	}
	else
	{	Form1->RichEdit1->Lines->Add("��� ������������ ���������");				// ��������, ��� ��� �������� ���������
    }
}
//---------------------------------------------------------------------------
//--- ���� (����� ������) � ���� ����� ������ ��� �������� ------------------
void __fastcall TForm1::Edit1KeyPress(TObject *Sender, wchar_t &Key)
{	wchar_t *CompetentChars=L"0123456789abcdefABCDEF";	   						// ����� ���������� �������� (UTF)
	wchar_t AddStrWChar[2]={0,0};  												// ��� �������������� ������� � ������-������
	UnicodeString	SourceStrBegin;												// �������� ������ �� ������ ���������
	UnicodeString	SourceStrEnd;												// �������� ������ ����� ����� ���������
	UnicodeString	AddStrUC;													// ���������� ������ (�� ������ �������)
	int				SelStartPos;												// ������� ������ ���������
	int				SelEndPos;													// ������� ����� ���������
	int				EndPartSize;												// ������ ����� ����� ���������
	int				SpaceIndex;													// ������ ������� � ������
	int				SpaceNumber;												// ���������� ����������� ��������
	int				SpaceCounter;												// ������� ��������

	SelStartPos = Form1->Edit1->SelStart;										// ���������� ������ ��������� (�������)

	if(wcschr(CompetentChars, Key) != 0)									 	// ���� ������ ������� �� ������
	{   SelEndPos = SelStartPos + Form1->Edit1->SelLength;						// ���������� ����� ���������
		EndPartSize = (Form1->Edit1->Text).Length() - SelEndPos;   				// ��������� ������ ����� ����� ���������

		AddStrWChar[0] = Key;  					 								// ��������� Key ��� ��������������
		AddStrUC = UnicodeString(AddStrWChar);									// ����������� Key � ������-������
		if(SelStartPos%3 == 2)        	                	                    // ���� �� � ������� ����� ��������, ��
		{	AddStrUC = L" "+AddStrUC; 											// ��������� ������ ����� �������� ��������
		}

		SourceStrBegin = (Form1->Edit1->Text).SubString(1,SelStartPos);			// �������� ������ �������� ������ (�� ���������)
		SourceStrEnd = (Form1->Edit1->Text).SubString(SelEndPos+1,EndPartSize);	// �������� ����� �������� ������ (����� ���������)

		// ������� ��� ������� �� ����� ������
		while((SpaceIndex = SourceStrEnd.Pos(L" "))!=0)				  			// ���������, ���� ������� ���� (������ �� ����� ����)
		{	SourceStrEnd = SourceStrEnd.Delete(SpaceIndex,1);		  			// ������� ��������� ������
		}

		SpaceIndex = 1 + 2 - (SelStartPos + AddStrUC.Length())%3;  			   	// ��������� ������� �� ������� �� 3 ����� ������� ������� � � ����������� �� �����
																				// ����������, ����� ��������� � ����� �������� ����� ��������� ������� � ����� ������
																				// 1 ������� �������� ��-�� ����, ��� ��� � ������ Insert ���������� �� ������� �������,
																				// � ������ ����� �������� ������, � ������� �������, ����� ������� ����� �������� ������
		// ������ ����� ���������� ������� � ����� ������ ������
		SpaceNumber = (SourceStrEnd.Length()+1)/2;								// ���������� ����������� �������� = ����� ����� �� (SourceStrEnd+1) / 2
		SpaceCounter = 0;														// ���������� ������� ����������� ��������
		while(SpaceCounter<SpaceNumber)											// ���������, ���� �� ������� ������ ���������� ��������
		{	SourceStrEnd.Insert(L" ",SpaceIndex+SpaceCounter*3);				// ��������� ��������� ������ � ������ �������
			SpaceCounter++;
		}
		SourceStrEnd = SourceStrEnd.TrimRight();								// ������� ������ ������ � �����, ���� �� ��������

		Form1->Edit1->Text = (SourceStrBegin + AddStrUC + SourceStrEnd).UpperCase();	// ��������� ����� ������ � ��������� � ������ ������
		// ���������� ������ �� �����
		Form1->Edit1->SelStart = SelStartPos + AddStrUC.Length(); 				// ���������� ������ �� ����� �����, � ����������� �� ����, ������� �� ��������� ��������
		Key = 0;																// ��� �������� ������ ������ �� �����
	}
	else	Key = 0; 															// �������� ����
}
//---------------------------------------------------------------------------
//--- ���� � ���� ����� ������ ��� �������� (backspase, del � insert) -------
//--- � ���� ����������� ���� �� ������� �� ��������� ������ ----------------
//--- ������, ���� ������ a(�) ��� ���������� ������� - ���� �������� 0x41, -
//--- � � Edit1KeyPressed - 0x444 -------------------------------------------
void __fastcall TForm1::Edit1KeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{   UnicodeString	SourceStrBegin;												// �������� ������ �� ������ ���������
	UnicodeString	SourceStrEnd;												// �������� ������ ����� ����� ���������
	int				SelStartPos;												// ������� ������ ���������
	int				SelEndPos;													// ������� ����� ���������
	int				EndPartSize;												// ������ ����� ����� ���������
	int				SpaceIndex;													// ������ ������� � ������
	int				SpaceNumber;												// ���������� ����������� ��������
	int				SpaceCounter;												// ������� ��������

	switch(Key)
	{   case VK_BACK:
            SelStartPos = Form1->Edit1->SelStart;						   		// ���������� ������ ��������� (�������)
			SelEndPos = SelStartPos + Form1->Edit1->SelLength;			   		// ���������� ����� ���������
			EndPartSize = (Form1->Edit1->Text).Length() - SelEndPos;	   		// ��������� ������ ����� ����� ���������

			if(SelStartPos != 0 && SelStartPos == SelEndPos)					// ���� ������ �� �������� � ������� ����� ��������� �� ����� ������� �������
			{	SelStartPos -=1;   						 						// ������� ������ ����� start
			}

			SourceStrBegin = (Form1->Edit1->Text).SubString(1,SelStartPos);		// �������� ������ �������� ������ (�� ���������)
			SourceStrEnd = (Form1->Edit1->Text).SubString(SelEndPos+1,EndPartSize);	// �������� ����� �������� ������ (����� ���������)

			// ������� ��� ������� �� ����� ������
			while((SpaceIndex = SourceStrEnd.Pos(L" "))!=0)			  			// ���������, ���� ������� ���� (������ �� ����� ����)
			{	SourceStrEnd = SourceStrEnd.Delete(SpaceIndex,1);	  			// ������� ��������� ������
			}

			SpaceIndex = 1 + 2 - (SelStartPos)%3;  							   	// ��������� ������� �� ������� �� 3 ����� ������� ������� � � ����������� �� �����
																				// ����������, ����� ��������� � ����� �������� ����� ��������� ������� � ����� ������
																				// 1 ������� �������� ��-�� ����, ��� ��� � ������ Insert ���������� �� ������� �������,
																				// � ������ ����� �������� ������, � ������� �������, ����� ������� ����� �������� ������
			// ������ ����� ���������� ������� � ����� ������ ������
			SpaceNumber = (SourceStrEnd.Length()+1)/2;		  					// ���������� ����������� �������� = ����� ����� �� (SourceStrEnd+1) / 2
			SpaceCounter = 0;											 		// ���������� ������� ����������� ��������
			while(SpaceCounter<SpaceNumber)								 		// ���������, ���� �� ������� ������ ���������� ��������
			{	SourceStrEnd.Insert(L" ",SpaceIndex+SpaceCounter*3);	 		// ��������� ��������� ������ � ������ �������
				SpaceCounter++;
			}
			SourceStrEnd = SourceStrEnd.TrimRight();					 		// ������� ������ ������ � �����, ���� �� ��������

			Form1->Edit1->Text = (SourceStrBegin + SourceStrEnd).UpperCase();	// ��������� ����� ������ � ��������� � ������ ������
			// ���������� ������ �� �����
			Form1->Edit1->SelStart = SelStartPos; 							  	// ���������� ������ �� ����� �����, � ����������� �� ����, ������� �� ��������� ��������
			Key = 0;														  	// ��� �������� ������ ������ �� �����
			break;
		case VK_DELETE:                                                         // ���������� ��� delete
			SelStartPos = Form1->Edit1->SelStart;						   		// ���������� ������ ��������� (�������)
			SelEndPos = SelStartPos + Form1->Edit1->SelLength;			   		// ���������� ����� ���������
			EndPartSize = (Form1->Edit1->Text).Length() - SelEndPos;	   		// ��������� ������ ����� ����� ���������

			if(SelEndPos != Form1->Edit1->Text.Length() && SelStartPos == SelEndPos)	// ���� ������ �� �������� � ������� ����� ��������� �� ����� ��������� �������
			{	SelEndPos +=1;							 						// ������� ������ ����� del
			}

			SourceStrBegin = (Form1->Edit1->Text).SubString(1,SelStartPos);		// �������� ������ �������� ������ (�� ���������)
			SourceStrEnd = (Form1->Edit1->Text).SubString(SelEndPos+1,EndPartSize);	// �������� ����� �������� ������ (����� ���������)

			// ������� ��� ������� �� ����� ������
			while((SpaceIndex = SourceStrEnd.Pos(L" "))!=0)			  			// ���������, ���� ������� ���� (������ �� ����� ����)
			{	SourceStrEnd = SourceStrEnd.Delete(SpaceIndex,1);	  			// ������� ��������� ������
			}

			SpaceIndex = 1 + 2 - (SelStartPos)%3;  							   	// ��������� ������� �� ������� �� 3 ����� ������� ������� � � ����������� �� �����
																				// ����������, ����� ��������� � ����� �������� ����� ��������� ������� � ����� ������
																				// 1 ������� �������� ��-�� ����, ��� ��� � ������ Insert ���������� �� ������� �������,
																				// � ������ ����� �������� ������, � ������� �������, ����� ������� ����� �������� ������
			// ������ ����� ���������� ������� � ����� ������ ������
			SpaceNumber = (SourceStrEnd.Length()+1)/2;		  					// ���������� ����������� �������� = ����� ����� �� (SourceStrEnd+1) / 2
			SpaceCounter = 0;											 		// ���������� ������� ����������� ��������
			while(SpaceCounter<SpaceNumber)								 		// ���������, ���� �� ������� ������ ���������� ��������
			{	SourceStrEnd.Insert(L" ",SpaceIndex+SpaceCounter*3);	 		// ��������� ��������� ������ � ������ �������
				SpaceCounter++;
			}
			SourceStrEnd = SourceStrEnd.TrimRight();					 		// ������� ������ ������ � �����, ���� �� ��������

			Form1->Edit1->Text = (SourceStrBegin + SourceStrEnd).UpperCase();	// ��������� ����� ������ � ��������� � ������ ������
			// ���������� ������ �� �����
			Form1->Edit1->SelStart = SelStartPos; 							  	// ���������� ������ �� ����� �����, � ����������� �� ����, ������� �� ��������� ��������
			Key = 0;														  	// ��� �������� ������ ������ �� �����
			break;
		case VK_INSERT:
			Key = 0;															// �������� insert
			break;
		default:
			break;
	}
}
//---------------------------------------------------------------------------
//--- ������ Send -----------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{	DWORD 			ByteCnt;   													// ������� ���������� ����
	AnsiString		SendStr;													// ���� ������ (� ANSI, ����� �� 1 ����� �� ������)
	AnsiString		HexAnsiStr;													// ����� � ��������� Hex-������
	int				SpaceIndex;													// ������ ������� � ������
	int				RemAfterDiv;												// �������� �� ������� ���������� ���� ��� �������� �� ���������� �������� ����, ���������� �� 1 ���
	int				PacketsCounter;												// ������� ������������ �������
	int				PacketsNum;													// ����� ���������� �������, ������� ����� ���������
	char 			*HexStr;													// ����� ����� ������� ������ � hex
	char			BinBuf[2];													// ����� �������� ��������� �������������� HexToBin
	UnicodeString	ComposedStr;												// ����� ����� ����������� ������ ������

	if(Running)
	{	SendStr = AnsiString(Form1->Edit1->Text);  								// �������� ����� � ���� Ansi-������
		// ������� ��� �������
		while((SpaceIndex = SendStr.Pos(" "))!=0)		 			  			// ���������, ���� ������� ���� (������ �� ����� ����)
		{	SendStr = SendStr.Delete(SpaceIndex,1);		 			  			// ������� ��������� ������
		}
		RemAfterDiv = SendStr.Length()%(OutReportSz - 1);		   				// �����, ������ �� ������ ������� ���������� �������� ���� � ����� OUT-�������
		PacketsNum = SendStr.Length()/(2*(OutReportSz - 1));	   				// ����� ���������� ������� ��� �������� (�� 2 �����, ��������� 1 ���� ���������� ����� ���������)
		if(RemAfterDiv!=0)										   				// ���� ������ �� ������ 2*(������ ������� - 1)
		{	Form1->RichEdit1->Lines->Add("���������� ������ ��� �������� �� ������ ���������� �������� ������ ������� OUT.");
			Form1->RichEdit1->Lines->Add("�������� ������ ����� �������, ��������� ������ �� ����� ����������");	// ��������, ��� ������� ��������� ������
		}
		// ���������� ���� ��� ��������
		PacketsCounter = 0;					 									// ���������� ������� ������������ �������
		while(PacketsCounter < PacketsNum)	 									// ���� �� �������� ��� ������
		{	OutReport[0] = 0;			 	 									// report ID (������� ���� � ������� - ��� ������ report ID)
			for(int i=1; i<OutReportSz; i++) 									// ��������� ����� ���������� OUT-�������
			{   HexAnsiStr = SendStr.SubString(PacketsCounter*(OutReportSz-1)*2+(i-1)*2+1,2);	// �������� ��������� ������� ������ � hex-������
				HexStr = HexAnsiStr.c_str(); 									// ����������� � char*
				HexToBin(HexStr,BinBuf,1);	 									// ����������� ������ char* � �����
				OutReport[i] = BinBuf[0];	 									// ��������� ��������� ������ ��� ��������
			}
			if(HID_Write(OutReport, OutReportSz, &ByteCnt))	 	   				// ���� ������ ������� ����������
			{	ComposedStr = "";	  											// ������� ������ ������
				for(int i=1; i<OutReportSz; i++)   								// ���������� ��� �������� ����� �������
				{	ComposedStr = ComposedStr + " " + IntToHex(OutReport[i],2);	// ����������� ��������� ���� � ��������� ������������� hex
				}
				Form1->RichEdit1->Lines->Add("��������� ����� �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� ��������� �����
				Form1->RichEdit1->Lines->Add("Sent data:" + ComposedStr);		//  ������� �������������� ������ � ���
			}
			else
			{   Form1->RichEdit1->Lines->Add("�� ������� ��������� ����� �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� �� ��������� �����
				OnError();														// ��������� ������� OnError
				return;		 													// ��������� ��������
			}
			PacketsCounter++;
		}
	}
	else
	{    Form1->RichEdit1->Lines->Add("��� ������������ ���������");			// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- ������� �� ������� �������� ������ ������ �� ���������� (������ 1 ���)
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{	DWORD Cnt;																	// ���� ����� �������� ���������� �������� ����

	if(Running)																	// ���� ���������� ����������
	{	if(HID_Read(InReport, InReportSz, &Cnt))								// ���� ������ �� ����������� �������
		{	if(Cnt)	OnInput();		                                            // ���� ���� �������� ������ - ��������� OnInput
		}
		else																	// ���� ������ ����������� �������
		{	OnError();															// ��������� ������� OnError
		}
	}
	else
	{	Form1->RichEdit1->Lines->Add("��� ������������ ���������");				// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- ��������� ������ ��� �������������� ������ ������ �� ������ -----------
void __fastcall TForm1::Button6Click(TObject *Sender)
{   if(Running)
	{	Form1->Timer1->Enabled = true;											// �������� ������ (������������� ������)
		Form1->RichEdit1->Lines->Add("������������� ����� �������");	   		// �������� ��� ������� ������������� ����� (������ �� ����������)
	}
	else
	{   Form1->RichEdit1->Lines->Add("��� ������������ ���������");				// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- ������������� ������ ��� �������������� ������ ������ �� ������ -------
void __fastcall TForm1::Button7Click(TObject *Sender)
{	Form1->Timer1->Enabled = false;												// ��������� ������
	Form1->RichEdit1->Lines->Add("������������� ����� ����������");	  			// �������� ��� ������������� ����� ���������� (������ �� ����������)
}
//---------------------------------------------------------------------------
//--- ������ � ������� ������� HidD_GetInputReport (� ����� ������ ��������)
void __fastcall TForm1::Button9Click(TObject *Sender)
{	if(Running)																	// ���� ���������� ����������
	{	if(HidD_GetInputReport(DevHandle, InReport, InReportSz))		 		// ���� ������ �� ����������� �������
		{	OnInput();				                                            // ���� ���� �������� ������ - ��������� OnInput
		}
		else																	// ���� ������ ����������� �������
		{	OnError();															// ��������� ������� OnError
		}
	}
	else
	{	Form1->RichEdit1->Lines->Add("��� ������������ ���������");				// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- �������� � ������� ������� HidD_SetOuputReport ------------------------
void __fastcall TForm1::Button10Click(TObject *Sender)
{   DWORD 			ByteCnt;   													// ������� ���������� ����
	AnsiString		SendStr;													// ���� ������ (� ANSI, ����� �� 1 ����� �� ������)
	AnsiString		HexAnsiStr;													// ����� � ��������� Hex-������
	int				SpaceIndex;													// ������ ������� � ������
	int				RemAfterDiv;												// �������� �� ������� ���������� ���� ��� �������� �� ���������� �������� ����, ���������� �� 1 ���
	int				PacketsCounter;												// ������� ������������ �������
	int				PacketsNum;													// ����� ���������� �������, ������� ����� ���������
	char 			*HexStr;													// ����� ����� ������� ������ � hex
	char			BinBuf[2];													// ����� �������� ��������� �������������� HexToBin
	UnicodeString	ComposedStr;												// ����� ����� ����������� ������ ������

	if(Running)
	{	SendStr = AnsiString(Form1->Edit1->Text);  								// �������� ����� � ���� Ansi-������
		// ������� ��� �������
		while((SpaceIndex = SendStr.Pos(" "))!=0)		 			  			// ���������, ���� ������� ���� (������ �� ����� ����)
		{	SendStr = SendStr.Delete(SpaceIndex,1);		 			  			// ������� ��������� ������
		}
		RemAfterDiv = SendStr.Length()%(OutReportSz - 1);		   				// �����, ������ �� ������ ������� ���������� �������� ���� � ����� OUT-�������
		PacketsNum = SendStr.Length()/(2*(OutReportSz - 1));	   				// ����� ���������� ������� ��� �������� (�� 2 �����, ��������� 1 ���� ���������� ����� ���������)
		if(RemAfterDiv!=0)										   				// ���� ������ �� ������ 2*(������ ������� - 1)
		{	Form1->RichEdit1->Lines->Add("���������� ������ ��� �������� �� ������ ���������� �������� ������ ������� OUT.");
			Form1->RichEdit1->Lines->Add("�������� ������ ����� �������, ��������� ������ �� ����� ����������");	// ��������, ��� ������� ��������� ������
		}
		// ���������� ���� ��� ��������
		PacketsCounter = 0;					 									// ���������� ������� ������������ �������
		while(PacketsCounter < PacketsNum)	 									// ���� �� �������� ��� ������
		{	OutReport[0] = 0;			 	 									// report ID (������� ���� � ������� - ��� ������ report ID)
			for(int i=1; i<OutReportSz; i++) 									// ��������� ����� ���������� OUT-�������
			{   HexAnsiStr = SendStr.SubString(PacketsCounter*(OutReportSz-1)*2+(i-1)*2+1,2);	// �������� ��������� ������� ������ � hex-������
				HexStr = HexAnsiStr.c_str(); 									// ����������� � char*
				HexToBin(HexStr,BinBuf,1);	 									// ����������� ������ char* � �����
				OutReport[i] = BinBuf[0];	 									// ��������� ��������� ������ ��� ��������
			}
			if(HidD_SetOutputReport(DevHandle, OutReport, OutReportSz))	 		// ���� ������ �� ����������� �������
			{	ComposedStr = "";	  											// ������� ������ ������
				for(int i=1; i<OutReportSz; i++)   								// ���������� ��� �������� ����� �������
				{	ComposedStr = ComposedStr + " " + IntToHex(OutReport[i],2);	// ����������� ��������� ���� � ��������� ������������� hex
				}
				Form1->RichEdit1->Lines->Add("��������� ����� �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� ��������� �����
				Form1->RichEdit1->Lines->Add("Sent data:" + ComposedStr);		//  ������� �������������� ������ � ���
			}
			else
			{   Form1->RichEdit1->Lines->Add("�� ������� ��������� ����� �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� �� ��������� �����
				OnError();														// ��������� ������� OnError
				return;		 													// ��������� ��������
			}
			PacketsCounter++;
		}
	}
	else
	{    Form1->RichEdit1->Lines->Add("��� ������������ ���������");			// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- ������ Feature-������ �� ���������� -----------------------------------
void __fastcall TForm1::Button11Click(TObject *Sender)
{   UnicodeString	ComposedStr;												// ����� ����� ����������� ������ ������

	if(Running)																	// ���� ���������� ����������
	{	if(HidD_GetFeature(DevHandle, FeatureReport, FeatureReportSz)) 	 		// ���� ������ �� ����������� �������
		{	ComposedStr = "";													// ������� ������ ������
			for(int i=1; i<FeatureReportSz; i++)								// ���������� ��� �������� ����� �������
			{	ComposedStr = ComposedStr + " " + IntToHex(FeatureReport[i],2);	// ����������� ��������� ���� � ��������� ������������� hex
			}
			Form1->RichEdit1->Lines->Add("Recieved data (feature):" + ComposedStr);	//  ������� �������������� ������ � ���
		}
		else																	// ���� ������ ����������� �������
		{	OnError();															// ��������� ������� OnError
		}
	}
	else
	{	Form1->RichEdit1->Lines->Add("��� ������������ ���������");				// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------
//--- ���������� Feature-������ ---------------------------------------------
void __fastcall TForm1::Button8Click(TObject *Sender)
{	DWORD 			ByteCnt;   													// ������� ���������� ����
	AnsiString		SendStr;													// ���� ������ (� ANSI, ����� �� 1 ����� �� ������)
	AnsiString		HexAnsiStr;													// ����� � ��������� Hex-������
	int				SpaceIndex;													// ������ ������� � ������
	int				RemAfterDiv;												// �������� �� ������� ���������� ���� ��� �������� �� ���������� �������� ����, ���������� �� 1 ���
	int				PacketsCounter;												// ������� ������������ �������
	int				PacketsNum;													// ����� ���������� �������, ������� ����� ���������
	char 			*HexStr;													// ����� ����� ������� ������ � hex
	char			BinBuf[2];													// ����� �������� ��������� �������������� HexToBin
	UnicodeString	ComposedStr;												// ����� ����� ����������� ������ ������

	if(Running)
	{	SendStr = AnsiString(Form1->Edit1->Text);  								// �������� ����� � ���� Ansi-������
		// ������� ��� �������
		while((SpaceIndex = SendStr.Pos(" "))!=0)		 			  			// ���������, ���� ������� ���� (������ �� ����� ����)
		{	SendStr = SendStr.Delete(SpaceIndex,1);		 			  			// ������� ��������� ������
		}
		RemAfterDiv = SendStr.Length()%(FeatureReportSz - 1); 	   				// �����, ������ �� ������ ������� ���������� �������� ���� � ����� OUT-�������
		PacketsNum = SendStr.Length()/(2*(FeatureReportSz - 1));   				// ����� ���������� ������� ��� �������� (�� 2 �����, ��������� 1 ���� ���������� ����� ���������)
		if(RemAfterDiv!=0)										   				// ���� ������ �� ������ 2*(������ ������� - 1)
		{	Form1->RichEdit1->Lines->Add("���������� ������ ��� �������� �� ������ ���������� �������� ������ ������� Feature.");
			Form1->RichEdit1->Lines->Add("�������� ������ ����� �������, ��������� ������ �� ����� ����������");	// ��������, ��� ������� ��������� ������
		}
		// ���������� ���� ��� ��������
		PacketsCounter = 0;					 									// ���������� ������� ������������ �������
		while(PacketsCounter < PacketsNum)	 									// ���� �� �������� ��� ������
		{	FeatureReport[0] = 0;			  									// report ID (������� ���� � ������� - ��� ������ report ID)
			for(int i=1; i<FeatureReportSz; i++) 								// ��������� ����� ���������� Feature-�������
			{   HexAnsiStr = SendStr.SubString(PacketsCounter*(FeatureReportSz-1)*2+(i-1)*2+1,2);	// �������� ��������� ������� ������ � hex-������
				HexStr = HexAnsiStr.c_str(); 									// ����������� � char*
				HexToBin(HexStr,BinBuf,1);	 									// ����������� ������ char* � �����
				FeatureReport[i] = BinBuf[0];									// ��������� ��������� ������ ��� ��������
			}
			if(HidD_SetFeature(DevHandle, FeatureReport, FeatureReportSz)) 		// ���� ������ �� ����������� �������
			{	ComposedStr = "";	  											// ������� ������ ������
				for(int i=1; i<FeatureReportSz; i++)   	  						// ���������� ��� �������� ����� �������
				{	ComposedStr = ComposedStr + " " + IntToHex(FeatureReport[i],2);	// ����������� ��������� ���� � ��������� ������������� hex
				}
				Form1->RichEdit1->Lines->Add("��������� ����� (feature) �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� ��������� �����
				Form1->RichEdit1->Lines->Add("Sent data (feature):" + ComposedStr);	//  ������� �������������� ������ � ���
			}
			else
			{   Form1->RichEdit1->Lines->Add("�� ������� ��������� ����� �" + IntToHex(PacketsCounter,2));	// ��������, ��� �� �� ��������� �����
				OnError();														// ��������� ������� OnError
				return;		 													// ��������� ��������
			}
			PacketsCounter++;
		}
	}
	else
	{    Form1->RichEdit1->Lines->Add("��� ������������ ���������");			// ��������, ��� ��� �������� ���������
	}
}
//---------------------------------------------------------------------------

