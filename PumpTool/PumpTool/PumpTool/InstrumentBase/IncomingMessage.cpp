// IncomingMessage.cpp: implementation of the CGIncomingMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "Stend.h"
#include "IncomingMessage.h"
#include "DataChannel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGIncomingMessage::CGIncomingMessage()
{
	m_CurrentPosition = m_Buffer+2;
	m_Size = 0;
}

CGIncomingMessage::~CGIncomingMessage()
{

}

bool CGIncomingMessage::GetFromChannel(CGDataChannel& channel)
{
	m_CurrentPosition = m_Buffer+2;
	m_Size = 0;
	return channel.GetReceivedData(m_Buffer, c_MaxInBufferSize, &m_Size);
}

