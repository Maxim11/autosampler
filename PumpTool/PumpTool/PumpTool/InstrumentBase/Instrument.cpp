// Instrument.cpp: implementation of the CGInstrument class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Instrument.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGInstrument::CGInstrument()
{
	m_DataChannel = 0;

	m_MeasurementState = c_PassiveState;
	m_InstrumentState = 0;
	m_ErrorCode = 0;

	m_InstrumentType = c_InstrumentTypeUnknown;
	m_InstrumentModel = c_InstrumentModelUnknown;

	m_CurrentTime = 0;
	m_StartMeasurementTime = 0;
	m_StartStepTime = 0;
	m_SavedStartMeasurementTime = 0;
	
	m_MeasurementFrequency = 0;
	m_InternalTimeUnitToSec = 0.0;
}

CGInstrument::~CGInstrument()
{

}

void CGInstrument::Create(CGDataChannel* dataChannel)
{
	m_DataChannel = dataChannel;

// ���������, ��� ��� ������� ��������� ���������� ����������� ��������� �������
	ASSERT(m_InternalTimeUnitToSec != 0.0);
}

// ���������� ���������� ��������� ��� ������
void CGInstrument::Initialize()
{
	m_ErrorCode = 0;
// �������� ��������� ���������� m_InstrumentState �� �������!
}

void CGInstrument::ResetInstrumentState()
{
	m_InstrumentState = 0;
}

void CGInstrument::ResetError()
{
	m_ErrorCode = 0;
// �������� ��������� ��������� ������ �����������!
}
/////////////////////////////////////////////////////////////////

// ���������� ����� ������� � ��������� ��������
int CGInstrument::GetCurrentTime()
{
	return m_CurrentTime;
}

// ����� ��������� ��� ������������� (� ��������� ��������)
int CGInstrument::GetMeasurementTime()
{
// ����� ������ ��������� ������� �� ������� ������� ��������� � �����������
// (������ � ������� � ��������� �����) ����� ������ ���������
	if(m_StartMeasurementTime == 0)
		return 0;

	return m_CurrentTime-m_StartMeasurementTime;
}

// ����� ��������� ��� ������������� (� ��������)
double CGInstrument::GetMeasurementTimeInSecs()
{
	if(m_StartMeasurementTime == 0)
		return 0.0;

	return double(m_CurrentTime-m_StartMeasurementTime)*m_InternalTimeUnitToSec;
}

