// OutgoingMessage.cpp: implementation of the CGOutgoingMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OutgoingMessage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CGOutgoingMessage::CGOutgoingMessage()
{
	m_CurrentPosition = m_Buffer;
}

// ����������� ����������� ���������
CGOutgoingMessage::CGOutgoingMessage(BYTE netAddress, BYTE processID, BYTE attribute, BYTE messageID)
{
	m_Buffer[0] = netAddress;
	m_Buffer[1] = processID;
	m_Buffer[2] = attribute;
	m_Buffer[3] = messageID;
	
	m_CurrentPosition = m_Buffer+4;
}

// ���������/��������� ������� ��������� messageID
CGOutgoingMessage::CGOutgoingMessage(BYTE netAddress, BYTE messageID, BYTE enable)
{
	m_Buffer[0] = netAddress;
	m_Buffer[1] = messageID;
	m_Buffer[2] = enable;
	
	m_CurrentPosition = m_Buffer+3;
}

// ����������� ��������� ��� ������ ������� (��� ������)
CGOutgoingMessage::CGOutgoingMessage(BYTE netAddress, BYTE messageID)
{
	m_Buffer[0] = netAddress;
	m_Buffer[1] = messageID;
	
	m_CurrentPosition = m_Buffer+2;
}

CGOutgoingMessage::~CGOutgoingMessage()
{

}

CGOutgoingMessage::CGOutgoingMessage(const CGOutgoingMessage& init)
{
	memcpy(m_Buffer, init.m_Buffer, init.GetSize());
}

CGOutgoingMessage CGOutgoingMessage::operator =(const CGOutgoingMessage& other)
{
	memcpy(m_Buffer, other.m_Buffer, other.GetSize());
	m_CurrentPosition = m_Buffer+other.GetSize();
	m_TimeStamp = other.m_TimeStamp;

	return *this;
}

/*
bool CGOutgoingMessage::Check()
{
#ifdef _DEBUG
	if(m_Buffer[0] != NET_ADDR_SPFOTO)
		return false;
	switch(m_Buffer[0])
	{
	case SET_PARAM_INT_STEPPER:
		ASSERT(GetSize() == 6);
		return true;
	case STEPPER_SET_STATUS:
		ASSERT(GetSize() == 6);
		return true;
	default:
		ASSERT(false);
		return false;
	}
#else
	return true;
#endif
}
*/
