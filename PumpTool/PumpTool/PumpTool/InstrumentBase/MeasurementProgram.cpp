// MeasurementProgram.cpp: implementation of the CGMeasurementProgram class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "Stend.h"
#include "MeasurementProgram.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGMeasurementProgram::CGMeasurementProgram(CGInstrument* instrument)
{
	m_Instrument = instrument;

	m_IsRunning = false;
	//m_IsPaused = false;
	m_State = c_ProgramIsStopped;
	m_CurrentProgramStep = 0;
	m_NumberOfSteps = 0;
}

CGMeasurementProgram::~CGMeasurementProgram()
{
}

void CGMeasurementProgram::Start()
{
	m_IsRunning = true;
	//m_IsPaused = false;
	m_State = c_ProgramIsStarting;

	m_CurrentProgramStep = 0;
	if(m_Instrument != 0)
		m_Instrument->Initialize();
}

void CGMeasurementProgram::Stop()
{
	m_IsRunning = false;
	m_State = c_ProgramIsStopped;
}

// ������� ���������, �������������� ��������
void CGMeasurementProgram::StopByInstrument()
{
	Stop();
}

void CGMeasurementProgram::Pause()
{
	//m_IsPaused = true;
	m_State = c_ProgramIsPaused;
	TRACE("CGMeasurementProgram::Pause() %d\n", m_State);
}

void CGMeasurementProgram::Continue()
{
	//m_IsPaused = false;
	m_State = c_ProgramIsStarting;
}

bool CGMeasurementProgram::IsRunning()
{
	return m_IsRunning;
}

bool CGMeasurementProgram::IsPaused()
{
	return (m_State == c_ProgramIsPaused);
}

bool CGMeasurementProgram::IsStarting()
{
	return (m_State == c_ProgramIsStarting);
}

void CGMeasurementProgram::PerformNextStep()
{
	m_CurrentProgramStep++;
	m_State = c_ProgramIsRunning;
}

void CGMeasurementProgram::RepeatLastStep()
{
}

void CGMeasurementProgram::SetMeasurementState(int state)
{
	ASSERT(m_Instrument != 0);
	m_Instrument->m_MeasurementState = state;
}

void CGMeasurementProgram::SetInstrumentState(int state)
{
	ASSERT(m_Instrument != 0);
	m_Instrument->m_InstrumentState = state;
}

int CGMeasurementProgram::CalculateNumberOfSteps(double from, double to, double step)
{
	if(step > 0)
	{
		return int((to-from+step*0.1)/step);
	}
	return 0;
}

// ���������� ������� ���������� ���������.
int CGMeasurementProgram::GetProgress()
{
	if(m_NumberOfSteps > 0)
	{
		return m_CurrentProgramStep*100/m_NumberOfSteps;
	}
	return -1;
}
