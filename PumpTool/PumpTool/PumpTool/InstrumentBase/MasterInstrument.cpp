// MasterInstrument.cpp: implementation of the CGMasterInstrument class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MasterInstrument.h"

#include "MeasurementProgram.h"
#include "MeasuredData.h"
#include "Instrument/Autosampler.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGMasterInstrument::CGMasterInstrument()
{
	m_MasterNetAddress = 0;
	// ��� ����������� �������� ������������� ������������� ������ ���� ���������.
	m_InstrumentId = 1;

	// ��������� �������� ��������������� ��� ��������, �������
	// �������� � USB.
	m_UsbVendorID = 0;
	m_UsbProductID = 0;

	m_DataChannel = &m_MasterDataChannel;
	m_ProtocolType = 0;
	m_IsModernProtocol = false;

	m_IsConnected = true;
	m_TimeoutCounter = 0;

	m_MeasurementProgram = 0;
	m_MeasuredData = 0;

	m_Window = 0;
	m_WindowMessageID = 0;

	memset(&m_InstrumentCommInfo, 0, sizeof(m_InstrumentCommInfo));
	m_NumberOfUsbDevices = 0;

	m_InstrumentMessageProcessingThread = 0;
	m_SoftTimerThread = 0;
	
	m_IsSoftTimerThreadRunning = false;

	m_LogDataCounter = 0;
	m_AfterErrorCounter = 0;
}

CGMasterInstrument::~CGMasterInstrument()
{
// ������������� �����
	CloseDataChannel();
	m_DataChannel->Delete();

	// ������������� ���� ������������ �������
	m_StopThreadEvent.SetEvent();
// ���������� ���������
	if(m_InstrumentMessageProcessingThread != 0)
	{
		if(m_InstrumentMessageProcessingThread->m_hThread != 0)
			::WaitForSingleObject(m_InstrumentMessageProcessingThread->m_hThread, INFINITE);

		// ���� ������� ����, ��������� m_bAutoDelete == FALSE.
		// ����� ����� ���������, ��� ���� ��������� � ������ ������
		// ������, ��� �� �������� ����� � ������ ��� ���������.
		delete m_InstrumentMessageProcessingThread;
	}
	//::Sleep(120);

	StopSoftTimerThread(false);

/*
	POSITION pos = m_SlaveInstrumentsList.GetHeadPosition();
	while(pos)
	{
		delete m_SlaveInstrumentsList.GetNext(pos);
	}
*/

/*
// ����� �������� ����� !!!
	if(m_DataChannel != 0)
	{
		delete m_DataChannel;
	}
*/
}

void CGMasterInstrument::Create(CGDataChannel* dataChannel)
{
// ����������� ������� �� ������ ���������� ��� ����� ������
	ASSERT(false);
}

void CGMasterInstrument::AddSlaveInstrument(CGInstrument* instrument)
{
	instrument->Create(m_DataChannel);
	m_SlaveInstrumentsList.AddTail(instrument);
}

void CGMasterInstrument::RemoveSlaveInstruments()
{
	m_SlaveInstrumentsList.RemoveAll();
}

// ������� ����� ������ ����������� � ��������� ����������� ����
bool CGMasterInstrument::Create()
{
// ���������, ��� ��� ������� ��������� ���������� ����������� ��������� �������
	ASSERT(m_InternalTimeUnitToSec != 0.0);

/*
// ������������� ��������� �� ����� ������ ��� �������� � ����
	POSITION pos = m_SlaveInstrumentsList.GetHeadPosition();
	while(pos)
	{
		CGInstrument* instrument = (CGInstrument*)m_SlaveInstrumentsList.GetNext(pos);
		instrument->Create(m_DataChannel);
	}
*/

	if(m_InstrumentMessageProcessingThread == 0)
	{
		// ��������� ���� ������ ����, ��� ��������� ���� ������, �� ���� ���������� ������� ����
		m_InstrumentMessageProcessingThread = AfxBeginThread((AFX_THREADPROC)MeasurementThreadExecute, (LPVOID) this, THREAD_PRIORITY_HIGHEST);
		if(m_InstrumentMessageProcessingThread == 0)
			return false;
		// ������ ����� ������� ���� ����� �������� ��������� ���������� ����
		m_InstrumentMessageProcessingThread->m_bAutoDelete = FALSE;
	}

	return true;
}

// ������ ��������� ������ ������ �����������
bool CGMasterInstrument::InitializeDataChannel(const CString& portName, const CGDataChannelInfo& dci)
{
	if(portName[0] == _T('U') && portName[2] == _T('B'))
	{
// �������� � USB
		CStringA portNameA(portName.GetString());
		m_ProtocolType = DCP_HID_1;
		m_IsModernProtocol = true;
		if( !m_DataChannel->Create(portNameA, DCT_USB_HID, dci.m_BufferSize, 0) )
		{
			return false;
		}
		
		// ������������� ������� ���� � �������� ��������� ���������
		// � ����������� ���������.
		if(dci.m_IsMaster)
		{
			m_DataChannel->SetDeviceNotificationWindow(AfxGetMainWnd()->GetSafeHwnd());
		}

// ��������� ���������� ������������ ���������
		ASSERT(m_UsbProductID != 0);

		m_NumberOfUsbDevices = m_DataChannel->EnumerateDevices(m_UsbVendorID, m_UsbProductID);
		TRACE("MI Init m_NumberOfUsbDevices = %d\n", m_NumberOfUsbDevices);
		if(m_NumberOfUsbDevices == 0)
		{
			// ���� ������ ��� �� �������, �������, ��� ������������� ������
			// �������. ��������� �������� �������� ����� ����������� �
			// ������ OnDeviceChange()
			m_DataChannel->SetNotification(NULL, 0, HANDLE(m_DataChannelEvent));
			// ���� ������������ ������� ��������� �����, ����� �� ������ �����
			// ��� ������������� �������.
			StartSoftTimerThread();
			// ���������� false, ��� ��� ������������ ����� ��� ������ ������.
			return false;
		}
		else
		{
			const int c_bufferLength = 256;
			wchar_t buffer[c_bufferLength];
			int usbEnumerationIndex = -1;

			if(!dci.m_UsbSerialNumber.IsEmpty() && m_NumberOfUsbDevices > 1)
			{
				// ���� ������ ���������� �� ��������� ������.
				for(int i = 0; i <= m_NumberOfUsbDevices; i++)
				{
					// ������ �������� ���������, ���������� �� ������ �� ������.
					m_DataChannel->GetNextSerialNumber((i == 0), buffer, c_bufferLength);
					if(CString(buffer) == dci.m_UsbSerialNumber)
					{
						m_DataChannel->GetNextDeviceName(false, buffer, c_bufferLength);
						m_UsbDeviceName = CString(buffer);
						usbEnumerationIndex = i;
						break;
					}
				}
				// �� ����� ������� ������.
				if(usbEnumerationIndex < 0)
				{
					if(dci.m_AutoConnect)
					{
						usbEnumerationIndex = 0;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				usbEnumerationIndex = max(0, dci.m_UsbEnumerationIndex);
			}

			usbEnumerationIndex = min(usbEnumerationIndex, m_NumberOfUsbDevices-1);
			// ���������� ��� ����������, ��� �������� ������� �����
			for(int i = 0; i <= usbEnumerationIndex; i++)
			{
				// ������ ������� ���������, ���������� �� ������ �� ������.
				m_DataChannel->GetNextDeviceName((i == 0), buffer, c_bufferLength);
			}
			m_UsbDeviceName = CString(buffer);

			// �������� � ��� �����������, ������� ������� � �������
			m_DataChannel->SelectDevice(usbEnumerationIndex);
		}
	}
	else
	{
// �������� � COM-������
		CString name;
		if(portName[0] == _T('C') && portName[2] == _T('M'))
		{
			name = _T("\\\\.\\");
			name += portName;
		}
		else
		{
			name = portName;
		}
		if(m_DataChannel->IsCreated())
			m_DataChannel->Close();

		CStringA nameA(name.GetString());
		if(!m_DataChannel->Create(nameA, dci.m_ChannelType, dci.m_BufferSize, dci.m_BaudRate))
		{
			return false;
		}

		m_ProtocolType = dci.m_ProtocolType;
	}

	m_DataChannel->SetNotification(NULL, 0, HANDLE(m_DataChannelEvent));
	m_DataChannel->StartDataExchange(m_ProtocolType, dci.m_InputBlocks, dci.m_OutputBlocks, dci.m_InternalBlocks);

	m_IsModernProtocol = (m_ProtocolType == DCP_TRANSPORT || m_ProtocolType == DCP_HID_1);

	m_IsConnected = true;
	m_TimeoutCounter = 0;
	m_MasterNetAddress = 0;

// ��� ������ � ������������ ���������� ������ ������������ ���������
	if(m_IsModernProtocol)
	{
		StartSoftTimerThread();
	}

	return true;
}

int CGMasterInstrument::GetUsbSerialNumber()
{
	// ����� � ����� ������� ��������, �� ������� ������� ������ 'N'.
	int index = m_UsbDeviceName.ReverseFind(_T(' '));
	if(index > 0)
	{
		return _tstoi(m_UsbDeviceName.Mid(index+1));
	}
	return 0;
}

void CGMasterInstrument::CloseDataChannel()
{
	if(IsDataChannelOpened())
		m_DataChannel->Close();
}

// ���������, ��� ����� ������ ������ ��� ������
bool CGMasterInstrument::IsDataChannelCreated()
{
	return m_DataChannel->IsCreated();
}

// ���������, ��� ����� ������ ����� �������� �������� ������
bool CGMasterInstrument::IsDataChannelOpened()
{
	//return m_DataChannel && m_DataChannel->IsCreated();
	return m_DataChannel->IsOpened();
}

// ���������� ��� ��������� ������ ������������ USB - ���������
bool CGMasterInstrument::OnDeviceChange(UINT nEventType, UINT* dwData, const CGDataChannelInfo& dci)
{
	// ���� �������������� �������.
	const UINT DBT_DEVICEARRIVAL        = 0x8000;	// 32768
	const UINT DBT_DEVICEREMOVECOMPLETE = 0x8004;	// 32772

	bool deviceIsRemoved = false;
	// ������ ��������� ���������
	/*
	if(nEventType != 7)
	{
		CString debugString;
		debugString.Format(_T("MI OnDeviceChange 0x%x %d"), nEventType, m_NumberOfUsbDevices);
		AfxMessageBox(debugString);
	}
	*/
	//TRACE("MI OnDeviceChange  %0x\n", nEventType);

	// �������� ��������� �������� ������� ������������. �������� ����������� �����-��
	// �������� � �������� Windows,
	::Sleep(50);
	if(m_DataChannel->OnDeviceNotificationMessage(nEventType, dwData))
	{
		// ��� ���������� ���������� ����� ����������� �� ������ ������
		// �� ��������� �����������. ����������, ��������� �� ������ ����������
		// ��� ������.
		bool isOpened = IsDataChannelOpened();
		int numberOfDevices = m_DataChannel->EnumerateDevices(m_UsbVendorID, m_UsbProductID);
		// ��� ������������ ��������� � ������ �� �������� ����������, ������ �������
		// (�������� ������) � ������ ������ �������. ������� ���������, ��� ����� �����
		// ���������� ����� ���� ������.
		numberOfDevices += int(isOpened);
		TRACE("MI OnDeviceChange numberOfUsbDevices = %d %d %d\n", numberOfDevices, m_NumberOfUsbDevices, isOpened);
		/*
		CString debugString;
		debugString.Format(_T("MI OnDeviceChange numberOfUsbDevices = %d %d %d"), numberOfDevices, m_NumberOfUsbDevices, isOpened);
		AfxMessageBox(debugString);
		*/
		if(nEventType == DBT_DEVICEARRIVAL)
		{
			const int c_bufferLength = 256;
			wchar_t buffer[c_bufferLength];
			TRACE(_T("Device is connected %d %d\n"), CTime::GetCurrentTime().GetTime(),
				GetTickCount());

			//AfxMessageBox(_T("Device is connected"));
			//StopDataExchange();
			if(m_NumberOfUsbDevices == 0 && dci.m_AutoConnect)
			{
// ������������ ����� ������������ ���������� - � ��� � ��������
				m_DataChannel->SelectDevice(0);
				m_DataChannel->StartDataExchange(m_ProtocolType, dci.m_InputBlocks, dci.m_OutputBlocks, dci.m_InternalBlocks);
				m_DataChannel->GetNextDeviceName(true, buffer, c_bufferLength);
				m_UsbDeviceName = CString(buffer);
				//AfxMessageBox(m_UsbDeviceName);
			}
			else
			{
// ���� ����� �� ������, �.� ������ ��� ��������
// � ��� ���� �������� ��� ���.
				if(!m_DataChannel->IsOpened() && !m_UsbDeviceName.IsEmpty())
				{
// ���� ���������� � ������
					for(int i = 0; i < numberOfDevices; i++)
					{
						m_DataChannel->GetNextDeviceName((i == 0), buffer, c_bufferLength);
						if(m_UsbDeviceName == CString(buffer))
						{
							m_DataChannel->SelectDevice(i);
							m_DataChannel->StartDataExchange(m_ProtocolType, dci.m_InputBlocks, dci.m_OutputBlocks, dci.m_InternalBlocks);
							break;
						}
					}
				}
// ��� ��������� (������������) ��������� ������ �� ������ - ����� ������������
// ���� ������� ���������� � ������
			}
		}
#if 0
		else if(numberOfDevices == m_NumberOfUsbDevices)
		{
			TRACE(_T("Device is restarted %d %d\n"), CTime::GetCurrentTime().GetTime(),
				GetTickCount());
			//AfxMessageBox(_T("Device is restarted"));
			if(numberOfDevices != 0 && !m_DataChannel->IsOpened())
			{
				m_DataChannel->StopDataExchange();
				m_DataChannel->StartDataExchange(m_ProtocolType, dci.m_InputBlocks, dci.m_OutputBlocks, dci.m_InternalBlocks);
			}
		}
		else   // numberOfDevices < m_NumberOfUsbDevices
#endif 
		if(nEventType == DBT_DEVICEREMOVECOMPLETE)
		{
			TRACE(_T("Device is disconnected %d %d\n"), CTime::GetCurrentTime().GetTime(),
				GetTickCount());
			//AfxMessageBox(_T("Device is disconnected"));
			if(numberOfDevices == 0)
			{
				// ��������� �����, ����� � ��� ��� ������ �� ������ ������.
				m_DataChannel->StopDataExchange();
		}
			deviceIsRemoved = true;
		}
		m_NumberOfUsbDevices = numberOfDevices;
	}
	return deviceIsRemoved;
}

bool CGMasterInstrument::StartSoftTimerThread()
{
// ���� ��� ��������
	if(m_IsSoftTimerThreadRunning)
		return true;

	ASSERT(m_SoftTimerThread == 0);
	m_IsSoftTimerThreadRunning = true;
	m_SoftTimerThread = AfxBeginThread((AFX_THREADPROC)SoftTimerThreadExecute, (LPVOID) this, THREAD_PRIORITY_NORMAL);
	if(m_SoftTimerThread == 0)
	{
		m_IsSoftTimerThreadRunning = false;
		return false;
	}
	// ������ ����� ������� ���� ����� �������� ��������� ���������� ����
	m_SoftTimerThread->m_bAutoDelete = FALSE;

	return true;
}

// ������������� ���� �������
bool CGMasterInstrument::StopSoftTimerThread(bool checkProtocol)
{
// ���� ��� �����������
	if(!m_IsSoftTimerThreadRunning)
		return false;

// ��� ������ � ������������ ���������� ���� ������� ������ ������������� �����������
	if(checkProtocol && m_IsModernProtocol)
		return false;

	m_IsSoftTimerThreadRunning = false;
	if(m_SoftTimerThread != 0)
	{
		if(m_SoftTimerThread->m_hThread != 0)
			::WaitForSingleObject(m_SoftTimerThread->m_hThread, INFINITE);
		delete m_SoftTimerThread;
		m_SoftTimerThread = 0;
	}

	return true;
}

void CGMasterInstrument::OnSoftTimerEvent()
{
	if(m_IsModernProtocol && m_DataChannel->IsOpened())
	{
		if(m_SoftTimerCounter%10 == 0)
		{
// ��� ��������� ���� � ����� �������� ��� � ������� ���������� ������� �����,
// ������� NET_ADDR = 0. �������� ������� ������� �����.
			CGOutgoingMessage message(0, 0);
			CTime time = CTime::GetCurrentTime();
			message << time.GetTime();
			m_DataChannel->SendData(message.GetBuffer(), message.GetSize());
		}
	}
}

void CGMasterInstrument::SetMeasurementProgram(CGMeasurementProgram* program)
{
// ���������, ��� ��������� ������������� ��� ���������� ������ ���� ��������
	ASSERT(program == 0 || program->GetInstrument() == this);

	m_MeasurementProgram = program;
}

void CGMasterInstrument::SetMeasuredData(CGMeasuredData* data)
{
	m_MeasuredData = data;
}

CGMeasurementProgram* CGMasterInstrument::GetMeasurementProgram()
{
	return m_MeasurementProgram;
}

CGMeasuredData* CGMasterInstrument::GetMeasuredData()
{
	return m_MeasuredData;
}

void CGMasterInstrument::SetWindowMessageID(CWnd* window, int id)
{
	m_Window = window;
	m_WindowMessageID = id;
}

void CGMasterInstrument::SetUsbProductID(unsigned short vendorID, unsigned short productID)
{
	m_UsbVendorID = vendorID;
	m_UsbProductID = productID;
}

// ���������� ��� ��������� ����� ��������� �� �������
void CGMasterInstrument::OnGetNewData()
{
// �������� ��������� ���� ��� ��������� � ������� ����
	if(m_Window)
	{
		::PostMessage(m_Window->GetSafeHwnd(), m_WindowMessageID, 
			(GetInstrumentId() << 8) | c_InstrumentEventNewData, 0);
	}
}

// ���������� ��� ��������� ��������� ������� (������ ��� ����������� ������ ���������)
void CGMasterInstrument::OnInstrumentStateChanged(CGInstrument* instrument)
{
//	TRACE("CGMasterInstrument::OnInstrumentStateChanged instrumentState %d\n", instrument->GetInstrumentState());

// ������� ���� �������� ��� �������, ��������� - ��� ��������� ����������
	int instrumentState = instrument->GetInstrumentState();
    if(instrumentState == 0)
    {
    	ASSERT(false);
        return;
    }

// �������� ��������� ���� ��� ��������� � ������� ����
	if(m_Window)
	{
		::PostMessage(m_Window->GetSafeHwnd(), m_WindowMessageID,
			(GetInstrumentId() << 8) | c_InstrumentEventStateChanged,
			 instrumentState);
	}

	instrument->ResetInstrumentState();
}

// ���������� ��� ��������� ��������� �� ������ �������
void CGMasterInstrument::OnErrorState(CGInstrument* instrument)
{
//	TRACE("CGMasterInstrument::OnErrorState errorCode %d\n", instrument->GetErrorCode());

// ������� ���� �������� ��� ������
	int errorCode = instrument->GetErrorCode();
	if(m_MeasurementProgram != 0)
	{
		m_MeasurementProgram->OnErrorState();

		// ��� ��������� ������ ��������� �����������
		if(instrument->GetMeasurementState() == c_PassiveState)
			OnProgramFinished(instrument);
	}

// ���� ��������� �������� ��������� ������, �� ��������� �� ��������
	if(instrument->GetErrorCode() == 0)
		return;

// �������� ��������� ���� ��� ��������� � ������� ����
	if(m_Window)
	{
		::PostMessage(m_Window->GetSafeHwnd(), m_WindowMessageID,
			(instrument->GetInstrumentId() << 8) | c_InstrumentEventError,
			 errorCode);
	}

	instrument->ResetError();
}

// ���������� ��� ���������� (�� �������������� �������������) �������� ��������� ���������
void CGMasterInstrument::OnProgramFinished(CGInstrument* instrument)
{
//	TRACE("CGMasterInstrument::OnProgramFinished instrument %d\n", instrument->GetNetAddress());

// �������� ��������� ���� ��� ��������� � ������� ����
	if(m_Window)
	{
		::PostMessage(m_Window->GetSafeHwnd(), m_WindowMessageID,
			(GetInstrumentId() << 8) | c_InstrumentEventProgramFinished, 0);
	}

// ��������� ����������� ���������� ����������� � ���������� ���������
	m_MeasurementProgram = 0;
}

// ����� ����������� ������������ ���������
bool CGMasterInstrument::DispatchIncomingMessage(CGIncomingMessage& message)
{
//	m_MessageCounter++;

	POSITION pos;
// ��������� ���������, ���������� ���� ��� � �������
//	TRACE("DispatchIncomingMessage %d\n", message.GetInstrumentID());
	if(message.GetInstrumentID() == 0)
	{
		if(m_IsModernProtocol)
		{
// �������� ����������, ����������� ��� ����������� �������
			DoOnSystemMessage(message);
		}
		else if(message.GetDataSize() >= 6)
		{
			ASSERT(m_ProtocolType == DCP_COM7BIT);

// ��� ���� ����� �� ������ � �����������, �� ��������� ���, ��� ���� �������
// �������, ����� �� ������� �������, ���������� � ���������.
			BYTE errFormat, errCheckSum;
			message >> m_MasterNetAddress >> 
				m_InstrumentCommInfo.m_WriteBufferMinFreeSpace >>
				errFormat >>					// ���� ���� �� ������������
				errFormat >> errCheckSum;
			m_InstrumentCommInfo.m_DataPacketFormatError = errFormat;
			m_InstrumentCommInfo.m_DataPacketCrcError = errCheckSum;

// �������� ����������, ������� ����� �������������� ��� ���������� ���������
			DoOnSystemMessage();
		}


		if(GetErrorCode())		// �������� ������, ��������� � ��������� �� �������
			OnErrorState(this);

// �������� ����������� ��� ���� �������� � ����
		pos = m_SlaveInstrumentsList.GetHeadPosition();
		while(pos)
		{
			((CGInstrument*)m_SlaveInstrumentsList.GetNext(pos))->DoOnSystemMessage();
		}

		return true;
	}

	CGInstrument* instrument = 0;

	if (NET_ADDR_AUTOSAMPLER == m_NetAddress)		// ��������� ���������� �������������
	{
		instrument = this;
		m_MasterNetAddress = m_NetAddress;
	}

	if(message.GetInstrumentID() == m_NetAddress)		// ��������� ���������� ������� �����������
	{
		instrument = this;
		m_MasterNetAddress = m_NetAddress;
	}
	else
	{
		pos = m_SlaveInstrumentsList.GetHeadPosition();
		while(pos)									// ����� ����������, ������������ ���������
		{
			instrument = (CGInstrument*)m_SlaveInstrumentsList.GetNext(pos);
			if(instrument->GetNetAddress() == message.GetInstrumentID())
				break;
		}
	}
	if(instrument == 0)								// �� �����
		return false;

	int oldMeasurementState = instrument->GetMeasurementState();
// �������� ���������� ���������, ������� ������ ���� ���������� � ������ ����������� �������
//	TRACE("instrument state before %d\n", instrument->GetMeasurementState());
	if(!instrument->ProcessIncomingMessage(message))
		return false;

// ����������� ��������� ���������, ������������� ������������ ���������
//	TRACE("instrument state after %d\n", instrument->GetMeasurementState());
	switch(instrument->GetMeasurementState())
	{
	case c_ReadyState:		// �������� ����� ���������� ������
		if(m_MeasuredData != 0)
		{
// ��������� ������ � ��������� � ����������� � ����������� ������
			m_MeasuredData->GetCriticalSection().Lock();
			m_MeasuredData->GetDataFromInstrument(instrument);
			m_MeasuredData->GetCriticalSection().Unlock();

// �������� ����������� � ������ ����� ������
			OnGetNewData();
		}

		if(m_MeasurementProgram != 0)
		{
			m_MeasurementProgram->PerformNextStep();
			if(instrument->GetMeasurementState() == c_PassiveState)		// ��������� ��������� ��� ���������
				OnProgramFinished(instrument);
		}
		break;
	case c_PassiveState:		
		// ��������� ����������� ��������.
		if(oldMeasurementState != c_PassiveState && m_MeasurementProgram != 0)
			OnProgramFinished(instrument);
		break;
	}

// ���� ���������� ��������� �������, �������� �����������
	if(instrument->GetInstrumentState() != 0)
		OnInstrumentStateChanged(instrument);
// ���� ��������� �� ������ �������, �������� �����������
	if(instrument->GetErrorCode() != 0)
		OnErrorState(instrument);

	return true;
}

// ���������� �� ������� ��� �������� ������� ����� � ��������
bool CGMasterInstrument::IsInstrumentConnected()
{
	return m_IsConnected;
}

// C���� � �������� ���� ����������� ����� ������������� ����������������� �����
bool CGMasterInstrument::IsConnectionEstablished()
{
	return (m_MasterNetAddress > 0);
}

// ������ ����������� ����� USB-����
bool CGMasterInstrument::UseUsbConnection()
{
	return (m_ProtocolType == DCP_HID_1);
}

// ��������, ������ �� ������ ���������
bool CGMasterInstrument::IsInstrumentValid()
{
// ������, ���� ������ ��������� ��� ��������� ��������� ��� �� �������
	return m_MasterNetAddress == m_NetAddress || m_MasterNetAddress == 0;
}

// ������ ������������ ��������� �� �������� ���������
bool CGMasterInstrument::IsProgramRunning()
{
	return m_MeasurementProgram && m_MeasurementProgram->IsRunning();
}

// �������� ���� ������ � ������� ��������� � ����������� ���� �������
UINT CGMasterInstrument::InstrumentMessageProcessingLoop()
{
	HANDLE hEvents[2];
	hEvents[0] = HANDLE(m_DataChannelEvent);
	hEvents[1] = HANDLE(m_StopThreadEvent);

	CGIncomingMessage incomingMessage;

	for( ; ; )
	{
// � ���������� �������� �� ���������� ������� �� ������ ������ � ��������
// ��� ���������. ��� ����� �������� ������ � �������� ������� �������� ������������
// ����� ����� ��� � ����������.
		int waitResult = ::WaitForMultipleObjects(2, hEvents, FALSE, 500);

		// ��� USB �������� ��������, ����� ����� ��� �� ���������������
		// ���������.
		//if(waitResult == WAIT_TIMEOUT && !m_DataChannel->IsOpened())
		//	continue;


		bool isMessageReceived = false;
		while(incomingMessage.GetFromChannel(*m_DataChannel))
		{
			isMessageReceived = true;
			DispatchIncomingMessage(incomingMessage);
		}

// �������� ������ �������� � ������� �� �������� �����, �������� ���������� ����.
		if(waitResult == WAIT_OBJECT_0+1)
			break;

		if(isMessageReceived)
		{
			m_IsConnected = true;
			m_TimeoutCounter = 0;
		}
		else
		{
			if(++m_TimeoutCounter > 5)				// � ������� 3 ���. ������ �� ��������
				m_IsConnected = false;
		}
	}

	return 0;
}

// ���� ������������ ������� ��������� ��������� ��������� ��������
// (��� ������� �������� ������� � ������) ����� ������ ���������� �������
// ���������� �� ��������� ����������� ����. ������������ ��� ������ ��������� ���������
// � ����������� "��������" ������� � ��������� � �������������� ��������.
UINT CGMasterInstrument::SoftTimerProcessingLoop()
{
	for(m_SoftTimerCounter = 0; m_IsSoftTimerThreadRunning; m_SoftTimerCounter++)
	{
		OnSoftTimerEvent();
		::Sleep(100);
	}

	return 0;
}

UINT MeasurementThreadExecute(LPVOID param)
{
	CGMasterInstrument* masterInstrument = (CGMasterInstrument*)param;
	return masterInstrument->InstrumentMessageProcessingLoop();
}

UINT SoftTimerThreadExecute(LPVOID param)
{
	CGMasterInstrument* masterInstrument = (CGMasterInstrument*)param;
	return masterInstrument->SoftTimerProcessingLoop();
}

/////////////////////////////////////////////////////////////////
// ������ � ������ ��������� ������

// �������� ���������, ��� ������������� ���������� � ����
void CGMasterInstrument::SendOutgoingMessage(const CGOutgoingMessage& message, const CString* logString)
{
	if(GetDataChannel() != 0)
		GetDataChannel()->SendData(message.GetBuffer(), message.GetSize());

	int logDataSize = m_LogDataArray.GetSize();
	if(IsLogFileEnabled() || logDataSize > 0)
	{
		CString string("<- ");

		if(logString != 0 && !logString->IsEmpty())
		{
	// ������, ����������� ��� ����������� ��������� ������������ �����
			string += *logString;
		}
		else
		{
			CString dataByte;
	// ��������� ����������� ������ � ���� ������������������� ������
			for(int i = 1; i < message.GetSize(); i++)
			{
				dataByte.Format(_T("%6d"), *(message.GetBuffer()+i));
				string += dataByte;
			}
		}

		// ��������� ����� ����������� �� ������ �����
		m_CriticalSection.Lock();

		// ������������� ��������� � ����������� ������ - ����� ����
		// ��� ������
		if(IsLogFileEnabled())
		{
			m_LogFile.WriteString(string+_T('\n'));
		}

		if(logDataSize > 0)
		{
			m_LogDataArray[m_LogDataCounter%logDataSize] = string+_T('\n');
			m_LogDataCounter++;
		}

		m_CriticalSection.Unlock();
	}
}

void CGMasterInstrument::SendOutgoingMessage(const char* message)
{
	if (GetDataChannel() != 0)
		GetDataChannel()->SendData((BYTE*)message, strlen(message));
		
}

void CGMasterInstrument::ReplyFromComPort(char* replyData)
{
	if (GetDataChannel() != 0)
		GetDataChannel()->GetData(replyData);
}
// ����� ���� ���� ���� ���������
CString CGMasterInstrument::DumpMessage(const CGIncomingMessage& message, const int from)
{
	CString logString;
	CString string;
	for(int i = from; i < message.GetDataSize(); i++)
	{
		string.Format(_T("%5d"), *(message.GetData()+i));
		logString += string;
	}
	return logString;
}


// ���������� ��������� ��������� � ����
void CGMasterInstrument::SaveIncomingMessage(CGIncomingMessage& message, const CString* logString)
{
	int logDataSize = m_LogDataArray.GetSize();
	if(IsLogFileEnabled() || logDataSize > 0)
	{
		CString string(_T("-> "));

		if(logString != 0 && !logString->IsEmpty())
		{
	// ������, ����������� ��� ����������� ��������� ������������ ����� 
			string += *logString;
		}
		else
		{
			CString dataByte;
	// ��������� ����������� ������ � ���� ������������������� ������
			dataByte.Format(_T("%6d"), message.GetMessageID());
			string += dataByte;
			string += DumpMessage(message);
/*
			for(int i = 0; i < message.GetDataSize(); i++)
			{
				dataByte.Format(_T("%6d"), *(message.GetData()+i));
				string += dataByte;
			}
*/
		}

		m_CriticalSection.Lock();				// ��������� ����� ����������� �� ������ �����
		if(IsLogFileEnabled())
		{
			m_LogFile.WriteString(string+_T('\n'));
			if(m_AfterErrorCounter > 0)
			{
				m_AfterErrorCounter--;
				if(m_AfterErrorCounter == 0)
				{
					// �������� ��������� ����� ����� ����� ������.
					m_LogFile.Close();
				}
			}
		}
		if(logDataSize > 0)
		{
			m_LogDataArray[m_LogDataCounter%logDataSize] = string+_T('\n');
			m_LogDataCounter++;
		}
		m_CriticalSection.Unlock();
	}
}

// ���������/��������� ���������� ��������� � �����
void CGMasterInstrument::EnableLogFile(const CString& fileName, bool append)
{
	ASSERT(!IsLogFileEnabled());

	int mode = append ? CFile::modeNoTruncate : 0;
	CString filePath;
	if(fileName.Find('\\') >= 0)
	{
// ��� ��� �������� ���� � �����
		filePath = fileName;
	}
	else
	{
// ���� ��������� � ��� �� ��������, ��� ��������� ����������� ������
//			CString exeFolder = ((CMainFrame*)AfxGetMainWnd())->GetExeFolderName();
//			filePath = exeFolder+fileName;
	}

	CFileException fileException;
	int result;

	m_CriticalSection.Lock();
	result = m_LogFile.Open(filePath, mode | CFile::modeCreate
			| CFile::modeReadWrite | CFile::shareDenyWrite,
			&fileException);
	m_CriticalSection.Unlock();

	if(!result)
	{
		AfxThrowFileException(fileException.m_cause, fileException.m_lOsError, fileException.m_strFileName);
	}
}

void CGMasterInstrument::DisableLogFile()
{
	ASSERT(IsLogFileEnabled());

	m_CriticalSection.Lock();				// ��������� ����� ����������� �� ������ �����
	m_LogFile.Close();
	m_CriticalSection.Unlock();
}
bool CGMasterInstrument::IsLogFileEnabled()
{
	return m_LogFile.m_hFile != CFile::hFileNull;
}

CString CGMasterInstrument::GetLogFileName()
{
	CString fileName;
	if(IsLogFileEnabled())
	{
		fileName = m_LogFile.GetFilePath();
	}
	return fileName;
}

const int c_AfterErrorDivider = 10;

// ������ � ���� ����������, ����������� � ������� ����� � ������.
void CGMasterInstrument::WriteSavedLogData(const CString& fileName)
{
	if(IsLogFileEnabled())
	{
		if(m_AfterErrorCounter > 0)
		{
			// ��������, �������������� ���������� ��� ��������� �
			// �������� ����� ������. ��������������� ���������� �����
			// ��� ����������.
			m_AfterErrorCounter = m_LogDataArray.GetSize() / c_AfterErrorDivider;
		}
		return;
	}

	ASSERT(!IsLogFileEnabled());

	EnableLogFile(fileName, false);

	// ��������� ����� ����������� �� ������ �����
	m_CriticalSection.Lock();
	int size = m_LogDataArray.GetSize();
	ASSERT(size > 0);

	int currentIndex = m_LogDataCounter % size;

	if(m_LogDataCounter >= size)
	{
		for(int i = currentIndex; i < size; i++)
		{
			m_LogFile.WriteString(m_LogDataArray[i]);
		}
	}
	for(int i = 0; i < currentIndex; i++)
	{
		m_LogFile.WriteString(m_LogDataArray[i]);
	}
	m_LogFile.Flush();

	// ����� ������ (��� ������� �� ������) ���������� ������ � ���
	// �������� ���������� �����.
	m_AfterErrorCounter = size / c_AfterErrorDivider;

	m_CriticalSection.Unlock();
}

// ������ ������������ ���������� � ���� ��������� ������
void CGMasterInstrument::WriteDebugInfo(const CString& string)
{
	m_CriticalSection.Lock();			// ����� ���������� �� ������ �����
	if(IsLogFileEnabled())
	{
		m_LogFile.WriteString(string);
		if(m_AfterErrorCounter > 0)
		{
			m_AfterErrorCounter--;
			if(m_AfterErrorCounter == m_LogDataArray.GetSize() / c_AfterErrorDivider - 100)
			{
				// ������ ���������� � ������� ��� ���������� � ���-�����.
				AskInstrumentInfo();
			}
			if(m_AfterErrorCounter == 0)
			{
				// �������� ��������� ����� ����� ����� ������.
				m_LogFile.Close();
			}
		}
	}
	// � �������  ���������� ���� ���� ���� ����� � ����. ��� ��������
	// ��������� ���������� ���������� ��� ��������� �������.
	int logDataSize = m_LogDataArray.GetSize();
	if(logDataSize > 0)
	{
		m_LogDataArray[m_LogDataCounter%logDataSize] = string;
		m_LogDataCounter++;
	}
	m_CriticalSection.Unlock();
}

