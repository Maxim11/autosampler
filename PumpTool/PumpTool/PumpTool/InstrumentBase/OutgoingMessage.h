// OutgoingMessage.h: interface for the CGOutgoingMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUTGOINGMESSAGE_H__D64EC7B0_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_)
#define AFX_OUTGOINGMESSAGE_H__D64EC7B0_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

const int c_MaxOutBufferSize = 30;

class CGOutgoingMessage  
{
public:
// ��������� ��� ������ �����������.
	CGOutgoingMessage();
	CGOutgoingMessage(BYTE netAddress, BYTE processID, BYTE attribute, BYTE messageID);
	CGOutgoingMessage(BYTE netAddress, BYTE messageID, BYTE enable);
// ����������� ��������� ��� ������ ������� (��� ������)
	CGOutgoingMessage(BYTE netAddress, BYTE messageID);

	virtual ~CGOutgoingMessage();

	BYTE* GetBuffer() const { return (BYTE*)m_Buffer; }
	BYTE* GetData() const { return (BYTE*)(m_Buffer+4); }
	int GetSize() const { return m_CurrentPosition-m_Buffer; }

	BYTE GetProcessID() const { return m_Buffer[1]; }
	BYTE GetMessageID() const { return m_Buffer[3]; }

	void SetTimeStamp() { m_TimeStamp = ::GetTickCount(); }
	UINT GetTimeStamp() const { return m_TimeStamp; }

	bool Check();

	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, BYTE value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, short value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned short value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, int value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned int value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, long value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned long value);
	friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, float value);
	template<class T> friend CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, T value);
// � �������� ���������� ������ ���������� �����������, �� ��� �������� ����������

	CGOutgoingMessage operator =(const CGOutgoingMessage& other);

protected:
	BYTE m_Buffer[c_MaxOutBufferSize];
	BYTE* m_CurrentPosition;

// ����� �������, ����� �������������� ��� ����������� ���������
	UINT m_TimeStamp;

private:						// ������ �� �����������
	CGOutgoingMessage(const CGOutgoingMessage& init);

	void CopyTwoBytes(BYTE* from);
	void CopyFourBytes(BYTE* from);
};

inline void CGOutgoingMessage::CopyTwoBytes(BYTE* from)
{
	*m_CurrentPosition++ = *from++;
	*m_CurrentPosition++ = *from++;
}

inline void CGOutgoingMessage::CopyFourBytes(BYTE* from)
{
	*m_CurrentPosition++ = *from++;
	*m_CurrentPosition++ = *from++;
	*m_CurrentPosition++ = *from++;
	*m_CurrentPosition++ = *from;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, BYTE value)
{
	*msg.m_CurrentPosition++ = value;
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, short value)
{
	msg.CopyTwoBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned short value)
{
	msg.CopyTwoBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, int value)
{
	msg.CopyFourBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned int value)
{
	msg.CopyFourBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, long value)
{
	msg.CopyFourBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, unsigned long value)
{
	msg.CopyFourBytes((BYTE*)&value);
	return msg;
}

inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, float value)
{
	msg.CopyFourBytes((BYTE*)&value);
	return msg;
}

template<class T>
inline CGOutgoingMessage& operator <<(CGOutgoingMessage& msg, T value)
{
	memcpy(msg.m_CurrentPosition, &value, sizeof(T));
	msg.m_CurrentPosition += sizeof(T);
	return msg;
}


#endif // !defined(AFX_OUTGOINGMESSAGE_H__D64EC7B0_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_)
