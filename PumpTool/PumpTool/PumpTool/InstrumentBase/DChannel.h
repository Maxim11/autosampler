#if !defined(_DCHANNEL_H_INCLUDED_)
#define _DCHANNEL_H_INCLUDED_


// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DCHANNEL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DCHANNEL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

# if !defined(__BORLANDC__)

#ifdef DCHANNEL_EXPORTS
#define DCHANNEL_API __declspec(dllexport)
#else
#define DCHANNEL_API __declspec(dllimport)
#endif

#else		// __BORLANDC__

#ifdef DCHANNEL_EXPORTS
#define DCHANNEL_API __export __stdcall
#else
#define DCHANNEL_API __import __stdcall
#endif

#endif		// __BORLANDC__

#define DRIVER_VERSION		200

#define DCT_SERIAL_PORT	        0x100
#define DCT_NAMED_PIPE_SERVER	0x200
#define DCT_NAMED_PIPE_CLIENT	0x400
#define DCT_USB_HID             0x800

// !!! ������ ���� ����������� � ����������� � CommDataPacket.h !!!
#define DCP_SIMPLE		0x1
#define DCP_COM7BIT		0x2
#define DCP_TRANSPORT	0x4
#define DCP_ASCII		0x8
#define DCP_HID_1		0x10

struct CSChannelStatistic
{
	int PacketsSent;
	int BytesSent;

	int PacketsReceived;
	int BytesReceived;

	int RawBytesReceived;

	int ErrorFormat;
	int ErrorCheckSum;

	int ErrorData;

	int ErrorInputBufferOverflow;
	int ErrorOutputBufferOverflow;
	int ErrorProtocolBufferOverflow;

	int ErrorRead;
	int ErrorWrite;

	int ErrorDataLost;
};


#undef CONST
#ifdef DCHANNEL_EXPORTS
#define CONST
#else
#define CONST const
#endif

#ifdef __cplusplus
extern "C" 
{ 
#endif

HANDLE DCHANNEL_API CreateDataChannel(CONST char *name, CONST UINT type, CONST int driverBufferSize, CONST int baudRate);
void DCHANNEL_API DeleteDataChannel(HANDLE hDataChannel);

void DCHANNEL_API CloseDataChannel(HANDLE hDataChannel);
BOOL DCHANNEL_API IsOpened(HANDLE hDataChannel);

// ��� USB-HID ��������� ���������� ����� �������������
BOOL DCHANNEL_API SelectDevice(HANDLE hDataChannel, int deviceIndex);

void DCHANNEL_API SetDataChannelNotification(HANDLE hDataChannel, CONST HWND hwnd, CONST UINT messageID, CONST HANDLE hEvent);

BOOL DCHANNEL_API StartDataExchange(HANDLE hDataChannel, CONST UINT protocol, CONST UINT inputBlocks, CONST UINT outputBlocks, CONST UINT internalBlocks);
void DCHANNEL_API StopDataExchange(HANDLE hDataChannel);

BOOL DCHANNEL_API GetReceivedData(HANDLE hDataChannel, BYTE* data, CONST int bufSize, int* dataSize);
void DCHANNEL_API SendData(HANDLE hDataChannel, CONST BYTE* data, CONST int dataSize);

void DCHANNEL_API GetChannelStatistic(HANDLE hDataChannel, int* result);

// ������ ������ ���������� ��� ������ � USB-������������
int  DCHANNEL_API EnumerateDevices(HANDLE hDataChannel, unsigned short vendorID, unsigned short productID);
BOOL DCHANNEL_API GetNextDeviceName(HANDLE hDataChannel, BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength);
BOOL DCHANNEL_API GetNextSerialNumber(HANDLE hDataChannel, BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength);
BOOL DCHANNEL_API GetNextProductString(HANDLE hDataChannel, wchar_t* nameBuffer, unsigned int bufferLength);
BOOL DCHANNEL_API SetDeviceNotificationWindow(HANDLE hDataChannel, HANDLE window);
BOOL DCHANNEL_API OnDeviceNotificationMessage(HANDLE hDataChannel, UINT nEventType, UINT* dwData);

int DCHANNEL_API GetDriverVersion();

#ifdef __cplusplus
}
#endif


#if defined(__cplusplus) && !defined(DCHANNEL_EXPORTS)

class CGDataChannel  
{
public:
	CGDataChannel();
	virtual ~CGDataChannel();

	bool Create(const char* name, const UINT type = DCT_SERIAL_PORT, const int driverBufferSize = 4096, const int baudRate = CBR_19200);
	void Delete();
	bool IsCreated();
	
	void Close();
	bool IsOpened();
	
	bool SelectDevice(int deviceIndex);
	void SetNotification(const HWND hwnd, const UINT messageID, const HANDLE hEvent = 0);

	bool StartDataExchange(const UINT protocol = DCP_COM7BIT, const UINT inputBlocks = 20, const UINT outputBlocks = 10, const UINT internalBlocks = 0);
	void StopDataExchange();

	bool GetReceivedData(BYTE* data, const int bufSize, int* dataSize);
	void SendData(const BYTE* data, const int dataSize);
	void GetData(char* data);

	void GetChannelStatistic(struct CSChannelStatistic* result);

	// ������ ������ ���������� ��� ������ � USB-������������
	int  EnumerateDevices(unsigned short vendorID, unsigned short productID);
	bool GetNextDeviceName(BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength);
	bool GetNextSerialNumber(BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength);
	bool GetNextProductString(wchar_t* nameBuffer, unsigned int bufferLength);
	bool SetDeviceNotificationWindow(HANDLE window);
	bool OnDeviceNotificationMessage(UINT nEventType, UINT* dwData);

protected:
	HANDLE m_Handle;
};

inline CGDataChannel::CGDataChannel()
{
	m_Handle = 0;
}

inline CGDataChannel::~CGDataChannel()
{
	Delete();
}

inline bool CGDataChannel::Create(const char* name, const UINT type, const int driverBufferSize, const int baudRate)
{
	if(m_Handle != 0)
	{
		Delete();
	}
	m_Handle = ::CreateDataChannel(name, type, driverBufferSize, baudRate);
	return m_Handle != 0;
}

inline void CGDataChannel::Delete()
{
	::DeleteDataChannel(m_Handle);
	m_Handle = 0;
}

inline bool CGDataChannel::IsCreated()
{
	return m_Handle != 0;
}

inline void CGDataChannel::Close()
{
	::CloseDataChannel(m_Handle);
}

inline bool CGDataChannel::IsOpened()
{
	return m_Handle != 0 && ::IsOpened(m_Handle);
}

inline bool CGDataChannel::SelectDevice(int deviceIndex)
{
	return ::SelectDevice(m_Handle, deviceIndex) != 0;
}

inline void CGDataChannel::SetNotification(const HWND hwnd, const UINT messageID, const HANDLE hEvent)
{
	::SetDataChannelNotification(m_Handle, hwnd, messageID, hEvent);
}

inline bool CGDataChannel::StartDataExchange(const UINT protocol, const UINT inputBlocks, const UINT outputBlocks, const UINT internalBlocks)
{
	return ::StartDataExchange(m_Handle, protocol, inputBlocks, outputBlocks, internalBlocks) != 0;
}

inline void CGDataChannel::StopDataExchange()
{
	::StopDataExchange(m_Handle);
}

inline bool CGDataChannel::GetReceivedData(BYTE* data, const int bufSize, int* dataSize)
{
	return ::GetReceivedData(m_Handle, data, bufSize, dataSize) != 0;
}

inline void CGDataChannel::SendData(const BYTE* data, const int dataSize)
{
	::SendData(m_Handle, (const BYTE*)data, dataSize);
}

inline void CGDataChannel::GetData(char* data)//���������� ������ ��
{
	BYTE dataBufer[1024];
	int bufSize = 1024;	
	int dataSize;
	::GetReceivedData(m_Handle, dataBufer, bufSize, &dataSize);

	//HANDLE hSerial = m_Handle;
	//DWORD iSize;
	//char sReceivedChar;
	//int counter = 0;

	//while (true) {
	//	ReadFile(m_Handle, &sReceivedChar, 1, &iSize, 0);  // �������� 1 ����
	//	if (iSize > 0)
	//	{
	//		data[counter] = sReceivedChar;
	//		++counter;
	//	}
	//	if (sReceivedChar == '\r') break;
	//}
}

inline void CGDataChannel::GetChannelStatistic(struct CSChannelStatistic* result)
{
	::GetChannelStatistic(m_Handle, (int*)result);
}

inline int  CGDataChannel::EnumerateDevices(unsigned short vendorID, unsigned short productID)
{
	return ::EnumerateDevices(m_Handle, vendorID, productID);
}

inline bool CGDataChannel::GetNextDeviceName(BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength)
{
	return ::GetNextDeviceName(m_Handle, fromHead, nameBuffer, bufferLength) != 0;
}

inline bool CGDataChannel::GetNextSerialNumber(BOOL fromHead, wchar_t* nameBuffer, unsigned int bufferLength)
{
	return ::GetNextSerialNumber(m_Handle, fromHead, nameBuffer, bufferLength) != 0;
}

inline bool CGDataChannel::GetNextProductString(wchar_t* nameBuffer, unsigned int bufferLength)
{
	return ::GetNextProductString(m_Handle, nameBuffer, bufferLength) != 0;
}

inline bool CGDataChannel::SetDeviceNotificationWindow(HANDLE window)
{
	return ::SetDeviceNotificationWindow(m_Handle, window) != 0;
}

inline bool CGDataChannel::OnDeviceNotificationMessage(UINT nEventType, UINT* dwData)
{
	return ::OnDeviceNotificationMessage(m_Handle, nEventType, dwData) != 0;
}


#endif		/* __cplusplus */

#endif		/* _DCHANNEL_H_INCLUDED_ */