// MeasuredData.h: interface for the CGMeasuredData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEASUREDDATA_H__C6780B11_01D4_11D6_9B34_E19EAE5DC019__INCLUDED_)
#define AFX_MEASUREDDATA_H__C6780B11_01D4_11D6_9B34_E19EAE5DC019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>

// ����������� ����� ��� ����������, ��������� ���������� ���������

class CGInstrument;

class CGMeasuredData  
{
public:
	CGMeasuredData();
	virtual ~CGMeasuredData();

	virtual int GetSize() = 0;
	virtual bool IsEmpty() { return GetSize() == 0; }
	virtual void Clear() = 0;

	virtual void GetDataFromInstrument(CGInstrument* instrument) = 0;

	virtual bool IsMeasurementFinished() { return true; }

// ������ � ���������� ��������� ���������
	void SetInterval(double from, double to, double step);
	double GetFromValue() { return m_From; }
	double GetToValue() { return m_To; }
	double GetStepValue() { return m_Step; }

	CCriticalSection& GetCriticalSection() { return m_CriticalSection; }
protected:
// �������� � ������� ������ ���������� �������� �� X (�������� ��������� ���������)
	double m_From;
	double m_To;
	double m_Step;

// ����������� ������ ��� ���������� ������� � ����������� ����������
	CCriticalSection m_CriticalSection;
};

#endif // !defined(AFX_MEASUREDDATA_H__C6780B11_01D4_11D6_9B34_E19EAE5DC019__INCLUDED_)
