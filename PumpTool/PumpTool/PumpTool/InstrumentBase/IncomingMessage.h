// IncomingMessage.h: interface for the CGIncomingMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INCOMINGMESSAGE_H__D64EC7B4_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_)
#define AFX_INCOMINGMESSAGE_H__D64EC7B4_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

const int c_MaxInBufferSize = 128;

class CGDataChannel;

class CGIncomingMessage
{
public:
	CGIncomingMessage();
	virtual ~CGIncomingMessage();

	// ������ ���� ��������� � ������
	int  GetDataSize() const { return m_Size - 2; }
	int  GetDataSize_ASCII() { return m_Size; }

	// ������������� (������� �����) �������, �� �������� ������ ���������
	BYTE GetInstrumentID() const { return m_Buffer[0]; }
	// ������������� ���������
	BYTE GetMessageID() const { return m_Buffer[1]; }
	// ����� ����� ������ ���������
	const BYTE* GetData() const { return m_Buffer + 2; }
	BYTE  GetData(int index) const { return m_Buffer[2 + index]; }
	const BYTE* GetData_ASCII() const { return m_Buffer; }

	// ������� ��������� ���������� ���� ��� ������� ��������� � ������� >>
	void Skip(int bytes);

	// ������������� �������� >> ��� ������� ��������� � ������������ � �����
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, BYTE& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, short& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned short& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, int& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned int& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, long& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned long& value);
	friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, float& value);
	template<class T> friend CGIncomingMessage& operator >>(CGIncomingMessage& msg, T& value);
	// � �������� ���������� ������ ���������� �����������, �� ��� �������� ����������

	// ��������� ���������� ��������� �� ������ ������ ������ �������.
	// ���������� false ���� ����� ��� ����
	bool GetFromChannel(CGDataChannel& channel);

protected:
	BYTE m_Buffer[c_MaxInBufferSize];
	BYTE* m_CurrentPosition;

	// ������ ����� ���������
	int m_Size;

private:						// ������ �� �����������
	CGIncomingMessage(const CGIncomingMessage& init);
	CGIncomingMessage operator =(const CGIncomingMessage& other);

	void CopyTwoBytes(BYTE* to);
	void CopyFourBytes(BYTE* to);
};

inline void CGIncomingMessage::CopyTwoBytes(BYTE* to)
{
	*to++ = *m_CurrentPosition++;
	*to++ = *m_CurrentPosition++;
}

inline void CGIncomingMessage::CopyFourBytes(BYTE* to)
{
	*to++ = *m_CurrentPosition++;
	*to++ = *m_CurrentPosition++;
	*to++ = *m_CurrentPosition++;
	*to = *m_CurrentPosition++;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, BYTE& value)
{
	value = *msg.m_CurrentPosition++;
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, short& value)
{
	msg.CopyTwoBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned short& value)
{
	msg.CopyTwoBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, int& value)
{
	msg.CopyFourBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned int& value)
{
	msg.CopyFourBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, long& value)
{
	msg.CopyFourBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, unsigned long& value)
{
	msg.CopyFourBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, float& value)
{
	msg.CopyFourBytes((BYTE*)&value);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

template<class T>
inline CGIncomingMessage& operator >>(CGIncomingMessage& msg, T& value)
{
	memcpy(&value, msg.m_CurrentPosition, sizeof(T));
	msg.m_CurrentPosition += sizeof(T);
	ASSERT(msg.m_CurrentPosition - msg.m_Buffer <= msg.m_Size);
	return msg;
}

inline void CGIncomingMessage::Skip(int bytes)
{
	m_CurrentPosition += bytes;
	ASSERT(m_CurrentPosition - m_Buffer <= m_Size);
}

#endif // !defined(AFX_INCOMINGMESSAGE_H__D64EC7B4_FA09_11D5_9B2F_B22B5D0EC019__INCLUDED_)
