// MeasuredData.cpp: implementation of the CGMeasuredData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "Stend.h"
#include "MeasuredData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGMeasuredData::CGMeasuredData()
{
	m_From = 0.0;
	m_To = 0.0;
	m_Step = 0.0;
}

CGMeasuredData::~CGMeasuredData()
{

}

void CGMeasuredData::SetInterval(double from, double to, double step)
{
	m_From = from;
	m_To = to;
	m_Step = step;
}

