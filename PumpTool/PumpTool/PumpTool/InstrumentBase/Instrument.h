// Instrument.h: interface for the CGInstrument class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INSTRUMENT_H__CBEDF47F_FBCF_4839_A4D8_55BC6225FD73__INCLUDED_)
#define AFX_INSTRUMENT_H__CBEDF47F_FBCF_4839_A4D8_55BC6225FD73__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IncomingMessage.h"
#include "OutgoingMessage.h"
#include "DChannel.h"

// ���������� � ������ ������ �������� ���� �������.
struct CSVersionInfo
{
	BYTE Software;
	BYTE Hardware;
	BYTE Subversion;
	BYTE Year;
	BYTE Month;
	BYTE Day;
};

// ����������� ��������� �� ������.
struct CSErrorInfo
{
	UINT16 ErrorCode;
	BYTE   Type;
	BYTE   Length;
	UINT32 Value;
};


// ����������� ������� �����, ������������ ��� �������� ����� ����������
// ���� ��������

class CGInstrument  
{
	friend class CGMeasurementProgram;

public:
	CGInstrument();
	virtual ~CGInstrument();

// ���������, ����� ������� ����� ������������
	virtual void Create(CGDataChannel* dataChannel);

// ���������� ���������� ��������� ��� ������
	virtual void Initialize();

// ������� ��������� ����������� �� ������� ���������. ������ ����������������
// ��� ������� ����������� �������
	virtual bool ProcessIncomingMessage(CGIncomingMessage& message) = 0;

// ������� ���������� ��� ��������� ������������ ���������� ���������
// ����������� ��� � �������. ����� �������������� ��� ����������� ���������.
	virtual void DoOnSystemMessage() { };
// ������ ����� ������ �����, ��� ����������� ������ �����������, �����������
// ����� ����� ������������ ������ �� ������ ��������
	virtual void DoOnSystemMessage(CGIncomingMessage& message) { };

// �������� ������� ������ ���������� ������
	virtual void AskSerialNumber() { };
// �������� ������� ���������� � �������
	virtual void AskInstrumentInfo() { AskSerialNumber(); }
// ������ ������� �������� ���������� ������� (������ ��� ������ ��������� ��� �������)
	virtual void AskParameters() { };
// �������� ������� ������� ����������� ��� �������������� ���������� � ���������� ����������
	virtual void LockKeyboard(bool lock) { };

// ��������� ���������� � ������ � ������������ �������
	virtual int GetSerialNumber() = 0;
	virtual int GetSoftVersionNumber() = 0;
	virtual int GetHardVersionNumber() = 0;
// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
	virtual const CString GetShortName() const = 0;

// ��������� ����������� ���������
	virtual bool GetMeasuredValues(double values[]) = 0;
// ���������, ��� � ������� ��������� ���������� �������� (������ ��������� ��������������)
// ������������ ��� ������� ������ ����������� ����� ����� � �������������
	virtual bool IsInstrumentReady() { return true; }

// �������� ������� ������� ������ ��������� ������
	virtual bool ResetErrorState() = 0;

// � ��������� ��������� (��������, �� ����������� ��������) ����������
// ����� ����������� ��������� ������ ���������
	virtual bool IsMeasurementStartEnabled() { return true; };

// ��� �������� � ���������� �������� ��������� - ���������� ��� ���������/�������
	virtual void AskMeasurementFrequency() { };
	virtual void SetMeasurementFrequency(int frequency) { };

public:
	BYTE GetNetAddress()    { return m_NetAddress; }
	BYTE GetInstrumentId()  { return m_InstrumentId; }

	virtual int GetInstrumentType() { return m_InstrumentType; }
	virtual int GetInstrumentModel() { return m_InstrumentModel; }

	// ����� ������� ��������� �������� �������. m_InstrumentType ����������� ��-�������
	// �������������� � ������ ��������.
	virtual bool IsSpectrofluorimeter() { return false; }
	virtual bool IsSpectrophotometer()  { return false; }
	virtual bool IsSpectral()           { return false; }

	int GetMeasurementState() { return m_MeasurementState; }
	int GetInstrumentState() { return m_InstrumentState; }
	int GetErrorCode() { return m_ErrorCode; }
	// ����������� ���������� �� ������ ����������� ������ � ����� ��������.
	virtual CSErrorInfo*  GetLastErrorInfo() { return NULL; }

	int GetMeasurementFrequency() { return m_MeasurementFrequency; }

	CGDataChannel* GetDataChannel() { return m_DataChannel; }

	void ResetInstrumentState();
	void ResetError();

public:
// ��������� ����� ������� (� �������� �������)
	int GetCurrentTime();
// ����� ��������� (� �������� �������)
	int GetMeasurementTime();
// ����� ��������� ��� ������������� (� ��������)
	double GetMeasurementTimeInSecs();

protected:
// ������������� ������� � ������������ � ���������� ����������� ������.
// ������������ ��� ������� ����, �� ������ ������� ������ ���������. 
	BYTE m_NetAddress;
// �������������, ����������� ��������� ��������� ���������� ��������, �������������
// ����� ����������.
	BYTE m_InstrumentId;

// ��������� �������. � ����� ������ ����� ������� ��������. ��������� ��������
// ��������� ����� ���������� ���������� �� ���������������� ���������� ���������
// ��������� ��������� �������
	int m_InstrumentState;

// ��������� �������� ��������� � ������������ � ������������� ����������� �������
	int m_MeasurementState;

// ��� ������ ������� ��� ���������, ��������� ����������� �� ��.
// � ����� - ����.
	int m_ErrorCode;

// ����� ������������ ��� ������ ������� ����� �������� � �����������
	CGDataChannel* m_DataChannel;

protected:
// ��� ������� �� �������� ������ (��������, ���������� � ��).
	int m_InstrumentType;
// �������������, ���������� ��������������� ������������ ������ �������.
	int m_InstrumentModel;

protected:
// ���������� ����� ������� � ��������� ��������
	int m_CurrentTime;
// ����� ������ ��������� � ��������� ��������
	int m_StartMeasurementTime;
// ����� ������ ���� ���������, ������������ ��� ������������������ ����������
	int m_StartStepTime;
// ����� ������ ����������������� ��������� (������������ ��� ��������� �������������
// �� ����� ������������� ���������)
	int m_SavedStartMeasurementTime;
	
// ������� ������������ ��������� (��)
	int m_MeasurementFrequency;
// ����������� �������� ���������� ������ ������� � �������
	double m_InternalTimeUnitToSec;
};

// ������ ������������ ������������� ���������� ����, ���������� ��� ���������
// ��������� RTTI � ������ �����������. ������� ������ CGInstrument �� �������
// ��-�� ������� �����������.
// ������������ ��� ��������� ���������� ����������� ����������� ��������
template<class T>
inline T* DynamicCast(CGInstrument* instrument)
{
//	return true;
	if(instrument != 0 && T::IsInstrumentModelEqual(instrument))
		return (T*)instrument;
	return 0;
}

// ���� ��������� ���������, ����� ��� ���� ��������
enum
{
	c_PassiveState,					// ��������� ��������� �� �����������
	c_ReadyState,					// ������ ��������� ��������� ��� ���������
	c_NormalState,					// ����������� ���������� ������ (��� ����������)
	c_PausedState,					// ���������� ���������
	c_WaitingWaveLengthState,		// �������� ��������� ����� �����
	c_WaitingMultipleEventsState,	// �������� ���������� �������, ����������� ���������� ���������
	c_FirstUnusedMeasurementState	// ����� �������������� ��� ������� ���������
									// ����������� ��� ����������� �������
};

// ���� ��������� ��������, ����� ��� ���� ��������
enum
{
	c_InstrumentRestarted = 1,			// ��������� ��� ����� �������
	c_InstrumentInfoChanged,			// �������� ���������� � ��������� ������ �������
	c_InstrumentErrorReset,				// �� ������� ���������� ����� ��������� �� ������
	c_ExternalMeasurementStart,			// ������ ����� ������ ������ ���������
	c_ExternalMeasurementStop,			// ������ ����� ������ ��������� ���������
	c_MeasurementSuspended,				// ��������� �������������� ��� ���������� ������� ���������
	c_ProgramStageFinished,				// ���������� ����� ��������� ���������
	c_FirstUnusedInstrumentState		// ������� � ������� ���� ���������
										// ��� ����������� �������
};

// ���� �������� �� �������� ����������� �������
enum
{
	c_InstrumentTypeUnknown,
	c_InstrumentTypePhotometric,
	c_InstrumentTypeFluorimetric,
	c_FirstUnusedInstrumentType
};

// ��������� - �������, ���������� ���������� ������������� ������������ �������
// ��������� �������� ������ ������������ � ���������� ����������� �������
enum
{
	c_InstrumentFamilyUnknown,
	c_FirstUnusedInstrumentFamily
};

// ������ �������� ������ ������������ � ���������� ����������� �������
enum
{
	c_InstrumentModelUnknown,
	c_FirstUnusedInstrumentModel
};

// ���������� ����, ������������ �� ��������������� ���������.
typedef short			 INT16;
typedef unsigned short	UINT16;
// INT32 � UINT32 ���������� � ����������� ������������ ������;

#endif // !defined(AFX_INSTRUMENT_H__CBEDF47F_FBCF_4839_A4D8_55BC6225FD73__INCLUDED_)
