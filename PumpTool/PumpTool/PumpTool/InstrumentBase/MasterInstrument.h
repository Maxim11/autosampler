// MasterInstrument.h: interface for the CGMasterInstrument class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MASTERINSTRUMENT_H__AF195424_2AE7_4E9F_97CA_F96D2153389D__INCLUDED_)
#define AFX_MASTERINSTRUMENT_H__AF195424_2AE7_4E9F_97CA_F96D2153389D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Instrument.h"

#include <afxmt.h>

// �������� ���������� ������������� ������ ������
struct CGDataChannelInfo
{
	UINT m_ChannelType;
	int m_BufferSize;
	int m_BaudRate;
	UINT m_ProtocolType;
	int m_InputBlocks;
	int m_OutputBlocks;
	int m_InternalBlocks;

	// ��� ������ �� USB
	//short m_UsbVendorID;
	//short m_UsbProductID;
	// ����� ���������� ���������� � ������ ���������
	int   m_UsbEnumerationIndex;
	// ����� ����������, ���������� �� �����������.
	CString m_UsbSerialNumber;
	// � ������ ������������� ������ � ����������� ���������
	// ������ ���� ��������� DataChannel ����� ���������� �����������
	// � ����������� ���������.
	bool  m_IsMaster;
	// ������� �� ������������� ������������� ����� � ������ �����������
	// � ������� ��������. ��������� ��� ������ ������������ �
	// ����������� ����������� ������������ (���� ��� ������ ������ � ���������).
	bool  m_AutoConnect;

	CGDataChannelInfo();
};

inline CGDataChannelInfo::CGDataChannelInfo()
{
	// ��� ������������ ������ ������ � ������������ ��������.
	m_IsMaster = true;
	m_AutoConnect = true;
}

// ���������� � ��������� � ������� �����, ���������� �� ��������
// �� ����� ������������, ���������� � ���������� ����. ����������
// � ��������������� �����
struct CGInstrumentCommInfo
{
	UINT16 m_DataPacketFormatError;			// �������������� ���� �������
	UINT16 m_DataPacketCrcError;
	UINT16 m_AckPacketCrcError;
	UINT16 m_ProtocolBufferFill;			// ���������� ������ ���������
	UINT16 m_ProtocolBufferOverflowError;
	UINT16 m_RepeatedSendCounter;			// ������� ��������� ������� ������ ������
	UINT16 m_ProtocolBufferFillLrt;			// ���������� ������ ��������� ��� ��������� ��������� �������
	UINT16 m_WriteBufferMinFreeSpace;		// ����������� ��������� ����� � ������ ��������
	UINT16 m_WriteBufferOverflowError;
	UINT16 m_SynchronizationCounter;		// ������� ������������� ���������
};


class CGMeasurementProgram;
class CGMeasuredData;

// ����������� ������� �����, ������������ ��� ��������
// ������ ������ �������, ������������ ��������������� � �����������

class CGMasterInstrument : public CGInstrument  
{
// ������� ������������� � ����������� ���� ��������� �� ����������
// ����� ������ InstrumentMessageProcessingLoop()
	friend UINT MeasurementThreadExecute(LPVOID param);

// ������� ������������� � ���� ������� ������������� ��������
// ��������� �� ���������� ����� ������ SoftTimerProcessingLoop()
	friend UINT SoftTimerThreadExecute(LPVOID param);

public:
	CGMasterInstrument();
	virtual ~CGMasterInstrument();

	virtual bool Create();
	virtual bool InitializeDataChannel(const CString& portName, const CGDataChannelInfo& dci);
	virtual void CloseDataChannel();
	// ������������� ��� ��������� ����� � ��������.
	virtual void OnConnectionEstablished() {};

	// ���������, ��� ����� ������ ������ ��� ������
	bool IsDataChannelCreated();
	// ���������, ��� ����� ������ ����� �������� �������� ������
	bool IsDataChannelOpened();

	virtual void SetMeasurementProgram(CGMeasurementProgram* program);
	virtual void SetMeasuredData(CGMeasuredData* data);
	CGMeasurementProgram* GetMeasurementProgram();
	CGMeasuredData* GetMeasuredData();

	void SetWindowMessageID(CWnd* window, int id);
	void SetUsbProductID(unsigned short vendorID, unsigned short productID);

// ������� �� ����� � �������� � ������ ������
	bool IsInstrumentConnected();
// C���� � �������� ���� ����������� ����� ������������� ����������������� �����
	bool IsConnectionEstablished();
// ������ ����������� ����� USB-����
	bool UseUsbConnection();
// ��������� ������ ��� ������, ������� ��������� ���������
	bool IsInstrumentValid();
// ������ ������������ ��������� �� �������� ���������
	bool IsProgramRunning();

	// ����� (�������������) �������, ���������� �� ������ �������
	BYTE GetMasterAddress() { return m_MasterNetAddress; }
	// ����� ������� �� USB-�����������.
	int  GetUsbSerialNumber();
	// ����� ������� � ���� ����� ���������� �� ������������.
	// BYTE GetNetAddress()    { return m_NetAddress; }

	// ��������� ���� �� �������� (������� ���� CAN).
	void AddSlaveInstrument(CGInstrument* instrument);
	void RemoveSlaveInstruments();

	// ���� � ��� �� ������ ����� ���� ��������������� � �������, � �����������.
	bool IsMaster() { return m_DataChannel == &m_MasterDataChannel; }

	// ������ � ���������� � ��������� ����� � ��������, ���������� �� �������
	int GetInstrumentCommFormatError() { return m_InstrumentCommInfo.m_DataPacketFormatError; }
	int GetInstrumentCommCrcError() { return m_InstrumentCommInfo.m_DataPacketCrcError; }
	const CGInstrumentCommInfo* GetInstrumentCommInfo() { return &m_InstrumentCommInfo; }

	// ���������� ��� ��������� ������ ������������ USB - ���������
	bool OnDeviceChange(UINT nEventType, UINT* dwData, const CGDataChannelInfo& dci);

public:
	// ���������/��������� ���������� ��������� � �����
	void EnableLogFile(const CString& fileName, bool append = false);
	void DisableLogFile();
	bool IsLogFileEnabled();
	CString GetLogFileName();
	// ������ � ���� ����������, ����������� � ������� ����� � ������.
	void WriteSavedLogData(const CString& fileName);
	// ������ ������������ ���������� � ���� ��������� ������
	void WriteDebugInfo(const CString& string);

protected:
	// ����� ����������� ���������� ��������� � ������������ � ������� �������
	virtual bool DispatchIncomingMessage(CGIncomingMessage& message);

	// ����������� ������� �������� ���������� ����������. �������� ���������
	// ������� ��������� ���� ��������� (��� ������� ��������) ��� �����������
	// � ����������.
	virtual void OnGetNewData();
	virtual void OnInstrumentStateChanged(CGInstrument* instrument);
	virtual void OnErrorState(CGInstrument* instrument);
	virtual void OnProgramFinished(CGInstrument* instrument);

	// ���������� ����� ������ ���������� �������(0.1 ���) �� ���� �������
	virtual void OnSoftTimerEvent();

protected:
	// �������� ���������, ��� ������������� ���������� � ����
	void SendOutgoingMessage(const CGOutgoingMessage& message, const CString* logString = 0);
	void SendOutgoingMessage(const char* message);
	void ReplyFromComPort(char* replyData);
	// ���������� ��������� ��������� � ����
	void SaveIncomingMessage(CGIncomingMessage& message, const CString* logString = 0);
// ����� ���� ���� ���� ���������
	CString DumpMessage(const CGIncomingMessage& message, const int from = 0);

protected:
	// �������� ���� ����������� ����
	UINT InstrumentMessageProcessingLoop();

	// �������� ���� ���� ������� ������������� ��������
	UINT SoftTimerProcessingLoop();

protected:
	// ������/������� ���� �������
	bool StartSoftTimerThread();
	bool StopSoftTimerThread(bool checkProtocol = true);

private:
	// ������� �� ������ ���������� ��� �������� ����������
	virtual void Create(CGDataChannel* dataChannel);

protected:
	// ������������� �������� ����������, ���������� �� ���������� ���������
	BYTE m_MasterNetAddress;

	// ����� ������ ������� � �����������.
	CGDataChannel m_MasterDataChannel;

// ��� ��������� ������ � ��������
	int m_ProtocolType;
	bool m_IsModernProtocol;

	// ��� ������ �� USB.
	unsigned short m_UsbVendorID;
	unsigned short m_UsbProductID;
// ���������� USB - ��������� ������� ����, ������������ � ���������� 
	int m_NumberOfUsbDevices;
// ��� ���������� USB - ����������. �������� ��������� �����.
	CString m_UsbDeviceName;

// ����������� ����� � ��������
	bool m_IsConnected;
// ������� ����������, �� ������� �� ������ ��������� �� �������
	int m_TimeoutCounter;

	CGInstrumentCommInfo m_InstrumentCommInfo;

// ������ ���������, ������������ � ���� � ������� �������
	CPtrList m_SlaveInstrumentsList;

// ��������� ���������
	CGMeasurementProgram* m_MeasurementProgram;
// ��������� ��� ����������� ���������
	CGMeasuredData* m_MeasuredData;

// ������������� ���������, ����������� ����� ��������� ����� ������ ��� ������
	int m_WindowMessageID;
// ����, �������� ���������� ���������
	CWnd* m_Window;

// ����, ����������� ��� ���������� ��������
	CWinThread* m_InstrumentMessageProcessingThread;
	CWinThread* m_SoftTimerThread;
	
// ����������� �� ������ ������ � ����������� ������ ������
	CEvent m_DataChannelEvent;
// ������� ����, ����������� ��������� ������ �� �������
	CEvent m_StopThreadEvent;

protected:
// ������� ����������� ������ ���� �������
	bool m_IsSoftTimerThreadRunning;

// ������� ������� �������
	UINT m_SoftTimerCounter;

protected:
// ���� ��� ���������� ��������� ������ ������� ����� ����������� � ��������
//	std::ofstream m_LogFile;
	CStdioFile m_LogFile;
// ����������� ������ ��� ���������� ������� � ����� ���������
	CCriticalSection m_CriticalSection;
	// ������ ����� ��� ��������������� ���������������� ��� ������.
	CStringArray m_LogDataArray;
	// �������, ����������� ��� ����������� ������ � ��������.
	int m_LogDataCounter;
	// ������� ����� ����� ������ ��� �������������� ���������� ����.
	int m_AfterErrorCounter;
};

enum
{
	c_InstrumentEventNewData,
	c_InstrumentEventStateChanged,
	c_InstrumentEventError,
	c_InstrumentEventProgramFinished,
};

#endif // !defined(AFX_MASTERINSTRUMENT_H__AF195424_2AE7_4E9F_97CA_F96D2153389D__INCLUDED_)
