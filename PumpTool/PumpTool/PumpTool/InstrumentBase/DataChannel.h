// DataChannel.h: interface for the CDataChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATACHANNEL_H__4954226F_95D5_4138_A506_724E78EC1194__INCLUDED_)
#define AFX_DATACHANNEL_H__4954226F_95D5_4138_A506_724E78EC1194__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef INSTRUMENT_DISABLED

#include "DChannel.h"

#else

// ��� ������� ������ � �������� (� ����-������) ���������� ����� � ���������-����������
#define DCT_SERIAL_PORT		0x100
#define DCT_NAMED_PIPE_SERVER	0x200
#define DCT_NAMED_PIPE_CLIENT	0x400

#define DCP_SIMPLE		0x1
#define DCP_COM7BIT		0x2
#define DCP_TRANSPORT		0x4

class CGDataChannel  
{
public:
	CGDataChannel() {};
	virtual ~CGDataChannel() {};

	bool Create(const char* name, const UINT type = DCT_SERIAL_PORT, const int driverBufferSize = 4096, const int baudRate = CBR_19200) { return true; }
	bool IsCreated() { return true; }
	void Close() {};
	void SetNotification(const HWND hwnd, const UINT messageID, const HANDLE hEvent = 0) {};

	bool StartDataExchange(const UINT protocol = DCP_COM7BIT, const UINT inputBlocks = 20, const UINT outputBlocks = 10, const UINT internalBlocks = 0) { return true; }
	void StopDataExchange() {};

	bool GetReceivedData(BYTE* data, const int bufSize, int* dataSize) { return false; }
	void SendData(const BYTE* data, const int dataSize) {};

//	void GetChannelStatisitic(struct CSChannelStatistic* result) {};
};

#endif

#endif // !defined(AFX_DATACHANNEL_H__4954226F_95D5_4138_A506_724E78EC1194__INCLUDED_)
