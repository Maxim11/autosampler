// MeasurementProgram.h: interface for the CGMeasurementProgram class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEASUREMENTPROGRAM_H__6D8656C4_0069_11D6_9B33_9CBBD042C319__INCLUDED_)
#define AFX_MEASUREMENTPROGRAM_H__6D8656C4_0069_11D6_9B33_9CBBD042C319__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Instrument.h"

class CGMeasurementProgram  
{
public:
	CGMeasurementProgram(CGInstrument* instrument = 0);
	virtual ~CGMeasurementProgram();

// ������ � ������� ���������
	virtual void Start();
	virtual void Stop();

// ������� ���������, �������������� ��������
	virtual void StopByInstrument();

// ���������� ��������� � �����������
	virtual void Pause();
	virtual void Continue();

// ���������� ���������� ���� ��������� (������ ����� ���������� ���������
// ��������� �����)
	virtual void PerformNextStep();
// ���������� ���������� ���� (����� �������������� ��� ��������� ������)
	virtual void RepeatLastStep();

// ��������� ������ � ��������� ���������
	virtual void OnErrorState() { };
// �������� ��������� ������� � ��������� ���������
	virtual void OnMessageReceived(int messageCode) { };

// ��������� ��������� � ������������ ��������
	virtual void SetInstrument(CGInstrument* instrument) { m_Instrument = instrument; }

// ������� ��������� ��������� ���������
	bool IsRunning();
	bool IsPaused();
	bool IsStarting();

// ��������� ��������� ���������
	void SetMeasurementState(int state);
// ��������� ��������� �������
	void SetInstrumentState(int state);

	int CalculateNumberOfSteps(double from, double to, double step);
	int GetProgress();

	CGInstrument* GetInstrument() { return m_Instrument; }

protected:
// ������, ������� �������������� ����������
	CGInstrument* m_Instrument;

// ������� ��������� ��������� ���������
	int  m_State;
	bool m_IsRunning;

// ����� ������������ ���� ���������
	int m_CurrentProgramStep;
	int m_NumberOfSteps;
};

enum
{
	c_ProgramIsStopped,
	c_ProgramIsRunning,
	c_ProgramIsPaused,
	c_ProgramIsStarting,
};

#endif // !defined(AFX_MEASUREMENTPROGRAM_H__6D8656C4_0069_11D6_9B33_9CBBD042C319__INCLUDED_)
