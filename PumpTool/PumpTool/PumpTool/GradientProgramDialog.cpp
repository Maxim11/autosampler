// ChromatographicProgramsDialog.cpp : implementation file
//

#include "stdafx.h"
//#include "Panorama.h"
#include "resource.h"
#include "GradientProgramDialog.h"

#include "Colors.h"
#include "Service.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CMGradientProgramDialog dialog

CString CMGradientProgramDialog::sm_StageBufferString;
enum
{
	c_StageIndex,
	c_StageFrom,
	c_StageTo,
	c_StageComment,
	c_StagePumpA_Begin,
	c_StagePumpA_End,
	c_StagePumpB_Begin,
	c_StagePumpB_End,
};

const TCHAR c_DefaultFixedString[] = _T("1000;1;20;1000");

const int c_MinWorkFlowRate = 1;
const int c_MaxWorkFlowRate = 2000;
const int c_MinConditioningTime = 1;
const int c_MaxConditioningTime = 100;
const int c_MinStartPressure = 0;
const int c_MaxStartPressure = 20;
const int c_MinStartFlowRate = 1;
const int c_MaxStartFlowRate = 2000;

CMGradientProgramDialog::CMGradientProgramDialog(CWnd* pParent, bool timeInMinutes)
	: CMCommonProgramDialog(CMGradientProgramDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMGradientProgramDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_TimeInMinutes = timeInMinutes;
	m_IsProgramSelectionDisabled = false;

// ������� �� ������ �������������
	m_FirstEnabledColumn = 2;
}


BEGIN_MESSAGE_MAP(CMGradientProgramDialog, CMCommonProgramDialog)
	//{{AFX_MSG_MAP(CMGradientProgramDialog)
	ON_EN_KILLFOCUS(IDC_EDIT_FLOW_RATE, OnWorkFlowRateKillFocus)
	ON_EN_KILLFOCUS(IDC_EDIT_CONDITIONING_TIME, OnTimeKillFocus)
	ON_EN_KILLFOCUS(IDC_EDIT_START_PRESSURE, OnStartPressureKillFocus)
	ON_EN_KILLFOCUS(IDC_EDIT_START_FLOW_RATE, OnStartFlowRateKillFocus)
	//ON_CBN_SELENDOK(IDC_COMBO_TIME_UNITS, OnSelectTimeUnits)
	//ON_COMMAND(IDC_CHECK_CYCLE, OnCheckCycleButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMGradientProgramDialog message handlers

BOOL CMGradientProgramDialog::OnInitDialog()
{
	CMCommonProgramDialog::OnInitDialog();
	InitEluentComboBox(IDC_COMBO_ELUENT_A, 0);
	InitEluentComboBox(IDC_COMBO_ELUENT_B, 1);

// ������������� ������� � ����������� ���������
	m_ProgramParametersGrid.SubclassDlgItem(IDC_LIST_GRID_CTRL, this);

	m_ProgramParametersGrid.CreateTable(8, 0);
	m_ProgramParametersGrid.SetFirstColumnButtonStyle();

	m_ProgramParametersGrid.SetColumnInfo(c_StageIndex,   IDS_HEADER_STAGE, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_StageFrom,    IDS_HEADER_FROM, 60);
	m_ProgramParametersGrid.SetColumnInfo(c_StageTo,      IDS_HEADER_TO, 60);
	m_ProgramParametersGrid.SetColumnInfo(c_StageComment, IDS_HEADER_COMMENT, 120);
	m_ProgramParametersGrid.SetColumnInfo(c_StagePumpA_Begin, IDS_HEADER_PUMP_A_BEGIN, 60);
	m_ProgramParametersGrid.SetColumnInfo(c_StagePumpA_End,   IDS_HEADER_PUMP_A_END, 60);
	m_ProgramParametersGrid.SetColumnInfo(c_StagePumpB_Begin, IDS_HEADER_PUMP_B_BEGIN, 60);
	m_ProgramParametersGrid.SetColumnInfo(c_StagePumpB_End,   IDS_HEADER_PUMP_B_END, 60);
	m_ProgramParametersGrid.ResizeColumns(true);
	m_ProgramParametersGrid.EnableColumnsResizing(false);

	m_ProgramParametersGrid.SetHeaderTipID(c_StageIndex, IDS_TIP_HEADER_STAGE);
	m_ProgramParametersGrid.SetHeaderTipID(c_StageFrom,  IDS_TIP_HEADER_TIME_MIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_StageTo,    IDS_TIP_HEADER_TIME_MIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_StageComment,     IDS_TIP_HEADER_COMMENT);
	m_ProgramParametersGrid.SetHeaderTipID(c_StagePumpA_Begin, IDS_TIP_HEADER_PUMP_A_BEGIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_StagePumpA_End,   IDS_TIP_HEADER_PUMP_A_END);
	m_ProgramParametersGrid.SetHeaderTipID(c_StagePumpB_Begin, IDS_TIP_HEADER_PUMP_B_BEGIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_StagePumpB_End,   IDS_TIP_HEADER_PUMP_B_END);
	m_ProgramParametersGrid.EnableToolTips();

// ������� "��" ���������� ��� ��������������
	m_ProgramParametersGrid.SetEnabledColumns(2, 5);
	m_ProgramParametersGrid.SetValidator(2, &m_FloatValidator);
	m_ProgramParametersGrid.SetValidator(4, &m_IntegerValidator);
	m_ProgramParametersGrid.SetValidator(5, &m_IntegerValidator);

	m_ProgramNamesGrid.SetSelectionColor(c_ColorBlack, c_ColorTurquoise);
	m_ProgramParametersGrid.SetSelectionColor(c_ColorBlack, c_ColorTurquoise);
// ������������� ��������� ���������
	m_TotalFlowRateEdit.SetValidator(&m_IntegerValidator);
	m_TotalFlowRateEdit.SubclassDlgItem(IDC_EDIT_FLOW_RATE, this);
	m_TotalFlowRateEdit.LimitText(5);

	m_ConditioningTimeEdit.SetValidator(&m_IntegerValidator);
	m_ConditioningTimeEdit.SubclassDlgItem(IDC_EDIT_CONDITIONING_TIME, this);
	m_ConditioningTimeEdit.LimitText(4);

	m_StartPressureEdit.SetValidator(&m_FloatValidator);
	m_StartPressureEdit.SubclassDlgItem(IDC_EDIT_START_PRESSURE, this);
	m_StartPressureEdit.LimitText(5);

	m_StartFlowRateEdit.SetValidator(&m_IntegerValidator);
	m_StartFlowRateEdit.SubclassDlgItem(IDC_EDIT_START_FLOW_RATE, this);
	m_StartFlowRateEdit.LimitText(5);

// ������ ���������� ����� ������������� m_ProgramParametersGrid
	int index = m_ProgramNamesGrid.FindRow(m_ActiveProgramName);
	if(index < 0)
		index = 0;

	m_ProgramNamesGrid.SetSelection(index);

// ������ ���������� ����� m_ProgramNamesGrid.SetSelection()
	EnableNameButtons();

	if(m_IsProgramSelectionDisabled)
	{
		m_ProgramNamesGrid.EnableWindow(false);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// ���������� ��������� ����� �������� ����������.
void CMGradientProgramDialog::ShowToolTipMessage(const int stringID, CWnd* control)
{
	CRect rect;
	control->GetWindowRect(&rect);

	CPoint point(rect.right+10, rect.top);
	CString text;
	text.LoadString(stringID);
	CMCommonProgramDialog::ShowToolTipMessage(text, point);
}

void CMGradientProgramDialog::CheckIntLimits(CWnd* editControl, int minLimit, int maxLimit)
{
	CString text;
	editControl->GetWindowText(text);

	int value = StringToInt(text);
	if(value > maxLimit)
	{
		IntToString(maxLimit, text);
		editControl->SetWindowText(text);
		ShowToolTipMessage(IDS_INFO_REPLACED_BY_MAX, editControl);
	}
	else if(value < minLimit || text.IsEmpty())
	{
		IntToString(minLimit, text);
		editControl->SetWindowText(text);
		ShowToolTipMessage(IDS_INFO_REPLACED_BY_MIN, editControl);
	}
}

void CMGradientProgramDialog::CheckFloatLimits(CWnd* editControl, int minLimit, int maxLimit)
{
	CString text;
	editControl->GetWindowText(text);

	int value = int(StringToDouble(text)*10.0+0.5);
	if(value > maxLimit)
	{
		text.Format(_T("%.1f"), maxLimit*0.1);
		editControl->SetWindowText(text);
		ShowToolTipMessage(IDS_INFO_REPLACED_BY_MAX, editControl);
	}
	else if(value < minLimit || text.IsEmpty())
	{
		text.Format(_T("%0.1f"), minLimit*0.1);
		editControl->SetWindowText(text);
		ShowToolTipMessage(IDS_INFO_REPLACED_BY_MIN, editControl);
	}
}

void CMGradientProgramDialog::OnWorkFlowRateKillFocus()
{
	CheckIntLimits(&m_TotalFlowRateEdit, c_MinWorkFlowRate, c_MaxWorkFlowRate);
}

void CMGradientProgramDialog::OnTimeKillFocus()
{
	CheckIntLimits(&m_ConditioningTimeEdit, c_MinWorkFlowRate, c_MaxWorkFlowRate);
}

void CMGradientProgramDialog::OnStartPressureKillFocus()
{
	CheckFloatLimits(&m_StartPressureEdit, c_MinStartPressure, c_MaxStartPressure);
}

void CMGradientProgramDialog::OnStartFlowRateKillFocus()
{
	CheckIntLimits(&m_StartFlowRateEdit, c_MinWorkFlowRate, c_MaxWorkFlowRate);
}

// ����� ��������� ��� �������� ����������
void CMGradientProgramDialog::GetTipText(CString& tipText, int controlID) const
{
/*
	switch(controlID)
	{
	case IDC_COMBO_PROGRAM_STROBE_DELAY:
		tipText.Format(IDC_COMBO_PROGRAM_STROBE_DELAY, g_Spectrofluorimeter.GetStandardStrobeDelay());
		break;
	case IDC_COMBO_PROGRAM_STROBE_DURATION:
		tipText.Format(IDC_COMBO_PROGRAM_STROBE_DURATION, g_Spectrofluorimeter.GetStandardStrobeDuration());
		break;
	default:
		tipText.LoadString(controlID);
	}
*/
		tipText.LoadString(controlID);
}

void CMGradientProgramDialog::ProgramToTable(int programIndex)
{
	CString programName = m_ProgramNamesGrid.GetItemText(programIndex, 0);
	CGProgramData programData;
	m_IsProgramModified = false;
	if(!programName.IsEmpty()
		&& (m_ChangedProgramContainer.Lookup(programName, programData)
		|| m_MainProgramContainer->Lookup(programName, programData)) )
	{
		m_FixedString = programData.GetFixedParameters();
		m_EluentA = programData.GetEluentA();
		m_EluentB = programData.GetEluentB();

		if(programData.IsEmpty())
		{
			CreateEmptyTable();
		}
		else
		{
			programData.SetToGridCtrl(&m_ProgramParametersGrid, 2);
			FillFromTimeColumn();

			if(m_FixedString.IsEmpty())
			{
				//m_FixedString = _T("1000;1;20;1000");
				m_FixedString = c_DefaultFixedString;
			}
			SetFixedFields(m_FixedString);
			GetDlgItem(IDC_COMBO_ELUENT_A)->SetWindowText(m_EluentA);
			GetDlgItem(IDC_COMBO_ELUENT_B)->SetWindowText(m_EluentB);

			if(!programData.IsValid())
				CMCommonProgramDialog::ShowErrorMessage(programData, false);
		}
	}
	else
	{
		CreateEmptyTable();
		m_TimeInMinutes = true;
	}

	EnableParametersButtons();
}

void CMGradientProgramDialog::TableToProgram(int programIndex)
{
	CString programName = m_ProgramNamesGrid.GetItemText(programIndex, 0);
	CGProgramData programData;
	if(programName.IsEmpty())
		return;

	CString fixedString;
	CString eluentA, eluentB;
	GetFixedFields(fixedString);
	GetDlgItem(IDC_COMBO_ELUENT_A)->GetWindowText(eluentA);
	GetDlgItem(IDC_COMBO_ELUENT_B)->GetWindowText(eluentB);

// ������� ������� � ������� ����������� ���������, ��� "��� ������������"

	if(!m_IsProgramModified && fixedString == m_FixedString
		&& eluentA == m_EluentA && eluentB == m_EluentB)
		return;

	programData.SetFixedParameters(fixedString);
	programData.SetEluentA(eluentA);
	programData.SetEluentB(eluentB);
	programData.GetFromGridCtrl(&m_ProgramParametersGrid, c_StageTo);
	programData.CheckGradientData();

	m_ChangedProgramContainer.SetAt(programName, programData);

	m_ProgramNamesGrid.SetImage(programIndex, programData.GetCheckResult());
	m_ProgramNamesGrid.Update(programIndex);
}

void CMGradientProgramDialog::SetFixedFields(const CString& fromString)
{
	CStringArray stringArray;
	ParseString(fromString, ';', stringArray);

	if(stringArray.GetSize() > 0)
	{
		m_TotalFlowRateEdit.SetWindowText(stringArray[0]);
	}
	if(stringArray.GetSize() > 1)
	{
		m_ConditioningTimeEdit.SetWindowText(stringArray[1]);
	}
	if(stringArray.GetSize() > 2)
	{
		CString& text = stringArray[2];
		if(text != _T("0"))
		{
			// ������ ������������ �� ������.
			int length = text.GetLength();
			text += text[length-1];
			text.SetAt(length-1, _T('.'));
		}
		m_StartPressureEdit.SetWindowText(text);
	}
	if(stringArray.GetSize() > 3)
	{
		m_StartFlowRateEdit.SetWindowText(stringArray[3]);
	}
}

void CMGradientProgramDialog::GetFixedFields(CString& toString)
{
	CString text;
	double p;
	m_TotalFlowRateEdit.GetWindowText(text);
	toString += text+c_Delimiter;
	m_ConditioningTimeEdit.GetWindowText(text);
	toString += text+c_Delimiter;
	m_StartPressureEdit.GetWindowText(text);
	p = StringToDouble(text);
	text = IntToString(int(p*10.0+0.5));
	toString += text+c_Delimiter;
	m_StartFlowRateEdit.GetWindowText(text);
	toString += text+c_Delimiter;
}

// ���������� ��� ���������� ������ � �������
void CMGradientProgramDialog::DoOnAddRow(int row, bool emptyRow)
{
	CString string;

	if(m_ProgramParametersGrid.GetItemCount() == 1)
	{
		m_TotalFlowRateEdit.GetWindowText(string);
		if(string.IsEmpty())
		{
			// ��� �������� ����� ��������� ��������� ����� ����.
			SetFixedFields(c_DefaultFixedString);
		}
	}

	if(row == 0)
	{
		m_ProgramParametersGrid.SetItemText(0, 1, _T("0"));
	}
	else
	{
		string = m_ProgramParametersGrid.GetItemText(row-1, c_StageTo);
		m_ProgramParametersGrid.SetItemText(row, c_StageFrom, string);
		string = m_ProgramParametersGrid.GetItemText(row-1, c_StagePumpA_End);
		m_ProgramParametersGrid.SetItemText(row, c_StagePumpA_Begin, string);
		string = m_ProgramParametersGrid.GetItemText(row-1, c_StagePumpB_End);
		m_ProgramParametersGrid.SetItemText(row, c_StagePumpB_Begin, string);
	}

	if(emptyRow)
	{
	}
	else
	{
		m_ProgramParametersGrid.SetRowContent(row, sm_StageBufferString, 2);

		string = m_ProgramParametersGrid.GetItemText(row, 2);
		m_ProgramParametersGrid.SetItemText(row+1, 1, string);
	}
}

// ���������� ��� �������� ������ �� �������
void CMGradientProgramDialog::DoOnDeleteRow(int row)
{
	if(row == 0)
	{
		m_ProgramParametersGrid.SetItemText(0, c_StageFrom, _T("0"));
	}
	else if(row < m_ProgramParametersGrid.GetItemCount())
	{
		CString string = m_ProgramParametersGrid.GetItemText(row, c_StageFrom);
		m_ProgramParametersGrid.SetItemText(row-1, c_StageTo, string);
	}
}

void CMGradientProgramDialog::DoOnEndCellEditing(int row, int column, const CString& cellText)
{
	int nOfRows = m_ProgramParametersGrid.GetItemCount();

	m_RowIsAutoAdded = false;

	if(column == c_StageTo && row+1 < nOfRows)
	{
// �������� ���� "��" ��� ��������� ���� "��"
		m_ProgramParametersGrid.SetItemText(row+1, c_StageFrom, cellText);
	}
	else
	{
		if(column == c_StagePumpA_Begin || column == c_StagePumpA_End)
		{
			CString text = m_ProgramParametersGrid.GetItemText(row, column);
			int value = StringToInt(text);
			if(value >= 100 || text.IsEmpty())
			{
				m_ProgramParametersGrid.SetItemText(row, column, _T("100"));
				m_ProgramParametersGrid.SetItemText(row, column+2, _T("0"));
			}
			else
			{
				IntToString(100-value, text);
				m_ProgramParametersGrid.SetItemText(row, column+2, text);
			}
		}
		if(column == c_StagePumpA_End)
		{
			if(m_ProgramParametersGrid.GetCurrentRow() < 0 && row+1 == nOfRows)
			{
				// ������������� ��������� ����� ���� ��� ��������� �������������� ���������� ����������.
				AddRow(true, true, true);

				m_RowIsAutoAdded = true;
			}
		}
		/*		
				text.Format("%d", nOfRows+1);
				m_ProgramParametersGrid.InsertItem(nOfRows, text);
				text = m_ProgramParametersGrid.GetItemText(row, 2);
				m_ProgramParametersGrid.SetItemText(row+1, 1, text);
				m_ProgramParametersGrid.EditCellText(row+1, 2);
		*/
	}
}

// ���������� ��������� � ������������ �������� ���������
void CMGradientProgramDialog::ShowErrorMessage(int number, const CString& errorText, bool editItem)
{
	CRect rect;

// ������ � ������� ������
/*
	switch(number)
	{
	case 0:
		m_StrobeDelayComboBox.GetWindowRect(rect);
		if(editItem)
			m_StrobeDelayComboBox.SetFocus();
		break;
	case 1:
		m_StrobeDurationComboBox.GetWindowRect(rect);
		if(editItem)
			m_StrobeDurationComboBox.SetFocus();
		break;
	case 2:
		m_TotalTimeEdit.GetWindowRect(rect);
		if(editItem)
			m_TotalTimeEdit.SetFocus();
		break;
	}
		*/

	CMCommonProgramDialog::ShowToolTipMessage(errorText, CPoint(rect.right+10, rect.top));
}

// ���������� ������� �� � ������� ������������������ ���������
void CMGradientProgramDialog::FillFromTimeColumn()
{
	int size = m_ProgramParametersGrid.GetItemCount();

	m_ProgramParametersGrid.SetItemText(0, c_StageFrom, _T("0"));
	for(int i = 1; i < size; i++)
	{
		m_ProgramParametersGrid.SetItemText(i, c_StageFrom, m_ProgramParametersGrid.GetItemText(i-1, c_StageTo));
	}
}

void CMGradientProgramDialog::InitEluentComboBox(const int comboBoxID, int selectItem)
{
	CString text;
	CComboBox* eluentComboBox = (CComboBox*)GetDlgItem(comboBoxID);
	for(int i = 0; i < 10; i++)
	{
		text.LoadString(IDS_TEXT_ELUENT_0+i);
		eluentComboBox->AddString(text);
	}

	eluentComboBox->SetCurSel(selectItem);
}


// ������������� ���������� ������� �� � �� � ������������ � �������� ���������
/*
void CMGradientProgramDialog::ConvertTimeUnits(bool secondsToMinutes)
{
// ������������� ������� "��". "��" ��������������� � ���������� �������
	CString text;
	for(int i = 0; i < m_ProgramParametersGrid.GetItemCount(); i++)
	{
		text = m_ProgramParametersGrid.GetItemText(i, 2);
		if(secondsToMinutes)
			MinutesToString(atof(text)/60.0, text);
		else
			text.Format("%.0f", atof(text)*60.0);

		m_ProgramParametersGrid.SetItemText(i, 2, text);
	}
}
*/

