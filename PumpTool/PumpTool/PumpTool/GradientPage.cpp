#include "stdafx.h"
#include "resource.h"
#include "GradientPage.h"

#include "Instrument/Piton.h"
//#include "ValveCloseDialog.h"
//#include "PressureReleasingDialog.h"
#include "GradientProgramDialog.h"
#include "GradientWaitDialog.h"

#include "Colors.h"
#include "Service.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

static const TCHAR c_RegistrySection[] = _T("Gradient");
static const TCHAR c_DefaultProgramFileName[] = _T("GradientPrograms.ini");

IMPLEMENT_DYNCREATE(CMGradientPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMGradientPage property page

const int c_InvalidStep = 0xFFFF;

CMGradientPage::CMGradientPage() : CMToolPage(CMGradientPage::IDD)
{
	/*
	m_Volumes[0] = 35000;
	m_Volumes[1] = 35000;
	m_Volumes[2] = 0;
	m_FlowRates[0] = 10000;
	m_FlowRates[1] = 10000;
	m_FlowRates[2] = 1000;
*/
	m_IsWorking = false;

	m_GradientWaitDialog = 0;
	m_IsInWaitState = false;

	m_StageDuration = 0;
}

CMGradientPage::~CMGradientPage()
{
	if(m_GradientWaitDialog != 0)
	{
		delete m_GradientWaitDialog;
	}
}

void CMGradientPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

/*
// ���� ������ ������ Mode.
enum
{
  MONO_MODE_INIT  = 0x08,
};
*/
BEGIN_MESSAGE_MAP(CMGradientPage, CMToolPage)
	ON_BN_CLICKED(IDC_CHECK_PUMP_A, &CMGradientPage::OnBnClickedCheckPumpA)
	ON_BN_CLICKED(IDC_CHECK_PUMP_B, &CMGradientPage::OnBnClickedCheckPumpB)
	ON_BN_CLICKED(IDC_CHECK_START_FILL_A, &CMGradientPage::OnBnClickedCheckStartFill)
	ON_BN_CLICKED(IDC_CHECK_START_DISCHARGE_A, &CMGradientPage::OnBnClickedCheckStartDischarge)
	ON_BN_CLICKED(IDC_CHECK_START_WORK_A, &CMGradientPage::OnBnClickedCheckStartWork)
	ON_BN_CLICKED(IDC_CHECK_VOLUME_FILL_A, &CMGradientPage::OnBnClickedCheckVolume)
	//ON_BN_CLICKED(IDC_CHECK_START_STOP, &CMGradientPage::OnBnClickedCheckStartStop)
	//ON_BN_CLICKED(IDC_RADIO_FILL, &CMGradientPage::OnBnClickedRadioFill)
	//ON_BN_CLICKED(IDC_RADIO_DISCHARGE, &CMGradientPage::OnBnClickedRadioDischarge)
	//ON_BN_CLICKED(IDC_RADIO_WORK, &CMGradientPage::OnBnClickedRadioWork)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_PROGRAM, &CMGradientPage::OnBnClickedButtonEdit)
	ON_CBN_SELCHANGE(IDC_COMBO_PROGRAM_NAME, OnComboBoxSelectProgram)
	ON_COMMAND(ID_COMMAND_START, OnCommandStart)
	ON_BN_CLICKED(IDC_CHECK_RESTART, &CMGradientPage::OnBnClickedCheckRestart)
	ON_EN_CHANGE(IDC_EDIT_PRESSURE_A, &CMGradientPage::OnEnChangeEditPressureA)
END_MESSAGE_MAP()

BOOL CMGradientPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();
	m_VolumeComboBox.SetValidator(&m_IntegerValidator);
	m_VolumeComboBox.SubclassDlgItem(IDC_COMBO_VOLUME_FILL_A, this);
	m_VolumeComboBox.LimitText(5);

	//m_VolumeBComboBox.SetValidator(&m_IntegerValidator);
	//m_VolumeBComboBox.SubclassDlgItem(IDC_COMBO_VOLUME_B, this);
	//m_VolumeBComboBox.LimitText(5);

	CButton* button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_A);
	button->SetCheck(true);
	button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_B);
	button->SetCheck(true);

	LoadFromRegistry();

	m_SelectedMode = PUMP_MODE_FILL;
	//m_VolumeAComboBox.SetWindowText(_T("35000"));
	//m_VolumeBComboBox.SetWindowText(_T("35000"));
	//GetDlgItem(IDC_EDIT_MODE_FLOW_RATE)->SetWindowText(_T("10000"));
	//GetDlgItem(IDC_EDIT_MODE_VOLUME)->SetWindowText(_T("35000"));
	//GetDlgItem(IDC_EDIT_START_PRESSURE)->SetWindowText(_T("20"));
	//GetDlgItem(IDC_EDIT_START_FLOW_RATE)->SetWindowText(_T("1000"));

	//GetDlgItem(IDC_CHECK_START_STOP)->EnableWindow(false);

	CString savedName = AfxGetApp()->GetProfileString(c_RegistrySection, _T("EluentA"), _T(""));
	InitEluentsComboBox(IDC_COMBO_ELUENT_A, savedName, 1);
	savedName = AfxGetApp()->GetProfileString(c_RegistrySection, _T("EluentB"), _T(""));
	InitEluentsComboBox(IDC_COMBO_ELUENT_B, savedName, 0);


	// �������� ����� �� �����������.
	CWnd* timeField = GetDlgItem(IDC_STATIC_TIME_A);
	CreateLargeFont(timeField);
	timeField->SetFont(&m_LargeFont);
	timeField->SetWindowText(_T(""));

	timeField = GetDlgItem(IDC_STATIC_TIME_B);
	timeField->SetFont(&m_LargeFont);
	timeField->SetWindowText(_T(""));

	// �������������� ���������� ������ ���� ��������.
	m_ProgramNamesComboBox.SubclassDlgItem(IDC_COMBO_PROGRAM_NAME, this);
// ������� ������ �������
	if(m_ProgramNamesImageList.GetSafeHandle() == 0)
		m_ProgramNamesImageList.Create(IDB_CHECK_MARKS, 16, 1, RGB(255, 255, 255));
	m_ProgramNamesComboBox.SetImageList(&m_ProgramNamesImageList);
// ����������� ����� ������ ����������� ������
	m_ProgramNamesComboBox.SetDroppedWidth(m_ProgramNamesComboBox.GetDroppedWidth()*2);

	CreateProgramTable();

	LoadProgramFile();

	FillProgramNamesComboBox();

	// ���������� ������ �������� ���������� ��� ���������� ������ "�����".
	//ShowControls(false);

	return result;
}

BOOL CMGradientPage::OnSetActive()
{
	BOOL result = CMFCPropertyPage::OnSetActive();

	//bool isConnected = m_PumpB->IsConnectionEstablished() || m_PumpB->IsCanSlave();
	//GetDlgItem(IDC_CHECK_START_STOP)->EnableWindow(isConnected);

	return result;
}

BOOL CMGradientPage::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN && (pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_CANCEL))
	{
		StopCurrentMode(m_GradientProgram.IsRunning());
		// ���������� ��� ����������� ��������� � ����������� � ������.
		UpdatePageHeader();
		return TRUE;
	}
	
	return CMToolPage::PreTranslateMessage(pMsg);
}


void CMGradientPage::FillProgramNamesComboBox()
{
	CString programName;

	int currentSelection = 0;
	if(m_ProgramNamesComboBox.GetCount() != 0)
	{
		currentSelection = m_ProgramNamesComboBox.GetCurSel();
		m_ProgramNamesComboBox.ResetContent();
	}

	POSITION pos = m_MainProgramContainer.GetSortedNames()->GetHeadPosition();
	while(pos)
	{
		programName = m_MainProgramContainer.GetSortedNames()->GetNext(pos);
		m_ProgramNamesComboBox.AddString(programName);

		CGProgramData programData;
		m_MainProgramContainer.Lookup(programName, programData);
		int index = m_ProgramNamesComboBox.FindString(-1, programName);
		m_ProgramNamesComboBox.SetImage(index, programData.GetCheckResult());
	}

	if(m_ProgramNamesComboBox.GetCount() == 0)
	{
		m_CurrentProgramName.Empty();
	}
	else if(m_CurrentProgramName.IsEmpty())
	{
		m_ProgramNamesComboBox.SetCurSel(0);
	}
	else
	{
		if(m_ProgramNamesComboBox.SelectString(-1, m_CurrentProgramName) == CB_ERR)
		{
			if(m_ProgramNamesComboBox.SetCurSel(currentSelection) == CB_ERR)
			{
				m_ProgramNamesComboBox.SetCurSel(0);
			}
		}
	}

	ShowProgramData();
}

void CMGradientPage::SetPumpASerialNumber(int serialNumber)
{
	CMToolPage::SetPumpSerialNumber(serialNumber, IDS_CONTROL_PUMP_A, IDC_STATIC_PUMP_A);
	m_EluentASerialNumberKey.Format(_T("EluentA_%d"), serialNumber);

	CString eluent = AfxGetApp()->GetProfileString(_T("Settings"), m_EluentASerialNumberKey, _T(""));
	if(!eluent.IsEmpty())
	{
		GetDlgItem(IDC_COMBO_ELUENT_A)->SetWindowText(eluent);
	}
}

void CMGradientPage::SetPumpBSerialNumber(int serialNumber)
{
	CMToolPage::SetPumpSerialNumber(serialNumber, IDS_CONTROL_PUMP_B, IDC_STATIC_PUMP_B);
	m_EluentBSerialNumberKey.Format(_T("EluentB_%d"), serialNumber);

	CString eluent = AfxGetApp()->GetProfileString(_T("Settings"), m_EluentBSerialNumberKey, _T(""));
	if(!eluent.IsEmpty())
	{
		GetDlgItem(IDC_COMBO_ELUENT_B)->SetWindowText(eluent);
	}
}


void CMGradientPage::ShowValuesPumpA(bool show)
{
	const int c_PumpControls[] =
	{
		IDC_EDIT_FLOW_RATE_A,
		IDC_EDIT_VOLUME_A,
		IDC_EDIT_PRESSURE_A,
		IDC_PROGRESS_PUMP_A,
		IDC_STATIC_PERCENTS_A,
		IDC_STATIC_TIME_A
	};
	if(show)
	{
		ShowValuesForPump(m_PumpA, c_PumpControls);
	}
	else
	{
		ResetValuesForPump(c_PumpControls);
		if(m_GradientProgram.IsRunning())
		{
			StopCurrentMode(true);
		}
	}
	if(show && m_StageDuration > 0)
	{
		int time = m_PumpA->GetCurrentModeTime();
		int percents = time*100/m_StageDuration;
		CString text;
		text.Format(_T("  %d"), percents);
		m_HeaderInfoString = m_HeaderInfoBegin + text + CString('%');
		UpdatePageHeader();
	}
}

void CMGradientPage::ShowValuesPumpB(bool show)
{
	const int c_PumpControls[] =
	{
		IDC_EDIT_FLOW_RATE_B,
		IDC_EDIT_VOLUME_B,
		IDC_EDIT_PRESSURE_B,
		IDC_PROGRESS_PUMP_B,
		IDC_STATIC_PERCENTS_B,
		IDC_STATIC_TIME_B
	};
	if(show)
	{
		ShowValuesForPump(m_PumpB, c_PumpControls);
	}
	else
	{
		ResetValuesForPump(c_PumpControls);
		if(m_GradientProgram.IsRunning())
		{
			StopCurrentMode(true);
		}
	}
}

void CMGradientPage::OnBnClickedCheckPumpA()
{
	CButton* buttonA = (CButton*)GetDlgItem(IDC_CHECK_PUMP_A);
	CButton* buttonB = (CButton*)GetDlgItem(IDC_CHECK_PUMP_B);
	if(buttonA->GetCheck() == 0)
	{
		if(buttonB->GetCheck() == 0)
		{
			buttonB->SetCheck(true);
		}
	}
}

void CMGradientPage::OnBnClickedCheckPumpB()
{
	CButton* buttonA = (CButton*)GetDlgItem(IDC_CHECK_PUMP_A);
	CButton* buttonB = (CButton*)GetDlgItem(IDC_CHECK_PUMP_B);
	if(buttonB->GetCheck() == 0)
	{
		if(buttonA->GetCheck() == 0)
		{
			buttonA->SetCheck(true);
		}
	}
}

void CMGradientPage::CheckPumpsState()
{
	bool isReady = (m_PumpB->IsReady() && m_PumpA->IsReady());

	// ������ ��������� ���������� ��������.
	if(isReady && m_IsWorking)
	{
		CButton* startButton = 0;
		switch(sm_CurrentMode)
		{
		case PUMP_MODE_FILL:
			startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
			break;
		case PUMP_MODE_DISCHARGE:
			startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
			break;
		case PUMP_MODE_WORK:
			if(!m_GradientProgram.IsWaitingStageActive())
			{
				startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
			
				m_ProgramParametersGrid.SetSelection(-1);
				m_ProgramParametersGrid.SetSelectionEnabled(false);
			}
			break;
		}
		if(startButton != 0)
		{
			startButton->SetCheck(false);
			EnableControls(true);
			EnableOtherPages(true);

			// ���������� ����������� �������� � ������������ ����.
			((CButton*)GetDlgItem(IDC_CHECK_PUMP_A))->SetCheck(true);
			((CButton*)GetDlgItem(IDC_CHECK_PUMP_B))->SetCheck(true);
			((CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A))->SetCheck(false);

			sm_CurrentMode = PUMP_MODE_WAIT;
		}
		if(!m_HeaderInfoString.IsEmpty())
		{
			m_HeaderInfoString.Empty();
			UpdatePageHeader();
		}
	}
	m_IsWorking = !isReady;

#if 0
	if(m_PumpA->IsInPressureReleasingMode() || m_PumpB->IsInPressureReleasingMode())
	{
		// ���������� �������� ��� ��������� ������ ����� ��������� �����.
		if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
		{
			m_PressureReleasingDialog->SetProgressPosition(m_PumpA->GetCurrentPressure());
		}
		if(sm_CurrentMode == PUMP_MODE_DISCHARGE)
		{
			sm_CurrentMode = PUMP_MODE_PRESSURE_RELEASING;
			ShowPressureReleasingDialog(m_PumpA->GetCurrentPressure(), 0, IDS_HEADER_AUTO_RELEASING);
		}
/*
		if(m_PumpA->GetCurrentPressure() <= m_Settings.m_MaxPressureDischarge)
		{
			if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
			{
				m_PressureReleasingDialog->EndDialog(IDOK);
				m_PressureReleasingDialog->DestroyWindow();
			}
			m_PumpA->StopCurrentMode();
			m_PumpB->StopCurrentMode();
		}
		else
		{
			if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
			{
				m_PressureReleasingDialog->SetProgressPosition(m_PumpA->GetCurrentPressure());
			}
		}
*/
	}

	if(m_PumpA->IsPressureReleased() && m_PumpB->IsPressureReleased())
	{
		// ���������� "����������" ���������.
		m_PumpA->StopCurrentMode();
		m_PumpB->StopCurrentMode();

		//EnableModeButtons(true);
		if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
		{
			m_PressureReleasingDialog->EndDialog(IDOK);
			m_PressureReleasingDialog->DestroyWindow();
		}

#if !USE_MODAL_P_R_DIALOG
		// �������� ����� ��������� ����� ��������.
		if(m_SelectedMode == PUMP_MODE_FILL)
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
			startButton->SetCheck(true);
			OnBnClickedCheckStartFill();
		}
		if(m_SelectedMode == PUMP_MODE_DISCHARGE)
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
			startButton->SetCheck(true);
			OnBnClickedCheckStartDischarge();
		}
#else
		// ��� ��������� ������� ����� ��� �������� �����������
		// ���������� ����������� ������� ������ ������ ������.
#endif
	}
#endif

	if(m_IsInWaitState)
	{
		if(!m_GradientProgram.IsWaitingStageActive())
		{
			CloseGradientWaitDialog();
			m_IsInWaitState = false;
		}
	}
	else
	{
		if(m_GradientProgram.IsWaitingStageActive())
		{
			ShowGradientWaitDialog();
			m_IsInWaitState = true;
		}
	}

	if(m_GradientProgram.IsRunning())
	{
		if(!m_GradientProgram.IsStartStageActive())
		{
			GetDlgItem(IDC_BUTTON_EDIT_PROGRAM)->EnableWindow(false);
		}
	}
	else
	{
		GetDlgItem(IDC_BUTTON_EDIT_PROGRAM)->EnableWindow(true);
	}
}

/*
void CMGradientPage::ShowControls(bool forWorkMode)
{
	GetDlgItem(IDC_STATIC_PROGRAM)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_COMBO_PROGRAM)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_STATIC_FLOW_RATE)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_EDIT_WORK_FLOW_RATE)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_STATIC_WORK_FLOW_RATE_U)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_BUTTON_EDIT)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_LIST_GRID_CTRL)->ShowWindow(forWorkMode);

	GetDlgItem(IDC_STATIC_MODE_VOLUME_A)->ShowWindow(!forWorkMode);
	m_VolumeComboBox.ShowWindow(!forWorkMode);
	GetDlgItem(IDC_STATIC_MODE_VOLUME_U_A)->ShowWindow(!forWorkMode);

	GetDlgItem(IDC_STATIC_MODE_VOLUME_B)->ShowWindow(!forWorkMode);
	m_VolumeBComboBox.ShowWindow(!forWorkMode);
	GetDlgItem(IDC_STATIC_MODE_VOLUME_U_B)->ShowWindow(!forWorkMode);

}
	*/
void CMGradientPage::EnableControls(bool enable)
{
	//GetDlgItem(IDC_RADIO_FILL)->EnableWindow(enable);
	//GetDlgItem(IDC_RADIO_DISCHARGE)->EnableWindow(enable);
	//GetDlgItem(IDC_RADIO_WORK)->EnableWindow(enable);

	//m_VolumeAComboBox.EnableWindow(enable);
	//m_VolumeBComboBox.EnableWindow(enable);
	/*
	if(enable)
	{
		GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(m_SelectedMode == PUMP_MODE_FILL);
		GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(m_SelectedMode == PUMP_MODE_FILL);
	}
	else
	{
		GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(enable);
		GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(enable);
	}
	*/
	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
	if(button->GetCheck() != 0)
	{
		m_VolumeComboBox.EnableWindow(enable);
	}
	button->EnableWindow(enable);

	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(enable);
	GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(enable);

	GetDlgItem(IDC_CHECK_PUMP_A)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_PUMP_B)->EnableWindow(enable);

	GetDlgItem(IDC_CHECK_START_FILL_A)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_START_DISCHARGE_A)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_START_WORK_A)->EnableWindow(enable);
	// ��������� ����� �� ������ (������ ��� ������), � ��������� ����� ������.
	if(enable)
	{
		GetDlgItem(IDC_COMBO_PROGRAM_NAME)->EnableWindow(true);
		GetDlgItem(IDC_BUTTON_EDIT_PROGRAM)->EnableWindow(true);
	}

	// ���������� ��� ���������� ��������� ������� Escape.
	SetFocus();
}

void CMGradientPage::ShowGradientWaitDialog()
{
	if(m_GradientWaitDialog == 0)
	{
		m_GradientWaitDialog = new CMGradientWaitDialog;
	}
	if(m_GradientWaitDialog->GetSafeHwnd() == 0)
	{
		m_GradientWaitDialog->Create(IDD_DIALOG_GRADIENT_WAIT, this);
	}
	m_GradientWaitDialog->ShowWindow(true);
	m_GradientWaitDialog->CenterWindow();
}

void CMGradientPage::CloseGradientWaitDialog()
{
	if(m_GradientWaitDialog && m_GradientWaitDialog->GetSafeHwnd() != 0)
	{
		m_GradientWaitDialog->DestroyWindow();
	}
}

void CMGradientPage::SetNewMode(int mode)
{
	//m_Volumes[m_SelectedMode] = m_VolumeComboBox.GetIntValue();
	//m_FlowRates[m_SelectedMode] = m_FlowRateComboBox.GetIntValue();

	m_SelectedMode = mode;
	//m_VolumeComboBox.SetIntValue(m_Volumes[m_SelectedMode]);
	//m_FlowRateComboBox.SetIntValue(m_FlowRates[m_SelectedMode]);
}

#if 0
void CMGradientPage::OnBnClickedRadioFill()
{
	ShowControls(false);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(true);
	SetNewMode(PUMP_MODE_FILL);
}

void CMGradientPage::OnBnClickedRadioDischarge()
{
	ShowControls(false);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(false);
	SetNewMode(PUMP_MODE_DISCHARGE);
}

void CMGradientPage::OnBnClickedRadioWork()
{
	ShowControls(true);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_ELUENT_B)->EnableWindow(false);
	SetNewMode(PUMP_MODE_WORK);

	CString text;
	GetDlgItem(IDC_COMBO_ELUENT_A)->GetWindowText(text);
	AfxGetApp()->WriteProfileString(c_RegistrySection, _T("EluentA"), text);
	GetDlgItem(IDC_COMBO_ELUENT_B)->GetWindowText(text);
	AfxGetApp()->WriteProfileString(c_RegistrySection, _T("EluentB"), text);
}

void CMGradientPage::OnBnClickedCheckStartStop()
{
	CString text;
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_STOP);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);
		m_PumpB->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpB->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		sm_CurrentMode = m_SelectedMode;

		int infoStringID = 0;
		switch(m_SelectedMode)
		{
		case PUMP_MODE_FILL:
			m_VolumeAComboBox.AddCurrentString();
			m_VolumeBComboBox.AddCurrentString();
			StartFillMode();
			infoStringID = IDS_INFO_FILL;
			//m_PumpA->StartFillMode(flowRate, targetVolumeA);
			//m_PumpB->StartFillMode(flowRate, targetVolumeB);
			break;
		case PUMP_MODE_DISCHARGE:
			m_VolumeAComboBox.AddCurrentString();
			m_VolumeBComboBox.AddCurrentString();
			StartDischargeMode();
			infoStringID = IDS_INFO_DISCHARGE;
			//m_PumpA->StartDischargeMode(flowRate, targetVolumeA);
			//m_PumpB->StartDischargeMode(flowRate, targetVolumeB);
			break;
		case PUMP_MODE_WORK:
			StartWorkMode();
			break;
		}
		if(sm_IsRunning)
		{
			if(infoStringID > 0)
			{
				m_HeaderInfoString.LoadString(infoStringID);
			}
		}
		else
		{
			m_HeaderInfoString.Empty();
		}
	}
	else
	{
		switch(m_SelectedMode)
		{
		case PUMP_MODE_FILL:
		case PUMP_MODE_DISCHARGE:
			m_PumpA->StopCurrentMode();
			m_PumpB->StopCurrentMode();
			break;
		case PUMP_MODE_WORK:
			m_GradientProgram.Stop();
			break;
		}
		sm_IsRunning = false;

		SetStartButtonText(IDS_CONTROL_START);
		EnableControls(true);
		m_HeaderInfoString.Empty();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}
#endif

bool CMGradientPage::StartFillMode()
{
	bool canStart = StartPumpMode(PUMP_MODE_FILL, m_Settings.m_MaxPressureDischarge, m_Settings.m_ReleasingFlowRate);
	// ��� ������ ��������� �������� � ������ ������� ����.
	if(canStart)
	{
		CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
		int targetVolume = c_MaxPumpVolume;
		bool usePumpA, usePumpB;
		bool fillWithoutDischarge = false;
		if(button->GetCheck() != 0)
		{
			int value = m_VolumeComboBox.GetIntValue();
			if(value > 0)
			{
				targetVolume = value;
				fillWithoutDischarge = (targetVolume <= 2000);
			}
		}

		button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_A);
		usePumpA = (button->GetCheck() != 0);
		button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_B);
		usePumpB = (button->GetCheck() != 0);

		if(usePumpA)
		{
			m_PumpA->StartSmartFill(c_FillFlowRate, targetVolume, fillWithoutDischarge);
		}
		if(usePumpB)
		{
			m_PumpB->StartSmartFill(c_FillFlowRate, targetVolume, fillWithoutDischarge);
		}
		if(usePumpA || usePumpB)
		{
			sm_CurrentMode = PUMP_MODE_FILL;
			return true;
		}
	}

	return false;
}

bool CMGradientPage::StartDischargeMode()
{
	// ��� ������ ��������� �������� � ������ ������� ����.
	bool canStart = StartPumpMode(PUMP_MODE_DISCHARGE, m_Settings.m_MaxPressureDischarge, m_Settings.m_ReleasingFlowRate);
	if(canStart)
	{
		CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
		int targetVolume = c_MaxPumpVolume;
		bool usePumpA, usePumpB;
		if(button->GetCheck() != 0)
		{
			int value = m_VolumeComboBox.GetIntValue();
			if(value > 0)
			{
				targetVolume = value;
			}
		}

		button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_A);
		usePumpA = (button->GetCheck() != 0);
		button = (CButton*)GetDlgItem(IDC_CHECK_PUMP_B);
		usePumpB = (button->GetCheck() != 0);

		if(usePumpA)
		{
			m_PumpA->StartDischargeMode(c_DischargeFlowRate, targetVolume);
		}
		if(usePumpB)
		{
			m_PumpB->StartDischargeMode(c_DischargeFlowRate, targetVolume);
		}
		if(usePumpA || usePumpB)
		{
			sm_CurrentMode = PUMP_MODE_DISCHARGE;
			return true;
		}
	}

	return false;
}

bool CMGradientPage::StartWorkMode()
{
	if(m_PumpA->IsExternalValveOn() || m_PumpB->IsExternalValveOn())
	{
		AfxMessageBox(IDS_MB_CHECK_VALVE_STATE);
		return false;
	}
/*
	// ��� ������ ������ ������� ����.
	CMValveCloseDialog dialog(this, false);
	if(dialog.DoModal() == IDCANCEL)
	{
		CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
		startButton->SetCheck(false);
		return false;
	}
*/
	if(GetGradientProgram())
	{
		m_PumpA->SetGradientProgram(&m_GradientProgram);
		m_GradientProgram.Start(m_PumpA, m_PumpB);
		SelectNextProgramStage();

		sm_CurrentMode = PUMP_MODE_WORK;
		EnableControls(false);
		return true;
	}

	return false;
}

void CMGradientPage::StopCurrentMode(bool isWorkMode)
{
	if(isWorkMode)
	{
		m_GradientProgram.Stop();
	}
	else
	{
		m_PumpA->StopCurrentMode();
		m_PumpB->StopCurrentMode();
	}
	//sm_IsRunning = false;
	sm_CurrentMode = PUMP_MODE_WAIT;
	// ��������� ����� ��������� ����������.
	m_StageDuration = 0;

	EnableControls(true);
	EnableOtherPages(true);

	// ������� �������� ����� ���� ����������, ������� ��������� ������ ����������.
	((CButton*)GetDlgItem(IDC_CHECK_START_FILL_A))->SetCheck(false);
	((CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A))->SetCheck(false);
	((CButton*)GetDlgItem(IDC_CHECK_START_WORK_A))->SetCheck(false);

	m_HeaderInfoString.Empty();
}


void CMGradientPage::StartPressureReleasing(int flowRate)
{
	m_PumpA->StartPressureReleasingMode(flowRate);
	m_PumpB->StartPressureReleasingMode(flowRate);
}

void CMGradientPage::OnBnClickedButtonEdit()
{
	CMGradientProgramDialog	dialog(this);
	dialog.SetContainer(&m_MainProgramContainer);
	dialog.SetActiveProgramName(m_CurrentProgramName);
	// ���� ��������� ��� ��������, ��������� ����� ������ ���������.
	dialog.DisableProgramSelection(m_GradientProgram.IsRunning());
	if(dialog.DoModal() == IDOK)
	{
		// ��� �������� ��������� ����� ���������� ��� ��������������
		//m_ChromatographicProgramPanel.SetCurrentProgramName(dialog.GetActiveProgramName());

		if(m_MainProgramContainer.IsModified())
		{
			// ����� ���������� ���������� ���������
			SaveProgramFile();

			FillProgramNamesComboBox();
			ShowProgramData();
			if(m_GradientProgram.IsRunning())
			{
				// ��������� ����� ���� �������� ������ �� ����� �������.
				if(GetGradientProgram())
				{
					m_GradientProgram.Restart();
				}
			}
		}
	}
}

void CMGradientPage::OnComboBoxSelectProgram()
{
	ShowProgramData();
}

void CMGradientPage::SaveToRegistry()
{
	CWinApp* app = AfxGetApp();
	CString string;

	m_VolumeComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("FillVolume"), string);
/*
	m_VolumeAComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("VolumeA"), string);

	m_VolumeBComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("VolumeB"), string);
*/
}

void CMGradientPage::SaveEluentInfo()
{
	// ���������� �� ������� � ���������� ������.
	if(!m_EluentASerialNumberKey.IsEmpty())
	{
		CString string;
		GetDlgItem(IDC_COMBO_ELUENT_A)->GetWindowText(string);
		AfxGetApp()->WriteProfileString(_T("Settings"), m_EluentASerialNumberKey, string);
	}
	if(!m_EluentBSerialNumberKey.IsEmpty())
	{
		CString string;
		GetDlgItem(IDC_COMBO_ELUENT_B)->GetWindowText(string);
		AfxGetApp()->WriteProfileString(_T("Settings"), m_EluentBSerialNumberKey, string);
	}
}


void CMGradientPage::LoadFromRegistry()
{
	CWinApp* app = AfxGetApp();
	CString string;

	string = app->GetProfileString(c_RegistrySection, _T("FillVolume"), _T(""));
	m_VolumeComboBox.GetFromString(string);

/*
	string = app->GetProfileString(c_RegistrySection, _T("VolumeA"), _T("35000;"));
	m_VolumeAComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("VolumeB"), _T("35000;"));
	m_VolumeBComboBox.GetFromString(string);
*/
}

//const CString c_DefaultProgramDirectory = _T("Programs");
// ������ � ������ �������� ���������
void CMGradientPage::CreateExeFolderName(CString& name)
{
	int index;
	name = CString(::GetCommandLine());
	if(name[0] == _T('\"'))
	{
		name = name.Mid(1);
	}
	index = name.Find(_T(".exe"));
	if(index > 0)
	{
		name = name.Left(index);
	}
	index = name.ReverseFind(_T('\\'));
	if(index > 0)
	{
		name = name.Left(index+1);
	}
}

void CMGradientPage::LoadProgramFile()
{
	CString defaultFolder;
	CreateExeFolderName(defaultFolder);
	defaultFolder += CString(_T("Templates"));
/*
	CString folderName2 = AfxGetApp()->GetProfileString(_T("Folders"), _T("Templates"), _T(""));
// ������������ ��� �������� ��� ������ ��������
	m_ProgramFileName = AfxGetApp()->GetProfileString("Folders", "Programs", "");
	if(m_ProgramFileName.IsEmpty())
	{
		m_ProgramFileName = __argv[0];
		int begin = m_ProgramFileName.ReverseFind('\\');
		m_ProgramFileName = m_ProgramFileName.Left(begin+1)+c_DefaultProgramDirectory;
	}
*/
// �������� ������� ���� �������
	::CreateDirectory(defaultFolder, 0);

// ������������ ��� ����� ���������
	m_ProgramFileName = defaultFolder + _T('\\') + c_DefaultProgramFileName;

	m_MainProgramContainer.SetFileName(m_ProgramFileName);
	m_MainProgramContainer.LoadFromUniversalDataFile();

/*
	std::ifstream file(m_ProgramFileName);
	file >> m_MainProgramContainer;
	if(file.bad() && showErrorMessages)
	{
		CString error;
		error.Format(IDS_ERROR_READING_FILE, m_ProgramFileName);
		AfxMessageBox(error);
		return;
	}

	if(!m_MainProgramContainer.IsValidType() && showErrorMessages)
	{
		CString string;
		string.Format(errorMessageID[m_MainProgramContainer.GetType()], m_ProgramFileName);
		AfxMessageBox(string);
		return;
	}
*/
// ������������ �������� ���� �������� �� ������������
	CString name;
	CGProgramData programData;
	POSITION pos = m_MainProgramContainer.GetStartPosition();
	while(pos)
	{
		m_MainProgramContainer.GetNextAssoc(pos, name, programData);
		programData.CheckGradientData();
		m_MainProgramContainer.SetAt(name, programData);
	}

// ���������� ���� ��������� ����������, ������������� SetAt()
	m_MainProgramContainer.SetModified(false);
}

void CMGradientPage::SaveProgramFile()
{
	if(m_MainProgramContainer.IsEmpty() || !m_MainProgramContainer.IsModified())
		return;

	ASSERT(!m_ProgramFileName.IsEmpty());
	if(!m_MainProgramContainer.SaveToUniversalDataFile())
	{
		CString error;
		error.Format(IDS_ERROR_WRITING_FILE, m_ProgramFileName);
		AfxMessageBox(error);
	}
	/*
	std::ofstream file(m_ProgramFileName);
//	ofstream file("a:\\test.ini");
	file << m_MainProgramContainer;
	if(file.rdstate() != 0)
	{
		CString error;
		error.Format(IDS_ERROR_WRITING_FILE, m_ProgramFileName);
		AfxMessageBox(error);
	}
	*/
}

enum
{
	c_ColumnIndex,
	c_ColumnFrom,
	c_ColumnTo,
	c_ColumnComment,
	c_ColumnPumpA_Begin,
	c_ColumnPumpA_End,
	c_ColumnPumpB_Begin,
	c_ColumnPumpB_End,
};

// ������������� ������� � ����������� ���������
void CMGradientPage::CreateProgramTable()
{
	m_ProgramParametersGrid.SubclassDlgItem(IDC_LIST_GRID_CTRL, this);
	m_ProgramParametersGrid.CreateTable(8, 0);
	m_ProgramParametersGrid.SetFirstColumnButtonStyle();

	m_ProgramParametersGrid.SetColumnInfo(c_ColumnIndex,   IDS_HEADER_STAGE, 120);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnFrom,    IDS_HEADER_FROM, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnTo,      IDS_HEADER_TO, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnComment, IDS_HEADER_COMMENT, 120);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnPumpA_Begin, IDS_HEADER_PUMP_A_BEGIN, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnPumpA_End,   IDS_HEADER_PUMP_A_END, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnPumpB_Begin, IDS_HEADER_PUMP_B_BEGIN, 50);
	m_ProgramParametersGrid.SetColumnInfo(c_ColumnPumpB_End,   IDS_HEADER_PUMP_B_END, 50);
	m_ProgramParametersGrid.ResizeColumns(true);
	m_ProgramParametersGrid.EnableColumnsResizing(false);
	m_ProgramParametersGrid.SetSelectionEnabled(false);
	m_ProgramParametersGrid.SetSelectionLocked(true);

	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnIndex, IDS_TIP_HEADER_STAGE);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnFrom,  IDS_TIP_HEADER_TIME_MIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnTo,    IDS_TIP_HEADER_TIME_MIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnComment,     IDS_TIP_HEADER_COMMENT);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnPumpA_Begin, IDS_TIP_HEADER_PUMP_A_BEGIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnPumpA_End,   IDS_TIP_HEADER_PUMP_A_END);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnPumpB_Begin, IDS_TIP_HEADER_PUMP_B_BEGIN);
	m_ProgramParametersGrid.SetHeaderTipID(c_ColumnPumpB_End,   IDS_TIP_HEADER_PUMP_B_END);
	m_ProgramParametersGrid.EnableToolTips();

	m_ProgramParametersGrid.SetSelectionColor(c_ColorBlack, c_ColorTurquoise);

	// �� �������� ������ �������� �������������� ���������
	m_ProgramParametersGrid.SetDoubleClickCommandID(IDC_BUTTON_EDIT_PROGRAM);
}

void CMGradientPage::ShowProgramData()
{
	int current = m_ProgramNamesComboBox.GetCurSel();
	if(current < 0)
	{
		m_ProgramParametersGrid.DeleteAllItems();
		return;
	}

	CString programName;
	m_ProgramNamesComboBox.GetLBText(current, programName);

	CGProgramData programData;
	if(m_MainProgramContainer.Lookup(programName, programData))
	{
		programData.SetToGridCtrl(&m_ProgramParametersGrid, 2);
		FillFromTimeColumn();
		InsertStartStages(programData);

		m_CurrentProgramName = programName;
		m_CurrentProgramIsValid = programData.IsValid();
		GetDlgItem(IDC_EDIT_ELUENT_A)->SetWindowText(programData.GetEluentA());
		GetDlgItem(IDC_EDIT_ELUENT_B)->SetWindowText(programData.GetEluentB());
	}
}

// ���������� ������� �� � ������� ������������������ ���������
void CMGradientPage::FillFromTimeColumn()
{
	int size = m_ProgramParametersGrid.GetItemCount();

	m_ProgramParametersGrid.SetItemText(0, c_ColumnFrom, _T("0"));
	for(int i = 1; i < size; i++)
	{
		m_ProgramParametersGrid.SetItemText(i, c_ColumnFrom, m_ProgramParametersGrid.GetItemText(i-1, c_ColumnTo));
	}
}

enum
{
	c_RowStart,
	c_RowConditioning,
	c_RowWaiting,
	c_RowFirstStage,
};

void CMGradientPage::InsertStartStages(CGProgramData& programData)
{
	CString text;
	// "������"
	text.LoadString(IDS_TEXT_GRADIENT_START);
	m_ProgramParametersGrid.InsertItem(c_RowStart, text);
	// "�����������������"
	text.LoadString(IDS_TEXT_CONDITIONING);
	m_ProgramParametersGrid.InsertItem(c_RowConditioning, text);
	// "��������"
	text.LoadString(IDS_TEXT_WAITING);
	m_ProgramParametersGrid.InsertItem(c_RowWaiting, text);

	text = m_ProgramParametersGrid.GetItemText(c_RowFirstStage, c_ColumnPumpA_Begin);
	m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnPumpA_Begin, text);
	m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnPumpA_End, text);
	text = m_ProgramParametersGrid.GetItemText(c_RowFirstStage, c_ColumnPumpB_Begin);
	m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnPumpB_Begin, text);
	m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnPumpB_End, text);

	CString fixedParameters = programData.GetFixedParameters();
	CStringArray fixed;
	ParseString(fixedParameters, ';', fixed);

	if(fixed.GetSize() > 0)
	{
		GetDlgItem(IDC_EDIT_WORK_FLOW_RATE)->SetWindowText(fixed[0]);
	}
	if(fixed.GetSize() > 1)
	{
		m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnFrom, _T("0"));
		m_ProgramParametersGrid.SetItemText(c_RowConditioning, c_ColumnTo, fixed[1]);
	}
	if(fixed.GetSize() > 3)
	{
		CString& text = fixed[2];
		if(text != _T("0"))
		{
			// ������ ������������ �� ������.
			int length = text.GetLength();
			text += text[length-1];
			text.SetAt(length-1, _T('.'));
		}
		text = CString(_T("P = "))+text;
		text += CString(_T("   Q = "))+fixed[3];

		m_ProgramParametersGrid.SetItemText(c_RowStart, c_ColumnComment, text);
	}
}

bool CMGradientPage::GetGradientProgram()
{
	CGProgramData programData;
	if(m_CurrentProgramIsValid && m_MainProgramContainer.Lookup(m_CurrentProgramName, programData))
	{
		m_GradientProgram.GetFromProgramData(&programData, true);
		return true;
	}
	return false;
}

void CMGradientPage::SelectNextProgramStage()
{
	int row = 0;
	m_StageDuration = 0;
	switch(m_GradientProgram.GetStageIndex())
	{
	case c_StageStart:
		row = 0;
		m_HeaderInfoString.LoadString(IDS_INFO_GRAD_START);
		break;
	case c_StageConditioning:
		row = 1;
		m_HeaderInfoString.LoadString(IDS_INFO_GRAD_CONDITIONING);
		m_StageDuration = m_GradientProgram.GetStageDuration();
		break;
	case c_StageWait:
		row = 2;
		m_HeaderInfoString.LoadString(IDS_INFO_GRAD_WAIT);
		break;
	case c_StageNone:
		row = -1;
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
			startButton->SetCheck(false);
		}
		m_HeaderInfoString.Empty();
		break;
	default:
		row = m_GradientProgram.GetStageIndex()+2;
		m_HeaderInfoString.Format(IDS_INFO_GRAD_STAGE, m_GradientProgram.GetStageIndex());
		m_StageDuration = m_GradientProgram.GetStageDuration();
		break;
	}

	m_ProgramParametersGrid.SetSelection(row);
	// ��� ����������� � ��������� �������� ����������.
	m_HeaderInfoBegin = m_HeaderInfoString;
	UpdatePageHeader();
}


void CMGradientPage::OnBnClickedCheckStartFill()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);
		m_PumpB->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpB->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		m_VolumeComboBox.AddCurrentString();

		if(StartFillMode())
		{
			m_HeaderInfoString.LoadString(IDS_INFO_FILL);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);
		
			SaveEluentInfo();
		}
		else
		{
			startButton->SetCheck(false);
			m_HeaderInfoString.Empty();
		}
	}
	else
	{
		StopCurrentMode(false);
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

void CMGradientPage::OnBnClickedCheckStartDischarge()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);
		m_PumpB->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpB->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		m_VolumeComboBox.AddCurrentString();

		if(StartDischargeMode())
		{
			m_HeaderInfoString.LoadString(IDS_INFO_DISCHARGE);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);
		}
		else
		{
			startButton->SetCheck(false);
			m_HeaderInfoString.Empty();
		}
	}
	else
	{
		StopCurrentMode(false);
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

void CMGradientPage::OnBnClickedCheckStartWork()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
	
	if(startButton->GetCheck())
	{
		// ��������� ������������ �������� ����������� � ���������.
		if(!CheckEluents())
		{
			startButton->SetCheck(false);
			return;
		}

		// � ����������� �� ��������� ������ ��������� ������� � m_GradientProgram.
		EnableRestartAfterStop();

		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);
		m_PumpB->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpB->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		if(StartWorkMode())
		{
			// ��������� ������� �� ����� ���������.
			//m_HeaderInfoString.LoadString(IDS_INFO_WORK);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� ������ ������.
			GetDlgItem(IDC_COMBO_PROGRAM_NAME)->EnableWindow(false);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);
		
			m_ProgramParametersGrid.SetSelectionEnabled(true);
			//m_ProgramParametersGrid.SetSelection(1);

			SaveEluentInfo();
		}
		else
		{
			m_HeaderInfoString.Empty();
			startButton->SetCheck(false);
		}
	}
	else
	{
		StopCurrentMode(true);
		m_ProgramParametersGrid.SetSelection(-1);
		m_ProgramParametersGrid.SetSelectionEnabled(false);

		CloseGradientWaitDialog();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

bool CMGradientPage::CheckEluents()
{
	CString pumpEluentA;
	CString pumpEluentB;
	CString programEluentA;
	CString programEluentB;
	GetDlgItem(IDC_COMBO_ELUENT_A)->GetWindowText(pumpEluentA);
	GetDlgItem(IDC_COMBO_ELUENT_B)->GetWindowText(pumpEluentB);
	GetDlgItem(IDC_EDIT_ELUENT_A)->GetWindowText(programEluentA);
	GetDlgItem(IDC_EDIT_ELUENT_B)->GetWindowText(programEluentB);
	if(pumpEluentA != programEluentA || pumpEluentB != programEluentB)
	{
		return (AfxMessageBox(IDS_MB_ELUENTS_DIFFER, MB_YESNO | MB_ICONQUESTION) == IDYES);
	}
	return true;
}


void CMGradientPage::OnBnClickedCheckVolume()
{
	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
	m_VolumeComboBox.EnableWindow(button->GetCheck());
}

// ���������� ��� ������� Enter � ������� ���������� � ������.
void CMGradientPage::OnCommandStart()
{
	m_GradientProgram.StartFirstWorkStage();
	SelectNextProgramStage();
}

void CMGradientPage::EnableRestartAfterStop()
{
	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_RESTART);
	bool enable = (button->GetCheck() != 0);
	m_GradientProgram.EnableRestartAfterStop(enable);
}

void CMGradientPage::OnBnClickedCheckRestart()
{
	EnableRestartAfterStop();
}


void CMGradientPage::OnEnChangeEditPressureA()
{
	// TODO:  ���� ��� ������� ���������� RICHEDIT, �� ������� ���������� �� �����
	// send this notification unless you override the CMToolPage::OnInitDialog()
	// ������� � ����� CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  �������� ��� �������� ����������
}
