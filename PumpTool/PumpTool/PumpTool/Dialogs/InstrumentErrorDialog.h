#if !defined(AFX_INSTRUMENTERRORDIALOG_H__25E95189_C0B8_451B_98B9_D5586AFA5015__INCLUDED_)
#define AFX_INSTRUMENTERRORDIALOG_H__25E95189_C0B8_451B_98B9_D5586AFA5015__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InstrumentErrorDialog.h : header file
//

#include "Service.h"

struct CSErrorInfo;

/////////////////////////////////////////////////////////////////////////////
// CMInstrumentErrorDialog dialog

class CMInstrumentErrorDialog : public CDialog
{
// Construction
public:
	CMInstrumentErrorDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMInstrumentErrorDialog)
	enum { IDD = IDD_DIALOG_ERROR_MESSAGE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// ������ ����� ������ �������
	bool SetErrorCode(int errorCode, bool useValue, int value, bool isWarning);
	int  GetErrorCode() { return m_ErrorCode; }
	void SetErrorText(const CString& errorText) { m_ErrorText = errorText; }
	void SetHeaderId(const int headerId) { m_HeaderStringId = headerId; }
	void SetErrorInfo(const CSErrorInfo& errorInfo, const CString& errorText);

	void Update();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMInstrumentErrorDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMInstrumentErrorDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

protected:
// ����� ������ �������
	int m_ErrorCode;
	bool m_IsWarning;

// ����� ��������� �� ������, �������������� �������� ���������
	CString m_ErrorText;
// �����, ���������� ���������� � ���. �������� ������
	CString m_AdditionalValueText;
	// ������������� ������ ���������.
	int m_HeaderStringId;

// ������ ���� ������� � ������ �������� (�� �������)
	CSize m_InitialSize;

	CGIntegerArray m_IgnoredErrorsArray;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTRUMENTERRORDIALOG_H__25E95189_C0B8_451B_98B9_D5586AFA5015__INCLUDED_)
