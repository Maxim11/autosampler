// InstrumentErrorDialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
//#include "DialogsResource.h"
//#include "Instrument\InstrumentResource.h"
#include "InstrumentErrorDialog.h"
#include "InstrumentBase\Instrument.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMInstrumentErrorDialog dialog


CMInstrumentErrorDialog::CMInstrumentErrorDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMInstrumentErrorDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMInstrumentErrorDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_HeaderStringId = 0;
	m_IgnoredErrorsArray.SetSize(0, 10);
}


void CMInstrumentErrorDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMInstrumentErrorDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


// ������ ����� ������ �������
bool CMInstrumentErrorDialog::SetErrorCode(int errorCode, bool useValue, int value, bool isWarning)
{
	for(int i = 0; i < m_IgnoredErrorsArray.GetSize(); i++)
	{
		TRACE("Ignored err %d %d\n", i, m_IgnoredErrorsArray[i]);
		if(errorCode == m_IgnoredErrorsArray[i])
			return false;
	}

	m_ErrorCode = errorCode;
	m_IsWarning = isWarning;

	if(useValue)
	{
		// ������� �������������� �������� � ������� ������
		m_AdditionalValueText.Format(IDS_INFO_ERROR_VALUE, value);
	}
	else
	{
		m_AdditionalValueText.Empty();
	}

	return true;
}

void CMInstrumentErrorDialog::SetErrorInfo(const CSErrorInfo& errorInfo, const CString& errorText)
{
	int errorCode = errorInfo.ErrorCode;
	bool useValue = errorInfo.Length > 0;
	int value = errorInfo.Value;
	bool isWarning = (errorInfo.Type == 0);

	SetErrorCode(errorCode, useValue, value, isWarning);
	m_ErrorText = errorText;
}

BEGIN_MESSAGE_MAP(CMInstrumentErrorDialog, CDialog)
	//{{AFX_MSG_MAP(CMInstrumentErrorDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMInstrumentErrorDialog message handlers

BOOL CMInstrumentErrorDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect dialogRect;
	GetWindowRect(&dialogRect);
	m_InitialSize.cx = dialogRect.Width();
	m_InitialSize.cy = dialogRect.Height();

	Update();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMInstrumentErrorDialog::Update()
{
	CString errorMessage;
	CString errorNumber;
	CString errorDescription1;
	CString errorDescription2;

	if(m_HeaderStringId != 0)
	{
		errorMessage.LoadString(m_HeaderStringId);
	}
	else
	{
		if(m_IsWarning)
		{
			errorMessage.LoadString(IDS_ERROR_WARNING);
		}
		else
		{
			errorMessage.LoadString(IDS_ERROR_INSTRUMENT_BASE);
		}
	}

	if(m_ErrorCode < 256)
	{
		errorNumber.Format(_T("%d"), m_ErrorCode);
	}
	else
	{
		// ����������� ��� ������ � ������ 205
		errorNumber.Format(_T("%d.%d"), m_ErrorCode >> 8, m_ErrorCode & 0xFF);
	}
	errorMessage += errorNumber;

	if(!m_AdditionalValueText.IsEmpty())
	{
		errorMessage += _T("    ")+m_AdditionalValueText;
	}

	if(m_ErrorText.IsEmpty())
		errorDescription1.LoadString(IDS_ERROR_INSTRUMENT_BASE+m_ErrorCode);
	else
		errorDescription1 = m_ErrorText;

// ����� ��������� �� ������ ����� ���� �� ���� �����
	int newLine = errorDescription1.Find('\n');
	if(newLine >= 0)
	{
		errorDescription2 = errorDescription1.Mid(newLine+1);
		errorDescription1 = errorDescription1.Left(newLine);
	}

// ���������� ������� ������
	CClientDC clientDC(this);
	clientDC.SelectObject(GetFont());
	CSize size0 = clientDC.GetTextExtent(errorMessage);
	CSize size1 = clientDC.GetTextExtent(errorDescription1);
	CSize size2 = clientDC.GetTextExtent(errorDescription2);
	int maxWidth = max(size0.cx, max(size1.cx, size2.cx))+4;
	
	CWnd* messageStatic = GetDlgItem(IDC_STATIC_PROMPT);
	CWnd* descriptionStatic1 = GetDlgItem(IDC_STATIC_TEXT);
	CWnd* descriptionStatic2 = GetDlgItem(IDC_STATIC_TEXT_2);
	CWnd* buttonOk = GetDlgItem(IDOK);

#ifdef IDC_CHECK_IGNORE_ERROR
	CWnd* ignoreCheckBox = GetDlgItem(IDC_CHECK_IGNORE_ERROR);
#else
	CWnd* ignoreCheckBox = NULL;
#endif

// ��������� ������� � ��������� ��������� � ����������� �� ����� ������
	CRect dialogRect;
	CRect buttonRect;
	CRect checkBoxRect;
	CRect rect0;
	CRect rect1;
	CRect rect2;
	GetWindowRect(&dialogRect);
	messageStatic->GetWindowRect(&rect0);
	descriptionStatic1->GetWindowRect(&rect1);
	descriptionStatic2->GetWindowRect(&rect2);
#ifdef IDC_CHECK_IGNORE_ERROR
	ignoreCheckBox->GetWindowRect(&checkBoxRect);
#endif
	buttonOk->GetWindowRect(&buttonRect);
	ScreenToClient(&checkBoxRect);
	ScreenToClient(&buttonRect);

// ����� ��������� ������� ��������� ������ ��������� ����������
	int dialogWidth = rect0.left-dialogRect.left+maxWidth+size0.cy*2;
//	dialogRect.right += maxWidth - rect1.Width();
	dialogWidth = max(dialogWidth, m_InitialSize.cx);
	dialogRect.right = dialogRect.left+dialogWidth;
	buttonRect.left = (dialogRect.Width()-buttonRect.Width())/2;
	
	messageStatic->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	descriptionStatic1->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	descriptionStatic2->SetWindowPos(NULL, 0, 0, maxWidth, rect1.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

// ����� ��������� ������� �� ����� ������
	if(errorDescription2.IsEmpty())
	{
		if(dialogRect.Height() == m_InitialSize.cy)
		{
			int height = rect2.top-rect1.top;
			buttonRect.top -= height;
			checkBoxRect.top -= height;
			dialogRect.bottom -= height;

			descriptionStatic2->ShowWindow(false);
		}
	}
	else
	{
		if(dialogRect.Height() != m_InitialSize.cy)
		{
			dialogRect.bottom = dialogRect.top+m_InitialSize.cy;
			descriptionStatic2->ShowWindow(true);
			checkBoxRect.top += rect2.top-rect1.top;
			buttonRect.top += rect2.top-rect1.top;
		}
	}

#ifdef IDC_CHECK_IGNORE_ERROR
	ignoreCheckBox->SetWindowPos(NULL, checkBoxRect.left, checkBoxRect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#endif
	buttonOk->SetWindowPos(NULL, buttonRect.left, buttonRect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
	SetWindowPos(NULL, 0, 0, dialogRect.Width(), dialogRect.Height(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	CenterWindow();
	buttonOk->Invalidate();

// ������� ����� ���������. ������ ��� � ����� �����, ����� ��������� ����� �����������.
	messageStatic->SetWindowText(errorMessage);
	descriptionStatic1->SetWindowText(errorDescription1);
	descriptionStatic2->SetWindowText(errorDescription2);
}

// ���� ������ ������������, ��� �����������, ���������� ������� ���� �� ��� ��������
void CMInstrumentErrorDialog::OnCancel()
{
#ifdef IDC_CHECK_IGNORE_ERROR
	CButton* ignoreButton = (CButton*)GetDlgItem(IDC_CHECK_IGNORE_ERROR);
	if(ignoreButton != 0 && ignoreButton->GetCheck() != 0)
	{
		m_IgnoredErrorsArray.Add(m_ErrorCode);
	}
#endif
	//((CMainFrame*)AfxGetMainWnd())->ResetInstrumentError();
	DestroyWindow();
}

void CMInstrumentErrorDialog::OnOK()
{
	OnCancel();
}
