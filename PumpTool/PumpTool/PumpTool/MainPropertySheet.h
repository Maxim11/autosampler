#pragma once

#include "Instrument/Piton.h"

#include "IsocraticPage.h"
#include "GradientPage.h"
#include "SettingsPage.h"
#include "ThermostatPage.h"
#include "AutosamplerPage.h"

/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet
//class CGPiton;

class CMMainPropertySheet : public CMFCPropertySheet
{
	DECLARE_DYNAMIC(CMMainPropertySheet)

// Construction
public:
	CMMainPropertySheet();

// Attributes
// Operations
public:

// Overrides
	virtual void OnDrawPageHeader (CDC* pDC, int nPage, CRect rectHeader);
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	//virtual void OnOK();

// Implementation
public:
	virtual ~CMMainPropertySheet();

//	void SendCanFrame(CGCanFrame* canFrame);
	//void SetUseUsbCanAdapter(bool useAdapter);
	bool SelectConnection(int index);
	void SetConnectionOnComPort(const CString& portName);

	void EnableAllPages(CMToolPage* sourcePage, bool enable);
	void SetMasterPage(CMToolPage* sourcePage);

protected:
	//void CreateDataChannel();
	bool StartDataExchange();
	void StopDataExchange();

	bool StartSlaveDataExchange();
	void StopSlaveDataExchange();

	bool StartThermoDataExchange();
	void StopThermoDataExchange();
	
	bool StartAutosamplerDataExchange();
	void StopAutosamplerDataExchange();

	void CreateStatusWindow();
	void ShowConnectionState(bool showSerialNumber);

	void SendCanFrameEnableStatus();
	void SendCanReadVersion();

	void DoOnInstrumentStateChanged(int instrumentId, int instrumentState);
	void DoOnInstrumentError(int instrumentId, int errorCode);

	void ShowInfoString(int stringID);
	void ShowThermostatState();

	void ShowAutosamplerState();

 	bool OpenLogFile();

	void EnableCanTracing(bool enable);


//	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnInstrumentEvent(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData);
	afx_msg void OnSelectTree(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()

// Overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
// ������/������� ���� �������
	//bool StartSoftTimerThread();
	//bool StopSoftTimerThread(bool checkProtocol = true);
// ���������� ����� ������ ���������� �������(0.1 ���) �� ���� �������
	//void OnSoftTimerEvent();

public:
// �������� ���� ���� ������� ������������� ��������
	//UINT SoftTimerProcessingLoop();

protected:
	CMIsocraticPage     m_IsocraticPage;
	CMGradientPage      m_GradientPage;
	CMSettingsPage      m_SettingsPage;
	CMThermostatPage	m_ThermostatPage;
	CMAutosamplerPage	m_AutosamplerPage;
	

	// ��������, � ������� ���������� �����.
	CMToolPage* m_MasterPage;

	class CMInstrumentErrorDialog* m_InstrumentErrorDialog;

protected:
	class CGPiton* m_PumpA;
	class CGPiton* m_PumpB;
	class CGThermostat* m_Thermostat;
	class CGAutosampler* m_Autosampler;
	// ��� ������ ������������� � ������� ���������.
	CGMasterInstrument* m_MasterInstrument;

	CString m_ComPort;
	CString m_ComPortB;
	CString m_ComPortAutosampler;
	CString m_UsbDeviceName;
	CString m_UsbDeviceNameB;

// ��������� ������ ������ � ��������
	CGDataChannelInfo m_DataChannelInfo;
	CGDataChannelInfo m_DataChannelInfoB;
	CGDataChannelInfo m_DataChannelInfoThermo;
	CGDataChannelInfo m_DataChannelInfoAutosampler;
	// �������� �� ����������� �����. BOOL - ����� �������� �������
	// c DDX_ ������� � ��������.
	BOOL m_IsGradientOn;

// ��������� �������� ������� ����� �� COM-�����
	bool m_IsPumpConnected;
	bool m_IsThermostatConnected;
	bool m_IsAutosamplerConnected;
	bool m_IsCheckConnectionEnabled;

	int m_TimerID;
	// ����� ������� ���������� CAN-������
	//unsigned int m_TimeStamp;
	// ����� ���������� ������� ���������� � �����
	//unsigned int m_LastCheckTime;
	// ����� ������ ���������� ������ �������
	//unsigned int m_LastStatusFrameTime;

	//bool m_UseUsbCanAdapter;
	//UINT16 m_UsbProductID;

	//bool m_IsCanConnected;
	CStatic m_StatusInfo;

protected:
// ������ ���������, ������������ � ���� � ������� �������.
//	CPtrList m_SlaveInstrumentsList;

};

