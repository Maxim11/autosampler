// ProgramData.h: interface for the CGProgramData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROGRAMDATA_H__EAB169F0_92E1_498E_85B3_9CEBAFE41DCB__INCLUDED_)
#define AFX_PROGRAMDATA_H__EAB169F0_92E1_498E_85B3_9CEBAFE41DCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include "FstreamMfc.h"

class CMGridCtrl;
class CGSectionInfo;

class CGProgramData : public CStringList  
{
public:
	CGProgramData();
	virtual ~CGProgramData();

	void GetFromGridCtrl(CMGridCtrl* gridCtrl, int startColumn);
	void SetToGridCtrl(CMGridCtrl* gridCtrl, int startColumn);

	CString& GetFixedParameters() { return m_FixedParameters; }
	void SetFixedParameters(CString& string) { m_FixedParameters = string; }

	CString& GetEluentA() { return m_EluentA; }
	CString& GetEluentB() { return m_EluentB; }
	void SetEluentA(CString& string) { m_EluentA = string; }
	void SetEluentB(CString& string) { m_EluentB = string; }

	void GetFromSectionInfo(CGSectionInfo* sectionInfo);
	void SetToSectionInfo(CGSectionInfo* sectionInfo);

	void CheckGradientData();
	bool IsValid();
	int GetCheckResult();
	int GetInvalidCellRow() { return m_InvalidCellRow; }
	int GetInvalidCellColumn() { return m_InvalidCellColumn; }
	void GetErrorText(CString& errorText);

	void operator =(CGProgramData& data);
	bool operator ==(CGProgramData& data);

	friend std::istream& operator >>(std::istream& is, CGProgramData& data);
	friend std::ostream& operator <<(std::ostream& os, CGProgramData& data);

protected:
// ���������, ����� ��� ���� ������
	CString m_FixedParameters;
	CString m_EluentA, m_EluentB;
// ��������� ���������� � ������ ����� ��� �������������
	//CString m_TimeParameters;

protected:
// ��� ������������
	//int m_ScanningType;
// ������ ��������� �������� ��� �������������� ��� ���������
	bool m_IsModified;

// ��������� �������� ���������.
	int m_CheckResult;

// ��������� ������������� �������� � �������
	int m_InvalidCellRow;
	int m_InvalidCellColumn;
};

// ����� ���� �������� � �� ��������
class CGProgramDataContainer : 
	public CMap<CString, CString&, CGProgramData, CGProgramData&>
{
public:
	CGProgramDataContainer();
	virtual ~CGProgramDataContainer();

// ��������� ���������� ������� ����������
	void Add(CGProgramDataContainer& container);
	void SetAt(CString& name, CGProgramData& programData);
	bool RemoveKey(CString& name);
	void RemoveAll();

	void SetModified(bool modified) { m_IsModified = modified; }
	bool IsModified() { return m_IsModified; }

	CStringList* GetSortedNames() { return &m_SortedNames; }

	void SetFileName(CString& fileName) { m_FileName = fileName; }
	CString& GetFileName() { return m_FileName; }

	void SetType(int type) { m_Type = type; }
	int  GetType() { return m_Type; }

	bool IsValidType() { return m_IsValidType; }

	bool LoadFromUniversalDataFile();
	bool SaveToUniversalDataFile();

	friend std::istream& operator >>(std::istream& is, CGProgramDataContainer& container);
	friend std::ostream& operator <<(std::ostream& os, CGProgramDataContainer& container);

	void ReadHeader(std::istream& is);
	void WriteHeader(std::ostream& os);

protected:
// ���� �� ���� ��������� �������� �� ����� ������ ������
	bool m_IsModified;

// ��������������� ����� ��������
	CStringList m_SortedNames;

// ��� �����, � ������� �������� ��������� ������� ������
	CString m_FileName;

// ��� ��������� (������������������, ������������, ������������)
	int m_Type;
// ������ (�������� ��� ������������)
//	int m_SubType;
// ��������� (�����������, ���������� �������������)
//	CString m_Category;
// ������ ���� ����������� ����
	bool m_IsValidType;
};

enum
{
	c_ProgramDataOk,
	c_ProgramDataIncomplete,
	c_ProgramDataItemIsEmpty,
	c_ProgramDataInvalidFromValue,
	c_ProgramDataFromValueGeToValue,
};

const char c_Delimiter = _T(';');

// ���������� ���-������� �� ������, ����� �� CMapStringToPtr
AFX_INLINE UINT AFXAPI HashKey(CString& key)
{
	UINT nHash = 0;
	for(int i = 0; i < key.GetLength(); i++)
		nHash = (nHash<<5) + nHash + key[i];
	return nHash;
}

template <> 
AFX_INLINE UINT AFXAPI HashKey(CString& key)
{
	UINT nHash = 0;
	for(int i = 0; i < key.GetLength(); i++)
		nHash = (nHash<<5) + nHash + key[i];
	return nHash;
}


#endif // !defined(AFX_PROGRAMDATA_H__EAB169F0_92E1_498E_85B3_9CEBAFE41DCB__INCLUDED_)
