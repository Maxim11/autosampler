// CommonProgramsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CommonProgramDialog.h"

//#include "Spectrofluorimeter.h"

//#include "HtmlHelpId.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//extern CGSpectrofluorimeter g_Spectrofluorimeter;

const CString c_LogProgramName = _T("?");

/////////////////////////////////////////////////////////////////////////////
// CMCommonProgramDialog dialog

HICON CMCommonProgramDialog::sm_AddProgramIcon = 0;
HICON CMCommonProgramDialog::sm_DeleteProgramIcon = 0;
HICON CMCommonProgramDialog::sm_CopyProgramIcon = 0;
HICON CMCommonProgramDialog::sm_RenameProgramIcon = 0;

HICON CMCommonProgramDialog::sm_AddRowIcon = 0;
HICON CMCommonProgramDialog::sm_DeleteRowIcon = 0;
HICON CMCommonProgramDialog::sm_CopyRowIcon = 0;
HICON CMCommonProgramDialog::sm_PasteRowIcon = 0;

CMCommonProgramDialog::CMCommonProgramDialog(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
{
	//{{AFX_DATA_INIT(CMCommonProgramDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_MainProgramContainer = 0;

	m_OldProgramIndex = -1;
	m_NewProgramIndex = -1;

	m_IsExternalLightMeasurement = false;
	m_FirstEnabledColumn = 1;

	m_RowIsAutoAdded = false;

// �������������� ����������� ����
	m_ProgramNamesMenuID = IDR_MENU_PROGRAM_LIST;
	m_ProgramParameteresMenuID = IDR_MENU_PROGRAM_STAGE;
	m_ProgramDeletePromptID = IDS_MB_DELETE_PROGRAM;
}

BEGIN_MESSAGE_MAP(CMCommonProgramDialog, CDialog)
	//{{AFX_MSG_MAP(CMCommonProgramDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_PROGRAMS, OnProgramSelected)
	ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_LIST_PROGRAMS, OnBeginNameEditing)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_PROGRAMS, OnEndNameEditing)
	ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_LIST_GRID_CTRL, OnBeginCellEditing)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_GRID_CTRL, OnEndCellEditing)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_GRID_CTRL, OnProgramItemSelected)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PROGRAM, OnButtonAddProgram)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_PROGRAM, OnButtonDeleteProgram)
	ON_BN_CLICKED(IDC_BUTTON_COPY_PROGRAM, OnButtonCopyProgram)
	ON_BN_CLICKED(IDC_BUTTON_RENAME_PROGRAM, OnButtonRenameProgram)
	ON_BN_CLICKED(IDC_BUTTON_ADD_ROW, OnButtonAddRow)
	ON_BN_CLICKED(IDC_BUTTON_DEL_ROW, OnButtonDeleteRow)
	ON_BN_CLICKED(IDC_BUTTON_COPY_ROW, OnButtonCopyRow)
	ON_BN_CLICKED(IDC_BUTTON_PASTE_ROW, OnButtonPasteRow)
	ON_BN_DOUBLECLICKED(IDC_BUTTON_ADD_ROW, OnDoubleClickedButtonAddRow)

	ON_COMMAND(IDC_POST_EDIT_NAME, OnStartEditProgramName)
	ON_COMMAND(IDC_POST_EDIT_TABLE, OnStartEditProgramParameters)
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMCommonProgramDialog message handlers

BOOL CMCommonProgramDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_nIDHelp = HID_EDIT_PROGRAM_DIALOG;

// ����� �� ���������� ������, ����� ��������� ��������� ���� �� ���������� �����
	CenterWindow();

// ������������� ������
	CImageList imageList;
	imageList.Create(IDB_BUTTONS_PROGRAM_DIALOG, 20, 1, RGB(255, 0, 255));
	if(!sm_AddProgramIcon)
	{
		sm_AddProgramIcon = imageList.ExtractIcon(0);
		sm_DeleteProgramIcon = imageList.ExtractIcon(1);
		sm_CopyProgramIcon = imageList.ExtractIcon(2);
		sm_RenameProgramIcon = imageList.ExtractIcon(3);

		sm_AddRowIcon = imageList.ExtractIcon(4);
		sm_DeleteRowIcon = imageList.ExtractIcon(5);
		sm_CopyRowIcon = imageList.ExtractIcon(6);
		sm_PasteRowIcon = imageList.ExtractIcon(7);
	}

	CButton* button;
	button = (CButton*)GetDlgItem(IDC_BUTTON_ADD_PROGRAM);
	ASSERT(button != 0);
	button->SetIcon(sm_AddProgramIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_DELETE_PROGRAM);
	ASSERT(button != 0);
	button->SetIcon(sm_DeleteProgramIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_COPY_PROGRAM);
	ASSERT(button != 0);
	button->SetIcon(sm_CopyProgramIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_RENAME_PROGRAM);
	ASSERT(button != 0);
	button->SetIcon(sm_RenameProgramIcon);

	button = (CButton*)GetDlgItem(IDC_BUTTON_ADD_ROW);
	if(button != 0)
		button->SetIcon(sm_AddRowIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_DEL_ROW);
	if(button != 0)
		button->SetIcon(sm_DeleteRowIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_COPY_ROW);
	if(button != 0)
		button->SetIcon(sm_CopyRowIcon);
	button = (CButton*)GetDlgItem(IDC_BUTTON_PASTE_ROW);
	if(button != 0)
		button->SetIcon(sm_PasteRowIcon);

// ���������
//	GetDlgItem(IDC_EDIT_PROGRAM_CATEGORY)->SetWindowText(m_MainProgramContainer->GetCategory());

// ������������� ������� �� ������� ��������
	m_ProgramNamesGrid.SubclassDlgItem(IDC_LIST_PROGRAMS, this);

	m_ProgramNamesGrid.CreateTable(1, 0);
	m_ProgramNamesGrid.SetLimitedEditing(true);
	m_ProgramNamesGrid.SetColumnInfo(0, IDS_HEADER_PROGRAMS, 100);
	m_ProgramNamesGrid.ResizeColumns();
	m_ProgramNamesGrid.EnableColumnsResizing(false);

// ������� ������ �������
	m_ProgramNamesImageList.Create(IDB_CHECK_MARKS, 16, 1, RGB(255, 255, 255));
	m_ProgramNamesGrid.SetImageList(&m_ProgramNamesImageList);
// �� �������� ������ �������� �������������� ���������
	m_ProgramNamesGrid.SetDoubleClickCommandID(IDC_BUTTON_RENAME_PROGRAM);

	CString programName;
	CGProgramData programData;
	int i = 0;

// ��������� ������ ���� �������� � ������������� ������
	POSITION pos = m_MainProgramContainer->GetSortedNames()->GetHeadPosition();
	while(pos)
	{
		programName = m_MainProgramContainer->GetSortedNames()->GetNext(pos);
		m_ProgramNamesGrid.InsertItem(i, programName);

		m_MainProgramContainer->Lookup(programName, programData);
		m_ProgramNamesGrid.SetImage(i, programData.GetCheckResult());
		i++;
	}
/*
	CComboBox* averaging = (CComboBox*)GetDlgItem(IDC_COMBO_AVERAGING);
	if(averaging != 0)
	{
		if(!m_IsExternalLightMeasurement)
		{
			averaging->AddString(_T("1"));
			averaging->AddString(_T("5"));
			averaging->AddString(_T("10"));
			averaging->AddString(_T("25"));
			averaging->AddString(_T("50"));
			averaging->AddString(_T("100"));
			averaging->AddString(_T("250"));
			averaging->SetCurSel(3);
		}
		else
		{
			averaging->AddString(_T("0.02"));
			averaging->AddString(_T("0.04"));
			averaging->AddString(_T("0.1"));
			averaging->AddString(_T("0.2"));
			averaging->AddString(_T("0.5"));
			averaging->AddString(_T("1"));
			averaging->AddString(_T("2"));
			averaging->AddString(_T("5"));
			averaging->AddString(_T("10"));
		}
	}
*/
	EnableToolTips();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// ������ � ����������� ����������
int CMCommonProgramDialog::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	int controlID = CWnd::OnToolHitTest(point, pTI);
	if(pTI != 0 && controlID > 0)
	{
		CString tipText;
		GetTipText(tipText, controlID);

		if(!tipText.IsEmpty())
		{
			pTI->lpszText = new TCHAR[tipText.GetLength()+1];
			lstrcpy(pTI->lpszText, (LPCTSTR)tipText);
		}
	}
	return controlID;
}

// ����� ��������� ��� �������� ����������
void CMCommonProgramDialog::GetTipText(CString& tipText, int controlID) const
{
	tipText.LoadString(controlID);
}

void CMCommonProgramDialog::SetContainer(CGProgramDataContainer* main)
{
	m_MainProgramContainer = main;
}

void CMCommonProgramDialog::CreateEmptyTable()
{
	m_ProgramParametersGrid.DeleteAllItems();
	AddRow(true, false);
/*
	m_ProgramParametersGrid.InsertItem(0, _T("1"));
//	m_ProgramParametersGrid.InsertItem(1, _T("2"));
	m_ProgramParametersGrid.SetItemText(0, 1, _T("0"));
*/

//	m_StrobeDelayComboBox.SetWindowText("");
//	m_StrobeDurationComboBox.SetWindowText("");
}

bool CMCommonProgramDialog::CopyStageBufferIsValid() const
{
	return !GetCopyStageBuffer().IsEmpty();
}

void CMCommonProgramDialog::OnButtonAddProgram()
{
	int size = m_ProgramNamesGrid.GetItemCount();

	m_ProgramNamesGrid.InsertItem(size, _T(""));
	m_ProgramNamesGrid.SetSelection(size);
	m_ProgramNamesGrid.EditCellText(size, 0);
	m_ProgramNamesGrid.SetImage(size, 3);

	EnableNameButtons();
}

void CMCommonProgramDialog::OnButtonDeleteProgram()
{
	int current = m_ProgramNamesGrid.GetSelectionMark();
	CString name = m_ProgramNamesGrid.GetItemText(current, 0);

	CString message;
	message.Format(m_ProgramDeletePromptID, name);
	if(AfxMessageBox(message, MB_YESNO | MB_ICONQUESTION) != IDYES)
		return;

	m_ProgramNamesGrid.DeleteItem(current);
	m_RemovedProgramNames.AddTail(name);
	m_ChangedProgramContainer.RemoveKey(name);

	if(current >= m_ProgramNamesGrid.GetItemCount())
		current--;
	if(m_ProgramNamesGrid.GetItemCount() > 0)
		m_ProgramNamesGrid.SetSelection(current);
	else
		m_ProgramParametersGrid.DeleteAllItems();

	EnableNameButtons();
}

void CMCommonProgramDialog::OnButtonCopyProgram()
{
	int current = m_ProgramNamesGrid.GetSelectionMark();
	CString string = m_ProgramNamesGrid.GetItemText(current, 0);

	current++;
	m_ProgramNamesGrid.InsertItem(current, string);
	m_ProgramNamesGrid.EditCellText(current, 0);
	m_ProgramNamesGrid.SetSelection(current);
	m_IsProgramModified = true;
}

void CMCommonProgramDialog::OnButtonRenameProgram()
{
	int current = m_ProgramNamesGrid.GetSelectionMark();
	m_ProgramNamesGrid.EditCellText(current, 0);

	m_OldProgramName = m_ProgramNamesGrid.GetItemText(current, 0);
	m_RemovedProgramNames.AddTail(m_OldProgramName);
	m_ChangedProgramContainer.RemoveKey(m_OldProgramName);
	m_IsProgramModified = true;
}

void CMCommonProgramDialog::OnProgramSelected(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMLISTVIEW* changed = (NMLISTVIEW*)pNMHDR;

	*pResult = 0;

	HideToolTipMessage();

// ��������� ��������� ���� ���������
	if(!(changed->uNewState & LVNI_SELECTED) && (changed->uOldState & LVNI_SELECTED))
	{
		TRACE("Old program %d\n", changed->iItem);
// ��������� ����� ������, ����� ��� �� ��������� �������������� � �������
// � ���� ������ ���������� ������ ��������� ��� ����������� ���������
		if(m_ProgramParametersGrid.IsEditingFinished())// && m_ProgramNamesGrid.IsEditingFinished())
		{
			m_OldProgramIndex = -1;
			TableToProgram(changed->iItem);
		}
		else
			m_OldProgramIndex = changed->iItem;

//		m_ProgramNamesGrid.GetItemText(changed->iItem, 0);

		return;
	}

// ��������� ����� ���������
	if((changed->uNewState & LVNI_SELECTED) && !(changed->uOldState & LVNI_SELECTED))
	{
		TRACE("New program %d\n", changed->iItem);
		if(m_ProgramParametersGrid.IsEditingFinished())// && m_ProgramNamesGrid.IsEditingFinished())
		{
			m_NewProgramIndex = -1;
			ProgramToTable(changed->iItem);
		}
		else
			m_NewProgramIndex = changed->iItem;

		m_ActiveProgramName = m_ProgramNamesGrid.GetItemText(changed->iItem, 0);
		return;
	}
}

void CMCommonProgramDialog::OnBeginNameEditing(NMHDR* pNMHDR, LRESULT* pResult)
{
	EnableNameButtons();
}

void CMCommonProgramDialog::OnEndNameEditing(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* dispInfo = (LV_DISPINFO*)pNMHDR;
	if(dispInfo->item.cchTextMax == 0)			// ������ ESC ��� ������� ������ ������
	{
		if(!m_OldProgramName.IsEmpty())
		{
// ���� ����������� ��������������, ���������� �������� ���
			m_ProgramNamesGrid.SetItemText(dispInfo->item.iItem, 0, m_OldProgramName);
			m_OldProgramName.Empty();
		}
		else
		{
// ���� ����������� ����� ��������� (��� �����), ������� ��
			m_ProgramNamesGrid.DeleteItem(dispInfo->item.iItem);

			if(m_ProgramNamesGrid.GetItemCount() > 0)
				m_ProgramNamesGrid.SetSelection(dispInfo->item.iItem-1);
			else
				m_ProgramParametersGrid.DeleteAllItems();
		}

		HideToolTipMessage();
		EnableNameButtons();
		return;
	}

	m_OldProgramName.Empty();

	LV_FINDINFO findInfo;
	findInfo.psz = dispInfo->item.pszText;
	findInfo.flags = LVFI_STRING;
	int index = m_ProgramNamesGrid.FindItem(&findInfo, -1);

// ��� ��������� ��������� � ��� ������������
	if(index >= 0 && m_ProgramNamesGrid.FindItem(&findInfo, index) >= 0)
	{
// ��������� ������� ��� ���������� � ���������� ������ �����
		m_ProgramNamesGrid.SetSelection(dispInfo->item.iItem);
// ����� ���������� ��������� ���� ��������� ����� ����� ������������� ���
		PostMessage(WM_COMMAND, IDC_POST_EDIT_NAME);
		return;
	}

	TRACE("OnEndNameEditing\n");
	HideToolTipMessage();

// ����� ���������� ��������� ���� ��������� ������ �������������� ���������
	if(m_ProgramNamesGrid.GetSelectionMark() == dispInfo->item.iItem)
	{
		PostMessage(WM_COMMAND, IDC_POST_EDIT_TABLE);
		m_ActiveProgramName = m_ProgramNamesGrid.GetItemText(dispInfo->item.iItem, 0);
	}

	EnableNameButtons();
}

void CMCommonProgramDialog::OnButtonAddRow()
{
	AddRow(true);

	EnableParametersButtons(true);
}

void CMCommonProgramDialog::OnDoubleClickedButtonAddRow()
{
	AddRow(true);

	EnableParametersButtons(true);
}

bool CMCommonProgramDialog::AddRow(bool emptyRow, bool editItem, bool afterLast)
{
	int row = afterLast ? m_ProgramParametersGrid.GetItemCount() : m_ProgramParametersGrid.GetSelectionMark()+1;
	if(m_ProgramParametersGrid.GetItemCount() == 0)
		row = 0;
	if(row < 0)
		return false;

	CString string;
	string.Format(_T("%d"), row+1);
	m_ProgramParametersGrid.InsertItem(row, string);

	int size = m_ProgramParametersGrid.GetItemCount();
	for(int i = row+1; i < size; i++)
	{
		string.Format(_T("%d"), i+1);
		m_ProgramParametersGrid.SetItemText(i, 0, string);
	}

	DoOnAddRow(row, emptyRow);

	m_ProgramParametersGrid.SetSelection(row);
	if(editItem)
	{
		int column = 1;
		if(!m_ProgramParametersGrid.IsColumnEnabled(1))
		{
// ������� 1 �� ��������� ��� ������������������ ���������
			column = 2;
			if(!afterLast)
				row--;
		}
		m_ProgramParametersGrid.EditCellText(row, column);
	}

	m_IsProgramModified = true;

	return true;
}

void CMCommonProgramDialog::OnButtonDeleteRow()
{
	int row = m_ProgramParametersGrid.GetSelectionMark();
	if(row < 0)
		return;

// ���������� ��������� ���� � ������
	OnButtonCopyRow();

	CString string;
	m_ProgramParametersGrid.DeleteItem(row);

	int size = m_ProgramParametersGrid.GetItemCount();
	for(int i = row; i < size; i++)
	{
		string.Format(_T("%d"), i+1);
		m_ProgramParametersGrid.SetItemText(i, 0, string);
	}

	DoOnDeleteRow(row);

	m_ProgramParametersGrid.SetSelection(max(row-1, 0));
	EnableParametersButtons(size > 0);

	m_IsProgramModified = true;
}

void CMCommonProgramDialog::OnButtonCopyRow()
{
	int row = m_ProgramParametersGrid.GetSelectionMark();
	if(row < 0)
		return;

	CString rowContent;
	m_ProgramParametersGrid.GetRowContent(row, rowContent, m_FirstEnabledColumn);
	SetCopyStageBuffer(rowContent);

	EnableParametersButtons(true);
}

void CMCommonProgramDialog::OnButtonPasteRow()
{
	if(GetCopyStageBuffer().IsEmpty())
		return;

	if(!AddRow(false))
		return;

	EnableParametersButtons(true);
}

void CMCommonProgramDialog::OnBeginCellEditing(NMHDR* pNMHDR, LRESULT* pResult)
{
	*pResult = 0;

	LV_DISPINFO* DispInfo = (LV_DISPINFO*)pNMHDR;
	m_OldCellText = DispInfo->item.pszText;
}

void CMCommonProgramDialog::OnEndCellEditing(NMHDR* pNMHDR, LRESULT* pResult)
{
	*pResult = 0;

	LV_DISPINFO* dispInfo = (LV_DISPINFO*)pNMHDR;
	int row = dispInfo->item.iItem;
	int column = dispInfo->item.iSubItem;
	LPCTSTR cellText = dispInfo->item.pszText;
	int nOfRows = m_ProgramParametersGrid.GetItemCount();

	if(cellText != 0)
	{
		CString newCellText = CString(cellText);
		if(newCellText != m_OldCellText)
		{
			m_IsProgramModified = true;

			int selected = m_ProgramNamesGrid.GetSelectionMark();
			m_ProgramNamesGrid.SetImage(selected, 3);
			m_ProgramNamesGrid.Update(selected);
		}

		DoOnEndCellEditing(row, column, newCellText);
	}
	else
	{
		DoOnEscapeCellEditing(row, column);
	}

// � ������, ����� �������������� ����������� ��� ������ ����� ���������
// ���������� ����������� �������� (������� � �������� ����������� ����������� �� ������)
	if(m_OldProgramIndex >= 0)
	{
		TableToProgram(m_OldProgramIndex);
		m_OldProgramIndex = -1;
	}
	if(m_NewProgramIndex >= 0)
	{
		ProgramToTable(m_NewProgramIndex);
		m_NewProgramIndex = -1;
	}
}

// ��� ������ ����� �������� ��������� ������ ���������� �������
void CMCommonProgramDialog::OnProgramItemSelected(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMLISTVIEW* changed = (NMLISTVIEW*)pNMHDR;

	EnableParametersButtons((changed->uNewState & LVNI_SELECTED) != 0);

	*pResult = 0;
}

void CMCommonProgramDialog::OnOK()
{
	int current = m_ProgramNamesGrid.GetSelectionMark();
	TableToProgram(current);

// ������� ���������, ��������� � ������ ���������
	POSITION pos = m_RemovedProgramNames.GetHeadPosition();
	while(pos)
	{
		m_MainProgramContainer->RemoveKey(m_RemovedProgramNames.GetNext(pos));
	}
	m_RemovedProgramNames.RemoveAll();

	CString programName;
	CGProgramData programData;

// ��������� ��������� � ������������� �����������
	pos = m_ChangedProgramContainer.GetSortedNames()->GetHeadPosition();
	while(pos)
	{
		programName = m_ChangedProgramContainer.GetSortedNames()->GetNext(pos);
		m_ChangedProgramContainer.Lookup(programName, programData);
		if(!programData.IsValid())
		{
			int index = m_ProgramNamesGrid.FindRow(programName);
			m_ProgramNamesGrid.SetSelection(index);
			ShowErrorMessage(programData, true);
			return;
		}

/*
// ���� ������������ ������� ���������-��������, ���������� ������������� ��
		if(programName == c_LogProgramName)
		{
			int index = m_ProgramNamesGrid.FindRow(programName);
			m_ProgramNamesGrid.SetSelection(index);
//			PostMessage(WM_COMMAND, IDC_BUTTON_RENAME_PROGRAM);
			OnButtonRenameProgram();

			ShowErrorMessage(index, IDS_INFO_RENAME_PROTOCOL);
			return;
		}
*/
	}

// ��������� ��� �������� ��������� � ������������� �����������
	if(!m_ChangedProgramContainer.IsEmpty())
	{
		m_MainProgramContainer->Add(m_ChangedProgramContainer);
		m_ChangedProgramContainer.RemoveAll();
	}

	CDialog::OnOK();
}

// ��������� ���������, ����������� ��� ����������� �������������� �����
// �������� ����� �������������� ����� ����� � ������ ��������������
void CMCommonProgramDialog::OnStartEditProgramName()
{
	if(GetFocus()->GetSafeHwnd() == GetDlgItem(IDCANCEL)->GetSafeHwnd())
		return;

	int current = m_ProgramNamesGrid.GetSelectionMark();
	m_ProgramNamesGrid.EditCellText(current, 0);

// ���������� ��������� � ������������ ����� ���������
	ShowErrorMessage(current, IDS_INFO_NAME_DUPLICATED);
}

// �������������� ������ �������������� ��������� �� ���������� ����� �����
void CMCommonProgramDialog::OnStartEditProgramParameters()
{
	int column = m_ProgramParametersGrid.IsColumnEnabled(1) ? 1 : 2;
	if(m_ProgramParametersGrid.GetItemText(0, column).IsEmpty())
	{
		m_ProgramParametersGrid.EditCellText(0, column);
	}
	TRACE("OnStartEditProgramParameters\n");
}

// ���������� �� ���������� �������������� �������� ��������� �������� ESC
void CMCommonProgramDialog::DoOnEscapeCellEditing(int row, int column)
{
	if(column == m_FirstEnabledColumn && row == m_ProgramParametersGrid.GetItemCount()-1
		&& m_RowIsAutoAdded)
	{
		OnButtonDeleteRow();
	}
}

// �������� ��������� ������ ������ ��� ������ �� ������� ���� ��������
void CMCommonProgramDialog::EnableNameButtons()
{
	bool enable = m_ProgramNamesGrid.IsEditingFinished();

	CWnd* button = GetDlgItem(IDC_BUTTON_ADD_PROGRAM);
	if(button != 0)
		button->EnableWindow(enable);
	enable = enable && m_ProgramNamesGrid.GetItemCount() > 0;

	button = GetDlgItem(IDC_BUTTON_DELETE_PROGRAM);
	if(button != 0)
		button->EnableWindow(enable);

	int current = m_ProgramNamesGrid.GetSelectionMark();
	enable = enable && !m_ProgramNamesGrid.GetItemText(current, 0).IsEmpty();

	button = GetDlgItem(IDC_BUTTON_COPY_PROGRAM);
	if(button != 0)
		button->EnableWindow(enable);
	button = GetDlgItem(IDC_BUTTON_RENAME_PROGRAM);
	if(button != 0)
		button->EnableWindow(enable);

	EnableParametersButtons();
}

// �������� ��������� ������ ������ ��� ������ � ����������� ���������
void CMCommonProgramDialog::EnableParametersButtons(bool itemSelected)
{
	bool enable = m_ProgramNamesGrid.GetSelectionMark() >= 0
		&& m_ProgramNamesGrid.IsEditingFinished();

	CWnd* button = GetDlgItem(IDC_BUTTON_ADD_ROW);
	if(button != 0)
		button->EnableWindow(enable && (itemSelected || m_ProgramParametersGrid.GetItemCount() == 0));

	button = GetDlgItem(IDC_BUTTON_DEL_ROW);
	if(button != 0)
		button->EnableWindow(enable && itemSelected);
	button = GetDlgItem(IDC_BUTTON_COPY_ROW);
	if(button != 0)
		button->EnableWindow(enable && itemSelected);

	enable = enable && CopyStageBufferIsValid() && (itemSelected || m_ProgramParametersGrid.GetItemCount() == 0);
	button = GetDlgItem(IDC_BUTTON_PASTE_ROW);
	if(button != 0)
		button->EnableWindow(enable);
}

// ���������� ��������� � ������������ �������� ���������
void CMCommonProgramDialog::ShowErrorMessage(CGProgramData& programData, bool editItem)
{
	CString messageText;

	int row = programData.GetInvalidCellRow();
	int column = programData.GetInvalidCellColumn();
	CRect rect;

// ������ � ������� ������������� ���������� ���������
	if(row < 0)
	{
		programData.GetErrorText(messageText);
		ShowErrorMessage(column, messageText, editItem);
		return;
	}

	if(m_ProgramParametersGrid.GetItemText(row, column).IsEmpty())
	{
		messageText.LoadString(IDS_INFO_FIELD_IS_EMPTY);
	}
	else
	{
		programData.GetErrorText(messageText);
	}

	m_ProgramParametersGrid.GetSubItemRect(row, column, LVIR_BOUNDS, rect);
	CPoint point(rect.right+10, rect.top);
	m_ProgramParametersGrid.ClientToScreen(&point);
	ShowToolTipMessage(messageText, point);

	if(editItem)
		m_ProgramParametersGrid.EditCellText(row, column);
}

// ���������� ��������� � ������������ ����� ���������
void CMCommonProgramDialog::ShowErrorMessage(int nameIndex, int messageID)
{
	CRect rect;
	m_ProgramNamesGrid.GetItemRect(nameIndex, &rect, LVIR_BOUNDS);

	CPoint point(rect.right+10, rect.top);
	CString text;
	text.LoadString(messageID);
	m_ProgramNamesGrid.ClientToScreen(&point);

	ShowToolTipMessage(text, point);
}

// ���������� ���������
void CMCommonProgramDialog::ShowToolTipMessage(const CString& message, CPoint& point, int interval)
{
	if(m_TrackingToolTip.GetSafeHwnd() == 0)
	{
		if(!m_TrackingToolTip.Create(this))
			return;
		m_TrackingToolTip.SetMaxTipWidth(300);
	}

	m_TrackingToolTip.SetPosition(point.x, point.y);
	m_TrackingToolTip.Activate(this, message, true, interval);
}

// ������� ��������� � ������������ �������� ���������
void CMCommonProgramDialog::HideToolTipMessage()
{
	if(m_TrackingToolTip.GetSafeHwnd() != 0)
	{
		m_TrackingToolTip.Activate(this, 0, false, 0);
	}
}

void CMCommonProgramDialog::OnContextMenu(CWnd* pWnd, CPoint point)
{
//	TRACE("OnContextMenu\n");
	if(pWnd == &m_ProgramParametersGrid)
	{
		CMenu menu;
		if(m_ProgramParameteresMenuID == 0 || !menu.LoadMenu(m_ProgramParameteresMenuID))
			return;

		CMenu* popupMenu;
		popupMenu = menu.GetSubMenu(0);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_ADD_ROW);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_DEL_ROW);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_COPY_ROW);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_PASTE_ROW);
		popupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, point.x, point.y, this);

		menu.DestroyMenu();
		return;
	}

	if(pWnd == &m_ProgramNamesGrid)
	{
		CMenu menu;
		if(m_ProgramNamesMenuID == 0 || !menu.LoadMenu(m_ProgramNamesMenuID))
			return;

		CMenu* popupMenu;
		popupMenu = menu.GetSubMenu(0);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_ADD_PROGRAM);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_DELETE_PROGRAM);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_COPY_PROGRAM);
		EnablePopupMenuItem(popupMenu, IDC_BUTTON_RENAME_PROGRAM);
		popupMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this);

		menu.DestroyMenu();
		return;
	}
}

// ��������� ����� ������������ ���� �������
void CMCommonProgramDialog::EnablePopupMenuItem(CMenu* popupMenu, int itemID)
{
	if(GetDlgItem(itemID) != 0 && !GetDlgItem(itemID)->IsWindowEnabled())
		popupMenu->EnableMenuItem(itemID, MF_GRAYED);
}
