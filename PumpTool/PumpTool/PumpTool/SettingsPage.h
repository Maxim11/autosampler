#pragma once

#include "ToolPage.h"

// ���������� ���� CMSettingsPage

class CMSettingsPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMSettingsPage)

public:
	CMSettingsPage();
	virtual ~CMSettingsPage();

	void EnableControls(bool enable);

// ������ ����������� ����
	enum { IDD = IDD_PAGE_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonConnection();
	afx_msg void OnBnClickedButtonParameters();

	static void SaveSettingsToRegistry();
	static void LoadSettingsFromRegistry();
};
