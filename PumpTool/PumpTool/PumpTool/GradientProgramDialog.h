#if !defined(AFX_GradientProgramDialog_H__04D162A7_F587_4765_8187_34182785BD62__INCLUDED_)
#define AFX_GradientProgramDialog_H__04D162A7_F587_4765_8187_34182785BD62__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChromatographicProgramsDialog.h : header file
//

#include "CommonProgramDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CMGradientProgramDialog dialog

//class CGChromatographicProgramDataContainer;
class CGProgramDataContainer;

class CMGradientProgramDialog : public CMCommonProgramDialog
{
// Construction
public:
	CMGradientProgramDialog(CWnd* pParent, bool timeInMinutes = true);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMGradientProgramDialog)
	enum { IDD = IDD_DIALOG_GRADIENT_PROGRAM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMGradientProgramDialog)
	//}}AFX_VIRTUAL
	// ��������� ������������� ������ ���� (��������)
	void DisableProgramSelection(bool disable) { m_IsProgramSelectionDisabled = disable; }

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMGradientProgramDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnWorkFlowRateKillFocus();
	afx_msg void OnTimeKillFocus();
	afx_msg void OnStartPressureKillFocus();
	afx_msg void OnStartFlowRateKillFocus();
	//}}AFX_MSG

protected:

// ��������� ������� ���������� �� ���������� ��������
	virtual void ProgramToTable(int programIndex);
// ���������� ���������� ������� � ����������
	virtual void TableToProgram(int programIndex);

// ����� ��������� ��� �������� ����������
	virtual void GetTipText(CString& tipText, int controlID) const;

// ���������� ��������� � ������������ ������������� �������� ���������
	virtual void ShowErrorMessage(int number, const CString& errorText, bool editItem);
// ���������� ��� ���������� ������ � �������
	virtual void DoOnAddRow(int row, bool emptyRow);
// ���������� ��� �������� ������ �� �������
	virtual void DoOnDeleteRow(int row);
// ���������� ��� ��������� �������� �������
	virtual void DoOnEndCellEditing(int row, int column, const CString& cellText);
	
// �������� �������� ���������� � ������������ � ���������� ��������� �������
	void ChangeTimeUnitsControls();

// ���������� ������� �� � ������� ������������������ ���������
	void FillFromTimeColumn();
// ������������� ���������� ������� �� � �� � ������������ � �������� ���������
	void ConvertTimeUnits(bool secondsToMinutes);

	void SetFixedFields(const CString& fromString);
	void GetFixedFields(CString& toString);

	void InitEluentComboBox(const int comboBoxID, int selectItem);

// ���������� ��������� ����� �������� ����������.
	void ShowToolTipMessage(const int stringID, CWnd* control);

	void CheckIntLimits(CWnd* editControl, int minLimit, int maxLimit);
	void CheckFloatLimits(CWnd* editControl, int minLimit, int maxLimit);

protected:

// ����� ��� �������� ���������� �����
	virtual CString GetCopyStageBuffer() const { return sm_StageBufferString; }
	virtual void SetCopyStageBuffer(const CString& string) { sm_StageBufferString = string; }
	//CMSmartComboBox m_AveragingComboBox;
protected:
	CMFilterEdit m_TotalFlowRateEdit;
	CMFilterEdit m_ConditioningTimeEdit;
	CMFilterEdit m_StartPressureEdit;
	CMFilterEdit m_StartFlowRateEdit;

	//CMSmartComboBox m_StrobeDelayComboBox;
	//CMSmartComboBox m_StrobeDurationComboBox;

protected:
	bool m_TimeInMinutes;
	bool m_IsProgramSelectionDisabled;

// ��������� �������� ���������� � ������� �������
	CString m_TimeString;

protected:
// ����� ��� �������� ���������� �����
	static CString sm_StageBufferString;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_GradientProgramDialog_H__04D162A7_F587_4765_8187_34182785BD62__INCLUDED_)
