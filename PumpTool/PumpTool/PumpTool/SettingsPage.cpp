// SettingsPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "SettingsPage.h"

#include "MainPropertySheet.h"
#include "IsocraticSettingsDialog.h"

static const TCHAR c_RegistrySection[] = _T("PumpSettings");

// ���������� ���� CMSettingsPage

IMPLEMENT_DYNAMIC(CMSettingsPage, CMToolPage)

CMSettingsPage::CMSettingsPage()
	: CMToolPage(CMSettingsPage::IDD)
{
	LoadSettingsFromRegistry();
}

CMSettingsPage::~CMSettingsPage()
{
}

void CMSettingsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}

BOOL CMSettingsPage::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN && (pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_CANCEL))
	{
		// ������ ��������� ���������, ����� ���� �� �����������.
		return TRUE;
	}
	
	return CMToolPage::PreTranslateMessage(pMsg);
}

void CMSettingsPage::EnableControls(bool enable)
{
	GetDlgItem(IDC_BUTTON_CONNECTION)->EnableWindow(enable);
	GetDlgItem(IDC_BUTTON_PARAMETERS)->EnableWindow(enable);
}


BEGIN_MESSAGE_MAP(CMSettingsPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_CONNECTION, &CMSettingsPage::OnBnClickedButtonConnection)
	ON_BN_CLICKED(IDC_BUTTON_PARAMETERS, &CMSettingsPage::OnBnClickedButtonParameters)
END_MESSAGE_MAP()


// ����������� ��������� CMSettingsPage

void CMSettingsPage::OnBnClickedButtonConnection()
{
	((CMMainPropertySheet*)GetParent())->SelectConnection(0);
}

void CMSettingsPage::OnBnClickedButtonParameters()
{
	CMIsocraticSettingsDialog dialog;
	dialog.m_MaxPressureWork.Format(_T("%.1f"), m_Settings.m_MaxPressureWork*0.1);
	dialog.m_MinPressureWork.Format(_T("%.1f"), m_Settings.m_MinPressureWork*0.1);
	dialog.m_MaxPressureDischarge.Format(_T("%.1f"), m_Settings.m_MaxPressureDischarge*0.1);
	dialog.m_ReleasingFlowRate.Format(_T("%d"), m_Settings.m_ReleasingFlowRate);
	dialog.m_AutomaticPressureReleasing = m_Settings.m_AutomaticPressureReleasing;
	if(dialog.DoModal() == IDOK)
	{
		m_Settings.m_MaxPressureWork = int(_tstof(dialog.m_MaxPressureWork)*10.0+0.5);
		m_Settings.m_MinPressureWork = int(_tstof(dialog.m_MinPressureWork)*10.0+0.5);
		m_Settings.m_MaxPressureDischarge = int(_tstof(dialog.m_MaxPressureDischarge)*10.0+0.5);
		m_Settings.m_ReleasingFlowRate = _tstoi(dialog.m_ReleasingFlowRate);
		m_Settings.m_AutomaticPressureReleasing = dialog.m_AutomaticPressureReleasing != 0;

		SaveSettingsToRegistry();
		m_Settings.m_AreChanged = true;
	}
}

void CMSettingsPage::SaveSettingsToRegistry()
{
	CWinApp* app = AfxGetApp();

	app->WriteProfileInt(c_RegistrySection, _T("MaxPressureWork"), m_Settings.m_MaxPressureWork);
	app->WriteProfileInt(c_RegistrySection, _T("MinPressureWork"), m_Settings.m_MinPressureWork);
	app->WriteProfileInt(c_RegistrySection, _T("MaxPressureDischarge"), m_Settings.m_MaxPressureDischarge);
	app->WriteProfileInt(c_RegistrySection, _T("AutomaticReleasing"), m_Settings.m_AutomaticPressureReleasing);
	app->WriteProfileInt(c_RegistrySection, _T("ReleasingFlowRate"), m_Settings.m_ReleasingFlowRate);
}

void CMSettingsPage::LoadSettingsFromRegistry()
{
	CWinApp* app = AfxGetApp();
	CString string;

	m_Settings.m_MaxPressureWork            = app->GetProfileInt(c_RegistrySection, _T("MaxPressureWork"), 300);
	m_Settings.m_MinPressureWork            = app->GetProfileInt(c_RegistrySection, _T("MinPressureWork"), 0);
	m_Settings.m_MaxPressureDischarge       = app->GetProfileInt(c_RegistrySection, _T("MaxPressureDischarge"), 8);
	m_Settings.m_AutomaticPressureReleasing = app->GetProfileInt(c_RegistrySection, _T("AutomaticReleasing"), 1) != 0;
	m_Settings.m_ReleasingFlowRate          = app->GetProfileInt(c_RegistrySection, _T("ReleasingFlowRate"), 1000);

	m_Settings.m_AreChanged = true;
}
