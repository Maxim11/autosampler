// PressureReleasingDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "PressureReleasingDialog.h"

#include "Instrument/Piton.h"

// ���������� ���� CMPressureReleasingDialog

IMPLEMENT_DYNAMIC(CMPressureReleasingDialog, CDialog)

CMPressureReleasingDialog::CMPressureReleasingDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMPressureReleasingDialog::IDD, pParent)
{
	m_PumpA = 0;
	m_PumpB = 0;
	m_From = 100;
	m_To   = 0;
}

CMPressureReleasingDialog::~CMPressureReleasingDialog()
{
}

void CMPressureReleasingDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMPressureReleasingDialog::OnInitDialog()
{
	BOOL result = CDialog::OnInitDialog();
	CenterWindow();

	CProgressCtrl* progressBar = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS);
	progressBar->SetRange(m_To, m_From);
	progressBar->SetPos(m_From);

	return result;
}

void CMPressureReleasingDialog::SetProgressLimits(int from, int to)
{
	m_From = from;
	m_To = to;

	if(GetSafeHwnd() != 0)
	{
		CProgressCtrl* progressBar = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS);
		progressBar->SetRange(to, from);
		progressBar->SetPos(from);
	}
}

void CMPressureReleasingDialog::SetProgressPosition(int value)
{
	CProgressCtrl* progressBar = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS);
	progressBar->SetPos(value);
	// �������� ������������, m_To - ������ �������.
/*
	if(value <= m_To)
	{
		CloseWindow();
	}
*/
}

void CMPressureReleasingDialog::SetPumps(CGPiton* pumpA, CGPiton* pumpB)
{
	m_PumpA = pumpA;
	m_PumpB = pumpB;
}

void CMPressureReleasingDialog::OnCancel()
{
	//CWnd* parent = GetParent();
	if(m_PumpA != 0)
	{
		m_PumpA->StopCurrentMode();
	}
	if(m_PumpB != 0)
	{
		m_PumpB->StopCurrentMode();
	}

	CDialog::OnCancel();
}


BEGIN_MESSAGE_MAP(CMPressureReleasingDialog, CDialog)
END_MESSAGE_MAP()


// ����������� ��������� CMPressureReleasingDialog
