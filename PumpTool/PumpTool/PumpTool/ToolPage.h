#pragma once

#include "Controls\FilterEdit.h"
#include "Controls\SmartComboBox.h"
#include "Controls\IntegerValidator.h"
#include "Controls\FloatValidator.h"
#include "Controls\TrackingToolTip.h"

// ������������� ���������� ������� ������ �������� �����
// �������� �� �� ������ ������ ��������� ������ ��� ������������.
#define USE_MODAL_P_R_DIALOG   1

// CMToolPage
//class CGPiton;
struct CSPumpSettings
{
	int m_MaxPressureWork;
	int m_MinPressureWork;
	int m_MaxPressureDischarge;
	int m_ReleasingFlowRate;
	bool m_AutomaticPressureReleasing;

	bool m_AreChanged;
};


class CMToolPage : public CMFCPropertyPage
{
	DECLARE_DYNAMIC(CMToolPage)

public:
	CMToolPage(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CMToolPage();

	void CreateTimeString(CString& text, const int volume, const int flowRate);

	bool ShowPressureReleasingDialog(int from, int to, int headerTextId = 0);
	//void CreateVersionString(const CGCanFrame* canFrame);
	//void ClearVersionString();
	CString GetHeaderInfoString() { return m_HeaderInfoString; }

	// ���������� ������������ ��������� ������� �������� ����.
	virtual void OnTimer() { };
	virtual void InitForRunningState() {};

	virtual void EnableControls(bool enable) {};

protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnApply();

protected:
	int   GetIntValue(int controlID);
	void  SetIntValue(int controlID, int value);
	int GetCheckedRadioButton(const int buttonID[]);

	void CreateLargeFont(const CWnd* control);
	void EnableControls(const int controlID[], const int enable);
	void EnableModeButtons(const int enable);
	void InitEluentsComboBox(const int comboBoxID, const CString& savedName, const int select);
	void InitEluentsComboBox(class CMSmartComboBox* eluenstComboBox);

	void EnableOtherPages(bool enable);

	void ShowValuesForPump(class CGPiton* pump, const int controlID[]);
	void ResetValuesForPump(const int controlID[]);

	bool StartPumpMode(int mode, int maxPressure, int flowRate);
	virtual void StartPressureReleasing(int flowRate) { };

	//virtual void StartFillMode() { };
	//virtual void StartDischargeMode() { };

	void ShowErrorMessage(const int textID, const int errorTextID, const int errorCode);
	void SetPumpSerialNumber(int serialNumber, int stringID, int controlID);
	void SetStartButtonText(const int textID);
	// �������������� ��������� ��������.
	void UpdatePageHeader();

	// ���������� ��������� � ����������� ��������� ��������
	void ShowToolTipMessage(CString& message, CWnd* control, int interval = 5000);
	// ������� ��������� � ����������� ��������� ��������
	void HideToolTipMessage();


protected:
	// ����� ���� �� ���� CAN.
	//BYTE m_NodeAddress;

	class CGPiton* m_PumpA;
	class CGPiton* m_PumpB;

	CString m_HeaderInfoString;

	// ���������� ��������� ����� ��� ������ ����� ������ �������.
	int m_SelectedMode;

	static CSPumpSettings m_Settings;

	//bool m_IsErrorShowing;
protected:
	// ���� ��� ����������� ��������� � ����������� ��������� ���������.
	CMTrackingToolTip m_TrackingToolTip;

	// �������, ������������ � �������� �������� � ����� �������������� �����.
	CGIntegerValidator m_IntegerValidator;
	CGIntegerValidator m_SignedIntegerValidator;
	CGFloatValidator m_FloatValidator;

	class CMErrorDialog* m_ErrorDialog;
	//class CMPressureReleasingDialog* m_PressureReleasingDialog;

	CFont m_LargeFont;
protected:
	// ��������� ����� ������ (������� ������). ����� ���������� ��
	// ��������� �������.
	static int  sm_CurrentMode;
};

enum
{
	// ��� ��������� �����.
	PUMP_MODE_UNKNOWN = -1,
	PUMP_MODE_WAIT,
	PUMP_MODE_FILL,
	PUMP_MODE_DISCHARGE,
	PUMP_MODE_WORK,
	PUMP_MODE_PRESSURE_RELEASING,
};

