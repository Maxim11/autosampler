#pragma once

class CGPiton;

// ���������� ���� CMPressureReleasingDialog

class CMPressureReleasingDialog : public CDialog
{
	DECLARE_DYNAMIC(CMPressureReleasingDialog)

public:
	CMPressureReleasingDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~CMPressureReleasingDialog();

	void SetProgressLimits(int from, int to);
	void SetProgressPosition(int value);

	void SetPumps(CGPiton* pumpA, CGPiton* pumpB);

// ������ ����������� ����
	enum { IDD = IDD_DIALOG_PRESSURE_RELEASING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual void OnCancel();
	virtual BOOL OnInitDialog();

protected:
	int m_From;
	int m_To;

	CGPiton* m_PumpA;
	CGPiton* m_PumpB;

	DECLARE_MESSAGE_MAP()
};
