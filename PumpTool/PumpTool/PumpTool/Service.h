// Service.h: interface for the CService class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICE_H__5E2BEDE6_1575_11D6_9B3F_F9CE3511C219__INCLUDED_)
#define AFX_SERVICE_H__5E2BEDE6_1575_11D6_9B3F_F9CE3511C219__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"

class CGIntegerArray : public CArray<int, int>
{
public:
	CGIntegerArray(int size = 100) { SetSize(0, size); }
	virtual ~CGIntegerArray() {};
	
// ������� ������ ��� ������������ ���������� ������	
	void Clear() { RemoveAt(0, GetSize()); }
};

class CGDoubleArray : public CArray<double, const double&>
{
public:
	CGDoubleArray(int size = 100) { SetSize(0, size); }
	virtual ~CGDoubleArray() {};
	
// ������� ������ ��� ������������ ���������� ������	
	void Clear() { RemoveAt(0, GetSize()); }
};

typedef short            int16;
typedef unsigned short  uint16;
typedef long             int32;
typedef unsigned long   uint32;

bool IsRunningUnderWindows5();

// ���������� �� ������ ��� ������� ������� ����������
double RoundUpValue(double value, double denominator, bool upper = false);

// ���������� ��������� ������ ������� ��� ������ ������������� �����
// ���������� ����� ������
int CharsBeforeDigitalPoint(double value);
int CharsAfterDigitalPoint(double value);

// �������������� � ����������� ���������� �������
void DoubleToString(double value, CString& string);
// ��������� ������� ����� ��������� ����� ��� �������
void LongIntToString(int value, CString& string);
// �������������� ����� � ����������� ���������� �������
void MinutesToString(double value, CString& string);

// ������ ������, ���������� ������ ����� ����� ����� �����������
void ParseString(const CString& string, const char delimiter, int result[], int size);
// ������ ������, ���������� ������ ��������
void ParseString(const CString& string, const char delimiter, CStringArray& result);

// ��������� �������� ListBox'� �� ������
void SetListBoxItems(CListBox* listBox, CString& string, int start = 0);
// ���������� �������� ListBox'� � ������
void GetListBoxItems(CListBox* listBox, CString& string, int start = 0);

void SetListCtrlItems(CListCtrl* listCtrl, CString& string, int start = 0);
void GetListCtrlItems(CListCtrl* listCtrl, CString& string, int start = 0);

void SetSymbolFont(CWnd* window);

// ����������, ����� ������ ������������ ��� ��������� ������� ����� � �������� ����������
TCHAR GetLocaleDecimalSeparator();

// �������������� ������
inline double SecondsToMinutes(double value) { return value/60.0; }
inline double MinutesToSeconds(double value) { return value*60.0; }

inline void IntToString(int value, CString& string)
{
	string.Format(_T("%d"), value);
}

inline CString IntToString(int value)
{
	CString string;
	string.Format(_T("%d"), value);
	return string;
}

inline int StringToInt(const CString& string)
{
	return _tstoi(LPCTSTR(string));
}

inline double StringToDouble(const CString& string)
{
	return _tstof(LPCTSTR(string));
}


#endif // !defined(AFX_SERVICE_H__5E2BEDE6_1575_11D6_9B3F_F9CE3511C219__INCLUDED_)
