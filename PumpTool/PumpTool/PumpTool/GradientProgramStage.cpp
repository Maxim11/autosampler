// GradientProgramStage.cpp: implementation of the CGGradientProgramStage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GradientProgramStage.h"

#include "Instrument/Piton.h"
#include "ProgramData.h"
#include "Service.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CGGradientProgramStage::SetDataFromString(CString string)
{
	CStringArray stringArray;
	ParseString(string, c_Delimiter, stringArray);

	if(stringArray.GetSize() < 4)
		return;

	if(stringArray[0].IsEmpty())
	{
		m_FinishTime  = -1;
	}
	else
	{
		m_FinishTime  = StringToInt(stringArray[0]);
	}

	if(stringArray[2].IsEmpty())
	{
		 m_PumpA_Begin = 0;
	}
	else
	{
		m_PumpA_Begin = StringToInt(stringArray[2]);
	}

	if(stringArray[3].IsEmpty())
	{
		m_PumpA_End  = 0;
	}
	else
	{
		m_PumpA_End   = StringToInt(stringArray[3]);
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGGradientProgram::CGGradientProgram()
{
	m_StartPressure = 0;
	m_StartFlowRate = 0;
	m_WorkFlowRate = 0;
	m_ConditioningTime = 0;
	m_RestartAfterStop = false;

	m_StageIndex = c_StageNone;
	m_PumpA = 0;
	m_PumpB = 0;
}

CGGradientProgram::~CGGradientProgram()
{

}

// �������� �������� ������������� ��������� �� ����������.
bool CGGradientProgram::GetFromProgramData(CGProgramData* programData, bool timeInMinutes)
{
	m_StagesList.RemoveAll();

	CGGradientProgramStage stage;
	POSITION pos = programData->GetHeadPosition();
	int number = 0;
	int startTime = 0;

	while(pos)
	{
		stage.SetDataFromString(programData->GetNext(pos));
		stage.m_Duration = stage.m_FinishTime - startTime;
		stage.m_Number = number++;

		m_StagesList.AddTail(stage);
		
		startTime = stage.m_FinishTime;
	}
	m_Position = m_StagesList.GetHeadPosition();

	CStringArray stringArray;
	ParseString(programData->GetFixedParameters(), c_Delimiter, stringArray);
	m_EluentA = programData->GetEluentA();
	m_EluentA = programData->GetEluentB();

	if(stringArray.GetSize() < 4)
	{
		return false;
	}

	m_WorkFlowRate = StringToInt(stringArray[0]);
	m_ConditioningTime = StringToInt(stringArray[1]);
	m_StartPressure = StringToInt(stringArray[2]);
	m_StartFlowRate = StringToInt(stringArray[3]);

	return true;
}

/*
CGGradientProgramStage& CGGradientProgram::GetNextStage()
{
	return m_StagesList.GetNext(m_Position);
}
*/
// "����������" ����� ��������� ���������� �������.
const int c_PumpStage[] = { 4, 5 };

void CGGradientProgram::Start(CGPiton* pumpA, CGPiton* pumpB, bool fromConditioning)
{
	m_PumpA = pumpA;
	m_PumpB = pumpB;
	if(m_PumpA->GetInternalValveState() != VALVE_STATE_WORK || m_PumpB->GetInternalValveState() != VALVE_STATE_WORK)
	{
		// ������������ ���� � ��������� ������
		m_PumpA->SetInternalValveState(VALVE_STATE_WORK);
		m_PumpB->SetInternalValveState(VALVE_STATE_WORK);
		m_StageIndex = c_StageSetValve;
		return;
	}

	if(!fromConditioning && m_StartPressure > 0
		&& m_PumpA->GetCurrentPressure() < m_StartPressure)
	{
		StartAcceleration();
	}
	else
	{
		StartConditioning();
	}
}

void CGGradientProgram::StartAcceleration()
{
	// � ������� ������ - �������� �� ������ �������.
	int accelerationFlowRate = m_StartFlowRate/2;

	m_StageIndex = c_StageStart;
	m_PumpA->StartGradientStage(1, accelerationFlowRate, accelerationFlowRate);
	m_PumpB->StartGradientStage(1, accelerationFlowRate, accelerationFlowRate);
}

void CGGradientProgram::StartConditioning()
{
	m_Position = m_StagesList.GetHeadPosition();

	CGGradientProgramStage& firstStage = m_StagesList.GetAt(m_Position);
	int beginFlowRateA = (m_WorkFlowRate*firstStage.m_PumpA_Begin+50)/100;
	int beginFlowRateB = m_WorkFlowRate - beginFlowRateA;

	// ������������ � ��������.
	m_CurrentStageDuration = m_ConditioningTime*60;
	m_StageIndex = c_StageConditioning;

	m_PumpA->StartGradientStage(1, beginFlowRateA, beginFlowRateA);
	m_PumpB->StartGradientStage(1, beginFlowRateB, beginFlowRateB);
}

void CGGradientProgram::Restart()
{
	if(m_StageIndex == c_StageStart)
	{
		Start(m_PumpA, m_PumpB, false);
	}
	if(m_StageIndex == c_StageConditioning)
	{
		// �������� � �����������������, ��������� ������.
		Start(m_PumpA, m_PumpB, true);
	}
}

void CGGradientProgram::Stop()
{
	if(m_PumpA != 0)
	{
		m_PumpA->StopCurrentMode();
	}
	if(m_PumpB != 0)
	{
		m_PumpB->StopCurrentMode();
	}

	m_StageIndex = c_StageNone;
}

/*
void CGGradientProgram::LoadPreliminaryStage(int n, int flowRateA, int flowRateB)
{
	//m_PumpA->SetGradientStage(c_PumpStage[n], 1, flowRateA, flowRateA);
	//m_PumpB->SetGradientStage(c_PumpStage[n], 1, flowRateB, flowRateB);
	m_PumpA->StartGradientStage(1, flowRateA, flowRateA);
	m_PumpB->StartGradientStage(1, flowRateB, flowRateB);
}
*/

bool CGGradientProgram::StartNextStage()
{
	if(m_Position)
	{
		// �� ������ ������� ����� m_Position ��� ��������� �� ���� ����� �����������������.
		CGGradientProgramStage& nextStage = m_StagesList.GetAt(m_Position);
		int beginFlowRateA = (m_WorkFlowRate*nextStage.m_PumpA_Begin+50)/100;
		int beginFlowRateB = (m_WorkFlowRate - beginFlowRateA);
		int endFlowRateA   = (m_WorkFlowRate*nextStage.m_PumpA_End+50)/100;
		int endFlowRateB   = (m_WorkFlowRate - endFlowRateA);

		m_StageIndex++;
		m_CurrentStageDuration = nextStage.m_Duration*60;
		m_PumpA->StartGradientStage(nextStage.m_Duration, beginFlowRateA, endFlowRateA);
		m_PumpB->StartGradientStage(nextStage.m_Duration, beginFlowRateB, endFlowRateB);

		// ��������� � ���������� �����.
		m_StagesList.GetNext(m_Position);
		return true;
	}
	return false;
}

/*
void CGGradientProgram::StartLoadedStage(int n)
{
	//m_PumpA->StartGradientStage(c_PumpStage[n % 2]);
	//m_PumpB->StartGradientStage(c_PumpStage[n % 2]);
}
*/

bool CGGradientProgram::IsRunning()
{
	return (m_StageIndex != c_StageNone);
}

bool CGGradientProgram::IsStartStageActive()
{
	return (m_StageIndex == c_StageStart);
}

bool CGGradientProgram::IsWaitingStageActive()
{
	return (m_StageIndex == c_StageWait);
}

// ��������� ������ ��� �����-��������.
void CGGradientProgram::StartFirstWorkStage()
{
	m_StageIndex = 0;
	StartNextStage();
}

int CGGradientProgram::GetStageDuration()
{
	return m_CurrentStageDuration;
}

#if 0
void CGGradientProgram::StartNextStage()
{/*
	m_StageIndex++;
	// ���������� ����� ����������� ���������.
	StartLoadedStage(m_StageIndex);
	m_CurrentStageDuration = m_StagesList.GetAt(m_Position).m_Duration*60;

	// ��������� � ���������� ����� ���������.
	m_StagesList.GetNext(m_Position);
	if(m_Position != 0)
	{
		// ���� �� ����������, ��������� ��� ��������� � ������.
		LoadNextStage(m_StageIndex+1);
	}
	*/
	m_StageIndex++;
		m_CurrentStageDuration = m_StagesList.GetAt(m_Position).m_Duration*60;
	if(m_Position != 0)
	{
		// ���� �� ����������, ��������� ��� ��������� � ������.
		LoadNextStage(1);
		StartLoadedStage(1);
		m_CurrentStageDuration = m_StagesList.GetAt(m_Position).m_Duration*60;
		// ��������� � ���������� ����� ���������.
		m_StagesList.GetNext(m_Position);
	}
}
#endif

// �������� ������������� ����� �����.
bool CGGradientProgram::Check(int stageTime)
{
	int oldStage = m_StageIndex;

	switch(m_StageIndex)
	{
	case c_StageNone:
		break;
	case c_StageSetValve:
		if(m_PumpA->GetInternalValveState() == VALVE_STATE_WORK && m_PumpB->GetInternalValveState() == VALVE_STATE_WORK)
		{
			// �������� ������.
			StartAcceleration();
		}
		break;
	case c_StageStart:
		if(m_PumpA->GetCurrentPressure() >= m_StartPressure && m_PumpB->GetCurrentPressure() >= m_StartPressure)
		{
			// �������� �����������������.
			StartConditioning();
		}
		else
		{
			// TODO ��������� �� ������, ���� �������� �� �������������.
		}
		break;
	case c_StageConditioning:
		if(stageTime >= m_CurrentStageDuration)
		{
			m_StageIndex = c_StageWait;

			// ��� �������.
			//m_CurrentStageDuration = 10;
		}
		break;
	case c_StageWait:
		//if(stageTime >= m_CurrentStageDuration)
		if(m_PumpA->IsExternalValveOn() || m_PumpB->IsExternalValveOn())
		{
			// ����� ��������� �� 1 � StartNextStage().
			m_StageIndex = 0;
			StartNextStage();
		}
		break;
	default:
		if(stageTime >= m_CurrentStageDuration)
		{
			if(!StartNextStage())
			{
				// ��� ����� ���������.
				if(m_RestartAfterStop)
				{
					// �������� �����������������.
					StartConditioning();
				}
				else
				{
					Stop();
				}
			}
		}
	}

	// ����� ����� ���������.
	return (oldStage != m_StageIndex);
}

