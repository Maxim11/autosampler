#pragma once
#include "ToolPage.h"


// ���������� ���� CMThermostatPage

class CMThermostatPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMThermostatPage)

public:
	CMThermostatPage();
	virtual ~CMThermostatPage();

// ������ ����������� ����
	enum { IDD = IDD_PAGE_THERMO };

	void ShowThermostatState();

public:
	afx_msg void OnBnClickedStartOperation();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();

	void StartWriteLog();
	void StopWriteLog();

protected:
	CMSmartComboBox m_TemperatureComboBox;

	class CGThermostat* m_Thermostat;
	bool m_IsThermostatOn;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeComboTemperature();
	afx_msg void OnStnClickedStaticTempWork();
	afx_msg void OnStnClickedStaticTempRoom();
	afx_msg void OnBnClickedButtonRegulator();
};
