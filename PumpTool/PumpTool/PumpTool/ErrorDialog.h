#pragma once


// ���������� ���� CMErrorDialog

class CMErrorDialog : public CDialog
{
	DECLARE_DYNAMIC(CMErrorDialog)

public:
	CMErrorDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~CMErrorDialog();

// ������ ����������� ����
	enum { IDD = IDD_DIALOG_ERROR_MESSAGE };

// ������ ����� ������ �������
	bool SetErrorCode(const int textID, const int errorTextID, const int errorCode);
	int  GetErrorCode() { return m_ErrorCode; }

	void Update();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual void OnCancel();
	virtual void OnOK();

	virtual BOOL OnInitDialog();

protected:
// ����� ������ �������
	int m_ErrorCode;

	int m_ErrorTextID;
	int m_GroupTextID;

// ������ ���� ������� � ������ �������� (�� �������)
	CSize m_InitialSize;

	CArray<int, int> m_IgnoredErrorsArray;

	DECLARE_MESSAGE_MAP()
};
