#include "stdafx.h"
#include "resource.h"
#include "MainPropertySheet.h"

#include "Instrument/UsbDevices.h"
#include "Instrument/Piton.h"
#include "Instrument/Thermostat.h"
#include "Instrument/Autosampler.h"
#include "SelectInstrumentTypeDialog.h"
#include "Dialogs/InstrumentErrorDialog.h"

//#include "Usb.h"
//#include "InstrumentBase\OutgoingMessage.h"
//#include "InstrumentBase\IncomingMessage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// �������� ��� �������.
//const unsigned short c_Piton4ProductID  = c_UsbCapelProductId;

CGPiton g_PumpA;
CGPiton g_PumpB;
CGThermostat g_Thermostat;
CGAutosampler g_Autosampler;

/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet

IMPLEMENT_DYNAMIC(CMMainPropertySheet, CMFCPropertySheet)

CMMainPropertySheet::CMMainPropertySheet()
	 : CMFCPropertySheet(IDS_MAIN_CAPTION, NULL)
{
	m_psh.dwFlags |= PSH_NOAPPLYNOW; // | PSH_MODELESS;
	m_psh.dwFlags &= ~PSH_HASHELP;


	SetLook(PropSheetLook_Tree, 250);
	SetIconsList(IDB_TREE_ICONS, 16);
	EnablePageHeader(30);

	m_MasterPage = 0;

	CMFCPropertySheetCategoryInfo* �ategoryNode = AddTreeCategory (
		_T("����� ������"), 0, 1);
	AddPageToTree (�ategoryNode, &m_IsocraticPage, -1, 2);
	AddPageToTree (�ategoryNode, &m_GradientPage, -1, 2);

	CMFCPropertySheetCategoryInfo* �ategorySetup = AddTreeCategory (
		_T("���������"), 0, 1);
	AddPageToTree (�ategorySetup, &m_SettingsPage, -1, 2);

	CMFCPropertySheetCategoryInfo* �ategoryThermo = AddTreeCategory (
		_T("���������"), 0, 1);
	AddPageToTree (�ategoryThermo, &m_ThermostatPage, -1, 2);

	CMFCPropertySheetCategoryInfo* �ategoryAutosampler = AddTreeCategory(
		_T("�����������"), 0, 1);
	AddPageToTree(�ategoryAutosampler, &m_AutosamplerPage, -1, 2);

	m_InstrumentErrorDialog = 0;

	m_PumpA = &g_PumpA;
	m_PumpB = &g_PumpB;
	m_PumpB->AssignRoleB();

	m_MasterInstrument = m_PumpA;
	m_IsGradientOn = false;

	m_Thermostat = &g_Thermostat;
	
	m_Autosampler = &g_Autosampler;

	m_IsPumpConnected = false;
	m_IsThermostatConnected = false;
	m_IsAutosamplerConnected = false;
	m_IsCheckConnectionEnabled = true;
	//m_ComPort  = AfxGetApp()->GetProfileString(_T("Settings"), _T("ComPort"),  _T("USB"));
	//m_ComPortB = AfxGetApp()->GetProfileString(_T("Settings"), _T("ComPortB"), _T("USB"));
	m_ComPort  = _T("USB");
	m_ComPortB = _T("USB");
	m_ComPortAutosampler = _T("COM3");
	m_UsbDeviceName	 = AfxGetApp()->GetProfileString(_T("Settings"), _T("UsbName"), _T(""));
	m_UsbDeviceNameB = AfxGetApp()->GetProfileString(_T("Settings"), _T("UsbNameB"), _T(""));

	// ��������� ������ �� COM-�����
	m_DataChannelInfo.m_ChannelType = DCT_USB_HID;			// ��� ������
	m_DataChannelInfo.m_ProtocolType = DCP_HID_1;			// ��� ���������
	m_DataChannelInfo.m_InputBlocks = 50;					// ������ �������� ������ ��������� (� �������)
	m_DataChannelInfo.m_OutputBlocks = 10;					// ������ ��������� ������ ���������
	m_DataChannelInfo.m_InternalBlocks = 0;					// ������ ����������� ������ ���������
	m_DataChannelInfo.m_BufferSize = 2048;

	m_DataChannelInfo.m_IsMaster = true;
	m_DataChannelInfo.m_AutoConnect = false;
	m_DataChannelInfo.m_UsbEnumerationIndex = -1;

	m_DataChannelInfoB.m_BufferSize = 16384;				// ������ ������� �������� COM-�����
	m_DataChannelInfoB.m_InputBlocks = 50;					// ������ �������� ������ ��������� (� �������)
	m_DataChannelInfoB.m_OutputBlocks = 10;					// ������ ��������� ������ ���������
	m_DataChannelInfoB.m_InternalBlocks = 0;				// ������ ����������� ������ ���������
	m_DataChannelInfoB.m_BufferSize = 2048;

	m_DataChannelInfoB.m_IsMaster = false;
	m_DataChannelInfoB.m_AutoConnect = false;
	m_DataChannelInfoB.m_UsbEnumerationIndex = -1;

	m_DataChannelInfoThermo.m_BufferSize = 16384;			// ������ ������� �������� COM-�����
	m_DataChannelInfoThermo.m_InputBlocks = 50;				// ������ �������� ������ ��������� (� �������)
	m_DataChannelInfoThermo.m_OutputBlocks = 10;			// ������ ��������� ������ ���������
	m_DataChannelInfoThermo.m_InternalBlocks = 0;			// ������ ����������� ������ ���������
	m_DataChannelInfoThermo.m_BufferSize = 2048;

	m_DataChannelInfoThermo.m_IsMaster = true;
	m_DataChannelInfoThermo.m_AutoConnect = true;
	m_DataChannelInfoThermo.m_UsbEnumerationIndex = -1;

	m_DataChannelInfoAutosampler.m_ChannelType = DCT_SERIAL_PORT;        // ��� ������
	m_DataChannelInfoAutosampler.m_ProtocolType = DCP_ASCII;            // ��� ���������
	m_DataChannelInfoAutosampler.m_InputBlocks = 50;                    // ������ �������� ������ ��������� (� �������)
	m_DataChannelInfoAutosampler.m_OutputBlocks = 10;                    // ������ ��������� ������ ���������
	m_DataChannelInfoAutosampler.m_InternalBlocks = 0;                    // ������ ����������� ������ ���������
	m_DataChannelInfoAutosampler.m_BufferSize = 2048;
}

CMMainPropertySheet::~CMMainPropertySheet()
{
/*
	m_DataChannel.Close();

//	StopSoftTimerThread(false);

	POSITION pos = m_SlaveInstrumentsList.GetHeadPosition();
	while(pos)
	{
		delete m_SlaveInstrumentsList.GetNext(pos);
	}
*/
}


UINT WM_INSTRUMENT_EVENT = ::RegisterWindowMessage(_T("Message Data Received"));
const int idTree = 101;

BEGIN_MESSAGE_MAP(CMMainPropertySheet, CMFCPropertySheet)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_WM_DEVICECHANGE()
	ON_REGISTERED_MESSAGE(WM_INSTRUMENT_EVENT, OnInstrumentEvent)
	//ON_BN_CLICKED(IDOK, OnOk)
	ON_NOTIFY(TVN_SELCHANGEDW, idTree, &CMMainPropertySheet::OnSelectTree)
END_MESSAGE_MAP()

BOOL CMMainPropertySheet::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN && (pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_CANCEL))
	{
		//SendMessage(WM_COMMAND, IDOK, 0);
		return TRUE;
	}
	
	return CMFCPropertySheet::PreTranslateMessage(pMsg);
}

// ������� ������� ���, ����� ������ ���������� ��c������.
void CMMainPropertySheet::OnSelectTree(NMHDR* pNMHDR, LRESULT* pResult)
{
	CMFCPropertySheet::OnSelectTree(pNMHDR, pResult);

	HTREEITEM firstItem = m_wndTree.GetRootItem();
	m_wndTree.Expand(firstItem, TVE_EXPAND);
	HTREEITEM secondItem = m_wndTree.GetNextSiblingItem(firstItem);
	m_wndTree.Expand(secondItem, TVE_EXPAND);

	if(m_IsPumpConnected)
	{
		ShowConnectionState(true);
	}
	// ����������� �������� ��� ���������� ������. ��������� ��������
	// ����������.
	if(m_MasterPage != 0)
	{
		CMToolPage* activePage = (CMToolPage*)GetActivePage();
		if(activePage != m_MasterPage)
		{
			activePage->EnableControls(false);
		}
		//EnableAllPages(m_MasterPage, false);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMMainPropertySheet message handlers
BOOL CMMainPropertySheet::OnInitDialog()
{
	CString text;
	CMFCPropertySheet::OnInitDialog();
	CenterWindow();

	GetDlgItem(IDHELP)->ShowWindow(false);
	GetDlgItem(IDOK)->ShowWindow(false);
	GetDlgItem(IDCANCEL)->ShowWindow(false);
	text.LoadString(IDS_CONTROL_EXIT);
	GetDlgItem(IDCANCEL)->SetWindowText(text);
	//GetDlgItem(IDCANCEL)->ShowWindow(false);

	CreateStatusWindow();

	StartDataExchange();
	if(!m_UsbDeviceNameB.IsEmpty() && m_UsbDeviceNameB != m_UsbDeviceName)
	{
		m_IsGradientOn = true;
		StartSlaveDataExchange();
	}

	StartThermoDataExchange();

	StartAutosamplerDataExchange();

	m_IsocraticPage.SetPump(m_PumpA);
	m_GradientPage.SetPumpA(m_PumpA);
	m_GradientPage.SetPumpB(m_PumpB);

	// ��������� ������ ��� �������� �����.
	m_TimerID = SetTimer(1, 1000, 0);
	if(m_TimerID == 0)
	{
		AfxMessageBox(IDS_ERROR_STARTING_TIMER);		// "Can't start timer"
		return -1;
	}
	

	/*
	// ������������� "��������" ������ �������� � ���� CMFCPropertySheet::OnSelectTree
	// � �� ������� �� �����. ���� 
	LONG style = ::GetWindowLong(m_wndTree.GetSafeHwnd(), GWL_STYLE);
	style |= TVS_SINGLEEXPAND;
	style |= TVS_HASLINES;
	::SetWindowLong(m_wndTree.GetSafeHwnd(), GWL_STYLE, style);
	*/

	SetActivePage(&m_IsocraticPage);

	HTREEITEM firstItem = m_wndTree.GetRootItem();
	m_wndTree.Expand(firstItem, TVE_EXPAND);
	HTREEITEM secondItem = m_wndTree.GetNextSiblingItem(firstItem);
	m_wndTree.Expand(secondItem, TVE_EXPAND);
	HTREEITEM thirdItem = m_wndTree.GetNextSiblingItem(secondItem);
	m_wndTree.Expand(thirdItem, TVE_EXPAND);

	return TRUE;
}

void CMMainPropertySheet::SetMasterPage(CMToolPage* sourcePage)
{
	// ���������� ��������, � ������� ���� �������.
	m_MasterPage = sourcePage;
}

void CMMainPropertySheet::EnableAllPages(CMToolPage* sourcePage, bool enable)
{
	
	if(sourcePage != &m_IsocraticPage && m_IsocraticPage.GetSafeHwnd() != 0)
	{
		m_IsocraticPage.EnableControls(enable);
	}
	if(sourcePage != &m_GradientPage && m_GradientPage.GetSafeHwnd() != 0)
	{
		m_GradientPage.EnableControls(enable);
	}
	if(m_SettingsPage.GetSafeHwnd() != 0)
	{
		m_SettingsPage.EnableControls(enable);
	}

	if(enable)
	{
		m_MasterPage = 0;
	}
	else
	{
		// ���������� ��������, � ������� ���� �������.
		m_MasterPage = sourcePage;
	}
}

void CMMainPropertySheet::OnTimer(UINT nIDEvent)
{
	if(nIDEvent != m_TimerID)
		return;
			// ���������� ��������� �� ���������� �����.
	//text.LoadString(IDS_CONTROL_CAN_NOT_CONNECTED);
	//m_StatusInfo.SetWindowText(text);
	if(m_MasterInstrument->IsDataChannelCreated() && m_IsCheckConnectionEnabled)
	{
		if(m_IsPumpConnected != m_MasterInstrument->IsInstrumentConnected()
			|| m_IsPumpConnected != m_MasterInstrument->IsConnectionEstablished())
		{
			m_IsPumpConnected = m_MasterInstrument->IsInstrumentConnected();
			ShowConnectionState(false);
			if(m_IsPumpConnected)
			{
				m_MasterInstrument->AskSerialNumber();
			}
		}

		//((CMToolPage*)GetActivePage())->OnTimer();
	}
	if(m_Thermostat->IsDataChannelCreated() && m_IsCheckConnectionEnabled)
	{
		if(m_IsThermostatConnected != m_Thermostat->IsInstrumentConnected()
			|| m_IsThermostatConnected != m_Thermostat->IsConnectionEstablished())
		{
			m_IsThermostatConnected = m_Thermostat->IsInstrumentConnected();
			ShowConnectionState(false);
			if(m_IsThermostatConnected)
			{
				m_Thermostat->AskSerialNumber();
			}
		}
	}

	if (m_Autosampler->IsDataChannelCreated() && m_IsCheckConnectionEnabled)
	{
		if (m_IsAutosamplerConnected != m_Autosampler->IsInstrumentConnected()
			|| m_IsAutosamplerConnected != m_Autosampler->IsConnectionEstablished())
		{
			m_IsAutosamplerConnected = m_Autosampler->IsInstrumentConnected();
			ShowConnectionState(false);
			if (m_IsAutosamplerConnected)
			{
				m_Autosampler->AskSerialNumber();
			}
		}
	}
#if 0
	// ��� ������� ��� �������.
	if(m_PumpA != 0)
	{
		if(m_PumpA->GenerateCurrentValues())
		{
			DoOnInstrumentStateChanged(m_PumpA->GetNetAddress(), c_PumpModeReceived);
		}
	}
#endif
}

BOOL CMMainPropertySheet::OnDeviceChange(UINT nEventType, DWORD_PTR dwData)
{
	if(dwData == 0)
	{
		return TRUE;
	}

	if(m_MasterInstrument != 0)
	{
		TRACE("OnDeviceChange( %d, 0x%x )\n", nEventType, dwData);

		bool deviceIsRemoved = m_MasterInstrument->OnDeviceChange(nEventType, (UINT*)dwData, m_DataChannelInfo);
		if(deviceIsRemoved)
		{
			if(m_PumpA != 0 && !m_PumpA->IsDataChannelOpened())
			{
				if(m_IsocraticPage.GetSafeHwnd() != 0)
				{
					m_IsocraticPage.ShowValuesPumpA(false);
				}
				if(m_GradientPage.GetSafeHwnd() != 0)
				{
					m_GradientPage.ShowValuesPumpA(false);
				}
			}
			if(m_PumpB != 0 && !m_PumpB->IsDataChannelOpened())
			{
				if(m_GradientPage.GetSafeHwnd() != 0)
				{
					m_GradientPage.ShowValuesPumpB(false);
				}
			}
		}
		else
		{
			if(!m_PumpA->IsDataChannelOpened())
			{
				StartDataExchange();
			}
			if(!m_PumpB->IsDataChannelOpened())
			{
				StartSlaveDataExchange();
			}
		}
	}
	
	if(m_Thermostat != 0)
	{
		TRACE("OnDeviceChangeT ( %d, 0x%x )\n", nEventType, dwData);

		bool deviceIsRemoved = m_Thermostat->OnDeviceChange(nEventType, (UINT*)dwData, m_DataChannelInfoThermo);
		if(deviceIsRemoved)
		{
		}
		else
		{
			if(!m_Thermostat->IsDataChannelOpened())
			{
				StartThermoDataExchange();
			}
		}
	}
	
	if (m_Autosampler != 0)
	{
		TRACE("OnDeviceChangeT ( %d, 0x%x )\n", nEventType, dwData);

		bool deviceIsRemoved = m_Autosampler->OnDeviceChange(nEventType, (UINT*)dwData, m_DataChannelInfoAutosampler);
		if (deviceIsRemoved)
		{
		}
		else
		{
			if (!m_Autosampler->IsDataChannelOpened())
			{
				StartAutosamplerDataExchange();
			}
		}
	}
	return TRUE;
}
/*
void CMMainPropertySheet::OnClose()
{
	int i = 2;
	return;
}
*/

// OnClose() ������� �� ����������, ������� ������ ���.
BOOL CMMainPropertySheet::DestroyWindow()
{
	StopDataExchange();

	m_IsocraticPage.SaveToRegistry();
	m_GradientPage.SaveToRegistry();

	return CMFCPropertySheet::DestroyWindow();
}

void CMMainPropertySheet::OnDrawPageHeader (CDC* pDC, int nPage, CRect rectHeader)
{
	CWnd* page = GetActivePage();
	CString text;

	page->GetWindowText(text);

	rectHeader.top += 2;
	rectHeader.right -= 2;
	rectHeader.bottom -= 2;

	pDC->FillRect (rectHeader, &afxGlobalData.brBtnFace);
	pDC->Draw3dRect (rectHeader, afxGlobalData.clrBtnShadow, afxGlobalData.clrBtnShadow);

	CDrawingManager dm (*pDC);
	dm.DrawShadow (rectHeader, 2);	

	//strText.Format (_T("Page %d description..."), nPage + 1);

	CRect rectText = rectHeader;
	rectText.DeflateRect (10, 0);

	CFont* pOldFont = pDC->SelectObject (&afxGlobalData.fontBold);
	pDC->SetBkMode (TRANSPARENT);
	pDC->SetTextColor (afxGlobalData.clrBtnText);

	CString infoString = ((CMToolPage*)GetActivePage())->GetHeaderInfoString();
	if(!infoString.IsEmpty())
	{
		text += _T("  - ");
		text += infoString;
	}

	pDC->DrawText (text, rectText, DT_SINGLELINE | DT_VCENTER);

	pDC->SelectObject (pOldFont);
}

////////////////////////////////////////////////
// ������ � ������� ������

/*
void CMMainPropertySheet::SetUseUsbCanAdapter(bool useAdapter)
{
	m_UseUsbCanAdapter = useAdapter;

	StopDataExchange();

	StartDataExchange();
	// �������� ������ ��������� �����.
	ShowConnectionState();
}
*/

static CString GetSerialNumber(const CString& deviceName)
{
	int index = deviceName.ReverseFind(_T(' '));
	if(index > 0)
	{
		return deviceName.Mid(index+1);
	}
	return CString();
}

bool CMMainPropertySheet::StartDataExchange()
{
	if(m_MasterInstrument == 0)
	{
		return false;
	}

	m_MasterInstrument->Create();
	// ������� ���� ����� �������� ��������� � �������� �� ����, ����������� ��������
	m_MasterInstrument->SetWindowMessageID(this, WM_INSTRUMENT_EVENT);

	//m_IsPumpConnected = m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo);
	if(!m_UsbDeviceName.IsEmpty())
	{
		m_DataChannelInfo.m_UsbSerialNumber = GetSerialNumber(m_UsbDeviceName);
	}

	bool isInitialized = m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo);
	ShowConnectionState(false);
	if(isInitialized)
	{
		m_PumpA->AskSerialNumber();
	}
	//return m_IsPumpConnected;
	return isInitialized;
}

void CMMainPropertySheet::StopDataExchange()
{
	m_MasterInstrument->CloseDataChannel();
}

// �������� ����� ����� ����������� ����� ������ B.
bool CMMainPropertySheet::StartSlaveDataExchange()
{
	m_PumpB->CGMasterInstrument::Create();
	// ������� ���� ����� �������� ��������� � �������� �� ����, ����������� ��������
	m_PumpB->SetWindowMessageID(this, WM_INSTRUMENT_EVENT);

	//m_IsPumpConnected = m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo);
	bool isInitialized = m_PumpB->InitializeDataChannel(_T("USB"), m_DataChannelInfoB);
	if(isInitialized)
	{
		m_PumpB->AskSerialNumber();
	}

	return isInitialized;
}

void CMMainPropertySheet::StopSlaveDataExchange()
{
	m_PumpB->CloseDataChannel();
}


bool CMMainPropertySheet::StartThermoDataExchange()
{
	if(m_Thermostat == 0)
	{
		return false;
	}

	m_Thermostat->Create();
	// ������� ���� ����� �������� ��������� � �������� �� ����, ����������� ��������
	m_Thermostat->SetWindowMessageID(this, WM_INSTRUMENT_EVENT);

	//m_IsPumpConnected = m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo);
	/*
	if(!m_UsbDeviceName.IsEmpty())
	{
		m_DataChannelInfo.m_UsbSerialNumber = GetSerialNumber(m_UsbDeviceName);
	}
	*/

	bool isInitialized = m_Thermostat->InitializeDataChannel(_T("USB"), m_DataChannelInfoThermo);
	//ShowConnectionState(false);
	if(isInitialized)
	{
		//m_Thermostat->AskSerialNumber();
	}
	//return m_IsPumpConnected;
	return isInitialized;
}

bool CMMainPropertySheet::StartAutosamplerDataExchange()
{
	if (m_Autosampler == 0)
	{
		return false;
	}

	m_Autosampler->Create();
	// ������� ���� ����� �������� ��������� � �������� �� ����, ����������� ��������
	m_Autosampler->SetWindowMessageID(this, WM_INSTRUMENT_EVENT);

	//m_IsPumpConnected = m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo);
	/*
	if(!m_UsbDeviceName.IsEmpty())
	{
		m_DataChannelInfo.m_UsbSerialNumber = GetSerialNumber(m_UsbDeviceName);
	}
	*/

	bool isInitialized = m_Autosampler->InitializeDataChannel(_T("COM3"), m_DataChannelInfoAutosampler);
	//ShowConnectionState(false);
	if (isInitialized)
	{
		//m_Autosampler->AskSerialNumber();
	}
	//return m_IsPumpConnected;
	return isInitialized;
}

void CMMainPropertySheet::StopThermoDataExchange()
{
	m_Thermostat->CloseDataChannel();
}

void CMMainPropertySheet::StopAutosamplerDataExchange()
{
	m_Autosampler->CloseDataChannel();
}
// �������� �������������� ��������, ������������ � ������ ��� ������ �������
const CString c_InstrumentNames[] = { _T("Piton3"), _T("Piton4") };
const int c_NumberOfInstruments = sizeof(c_InstrumentNames)/sizeof(c_InstrumentNames[0]);

// ����� ���� ������� � ����������� �� �������� m_SelectedInstrumentIndex
bool CMMainPropertySheet::SelectConnection(int selectedInstrumentIndex)
{

// �������� ������ ��� ������ ����������. ������������, ����� � ������ ������
// ���������� ������ ���������
	CString usedPorts[c_NumberOfInstruments];

// ���������, ��� ��������� ����� ����������������� � ����������� ������
	//ASSERT(m_DataChannelInfo.m_InputBlocks != 0);

// ��������� ������������ ����, ����� �� ����� � ������ ��������� ������
	if(m_MasterInstrument != 0)
	{
		m_MasterInstrument->CloseDataChannel();
	}
	if(m_PumpB->IsMaster())
	{
		m_PumpB->CloseDataChannel();
	}

	m_IsPumpConnected = false;
	// ��� ������������ ����������� USB-COM ����������, ��� �� �������� ������������
	// ����� ��������� �������� ��������.
	::Sleep(150);

	CMSelectPumpConnectionDialog dialog(this);

	//dialog.m_ConnectionPort  = m_ComPort;
	//dialog.m_ConnectionPortB = m_ComPortB;
	dialog.m_UsbDevice  = m_UsbDeviceName;
	dialog.m_UsbDeviceB = m_UsbDeviceNameB;
	//dialog.m_UsbIndex = m_DataChannelInfo.m_UsbEnumerationIndex;
	//dialog.m_UsbIndexB = m_DataChannelInfoB.m_UsbEnumerationIndex;
	dialog.m_InstrumentIndex = selectedInstrumentIndex;
	//dialog.m_IsGradientOn = m_IsGradientOn;
	//dialog.SetPortNames(usedPorts);
// �������� ����� ���� ��������� ����� ������� ��������� �� ������ ���������,
// ������� ������ ��� � ���� ������ ������
	//dialog.EnablePortSelectionOnly(false);

// ��������� ������� ����� ��� �������� ������� ��  �������, � ��������� ������
// � ������ USB ������������. ������� ������ ������ ����. ��� �� ������ ���������
// � ����������, ��������� ������ ���������� ������������ ������ �������� � ������
// ������ ������
	m_IsCheckConnectionEnabled = false;

	//AfxMessageBox(_T("Test"));
	if (dialog.DoModal() != IDOK)
	{
		return false;
	}
/*
 	if(dialog.m_ConnectionPort != m_ComPort)
	{
		m_ComPort = dialog.m_ConnectionPort;
		AfxGetApp()->WriteProfileString(_T("Settings"), _T("ComPort"), m_ComPort);
	}
*/
	if(	dialog.m_InstrumentIndex != selectedInstrumentIndex)
	{
		AfxGetApp()->WriteProfileString(_T("Settings"), _T("Instrument"),
			c_InstrumentNames[selectedInstrumentIndex]);

	}

	m_IsCheckConnectionEnabled = true;

	CString serialNumber = GetSerialNumber(dialog.m_UsbDevice);

	// ��� ��������� �������
	if(dialog.m_InstrumentIndex != selectedInstrumentIndex || 
		m_DataChannelInfo.m_UsbSerialNumber != serialNumber)
	{
		//m_SelectedInstrumentIndex = dialog.m_InstrumentIndex;

		m_UsbDeviceName = dialog.m_UsbDevice;
		AfxGetApp()->WriteProfileString(_T("Settings"), _T("UsbName"), m_UsbDeviceName);

		m_DataChannelInfo.m_UsbEnumerationIndex = -1;
		m_DataChannelInfo.m_UsbSerialNumber = serialNumber;

		//SelectInstrumentType(selectedInstrumentIndex);

		if(StartDataExchange())
		{
// ������ ���������, ����� ������ ����� ��������
// (���� �� �������� ���������).
			//m_PumpA->AskSerialNumber();
		}
/*
		if(m_DataChannelStatisticDialog != 0)
		{
			m_DataChannelStatisticDialog->SetInstrument(m_MasterInstrument);
		}
*/
	}
	else
	{
		SetConnectionOnComPort(m_ComPort);
	}

/*	if(dialog.m_IsGradientOn != m_IsGradientOn)
	{
		//AfxGetApp()->WriteProfileInt(_T("Settings"), _T("Gradient"), m_IsGradientOn);
	}
	if(dialog.m_ConnectionPortB != m_ComPortB)
	{
		AfxGetApp()->WriteProfileString(_T("Settings"), _T("ComPortB"), m_ComPortB);
	}

	m_IsGradientOn = dialog.m_IsGradientOn;
	m_ComPortB = dialog.m_ConnectionPortB;
*/
	// ��� ������� ������ ������� �������� ���.
	m_IsGradientOn = !dialog.m_UsbDeviceB.IsEmpty();

	if(m_IsGradientOn)
	{
		//if(dialog.m_ConnectionPortB[0] == _T('U'))
		if(true)
		{
			CString serialNumberB = GetSerialNumber(dialog.m_UsbDeviceB);
			if(serialNumberB != serialNumber)
			{
				m_UsbDeviceNameB = dialog.m_UsbDeviceB;
				AfxGetApp()->WriteProfileString(_T("Settings"), _T("UsbNameB"), m_UsbDeviceNameB);
				m_DataChannelInfoB.m_UsbEnumerationIndex = -1;
				m_DataChannelInfoB.m_UsbSerialNumber = serialNumberB;
				if(StartSlaveDataExchange())
				{
					m_PumpB->AskSerialNumber();
				}
			}
			else
			{
				AfxMessageBox(_T("Error! Pump B coincide with Pump A"));
				return false;
			}
		}
		else
		{
			// ��������� �� ���� CAN.
			m_PumpA->ConnectSlavePump(m_PumpB);
		}
	}
	else
	{
		//if(m_PumpB->IsMaster())
		if(true)
		{
			StopSlaveDataExchange();
			m_UsbDeviceNameB.Empty();
			AfxGetApp()->WriteProfileString(_T("Settings"), _T("UsbNameB"), m_UsbDeviceNameB);
		}
		else
		{
			m_PumpA->DisconnectSlavePump(m_PumpB);
		}
	}

	return true;
}

// ������������� ����� ����� ��������� ����
void CMMainPropertySheet::SetConnectionOnComPort(const CString& portName)
{
	/*
	if(portName.IsEmpty())
	{
		CString message;
		message.LoadString(IDS_ERROR_ALL_PORTS_ARE_BUSY);
		AfxMessageBox(message);
// "��� ���������������� ����� ������"
		SetMessageText(IDS_INFO_BROKEN_CONNECTION);
		DoOnComPortError();
		return;
	}
*/
	m_ComPort = portName;

	if(m_MasterInstrument->InitializeDataChannel(m_ComPort, m_DataChannelInfo))
	{
		if(m_MasterInstrument->IsDataChannelOpened())
		{
			m_PumpA->AskSerialNumber();
		}
		//m_IsPumpConnected = true;
		AfxGetApp()->WriteProfileString(_T("Settings"), _T("ComPort"), m_ComPort);
	}
	else
	{
		if(m_ComPort[0] != _T('U'))
		{
			CString message;
			message.Format(IDS_ERROR_OPENING_COM_PORT, m_ComPort);
			AfxMessageBox(message);
	// "������ �������� ����������������� ����� "
//			SetMessageText(IDS_INFO_BROKEN_CONNECTION);
//			DoOnComPortError();
		}
	}
	ShowConnectionState(false);
}


void CMMainPropertySheet::ShowConnectionState(bool showSerialNumber)
{
	CString text = m_ComPort;
	if(!m_IsPumpConnected)
	{
		text.Format(IDS_CONTROL_PUMP_NOT_FOUND, m_ComPort);
	}
	else
	{
		if(!m_MasterInstrument->IsConnectionEstablished())
		{
			text.Format(IDS_CONNECTION_ESTABLISHING, m_ComPort);
		}
		else
		{
			int serialNumberA = -1;
			int serialNumberB = -1;
			if(showSerialNumber)
			{
				serialNumberA = m_PumpA->GetSerialNumber();
				serialNumberB = m_PumpB->GetSerialNumber();
			}
			if(m_IsocraticPage.GetSafeHwnd() != 0)
			{
				m_IsocraticPage.SetPumpSerialNumber(serialNumberA);
			}
			if(m_GradientPage.GetSafeHwnd() != 0)
			{
				m_GradientPage.SetPumpASerialNumber(serialNumberA);
				m_GradientPage.SetPumpBSerialNumber(serialNumberB);
			}
		}
	}
	m_StatusInfo.SetWindowText(text);
}

void CMMainPropertySheet::CreateStatusWindow()
{
	CRect rect;
	int width;
	GetDlgItem(IDOK)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	width = rect.Width();
	rect.left = rect.Height();
	rect.right = rect.left + width*4;
	m_StatusInfo.Create(NULL, WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE, rect, this);
	m_StatusInfo.SetFont(GetDlgItem(IDOK)->GetFont());
}

LRESULT CMMainPropertySheet::OnInstrumentEvent(WPARAM wParam, LPARAM lParam)
{
	switch(wParam & 0xFF)
	{
	case c_InstrumentEventStateChanged:
		// ��������� ��������� �������
		DoOnInstrumentStateChanged(wParam >> 8, lParam);
		break;
	case c_InstrumentEventError:
		// ������ �������
		DoOnInstrumentError(wParam >> 8, lParam);
		break;
	}

	return 0;
}

void CMMainPropertySheet::DoOnInstrumentStateChanged(int instrumentId, int instrumentState)
{
	switch(instrumentState)
	{
	case c_InstrumentInfoChanged:
		// �������� ���������� � ������ � ���� �������
		// �������� ���������� � ������� � ��������� ����.
		ShowConnectionState(true);

		break;
	case c_PumpStateReceived:
		// ���������� � ���������, �������� � �������.
		break;
	case c_PumpModeReceived:
		// ���������� � ������� ������ ������. �������� ��� � �������.

		if(instrumentId == m_PumpA->GetInstrumentId())
		{
			if(m_IsocraticPage.GetSafeHwnd() != 0)
			{
				m_IsocraticPage.ShowValuesPumpA();
				if(m_MasterPage == &m_IsocraticPage)
				{
					m_IsocraticPage.CheckPumpState();
				}
			}
			if(m_GradientPage.GetSafeHwnd() != 0)
			{
				m_GradientPage.ShowValuesPumpA();
				if(m_MasterPage == &m_GradientPage)
				{
					m_GradientPage.CheckPumpsState();
				}
			}
		}
		else
		{
			if(m_GradientPage.GetSafeHwnd() != 0)
			{
				m_GradientPage.ShowValuesPumpB();
				if(m_MasterPage == &m_GradientPage)
				{
					m_GradientPage.CheckPumpsState();
				}
			}
		}
		break;
	case c_PumpProgramStageChanged:
		if(m_GradientPage.GetSafeHwnd() != 0)
		{
			m_GradientPage.SelectNextProgramStage();
		}
		break;
	case c_PumpSmartFillWait:
		ShowInfoString(IDS_INFO_FILL_WAIT);
	case c_PumpSmartFillWork:
		ShowInfoString(IDS_INFO_FILL_WORK);
	case c_PumpSmartFillAirInCamera:
		ShowInfoString(IDS_INFO_FILL_AIR_IN_CAMERA);
		break;
	case c_PumpSmartFillStopped:
		// �������� ����� �� ������� ����.
		ShowInfoString(IDS_INFO_FILL_FINISHED);
		break;

	case c_ThermostatStateReceived:
		ShowThermostatState();
		break;
	}
}

void CMMainPropertySheet::ShowInfoString(int stringID)
{
	if(m_IsocraticPage.GetSafeHwnd() != 0)
	{
		m_IsocraticPage.ShowInfoString(stringID);
	}
}

void CMMainPropertySheet::ShowThermostatState()
{
	if(m_ThermostatPage.GetSafeHwnd() != 0)
	{
		m_ThermostatPage.ShowThermostatState();
	}
}

#include "Instrument\PitonErrors.h"
static CString GetPitonErrorText(int errorCode, bool singleLine)
{
	const int c_groupsID[] =
	{
		0,
		IDS_ERROR_PITON_MOTOR       ,
		IDS_ERROR_PITON_CAN_DRIVER  ,
		IDS_ERROR_PITON_COMM_DRIVER ,
		IDS_ERROR_PITON_INTERNAL       
	};

	int errorGroup = errorCode >> 8;
	CString text;
	CString errorText;
	int stringID;
	if(errorGroup >= sizeof(c_groupsID)/sizeof(c_groupsID[0]))
	{
		return errorText;
	}

	stringID = c_groupsID[errorGroup];
	errorText.LoadString(stringID);
	stringID += BYTE(errorCode);
	text.LoadString(stringID);

	if(singleLine)
		errorText += _T(". ");
	else
		errorText += _T('\n');

	errorText += text;

	return errorText;
}

static CString GetThermostatErrorText(int errorCode)
{
	CString errorText;
	errorText.LoadString(IDS_ERROR_THERMOSTAT_BASE+BYTE(errorCode));

	return errorText;
}

void CMMainPropertySheet::DoOnInstrumentError(int instrumentId, int errorCode)
{
	if(m_InstrumentErrorDialog == 0)
		m_InstrumentErrorDialog = new CMInstrumentErrorDialog(this);

// ����������� ��������� - ���������� ������ � ���������� �� ������.
// ���� ������ ������������� ����������� �� ������ ������ � ������ �������.
	
	CString errorText;

	if(instrumentId == c_InstrumentId_Piton)
	{
		errorText = ::GetPitonErrorText(errorCode, false);
		m_InstrumentErrorDialog->SetHeaderId(IDS_ERROR_PITON_BASE);
	}
	else if(instrumentId == c_InstrumentId_Thermostat)
	{
		errorText = GetThermostatErrorText(errorCode);
		m_InstrumentErrorDialog->SetHeaderId(IDS_ERROR_THERMOSTAT_BASE);
	}

	if(!m_InstrumentErrorDialog->SetErrorCode(errorCode, false, 0, false))
	{
		// ������������ ����� ������������ ��������� �� ������
		//ResetInstrumentError();
		return;
	}

	m_InstrumentErrorDialog->SetErrorText(errorText);

	if(m_InstrumentErrorDialog->GetSafeHwnd() == 0)
	{
		m_InstrumentErrorDialog->Create(IDD_DIALOG_ERROR_MESSAGE, this);
	}
	else
	{
//		m_InstrumentErrorDialog->ShowWindow(true);
		m_InstrumentErrorDialog->Update();
	}

	if(m_InstrumentErrorDialog->GetSafeHwnd() == 0)
	{
		m_InstrumentErrorDialog->ShowWindow(true);
	}
}

