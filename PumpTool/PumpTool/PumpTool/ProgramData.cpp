// ProgramData.cpp: implementation of the CGProgramData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ProgramData.h"

#include "Controls\GridCtrl.h"
#include "User\UniversalDataFile.h"
#include "GradientProgramStage.h"
#include "Service.h"

#include <iostream>
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGProgramData::CGProgramData() : CStringList(5)
{
}

CGProgramData::~CGProgramData()
{

}

void CGProgramData::GetFromGridCtrl(CMGridCtrl* gridCtrl, int startColumn)
{
	RemoveAll();
	CString string;

	for(int i = 0; i < gridCtrl->GetItemCount(); i++)
	{
		gridCtrl->GetRowContent(i, string, startColumn);
		AddTail(string);
	}
}

void CGProgramData::SetToGridCtrl(CMGridCtrl* gridCtrl, int startColumn)
{
	CString string;

	POSITION pos = GetHeadPosition();
	int i = 0;

	gridCtrl->DeleteAllItems();
	while(pos)
	{
		string.Format(_T("%d"), i+1);
		gridCtrl->InsertItem(i, string);
		string = GetNext(pos);

		gridCtrl->SetRowContent(i++, string, startColumn);
	}
}

void CGProgramData::GetFromSectionInfo(CGSectionInfo* sectionInfo)
{
	RemoveAll();

	CString fixedParameters;
	CString key, item;
 
	if(sectionInfo->Lookup(_T("Fixed"), fixedParameters))
	{
		//fixedParameters = ';'+fixedParameters;
		m_FixedParameters = fixedParameters;
	}

	if(sectionInfo->Lookup(_T("EluentA"), item))
	{
		m_EluentA = item;
	}
	if(sectionInfo->Lookup(_T("EluentB"), item))
	{
		m_EluentB = item;
	}

	for(int i = 1; ; i++)
	{
		IntToString(i, key);
		if(sectionInfo->Lookup(key, item))
			AddTail(item);
		else
			break;
	}
}

void CGProgramData::SetToSectionInfo(CGSectionInfo* sectionInfo)
{
	CString key;
	int i = 0;
	sectionInfo->AddString(_T("Fixed"), m_FixedParameters);
	if(!m_EluentA.IsEmpty())
	{
		sectionInfo->AddString(_T("EluentA"), m_EluentA);
	}
	if(!m_EluentB.IsEmpty())
	{
		sectionInfo->AddString(_T("EluentB"), m_EluentB);
	}

	POSITION pos = GetHeadPosition();
	while(pos)
	{
		IntToString(++i, key);
		sectionInfo->AddString(key, GetNext(pos));
	}
}

void CGProgramData::CheckGradientData()
{
	CGGradientProgramStage stage;
	POSITION pos = GetHeadPosition();
	int startTime = 0;
	bool isIncomplete = false;

	m_CheckResult = c_ProgramDataOk;
	m_InvalidCellRow = 0;

// ������� �����
	if(pos == 0)
	{
		m_CheckResult = c_ProgramDataItemIsEmpty;
		m_InvalidCellColumn = 2;
		return; 
	}

	while(pos)
	{
		stage.SetDataFromString(GetNext(pos));
// �������������� ����� ��������� �����
		if(stage.m_FinishTime < 0)
		{
			m_CheckResult = c_ProgramDataItemIsEmpty;
			m_InvalidCellColumn = 2;
			return; 
		}
		if(stage.m_FinishTime <= startTime)
		{
			m_CheckResult = c_ProgramDataFromValueGeToValue;
			m_InvalidCellColumn = 2;
			return; 
		}

		if(stage.m_PumpA_Begin < 0)
		{
			m_CheckResult = c_ProgramDataItemIsEmpty;
			m_InvalidCellColumn = 4;
			return; 
		}
		if(stage.m_PumpA_End < 0)
		{
			m_CheckResult = c_ProgramDataItemIsEmpty;
			m_InvalidCellColumn = 5;
			return; 
		}

		m_InvalidCellRow++;
		startTime = stage.m_FinishTime;
	}
/*
// ��������� ��������� ������
	if(!m_FixedParameters.IsEmpty())
	{
		int begin = m_FixedParameters.Find(';');
		if(begin >= 0)
		{
			CString delayText = m_FixedParameters.Left(begin);
			CString durationText = m_FixedParameters.Mid(begin+1);
			double strobeDelay = atof(delayText);
			double strobeDuration = atof(durationText);

			m_InvalidCellRow = -1;

			if(strobeDelay < g_Spectrofluorimeter.GetStrobeDelayLowLimit() && delayText[0] != '*')
			{
				m_CheckResult = c_ProgramDataStrobeDelayLow;
				m_InvalidCellColumn = 0;
				return; 
			}
			if(strobeDelay > g_Spectrofluorimeter.GetStrobeDelayHighLimit())
			{
				m_CheckResult = c_ProgramDataStrobeDelayHigh;
				m_InvalidCellColumn = 0;
				return; 
			}
			if(strobeDuration < g_Spectrofluorimeter.GetStrobeDurationLowLimit() && durationText[0] != '*')
			{
				m_CheckResult = c_ProgramDataStrobeDurationLow;
				m_InvalidCellColumn = 1;
				return; 
			}
			if(strobeDuration > g_Spectrofluorimeter.GetStrobeDurationHighLimit())
			{
				m_CheckResult = c_ProgramDataStrobeDurationHigh;
				m_InvalidCellColumn = 1;
				return; 
			}
			if(strobeDelay+strobeDuration > g_Spectrofluorimeter.GetStrobeSumHighLimit())
			{
				m_CheckResult = c_ProgramDataStrobeSumHigh;
				m_InvalidCellColumn = 1;
				return; 
			}
		}
	}
	*/
}

bool CGProgramData::IsValid()
{
	return (m_CheckResult <= c_ProgramDataIncomplete);
}

// ���������� ��������� ��������, ������� ������������, ��� ������ ������
int CGProgramData::GetCheckResult()
{
	if(m_CheckResult > c_ProgramDataIncomplete)
		return 2;
	if(m_CheckResult == c_ProgramDataIncomplete)
		return 1;
// Ok
	return 0;
}

void CGProgramData::GetErrorText(CString& errorText)
{
	switch(m_CheckResult)
	{
	case c_ProgramDataOk:
	case c_ProgramDataIncomplete:
		errorText.Empty(); break;
	case c_ProgramDataItemIsEmpty:
		errorText.LoadString(IDS_INFO_FIELD_IS_EMPTY);
		break;
/*
	case c_ProgramDataInvalidFromValue:
		errorText.LoadString(IDS_INFO_INVALID_FROM);
		break;
*/
	case c_ProgramDataFromValueGeToValue:
		errorText.LoadString(IDS_INFO_FROM_GE_TO);
		break;
	}
}

void CGProgramData::operator =(CGProgramData& data)
{
	RemoveAll();
	AddHead(&data);
	m_FixedParameters = data.m_FixedParameters;
	m_EluentA = data.m_EluentA;
	m_EluentB = data.m_EluentB;

	m_IsModified = data.m_IsModified;
	m_CheckResult = data.m_CheckResult;
	m_InvalidCellRow = data.m_InvalidCellRow;
	m_InvalidCellColumn = data.m_InvalidCellColumn;
}

bool CGProgramData::operator ==(CGProgramData& data)
{
	if(GetCount() != data.GetCount())
	{
		return false;
	}

	if(m_FixedParameters != data.m_FixedParameters)
	{
		return false;
	}
	if(m_EluentA != data.m_EluentA || m_EluentB != data.m_EluentB)
	{
		return false;
	}

	POSITION pos1 = GetHeadPosition();
	POSITION pos2 = data.GetHeadPosition();
	while(pos1 && pos2)
	{
		if(GetNext(pos1) != data.GetNext(pos2))
			return false;
	}
	return true;
}

std::istream& operator >>(std::istream& is, CGProgramData& data)
{
	const int c_bufferSize = 256;
	char buffer[c_bufferSize];

	CString string;
	int begin;

	data.RemoveAll();
//	data.GetFixedParameters().Empty();

	while(!is.eof() && is.peek() != '[')
	{
		is.getline(buffer, c_bufferSize);
		string = buffer;

		if((begin = string.Find('=')) < 0)
			continue;

		CString left = string.Left(begin);
		CString right = string.Mid(begin+1);
		if(left == _T("Fixed"))
			data.m_FixedParameters = right;
		else if(left == _T("EluentA"))
			data.m_EluentA = right;
		else if(left == _T("EluentB"))
			data.m_EluentB = right;
		else
			data.AddTail(right);
	}

	return is;
}

std::ostream& operator <<(std::ostream& os, CGProgramData& data)
{
	CString string;

	if(!data.m_FixedParameters.IsEmpty())
		os << _T("Fixed") << '=' << data.m_FixedParameters << _T('\n');

	if(!data.m_EluentA.IsEmpty())
		os << _T("EluentA") << '=' << data.m_EluentA << _T('\n');
	if(!data.m_EluentB.IsEmpty())
		os << _T("EluentB") << '=' << data.m_EluentB << _T('\n');

	POSITION pos = data.GetHeadPosition();
	int i = 0;
	while(pos)
	{
		string.Format(_T("%d"), i+1);
		os << string << '=' << data.GetNext(pos) << '\n';

		i++;
	}

	return os;
}

//////////////////////////////////////////////////////////////////////

CGProgramDataContainer::CGProgramDataContainer()
{
	m_IsModified = false;
	m_Type = 0;

	InitHashTable(101);
}

CGProgramDataContainer::~CGProgramDataContainer()
{
}

// ��������� ���������� ������� ����������
void CGProgramDataContainer::Add(CGProgramDataContainer& container)
{
	CString programName;
	CGProgramData programData;

	POSITION pos = container.GetStartPosition();
	while(pos)
	{
		container.GetNextAssoc(pos, programName, programData);
		SetAt(programName, programData);
	}
}

void CGProgramDataContainer::SetAt(CString& name, CGProgramData& programData)
{
	CMap<CString, CString&, CGProgramData, CGProgramData&>::SetAt(name, programData);

	m_IsModified = true;

// ��������� ��� � ��������������� ������
	POSITION pos = m_SortedNames.GetTailPosition();
	while(pos)
	{
//		CString s = m_SortedNames.GetAt(pos);
		int res = m_SortedNames.GetAt(pos).CollateNoCase(name);
		if(res < 0)
		{
			m_SortedNames.InsertAfter(pos, name);
			return;
		}
		if(res == 0)
			return;

		m_SortedNames.GetPrev(pos);
	}

	m_SortedNames.AddHead(name);
}

bool CGProgramDataContainer::RemoveKey(CString& name)
{
	if(CMap<CString, CString&, CGProgramData, CGProgramData&>::RemoveKey(name))
	{
		POSITION pos = m_SortedNames.Find(name);
		if(pos)
			m_SortedNames.RemoveAt(pos);

		m_IsModified = true;
		return true;
	}
	return false;
}

void CGProgramDataContainer::RemoveAll()
{
	CMap<CString, CString&, CGProgramData, CGProgramData&>::RemoveAll();
	m_SortedNames.RemoveAll();
}

// ������-������

const CString c_TypeNames[] = {
	_T("Gradient"),
};

void CGProgramDataContainer::ReadHeader(std::istream& is)
{
	const int c_bufferSize = 256;
	char buffer[c_bufferSize];

	CString string;
	CString key, value;
	int begin;

	m_IsValidType = false;
	while(!is.eof() && is.peek() != '[')
	{
		is.getline(buffer, c_bufferSize);
		string = buffer;

		if((begin = string.Find('=')) < 0)
			continue;
		key = string.Left(begin);
		value = string.Mid(begin+1);
		if(key == "Type")
		{
			ASSERT(m_Type == 0);
			if(value == c_TypeNames[m_Type])
				m_IsValidType = true;
			else
				break;
		}
	}
}

void CGProgramDataContainer::WriteHeader(std::ostream& os)
{
	os << "[Header]\n";
	os << "Type=" << c_TypeNames[m_Type] << "\n\n";
}

std::istream& operator >>(std::istream& is, CGProgramDataContainer& container)
{
	const int c_bufferSize = 256;
	char buffer[c_bufferSize];

	CGProgramData programData;
	CString name;
	int end;
	int counter = 0;

	container.RemoveAll();
	while(is.rdstate() == 0)
	{
		is.getline(buffer, c_bufferSize);
		if(buffer[0] != '[')
			continue;

		name = buffer+1;
		end = name.Find(']');
		if(end >= 0)
			name = name.Left(end);

		if(counter == 0)	// ���������
		{
			container.ReadHeader(is);
			if(!container.m_IsValidType)
				break;
		}
		else
		{
			is >> programData;
			container.SetAt(name, programData);
		}
		counter++;
	}

	container.m_IsModified = false;

	return is;
}

std::ostream& operator <<(std::ostream& os, CGProgramDataContainer& container)
{
	CString programName;
	CGProgramData programData;

	container.WriteHeader(os);
//	POSITION pos = container.GetStartPosition();
	POSITION pos = container.GetSortedNames()->GetHeadPosition();
	while(pos)
	{
		programName = container.GetSortedNames()->GetNext(pos);
		container.Lookup(programName, programData);
		os << '[' << programName << "]\n";
		os << programData << '\n';
	}

	return os;
}

bool CGProgramDataContainer::LoadFromUniversalDataFile()
{
	CGUniversalDataFile programFile;
	if(programFile.Read(m_FileName) == c_NoError)
	{
		RemoveAll();

		bool isHeader = true;
		POSITION pos = programFile.GetFirstSectionPosition();
		while(pos)
		{
			CGSectionInfo& sectionInfo = programFile.GetSectionInfo(pos);
			if(isHeader)
			{
				CString value;
				isHeader = false;
				if(sectionInfo.Lookup(_T("Type"), value))
				{
					m_IsValidType = (value == c_TypeNames[m_Type]);
				}
				else
				{
					m_IsValidType = false;
				}
				if(!m_IsValidType)
				{
					return false;
				}
			}
			else
			{
				CGProgramData programData;
				programData.GetFromSectionInfo(&sectionInfo);
				SetAt(sectionInfo.GetName(), programData);
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

bool CGProgramDataContainer::SaveToUniversalDataFile()
{
	CString programName;
	CGProgramData programData;

	CGUniversalDataFile programFile;
	CGSectionInfo header(_T("Header"));
	header.AddString(_T("Type"), c_TypeNames[m_Type]);
	programFile.AddSectionInfo(header);

	POSITION pos = GetSortedNames()->GetHeadPosition();
	while(pos)
	{
		programName = GetSortedNames()->GetNext(pos);
		Lookup(programName, programData);

		CGSectionInfo sectionInfo(programName);
		programData.SetToSectionInfo(&sectionInfo);
		programFile.AddSectionInfo(sectionInfo);
	}

	return programFile.Write(m_FileName);
}
