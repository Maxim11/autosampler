// Service.cpp: implementation of the CService class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Service.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <math.h>


bool IsRunningUnderWindows5()
{
	OSVERSIONINFO osVersionInfo = { 0 };
	osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(!GetVersionEx(&osVersionInfo))
    {
	    return false;
    }

    return (osVersionInfo.dwMajorVersion >= 5);
}

double RoundUpValue(double value, double denominator, bool upper)
{
	const double delta = 1.0e-6;
	double rv;
	if(upper)
		rv = ceil(value/denominator-delta)*denominator;
	else
		rv = floor(value/denominator+delta)*denominator;

	return rv;
}

int CharsBeforeDigitalPoint(double value)
{
	if(value == 0.0)
		return 1;

	int before;					// before decimal point
	int v = abs(int(value));

	for(before = 0; v > 0 && before < 20; before++, v /= 10);
	if(before == 0)
		before = 1;
	if(value < 0.0)
		before++;					// ����

	return before;
}

int CharsAfterDigitalPoint(double value)
{
	int after;					// after decimal point
	double v;

	for(after = 0, v = fabs(value); fabs(v-floor(v+0.5)) > 1.0e-4*v && after < 20; after++, v *= 10.0);
// ������ ������������ >= ��� v==0

	return after;
}

// �������������� � ����������� ���������� �������
void DoubleToString(double value, CString& string)
{
	int after = CharsAfterDigitalPoint(value);
//	int total = after+CharsBeforeDigitalPoint(value)+1;
	CString format;
	format.Format(_T("%%.%df"), after);
	string.Format(format, value);
}

// �������������� � ����������� ���������� �������
void DoubleToString(double value, CStringA& string)
{
	int after = CharsAfterDigitalPoint(value);
//	int total = after+CharsBeforeDigitalPoint(value)+1;
	CStringA format;
	format.Format("%%.%df", after);
	string.Format(format, value);
}

// ��������� ������� ����� ��������� ����� ��� �������
void LongIntToString(int value, CString& string)
{
	string.Format(_T("%d"), value);
	for(int index = string.GetLength()-3; index > 0; index -= 3)
		string.Insert(index, ' ');
}

// ������ ������, ���������� ������ ����� ����� ����� �����������
void ParseString(const CString& string, const char delimiter, int result[], int size)
{
	int start = 0;
	int found;
	CString next;

	for(int i = 0; i < size; i++)
	{
		found = string.Find(delimiter, start);
		if(found < 0)
			next = string.Mid(start);
		else
			next = string.Mid(start, found-start);

		result[i] = _tstoi(next);

		if(found < 0)
			break;
		start = found+1;
	}
}

// ������ ������, ���������� ������ ��������
void ParseString(const CString& string, const char delimiter, CStringArray& result)
{
	int start = 0;
	int found;
	CString next;

	result.SetSize(0, 10);

	for(;;)
	{
		found = string.Find(delimiter, start);
		if(found < 0)
			next = string.Mid(start);
		else
			next = string.Mid(start, found-start);

		result.Add(next);

		if(found < 0)
			break;
		start = found+1;
	}
}

void SetListBoxItems(CListBox* listBox, CString& string, int start)
{
	listBox->ResetContent();
	for( ; ; )
	{
		int found = string.Find(';', start);
		if(found < 0)
			break;
		CString next = string.Mid(start, found-start);
		if(!next.IsEmpty())
			listBox->AddString(next);
		start = found+1;
	}
}

void GetListBoxItems(CListBox* listBox, CString& string, int start)
{
//	string.Empty();
	string.Delete(start, string.GetLength()-start);
	for(int i = 0; i < listBox->GetCount(); i++)
	{
		CString text;
		listBox->GetText(i, text);
		if(text.IsEmpty())
			continue;
		string += text;
		string += ';';
	}
}

void SetListCtrlItems(CListCtrl* listCtrl, CString& string, int start)
{
	listCtrl->DeleteAllItems();
	for(int i = 0; ; i++)
	{
		int semicolon = string.Find(';', start);
		if(semicolon < 0)
			break;
		CString next = string.Mid(start, semicolon-start);
		int comma = next.Find(',', 0);
		if(comma > 0)
		{
			listCtrl->InsertItem(i, next.Left(comma));
			listCtrl->SetItemText(i, 1, next.Right(next.GetLength()-(comma+1)));
		}
		start = semicolon+1;
	}
}

void GetListCtrlItems(CListCtrl* listCtrl, CString& string, int start)
{
//	string.Empty();
	string.Delete(start, string.GetLength()-start);
	for(int i = 0; i < listCtrl->GetItemCount(); i++)
	{
		CString text;
		text = listCtrl->GetItemText(i, 0);
		if(text.IsEmpty())
			continue;
		string += text;
		string += ',';
		text = listCtrl->GetItemText(i, 1);
		string += text;
		string += ';';
	}
}

void SetSymbolFont(CWnd* window)
{
	ASSERT(window->GetSafeHwnd() != 0);
	static CFont s_Font;

	if(HFONT(s_Font) == 0)
	{
		LOGFONT logFont = {0};
		window->GetFont()->GetLogFont(&logFont);
		logFont.lfCharSet = SYMBOL_CHARSET;
		lstrcpy(logFont.lfFaceName, _T("Symbol"));

		s_Font.CreateFontIndirect(&logFont);
	}
	window->SetFont(&s_Font);
}

//////////////////////////////////////////////////////////////////////

// ����������, ����� ������ ������������ ��� ��������� ������� ����� � �������� ����������
TCHAR GetLocaleDecimalSeparator()
{
	TCHAR buffer[4];
	::GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, buffer, 4);
	return buffer[0];
}

