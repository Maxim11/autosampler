// GradientWaitDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "GradientWaitDialog.h"


// ���������� ���� CMGradientWaitDialog

IMPLEMENT_DYNAMIC(CMGradientWaitDialog, CDialog)

CMGradientWaitDialog::CMGradientWaitDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMGradientWaitDialog::IDD, pParent)
{

}

CMGradientWaitDialog::~CMGradientWaitDialog()
{
}

void CMGradientWaitDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMGradientWaitDialog::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		GetParent()->SendMessage(WM_COMMAND, ID_COMMAND_START, 0);
		return TRUE;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CMGradientWaitDialog, CDialog)
END_MESSAGE_MAP()


// ����������� ��������� CMGradientWaitDialog
