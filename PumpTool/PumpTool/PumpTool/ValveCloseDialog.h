#pragma once


// ���������� ���� CMValveCloseDialog

class CMValveCloseDialog : public CDialog
{
	DECLARE_DYNAMIC(CMValveCloseDialog)

public:
	CMValveCloseDialog(CWnd* pParent = NULL);   // ����������� �����������
	CMValveCloseDialog(CWnd* pParent, bool openValve);
	virtual ~CMValveCloseDialog();

// ������ ����������� ����
	enum { IDD = IDD_DIALOG_VALVE_CLOSE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();

protected:
	bool m_OpenValve;

	DECLARE_MESSAGE_MAP()
};
