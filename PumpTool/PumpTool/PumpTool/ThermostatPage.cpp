// ThermostatPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "ThermostatPage.h"

#include "Instrument/Thermostat.h"

extern CGThermostat g_Thermostat;

// ���������� ���� CMThermostatPage

IMPLEMENT_DYNAMIC(CMThermostatPage, CMToolPage)

CMThermostatPage::CMThermostatPage()
	: CMToolPage(CMThermostatPage::IDD)
{
	m_Thermostat = &g_Thermostat;
	m_IsThermostatOn = m_Thermostat->IsWorking();
}

CMThermostatPage::~CMThermostatPage()
{
}

void CMThermostatPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}

BOOL CMThermostatPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();

	m_TemperatureComboBox.SetValidator(&m_IntegerValidator);
	m_TemperatureComboBox.SubclassDlgItem(IDC_COMBO_TEMPERATURE, this);
	m_TemperatureComboBox.LimitText(4);

	m_TemperatureComboBox.SetWindowText(_T("25"));

	return result;
}

void CMThermostatPage::ShowThermostatState()
{
	CString text;
	text.Format(_T("%.1f"), m_Thermostat->GetWorkTemperature());
	GetDlgItem(IDC_STATIC_TEMP_WORK)->SetWindowText(text);
	text.Format(_T("%.1f"), m_Thermostat->GetRoomTemperature());
	GetDlgItem(IDC_STATIC_TEMP_ROOM)->SetWindowText(text);
	if(m_Thermostat->IsWorking() && !m_IsThermostatOn)
	{
		// ��������� �������� ��� ��� ���������� ����������.
		CString text;
		m_IsThermostatOn = true;
		m_TemperatureComboBox.SetIntValue(m_Thermostat->GetTargetTemperature()/100);
		m_TemperatureComboBox.EnableWindow(false);
		text.LoadString(IDS_CONTROL_OFF);
		GetDlgItem(IDC_BUTTON_START_OPERATION)->SetWindowText(text);
	}
}

// ����������� ��������� CMThermostatPage
BEGIN_MESSAGE_MAP(CMThermostatPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_START_OPERATION, &CMThermostatPage::OnBnClickedStartOperation)
	ON_CBN_SELCHANGE(IDC_COMBO_TEMPERATURE, &CMThermostatPage::OnCbnSelchangeComboTemperature)
	ON_STN_CLICKED(IDC_STATIC_TEMP_WORK, &CMThermostatPage::OnStnClickedStaticTempWork)
	ON_STN_CLICKED(IDC_STATIC_TEMP_ROOM, &CMThermostatPage::OnStnClickedStaticTempRoom)
	ON_BN_CLICKED(IDC_BUTTON_REGULATOR, &CMThermostatPage::OnBnClickedButtonRegulator)
END_MESSAGE_MAP()

void CMThermostatPage::OnBnClickedStartOperation()
{
	int targetTemperature = 0;
	CString text;
	int stringID;
	bool enable = false;

	if(m_IsThermostatOn)
	{
		stringID = IDS_CONTROL_ON;
		enable = true;
		StopWriteLog();
	}
	else
	{
		targetTemperature = m_TemperatureComboBox.GetIntValue()*100;
		m_TemperatureComboBox.AddCurrentString();
		stringID = IDS_CONTROL_OFF;

		StartWriteLog();
	}
	text.LoadString(stringID);
	GetDlgItem(IDC_BUTTON_START_OPERATION)->SetWindowText(text);
	m_TemperatureComboBox.EnableWindow(enable);

	m_Thermostat->SetWorkTemperature(targetTemperature);
	// ������ ������ ����� ��� ������������ ��� �������.
	m_IsThermostatOn = !m_IsThermostatOn;	
}

void CMThermostatPage::StartWriteLog()
{
	if(!m_Thermostat->IsLogFileEnabled())
	{
		CString fileName;
		CString filePath;
		CTime time;

		time = time.GetCurrentTime();
		fileName = time.Format(_T("TC_%y%m%d_%H%M.log"));

		filePath = CString(_T("Logs"));
		::CreateDirectory(filePath, 0);
		filePath += CString(_T('\\'))+fileName;
		m_Thermostat->EnableLogFile(filePath);
	}
}

void CMThermostatPage::StopWriteLog()
{
	if(m_Thermostat->IsLogFileEnabled())
	{
		m_Thermostat->DisableLogFile();
	}
}


void CMThermostatPage::OnCbnSelchangeComboTemperature()
{
	// TODO: �������� ���� ��� ����������� �����������
}

void CMThermostatPage::OnStnClickedStaticTempWork()
{
	// TODO: �������� ���� ��� ����������� �����������
}


void CMThermostatPage::OnStnClickedStaticTempRoom()
{
	// TODO: �������� ���� ��� ����������� �����������
}


void CMThermostatPage::OnBnClickedButtonRegulator()
{
	// TODO: �������� ���� ��� ����������� �����������
}
