﻿// CMAutosamplerPage.cpp: файл реализации
//

//#include "pch.h"
#include "stdafx.h"
#include "PumpTool.h"
#include "AutosamplerPage.h"
//#include "afxdialogex.h"

#include "Instrument/Autosampler.h"

extern CGAutosampler g_Autosampler;
// Диалоговое окно CMAutosamplerPage


//IMPLEMENT_DYNAMIC(CMAutosamplerPage, CDialogEx)
IMPLEMENT_DYNAMIC(CMAutosamplerPage, CMToolPage)


//CMAutosamplerPage::CMAutosamplerPage(CWnd* pParent /*=nullptr*/)
	//: CDialogEx(IDD_PAGE_AUTOSAMPLER, pParent)
//{

//}

CMAutosamplerPage::CMAutosamplerPage()
	: CMToolPage(CMAutosamplerPage::IDD)
{
	m_Autosampler = &g_Autosampler;
	m_IsAutosamplerOn = m_Autosampler->IsWorking();
}

CMAutosamplerPage::~CMAutosamplerPage()
{
}

void CMAutosamplerPage::DoDataExchange(CDataExchange* pDX)
{
	//CDialogEx::DoDataExchange(pDX);
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SPIN_WashVolume, m_spin_WashVolume);
}

BOOL CMAutosamplerPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();

	m_VialNumberComboBox.SetValidator(&m_IntegerValidator);
	m_VialNumberComboBox.SubclassDlgItem(SolventVial, this);
	m_VialNumberComboBox.LimitText(4);

	m_WasteVialComboBox.SetValidator(&m_IntegerValidator);
	m_WasteVialComboBox.SubclassDlgItem(IDC_WASTE_VIAL, this);
	m_WasteVialComboBox.LimitText(4);

	//m_VialNumberComboBox.SetWindowText(_T("46"));
		
	m_spin_WashVolume.SetRange32(LOWEST_SPIN_VALUE, HIGHEST_SPIN_VALUE);
	m_spin_WashVolume.SetPos(LOWEST_SPIN_VALUE);

	m_WashOn.SubclassDlgItem(IDC_WASH_ON, this);
	m_WashOn.SetMaxNumberOfStrings(2);

	return result;
}

BEGIN_MESSAGE_MAP(CMAutosamplerPage, CMToolPage)
	ON_BN_CLICKED(IDC_BUTTON_START_OPERATION, &CMAutosamplerPage::OnBnClickedStartwash)
	ON_CBN_SELCHANGE(SolventVial, &CMAutosamplerPage::OnCbnSelchangeSolventvial)	
	ON_CBN_SELCHANGE(IDC_WASH_ON, &CMAutosamplerPage::OnCbnSelchangeSolventvial)
	ON_CBN_SELCHANGE(IDC_WASTE_VIAL, &CMAutosamplerPage::OnCbnSelchangeSolventvial)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_WashVolume, &CMAutosamplerPage::OnDeltaposSpinWashvolume)
	ON_EN_CHANGE(IDC_EDIT_WashVolume, &CMAutosamplerPage::OnEnChangeEditWashvolume)
END_MESSAGE_MAP()
// Обработчики сообщений CMAutosamplerPage


void CMAutosamplerPage::OnBnClickedStartwash()
{
	//int targetTemperature = 0;
	int targetVial = 0;
	int wasteVial = 0;
	int washVolume = 0;	
	char *washParameter = new char[6];
	CString stringWashParameter;
	CString stringLoop(_T("Порт"));
	CString text;
	int stringID;
	bool enable = false;

	if (m_IsAutosamplerOn)
	{
		stringID = IDS_CONTROL_ON;
		enable = true;
		//StopWriteLog();
	}
	else
	{
		targetVial = m_VialNumberComboBox.GetIntValue() -1;// *100;
		wasteVial = m_WasteVialComboBox.GetIntValue() -1;// *100;
		washVolume = m_spin_WashVolume.GetPos() * 100;		
		
		m_WashOn.GetWindowText(stringWashParameter);
		washParameter = (stringWashParameter == stringLoop) ? "00" : "01";

		stringID = IDS_CONTROL_OFF;
		//StartWriteLog();
	}
		text.LoadString(stringID);
		GetDlgItem(IDC_BUTTON_START_OPERATION)->SetWindowText(text);
		//m_VialNumberComboBox.EnableWindow(enable);

		m_Autosampler->StartWashExecution(targetVial, wasteVial, washVolume, washParameter);

		int status = m_Autosampler->StatusWashExecution();
		//stringID = IDS_CONTROL_ON;
		//text.LoadString(stringID);
		//GetDlgItem(IDC_BUTTON_START_OPERATION)->SetWindowText(text);
		// Данная логика нужна для тестирования без прибора.
		m_IsAutosamplerOn = !m_IsAutosamplerOn;	
}


void CMAutosamplerPage::OnCbnSelchangeSolventvial()
{
	// TODO: добавьте свой код обработчика уведомлений
	//m_VialNumberComboBox.AddString(_T("48"));
	//m_VialNumberComboBox.AddString(_T("47"));
	//m_VialNumberComboBox.AddString(_T("46"));
}


void CMAutosamplerPage::OnDeltaposSpinWashvolume(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: добавьте свой код обработчика уведомлений
	//*pResult = 0;
}


void CMAutosamplerPage::OnEnChangeEditWashvolume()
{
	// TODO:  Если это элемент управления RICHEDIT, то элемент управления не будет
	// send this notification unless you override the CMToolPage::OnInitDialog()
	// функция и вызов CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Добавьте код элемента управления
}

