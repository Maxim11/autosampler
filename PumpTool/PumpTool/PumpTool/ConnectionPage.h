#pragma once

#include "ToolPage.h"

// ���������� ���� CMConnectionPage

class CMConnectionPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMConnectionPage)

public:
	CMConnectionPage();
	virtual ~CMConnectionPage();

//	virtual void ProcessStatusFrame(const CGCanFrame* canFrame);

// ������ ����������� ����
	enum { IDD = IDD_PAGE_CONNECTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedRadioButton();

	DECLARE_MESSAGE_MAP()
};
