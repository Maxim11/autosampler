#if !defined(AFX_GRIDCTRL_H__BC0D5EEA_0634_11D7_9C05_C1FFF9B5F218__INCLUDED_)
#define AFX_GRIDCTRL_H__BC0D5EEA_0634_11D7_9C05_C1FFF9B5F218__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridCtrl.h : header file
//

#include <afxtempl.h>

#include "HeaderCtrlExt.h"

/////////////////////////////////////////////////////////////////////////////
// CMGridCtrl window

class CMGridCtrl : public CListCtrl
{
// Construction
public:
	CMGridCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMGridCtrl)
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void PreSubclassWindow();
	virtual int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMGridCtrl();

	virtual void CreateTable(const int numberOfColumns, const int numberOfRows);

	int InsertColumn(int column);
	BOOL DeleteColumn(int column);

	void SetColumnInfo(int column, CString& header, const int width = 0, const int alignment = LVCFMT_LEFT);
	void SetColumnInfo(int column, const int headerID, const int width = 0, const int alignment = LVCFMT_LEFT);

	void SetFirstColumnButtonStyle();

// ������������� ������ ����������� ��� ������ �������
	void SetImageList(CImageList* imageList);
// ������������� ����������� ��� �������� ������
	void SetImage(int row, int imageIndex);

	void ResizeColumns(bool recalculate = false);

	void EnableColumnsResizing(bool enable);

	virtual bool IsRowEnabled(int row);
	virtual bool IsColumnEnabled(int column);
	virtual bool IsCellEnabled(int row, int column);

	void SetEnabledRows(int from = 0, int to = -1);
	void SetEnabledColumns(int from = 0, int to = -1);
	void SetDisabledCell(int row = -1, int col = -1);

	void SetSelectionEnabled(bool enable) { m_IsSelectionEnabled = enable; }
	void SetSelectionLocked(bool lock) { m_IsSelectionLocked = lock; }
	void SetSelection(int row = 0);
	void SetSelectionColor(COLORREF text, COLORREF background);

	int FindRow(CString& itemText, int startIndex = -1);

	void SetHeaderTipID(const int column, const int resourseID);
	void SetHeaderTip(const int column, const CString& tipText);

	void SetListStringID(const int column, int* resourseID);

// ����������� ������ ���������, ����������� �������� � ������ �������
	void SetRowContent(int row, CString& string, int startColumn = 0);
	void GetRowContent(int row, CString& string, int startColumn = 0);
// ����������� ������ ���������, ����������� �������� � ������� �������
	void SetColumnContent(int column, CString& string, int startRow = 0);
	void GetColumnContent(int column, CString& string, int startRow = 0);

// �������������� ����������� ������ � ��������� ��������
	int GetIntegerValue(int row, int column);
	double GetFloatValue(int row, int column);

// ������������� �������, ���������� �� �������� ������
	void SetDoubleClickCommandID(UINT id) { m_DoubleClickCommandID = id; }
	void SetInsertKeyCommandID(UINT id) { m_InsertKeyCommandID = id; }
	void SetDeleteKeyCommandID(UINT id) { m_DeleteKeyCommandID = id; }

// �������������� ������� �������� ������
	int InsertItem(int nItem, LPCTSTR lpszItem);

// �������������� ������� �������� ������
	BOOL DeleteItem(int nItem);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMGridCtrl)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
//	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG

protected:
	CMHeaderCtrlExt m_HeaderCtrl;

protected:
// ������������ �� ��������� ������
	bool m_IsSelectionEnabled;
// ����� �� ������������ ������ ��������� ������
	bool m_IsSelectionLocked;

// ���� ��������� (�������� �������������)
	COLORREF m_SelectionTextColor;
	COLORREF m_SelectionBackgroundColor;

	int m_FirstEnabledRow;
	int m_LastEnabledRow;
	int m_FirstEnabledColumn;
	int m_LastEnabledColumn;

	CPoint m_DisabledCell;				// ���� ���������� ������

// ���������� ������ ���� ��� ������ OnSize()
	CSize m_OldSize;
// ������ ������� ������������� ���������� � ���������� �������� ����
	bool m_AutoResizeColumns;
// ������ ���� ������� ����� � ����������� ����������
	bool m_AutoColumnsWidth;
// �������� ������ ������ ������� ��� ������
	bool m_FirstColumnButtonStyle;

// ������������� �������, ���������� �� �������� ������
	UINT m_DoubleClickCommandID;
// ������������� �������, ���������� �� ������� ������� Insert
	UINT m_InsertKeyCommandID;
// ������������� �������, ���������� �� ������� ������� Delete
	UINT m_DeleteKeyCommandID;

// ���� ��� ��������� ��������� ���� � ���������
//	CFont m_SymbolFont;

// ��������� ������ ��������
	CArray<int, int> m_InitialWidth;
// �������������� ��������� ���������� ��������
	CStringArray m_HeaderTips;
// ������ ���������� �� ����� ��������������� ��������� ���������� �������
	CArray<int*, int*> m_ListStringID;

	DECLARE_MESSAGE_MAP()
};

const int c_AlignLeft = LVCFMT_LEFT;
const int c_AlignRight = LVCFMT_RIGHT;
const int c_AlignCenter = LVCFMT_CENTER;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDCTRL_H__BC0D5EEA_0634_11D7_9C05_C1FFF9B5F218__INCLUDED_)
