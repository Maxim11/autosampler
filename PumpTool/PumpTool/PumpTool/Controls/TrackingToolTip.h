// TrackingToolTip.h: interface for the CMTrackingToolTip class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRACKINGTOOLTIP_H__BD03E72C_6A61_413B_8609_7E47AB6FE36F__INCLUDED_)
#define AFX_TRACKINGTOOLTIP_H__BD03E72C_6A61_413B_8609_7E47AB6FE36F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMTrackingToolTip : public CToolTipCtrl  
{
public:
	CMTrackingToolTip();
	virtual ~CMTrackingToolTip();

	BOOL Create(CWnd* pParentWnd, DWORD dwStyle = 0);

	void SetPosition(int posX, int posY);
	void AdjustRect(CRect* rect, bool allMargins = false);
	void Activate(CWnd* pWnd, LPCTSTR text, bool activate, int interval = 0);

// ������������� ����� ��� ��������� ��������� ������� � ������
	void SetColors(bool isItemSelected);
};

#endif // !defined(AFX_TRACKINGTOOLTIP_H__BD03E72C_6A61_413B_8609_7E47AB6FE36F__INCLUDED_)
