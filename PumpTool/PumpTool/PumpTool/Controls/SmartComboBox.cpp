// SmartComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "SmartComboBox.h"

#include "../Service.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMSmartComboBox

CMSmartComboBox::CMSmartComboBox()
{
	m_Validator = 0;
	m_AcceptEnter = false;
	m_MaxNumberOfStrings = 8;
}

CMSmartComboBox::~CMSmartComboBox()
{
}


BEGIN_MESSAGE_MAP(CMSmartComboBox, CComboBox)
	//{{AFX_MSG_MAP(CMSmartComboBox)
//	ON_CONTROL_REFLECT(CBN_KILLFOCUS, OnKillFocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMSmartComboBox virtual members

/*
void CMSmartComboBox::OnKillFocus(CWnd* newFocusedWindow) 
{
	CString value;
	GetWindowText(value);

	if(value.IsEmpty())
	{
		SetFocus();
		return;
	}

	CComboBox::OnKillFocus(newFocusedWindow);
	if(!m_Validator)
		return;

	if(!m_Validator->IsValidResult(value))
	{
		m_Validator->GetValidString(value);
		SetWindowText(value);
		SetEditSel(-1, -1);
	}
}
*/

void CMSmartComboBox::PreSubclassWindow() 
{
	CComboBox::PreSubclassWindow();
	HWND hwndEdit = ::GetWindow(GetSafeHwnd(), GW_CHILD);
	if(hwndEdit)
		m_FilterEdit.SubclassWindow(hwndEdit);

	m_FilterEdit.SetValidator(m_Validator);
//	m_FilterEdit.SetAcceptEnter(m_AcceptEnter);
	m_FilterEdit.SetParentComboBox(this);

	POSITION pos = m_StringList.GetHeadPosition();
	while(pos)
		CComboBox::AddString(m_StringList.GetNext(pos));
}

void CMSmartComboBox::DoOnEnterPressed() 
{
// �������� ���������, ������� �� ����� ���� ������� ���������
// � ���������� �������
	GetParent()->SendMessage(WM_COMMAND,
		MAKEWPARAM(GetDlgCtrlID(), CBN_DBLCLK), (LPARAM)m_hWnd);
}

int CMSmartComboBox::GetIntValue()
{
	CString text;
	GetWindowText(text);
	return _tstoi(text);
}

double CMSmartComboBox::GetFloatValue()
{
	CString text;
	GetWindowText(text);
	return _tstof(text);
}

void CMSmartComboBox::SetIntValue(int value)
{
	CString string;
	string.Format(_T("%d"), value);
	SetWindowText(string);
}

void CMSmartComboBox::SetFloatValue(double value)
{
	CString string;
	DoubleToString(value, string);
	SetWindowText(string);
}

void CMSmartComboBox::GetSelectedString(CString& text)
{
	int index = GetCurSel();
	if(index != CB_ERR)
		GetLBText(index, text);
}

void CMSmartComboBox::GetSelectedString()
{
	CString text;
	int index = GetCurSel();
	if(index != CB_ERR)
		GetLBText(index, text);
	if(!text.IsEmpty())
		SetWindowText(text);
}

// �������� ��������� �������� ������
void CMSmartComboBox::GetEditSelection(int& begin, int& end)
{
	begin = m_FilterEdit.m_SelectionBegin;
	end = m_FilterEdit.m_SelectionEnd;
}

/*
int CMSmartComboBox::GetIntFromSelectedItem()
{
	CString text;
	int index = GetCurSel();
	if(index != CB_ERR)
		GetLBText(index, text);

	return _tstoi(text);
}

double CMSmartComboBox::GetFloatFromSelectedItem()
{
	CString text;
	int index = GetCurSel();
	if(index != CB_ERR)
		GetLBText(index, text);

	return _tstof(text);
}
*/

void CMSmartComboBox::AddCurrentString()
{
	CString text;
	GetWindowText(text);
	AddString(text);
}

bool CMSmartComboBox::HaveEmptyString()
{
	CString text;
	GetWindowText(text);
	if(text.IsEmpty())
	{
		SetFocus();
		return true;
	}

	return false;
}

void CMSmartComboBox::SetSpecialString(CString string)
{
	m_SpecialString = string;
	if(!m_SpecialString.IsEmpty())
		AddString(m_SpecialString);
}

void CMSmartComboBox::AddString(const CString& string)
{
	if(string.IsEmpty())
		return;

	POSITION pos;
	if(!m_SpecialString.IsEmpty() && string != m_SpecialString)			// ����������� ����. ������ � ������ ������
	{										// ����� ������� ��� ������ ����� ������
		pos = m_StringList.Find(m_SpecialString);
		if(pos)
			m_StringList.RemoveAt(pos);
		m_StringList.AddHead(m_SpecialString);
	}

	pos = m_StringList.Find(string);
	if(pos)
	{
		if(pos == m_StringList.GetHeadPosition())
			return;
		int n = FindString(0, string);
		if(n != CB_ERR)
			DeleteString(n);
		m_StringList.RemoveAt(pos);
	}
	else
	{
		if(m_StringList.GetCount() >= m_MaxNumberOfStrings)
		{
			CString last = m_StringList.GetTail();
			int n = FindString(0, last);
			if(n != CB_ERR)
				DeleteString(n);
			m_StringList.RemoveTail();
		}
	}

//	CComboBox::
	InsertString(0, string);
	SetCurSel(0);
	m_StringList.AddHead(string);
}

void CMSmartComboBox::SaveToString(CString& dst) const
{
	dst.Empty();
	POSITION pos = m_StringList.GetHeadPosition();
	while(pos)
		dst += m_StringList.GetNext(pos)+_T(";");
}

void CMSmartComboBox::GetFromString(const CString& src)
{
	int start = 0;
	int found;

	m_StringList.RemoveAll();
	for( ; ; )
	{
		CString next;
		found = src.Find(';', start);
		if(found < 0)
			next = src.Mid(start);
		else
			next = src.Mid(start, found-start);

		if(!next.IsEmpty() && m_StringList.Find(next) == 0)
		{
			m_StringList.AddTail(next);
			if(GetSafeHwnd())
				CComboBox::AddString(next);
		}
		if(found < 0)
			break;
		start = found+1;
	}
	if(GetSafeHwnd() && !m_StringList.IsEmpty())
		SelectString(-1, m_StringList.GetHead());
}

const CMSmartComboBox& CMSmartComboBox::operator =(const CMSmartComboBox& src)
{
	m_Validator = src.m_Validator;

	m_StringList.RemoveAll();
	m_StringList.AddHead((CStringList*)&src.m_StringList);
	if(GetSafeHwnd())
	{
		ResetContent();
		if(!m_StringList.IsEmpty())
		{
			POSITION pos = m_StringList.GetHeadPosition();
			while(pos)
				CComboBox::AddString(m_StringList.GetNext(pos));
			SelectString(-1, m_StringList.GetHead());
		}
	}

	return *this;
}

