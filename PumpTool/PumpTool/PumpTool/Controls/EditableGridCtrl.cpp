// EditableGridCtrl.cpp : implementation file
//

#include "stdafx.h"
//#include "GridTest.h"
#include "EditableGridCtrl.h"

#include "GridEdit.h"
#include "GridComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef IDC_EDIT_GRID_CELL
#define IDC_EDIT_GRID_CELL 30333
#endif

/////////////////////////////////////////////////////////////////////////////
// CMEditableGridCtrl

//BEGIN_MESSAGE_MAP(CMEditableGridCtrl, CListCtrl)
BEGIN_MESSAGE_MAP(CMEditableGridCtrl, CMGridCtrl)
	//{{AFX_MSG_MAP(CMEditableGridCtrl)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT_EX(LVN_ENDLABELEDIT, OnEndLabelEdit)
	ON_CBN_SELENDOK(IDC_EDIT_GRID_CELL, OnSelectComboBox)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMEditableGridCtrl::CMEditableGridCtrl()
{
	m_IsSelectionEnabled = true;
//	m_IsSelectionEnabled = false;
//	m_Validators.SetSize(10);

	m_CurrentRow = -1;
	m_CurrentColumn = -1;

	m_LimitedEditing = false;
	m_IsEditingFinished = true;

	m_ComboBoxColumnNumber = -1;
	m_ComboBoxStyle = CBS_DROPDOWNLIST;

	m_EditCellWindow = 0;
	m_ComboBoxCellWindow = 0;
}

CMEditableGridCtrl::~CMEditableGridCtrl()
{
}

void CMEditableGridCtrl::CreateTable(const int numberOfColumns, const int numberOfRows)
{
	CMGridCtrl::CreateTable(numberOfColumns, numberOfRows);

	m_Validators.SetSize(numberOfColumns, numberOfColumns);
	for(int i = 0; i < numberOfColumns; i++)
	{
		m_Validators[i] = 0;
	}
}

int CMEditableGridCtrl::InsertColumn(int column)
{
	int res = CMGridCtrl::InsertColumn(column);

	if(res >= 0)
	{
		m_Validators.InsertAt(res, (CGValidator*)0);
	}

	return res;
}

BOOL CMEditableGridCtrl::DeleteColumn(int column)
{
	BOOL res = CMGridCtrl::DeleteColumn(column);

	if(res)
	{
		m_Validators.RemoveAt(column);
	}

	return res;
}

void CMEditableGridCtrl::SetValidator(const int column, CGValidator* validator)
{
	if(column >= 0 && column < m_Validators.GetSize())
		m_Validators[column] = validator;
}

int CMEditableGridCtrl::GetFirstEnabledRow()
{
	return max(0, m_FirstEnabledRow);
}

int CMEditableGridCtrl::GetLastEnabledRow()
{
	return m_LastEnabledRow > 0 ? m_LastEnabledRow : GetItemCount()-1;
}

void CMEditableGridCtrl::SetLimitedEditing(bool enable)
{
	m_LimitedEditing = enable;
	m_IsSelectionEnabled = enable;
}

void CMEditableGridCtrl::SetComboBoxColumn(int number, CStringList* list, DWORD style)
{
//	ASSERT(number >= 0);
//	ASSERT(list != 0);
	m_ComboBoxColumnNumber = number;
	m_ComboBoxStringList = list;
	m_ComboBoxStyle = style;
}

// �������� �������������� �������� �������
void CMEditableGridCtrl::EditCellText(int row, int column)
{
//	if(!EnsureVisible(row, false))
//		return;

	if(!IsCellEnabled(row, column))
		return;

// ������������� ��������� �� ��������� �������������� ��� �������������� ��������
	SetSelection(row);

	m_CurrentRow = row;
	m_CurrentColumn = column;
	TRACE("Set current %d %d\n", m_CurrentRow, m_CurrentColumn);

	CRect cellRectangle;
	CRect clientRectangle;
	CSize scroll(0, 0);

	GetSubItemRect(row, column, LVIR_BOUNDS, cellRectangle);
	if(column == 0)
		cellRectangle.right = cellRectangle.left+GetColumnWidth(0);

// Horizontal scrolling (if neccessary)
	GetClientRect(&clientRectangle);
	if(cellRectangle.right > clientRectangle.right)
		scroll.cx = cellRectangle.right-clientRectangle.right;
	if(cellRectangle.left < 0)
		scroll.cx = cellRectangle.left;
	if(scroll.cx != 0)
	{
		Scroll(scroll);
		cellRectangle.left -= scroll.cx;
		cellRectangle.right -= scroll.cx;
	}

// Creating the edit control
	cellRectangle.right--;
	cellRectangle.bottom--;

	m_IsEditingFinished = false;

// �������� ����������� LVN_ITEMACTIVATE, ���������� �������� ����������
// �� ������ ��������������. ��� ��������� ������ ����� ������ ��� ������ ������.
    NMHDR nmhdr;
    nmhdr.hwndFrom = GetSafeHwnd();
    nmhdr.idFrom = GetDlgCtrlID();
    nmhdr.code = LVN_ITEMACTIVATE;
	GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(),
					(LPARAM)&nmhdr);

	if(m_ListStringID[column] == 0 && m_ComboBoxColumnNumber != column)	// ���� ��������������
	{
		m_ComboBoxCellWindow = 0;
		DWORD editStyle = ES_LEFT | ES_AUTOHSCROLL | WS_BORDER | WS_CHILD | WS_VISIBLE;
//		CMGridEdit* cellEdit;
		m_EditCellWindow = new CMGridEdit(row, column, GetItemText(row, column));
		m_EditCellWindow->Create(editStyle, cellRectangle, this, IDC_EDIT_GRID_CELL);

		m_EditCellWindow->SetValidator(m_Validators[column]);

//		m_EditCellWindow = cellEdit;
	}
	else							// ���������� ������
	{
		m_EditCellWindow = 0;
//		DWORD comboStyle = CBS_DROPDOWNLIST | WS_BORDER | WS_CHILD | WS_VISIBLE;
		DWORD comboStyle = WS_CHILD | WS_VISIBLE | WS_VSCROLL;
// ����� ������
		comboStyle |= (m_ComboBoxColumnNumber != column) ? CBS_DROPDOWNLIST : m_ComboBoxStyle;
		int heigth = cellRectangle.Height();
		cellRectangle.bottom += heigth*7;
		cellRectangle.top--;

//		CMGridComboBox* cellComboBox;
		CString text = GetItemText(row, column);
		if(text.IsEmpty() && row > 0 && m_ComboBoxStringList == 0)
			text = GetItemText(row-1, column);
		m_ComboBoxCellWindow = new CMGridComboBox(row, column, text);
		m_ComboBoxCellWindow->Create(comboStyle, cellRectangle, this, IDC_EDIT_GRID_CELL);
		m_ComboBoxCellWindow->SetItemHeight(-1, heigth);

		if(m_ComboBoxColumnNumber != column)
			m_ComboBoxCellWindow->FillItems(m_ListStringID[column]);
		else
			m_ComboBoxCellWindow->FillItems(m_ComboBoxStringList);

//		m_ComboBoxCellWindow = cellComboBox;
	}
}

void CMEditableGridCtrl::EditCellText(int row, int column, int deltaRow, int deltaColumn)
{
	int numberOfColumns = GetHeaderCtrl()->GetItemCount();

	do
	{
		row += deltaRow;
		column += deltaColumn;
		if(column == numberOfColumns)	// ������� �� ��������� ������
		{
			column = 0;
			row++;
		}
		else if(column == -1)	// ������� �� ���������� ������
		{
			column += numberOfColumns;
			row--;
		}
		if(row < 0 || row >= GetItemCount()
			|| row < m_FirstEnabledRow || (m_LastEnabledRow >= 0 && row > m_LastEnabledRow))
		{
			m_CurrentRow = -1;
			m_CurrentColumn = -1;

			SetFocus();
			return;
		}
	}
	while(!IsCellEnabled(row, column));

	SetFocus();

	if(!m_LimitedEditing)
		EditCellText(row, column);
}

// ���������� ������ � ���� �������������� ������
CString CMEditableGridCtrl::GetEditingText()
{
	CString text;
	if(m_IsEditingFinished)
		return text;

	if(m_EditCellWindow != 0)
	{
		m_EditCellWindow->GetWindowText(text);
	}
	else if(m_ComboBoxCellWindow != 0)
	{
		int index = m_ComboBoxCellWindow->GetCurSel();
		if(index >= 0)
		{
			m_ComboBoxCellWindow->GetLBText(index, text);
		}
		else
		{
			m_ComboBoxCellWindow->GetWindowText(text);
		}
	}
	else
	{
		ASSERT(false);
	}

	return text;
}

void CMEditableGridCtrl::SetEditingText(const CString& text)
{
	if(m_IsEditingFinished)
		return;

	if(m_EditCellWindow != 0)
	{
		m_EditCellWindow->SetWindowText(text);
		return;
	}
	if(m_ComboBoxCellWindow != 0)
	{
		m_ComboBoxCellWindow->SetWindowText(text);
		m_ComboBoxCellWindow->SetInitialString(text);
		return;
	}

	ASSERT(false);
}

/////////////////////////////////////////////////////////////////////////////
// CMEditableGridCtrl message handlers

void CMEditableGridCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CMGridCtrl::OnLButtonDown(nFlags, point);
	if(m_LimitedEditing || m_IsSelectionLocked)
		return;

	LVHITTESTINFO info;
	info.pt = point;

	if(SubItemHitTest(&info) >= 0)
	{
		EditCellText(info.iItem, info.iSubItem);
	}
}

BOOL CMEditableGridCtrl::OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	LV_ITEM	*pItem = &pDispInfo->item;

	if (pItem->pszText != NULL)
	{
		SetItemText(pItem->iItem, pItem->iSubItem, pItem->pszText);
	}

	m_IsEditingFinished = true;

	*pResult = 0;

// ������ ����� ������������ �����������
	return FALSE;
}

void CMEditableGridCtrl::OnSelectComboBox()
{
// �������� ����������� LVN_ITEMCHANGED.
	NMLISTVIEW nmlistview;
	memset(&nmlistview, 0, sizeof(nmlistview));
    nmlistview.hdr.hwndFrom = GetSafeHwnd();
    nmlistview.hdr.idFrom = GetDlgCtrlID();
    nmlistview.hdr.code = LVN_ITEMCHANGED;
	nmlistview.iItem = m_CurrentRow;
	nmlistview.iSubItem = m_CurrentColumn;
	nmlistview.lParam = CBN_SELENDOK;
	GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(),
					(LPARAM)&nmlistview);
}

void CMEditableGridCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(GetFocus() != this)
		SetFocus();
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CMEditableGridCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(GetFocus() != this)
		SetFocus();
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CMEditableGridCtrl::OnMouseWheel(UINT fFlags, short zDelta, CPoint point)
{
	if(GetFocus() != this)
		SetFocus();
	return CListCtrl::OnMouseWheel(fFlags, zDelta, point);
}

