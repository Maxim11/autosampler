// GridCtrl.cpp : implementation file
//

#include "stdafx.h"
//#include "GridTest.h"
#include "GridCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const char c_Delimiter = ';';

/////////////////////////////////////////////////////////////////////////////
// CMGridCtrl

CMGridCtrl::CMGridCtrl()
{
//	m_ToolTipCtrl = 0;

	m_IsSelectionEnabled = true;
	m_IsSelectionLocked = false;
	m_SelectionTextColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
	m_SelectionBackgroundColor = ::GetSysColor(COLOR_HIGHLIGHT);

	m_FirstEnabledRow = 0;
	m_LastEnabledRow = -1;
	m_FirstEnabledColumn = -1;
	m_LastEnabledColumn = -1;
    m_DisabledCell.x = -1;
    m_DisabledCell.y = -1;

	m_OldSize = CSize(0, 0);
	m_AutoResizeColumns = false;
	m_AutoColumnsWidth = true;
	m_FirstColumnButtonStyle = false;

	m_DoubleClickCommandID = 0;
	m_InsertKeyCommandID = 0;
	m_DeleteKeyCommandID = 0;
}

CMGridCtrl::~CMGridCtrl()
{
}

/*
void CMGridCtrl::SetToolTipCtrl(CToolTipCtrl* toolTipCtrl)
{
	m_ToolTipCtrl = toolTipCtrl;
	m_ToolTipCtrl->AddTool(this, "Table");
	m_ToolTipCtrl->AddTool(GetHeaderCtrl(), "Header");
}
*/

BEGIN_MESSAGE_MAP(CMGridCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CMGridCtrl)
//	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
//	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMGridCtrl message handlers

/*
BOOL CMGridCtrl::PreTranslateMessage(MSG* pMsg)
{
	if(m_ToolTipCtrl)
		m_ToolTipCtrl->RelayEvent(pMsg);

	return CListCtrl::PreTranslateMessage(pMsg);
}
*/
void CMGridCtrl::PreSubclassWindow()
{
	CHeaderCtrl* headerCtrl = GetHeaderCtrl();
	if(headerCtrl && headerCtrl->GetSafeHwnd() != 0)
		m_HeaderCtrl.SubclassWindow(headerCtrl->GetSafeHwnd());

// ������������� ����������� ����� (WS_CLIPCHILDREN ��������� ��� ����������� ���������
// ��� ������������� � OnPaint memoryDC)
	DWORD styles = ::GetWindowLong(GetSafeHwnd(), GWL_STYLE);
	::SetWindowLong(GetSafeHwnd(), GWL_STYLE, styles | LVS_REPORT | WS_CLIPCHILDREN);
}

void CMGridCtrl::CreateTable(const int numberOfColumns, const int numberOfRows)
{
	CRect rect;
	GetClientRect(&rect);
	if(GetCountPerPage() < numberOfRows)
		rect.right -= ::GetSystemMetrics(SM_CXVSCROLL);

	int width = rect.Width()/numberOfColumns;
	int i;

//	m_HeaderTipID.SetSize(numberOfColumns);
	m_InitialWidth.SetSize(numberOfColumns);
	m_HeaderTips.SetSize(numberOfColumns);
	m_ListStringID.SetSize(numberOfColumns);

	for(i = 0; i < numberOfColumns; i++)
	{
		int w = (i == 0 ? rect.Width()-width*(numberOfColumns-1) : width);
		CListCtrl::InsertColumn(i, _T(""), LVCFMT_LEFT, w, i);

//		m_HeaderTipID[i] = 0;
		m_ListStringID[i] = 0;
	}

	for(i = 0; i < numberOfRows; i++)
		InsertItem(i, _T(""));

	m_OldSize = CSize(0, 0);
}

int CMGridCtrl::InsertColumn(int column)
{
	int res = CListCtrl::InsertColumn(column, _T(""));

	if(res >= 0)
	{
//		m_HeaderTipID.InsertAt(res, 0);
		m_InitialWidth.InsertAt(res, 0);
		m_HeaderTips.InsertAt(res, CString());
		m_ListStringID.InsertAt(res, (int*)0);
	}

	return res;
}

BOOL CMGridCtrl::DeleteColumn(int column)
{
	BOOL res = CListCtrl::DeleteColumn(column);

	if(res)
	{
//		m_HeaderTipID.RemoveAt(column);
		m_InitialWidth.RemoveAt(column);
		m_HeaderTips.RemoveAt(column);
		m_ListStringID.RemoveAt(column);
	}

	return res;
}

void CMGridCtrl::SetColumnInfo(int column, CString& header, const int width, const int alignment)
{
	bool isOwnerDrawn = false;

// ���������� ��������� ������
	if(width > 0)
		m_InitialWidth[column] = width;

// ���� ������ ������ ������ _
	if(!header.IsEmpty() && header[0] == '_')
	{
		header = header.Mid(1);
		isOwnerDrawn = true;
	}

    LV_COLUMN lvc;
    lvc.mask = LVCF_FMT | LVCF_TEXT ;
	lvc.fmt = alignment;
	lvc.pszText = LPTSTR(LPCTSTR(header));
	if(width != 0)
	{
		lvc.mask |= LVCF_WIDTH;
		lvc.cx = width;
		m_AutoColumnsWidth = false;
	}
	SetColumn(column, &lvc);

	if(isOwnerDrawn)
	{
		CHeaderCtrl* headerCtrl = GetHeaderCtrl();
		HDITEM headerItem;

		headerItem.mask = HDI_FORMAT;
		headerCtrl->GetItem(column, &headerItem);
		headerItem.fmt |= HDF_OWNERDRAW;;
		headerCtrl->SetItem(column, &headerItem);
	}
}

void CMGridCtrl::SetColumnInfo(int column, const int headerID, const int width, const int alignment)
{
	CString header;
	header.LoadString(headerID);
	SetColumnInfo(column, header, width, alignment);
}

void CMGridCtrl::SetFirstColumnButtonStyle()
{
	m_FirstColumnButtonStyle = true;
	if(m_FirstEnabledColumn <= 0)
		m_FirstEnabledColumn = 1;
}

// ������������� ������ ����������� ��� ������ �������
void CMGridCtrl::SetImageList(CImageList* imageList)
{
// ��� ��������� ������� ���������� ������ ���� ������,
// ��������� ������������� ��������� �������� � ���������
// ������ ����� �������. �������������� ������ ������ 16*16
	CListCtrl::SetImageList(imageList, LVSIL_NORMAL);
}

// ������������� ����������� ��� �������� ������
void CMGridCtrl::SetImage(int row, int imageIndex)
{
	LV_ITEM lvi;
	lvi.mask = LVIF_IMAGE;
	lvi.iItem = row;
	lvi.iSubItem = 0;
	lvi.iImage = imageIndex;
	SetItem(&lvi);
}

void CMGridCtrl::EnableColumnsResizing(bool enable)
{
//	CHeaderCtrl* headerCtrl = GetHeaderCtrl() ;
//	if( headerCtrl)
//		headerCtrl->EnableWindow(enable);

	m_HeaderCtrl.m_IsResizingEnabled = enable;
	m_AutoResizeColumns = !enable;
}

bool CMGridCtrl::IsRowEnabled(int row)
{
	if(row < m_FirstEnabledRow)
		return false;
	if(m_LastEnabledRow >= 0 && row > m_LastEnabledRow)
		return false;
	return true;
}

bool CMGridCtrl::IsColumnEnabled(int column)
{
	if(column < m_FirstEnabledColumn)
		return false;
	if(m_LastEnabledColumn >= 0 && column > m_LastEnabledColumn)
		return false;
	return true;
}

bool CMGridCtrl::IsCellEnabled(int row, int column)
{
	if(!IsRowEnabled(row))
		return false;
	if(!IsColumnEnabled(column))
		return false;
	if(row == m_DisabledCell.x && column == m_DisabledCell.y)
		return false;
	return true;
}

void CMGridCtrl::SetEnabledRows(int from, int to)
{
	m_FirstEnabledRow = from;
	m_LastEnabledRow = to;
}

void CMGridCtrl::SetEnabledColumns(int from, int to)
{
	if(m_FirstColumnButtonStyle && from > 0)
		m_FirstEnabledColumn = from;
	m_LastEnabledColumn = to;
}

void CMGridCtrl::SetDisabledCell(int row, int col)
{
	m_DisabledCell = CPoint(row, col);
}

void CMGridCtrl::SetSelection(int row)
{
	const UINT c_flag = LVIS_SELECTED | LVIS_FOCUSED;
	if(row < 0)
	{
		int selected = GetSelectionMark();
		SetItemState(selected, 0, c_flag);
		SetSelectionMark(row);
		return;
	}
	EnsureVisible(row-1, false);
	EnsureVisible(row+1, false);
	EnsureVisible(row, false);
	SetItemState(row, c_flag, c_flag);
	SetSelectionMark(row);
}

void CMGridCtrl::SetSelectionColor(COLORREF text, COLORREF background)
{
	m_SelectionTextColor = text;
	m_SelectionBackgroundColor = background;
}

int CMGridCtrl::FindRow(CString& itemText, int startIndex)
{
	LV_FINDINFO findInfo;
	findInfo.psz = itemText;
	findInfo.flags = LVFI_STRING;

	return FindItem(&findInfo, startIndex);
}

// ������ ������������� ������ ��������� ��� ��������� �������
void CMGridCtrl::SetHeaderTipID(const int column, const int resourseID)
{
	if(column >= 0 && column < m_HeaderTips.GetSize())
	{
//		m_HeaderTipID[column] = resourseID;
		m_HeaderTips[column].LoadString(resourseID);
	}
}

void CMGridCtrl::SetHeaderTip(const int column, const CString& tipText)
{
	if(column >= 0 && column < m_HeaderTips.GetSize())
	{
		m_HeaderTips[column] = tipText;
	}
}

// ������ � ����������� ���������� ��� ������������� � �������
int CMGridCtrl::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	CString tipText;
	CRect rect;

	for(int i = 0; i < m_HeaderCtrl.GetItemCount(); i++)
	{
		m_HeaderCtrl.GetItemRect(i, &rect);
		if(rect.PtInRect(point))
		{
//			tipText.LoadString(m_HeaderTipID[i]);
			tipText = m_HeaderTips[i];
			if(!tipText.IsEmpty())
			{
				pTI->hwnd = m_hWnd;
				pTI->uId = (UINT)m_HeaderCtrl.GetSafeHwnd();
				pTI->uFlags |= TTF_IDISHWND;
				pTI->lpszText = new TCHAR[tipText.GetLength()+1];
				lstrcpy(pTI->lpszText, (LPCTSTR)tipText);
			}
			return i; //m_HeaderTipID[i];
		}
	}
	return -1;
}

void CMGridCtrl::SetListStringID(const int column, int* resourseID)
{
	if(column >= 0 && column < m_ListStringID.GetSize())
		m_ListStringID[column] = resourseID;
}

// ���������������� ��������� ������ �������� ��� ��������� ������
// ���� (�������� ��������� ������� ���������). ��� ������� ������
// �� ����������� CMGridCtrl::OnSize() �������� ��������� ������
// ��������� � �����������. ������� ������ ������� ������ ����������
// ����� ��������� ������ �������.
void CMGridCtrl::ResizeColumns(bool recalculate)
{
	if(!GetHeaderCtrl())	// The number of columns is unknown
		return;

	CRect rect;
	GetClientRect(&rect);

	if(m_OldSize.cx == rect.Width())
	{
		return;
	}
	m_OldSize.cx = rect.Width();

	int numberOfColumns = GetHeaderCtrl()->GetItemCount();
	int width = rect.Width()/numberOfColumns;
	int i;

	if(m_AutoColumnsWidth)
	{
		for(i = 0; i < numberOfColumns; i++)
		{
			int w = (i == 0 ? rect.Width()-width*(numberOfColumns-1) : width);
			SetColumnWidth(i, w);
		}
		return;
	}

	int total = 0;

	if(recalculate)
	{
		int totalInit = 0;
		for(i = 0; i < numberOfColumns; i++)
		{
			totalInit += m_InitialWidth[i];
		}
		for(i = 0; i < numberOfColumns; i++)
		{
			width = m_InitialWidth[i]*rect.Width()/totalInit;
			total += width;
			SetColumnWidth(i, width);
		}
	}
	else
	{
		for(i = 0; i < numberOfColumns; i++)
		{
			total += GetColumnWidth(i);
		}
	}

	i = numberOfColumns-1;
	SetColumnWidth(i, rect.Width()-total+GetColumnWidth(i));
}

void CMGridCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	RECT& itemRectangle = lpDrawItemStruct->rcItem;
	if( lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT) )
	{
		COLORREF textColor;
		COLORREF backgroundColor;
		COLORREF defaultTextColor = pDC->GetTextColor();
		COLORREF defaultBackgroundColor = pDC->GetBkColor();
		COLORREF lineColor = ::GetSysColor(COLOR_GRAYTEXT);
		int row = lpDrawItemStruct->itemID;
		bool rowEnabled = IsRowEnabled(row);
		bool drawSelection = (lpDrawItemStruct->itemState & ODS_SELECTED) && rowEnabled && m_IsSelectionEnabled;

// If the item is selected then highlight the item.
		if(drawSelection)
		{
			textColor = m_SelectionTextColor;
			backgroundColor = m_SelectionBackgroundColor;
		}
		else
		{
			textColor = defaultTextColor;
			backgroundColor = defaultBackgroundColor;
		}
		CPen newPen(PS_SOLID, 1, lineColor);
		CPen* oldPen = pDC->SelectObject(&newPen);

		CString text;
		CRect subItemRectangle; // = itemRectangle;
		int numberOfColumns = GetHeaderCtrl()->GetItemCount();
		int space = pDC->GetTextExtent(_T("0")).cx;

		for(int i = 0; i < numberOfColumns; i++)
		{
			text = GetItemText(lpDrawItemStruct->itemID, i);
			GetSubItemRect(lpDrawItemStruct->itemID, i, LVIR_BOUNDS, subItemRectangle);
			if(i == 0)
				subItemRectangle.right = subItemRectangle.left+GetColumnWidth(0);

			if(i == 0 && m_FirstColumnButtonStyle)
			{
// "���������" ��� ������ �������
				int top = subItemRectangle.top;
				int height = subItemRectangle.Height();

				pDC->FillSolidRect(&subItemRectangle, ::GetSysColor(COLOR_BTNFACE));
				pDC->SetTextColor(defaultTextColor);
				pDC->Draw3dRect(subItemRectangle.left, top,
					subItemRectangle.Width()-1, height-1,
					::GetSysColor(COLOR_3DHIGHLIGHT), ::GetSysColor(COLOR_3DDKSHADOW));
			}
			else if(!drawSelection && !IsCellEnabled(row, i))
			{
// If the cell is disabled then gray it.
				pDC->FillSolidRect(&subItemRectangle, ::GetSysColor(COLOR_BTNFACE));
				pDC->SetTextColor(defaultTextColor);
			}
			else
			{
				pDC->FillSolidRect(&subItemRectangle, backgroundColor);
				pDC->SetTextColor(textColor);
			}
// Draw the vertical line
			pDC->MoveTo(subItemRectangle.right-1, itemRectangle.top);
			pDC->LineTo(subItemRectangle.right-1, itemRectangle.bottom);

// Draw the image in the first column (if necessary)
			if(i == 0)
			{
				CImageList* imageList = GetImageList(LVSIL_NORMAL);
				if (imageList)
				{
// Get item image
					LV_ITEM lvi;
					lvi.mask = LVIF_IMAGE;
					lvi.iItem = row;
					lvi.iSubItem = 0;
					GetItem(&lvi);

					imageList->Draw(pDC, lvi.iImage,
						CPoint(subItemRectangle.left+2, subItemRectangle.top+(subItemRectangle.Height()-16)/2), ILD_TRANSPARENT);
//						ILD_BLEND50 | ILD_TRANSPARENT);
					subItemRectangle.left += 16+2;
				}
			}
			subItemRectangle.left += space;
			subItemRectangle.right -= space;

			LV_COLUMN lvc;
			lvc.mask = LVCF_FMT;
			GetColumn(i, &lvc);

			UINT justify;
			switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
			{
			case LVCFMT_RIGHT:
				justify = DT_RIGHT;
				break;
			case LVCFMT_CENTER:
				justify = DT_CENTER;
				break;
			default:
				justify = DT_LEFT;
				break;
			}

			pDC->DrawText(text, subItemRectangle, DT_SINGLELINE | DT_VCENTER | justify);

			subItemRectangle.left = subItemRectangle.right+space;
		}

// Draw the horizontal line
		pDC->MoveTo(itemRectangle.left, itemRectangle.bottom-1);
		pDC->LineTo(itemRectangle.right-1, itemRectangle.bottom-1);

// Return the device context to its original state.
		pDC->SelectObject(oldPen);
		pDC->SetTextColor(defaultTextColor);
		pDC->SetBkColor(defaultBackgroundColor);

	} // end of if on	ODA_DRAWENTIRE | ODA_SELECT
}

void CMGridCtrl::OnSize(UINT nType, int cx, int cy)
{
	bool savedAutoResizeColumns = m_AutoResizeColumns;

	m_AutoResizeColumns = false;
	CListCtrl::OnSize(nType, cx, cy);
	if(savedAutoResizeColumns)
	{
//		SetColumnWidth(last, width);
		//SetScrollPos(SB_VERT, 0, TRUE);
		ResizeColumns();
	}
	m_AutoResizeColumns = savedAutoResizeColumns;
}

void CMGridCtrl::OnPaint()
{
/*	if(!m_AutoResizeColumns)	// ������� ����� ����������� ��� ��������� ������ �������
	{
		Invalidate(false);
	}
*/
// non flickering painting
	CPaintDC dc(this); // device context for painting
	CDC memoryDC;
	CBitmap memoryBitmap;
	CBitmap* oldBitmap;
	CRect rect;

	memoryDC.CreateCompatibleDC(&dc);
	dc.GetClipBox(&rect);
	memoryBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	oldBitmap = memoryDC.SelectObject(&memoryBitmap);
	memoryDC.SetWindowOrg(rect.left, rect.top);

// ������ � ������
	memoryDC.FillSolidRect(rect, dc.GetBkColor());
	DefWindowProc(WM_PAINT, (WPARAM)memoryDC.m_hDC, (LPARAM)0);

// ������� �� �����
	dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(),
		&memoryDC, rect.left, rect.top, SRCCOPY);
	memoryDC.SelectObject(oldBitmap);
}

BOOL CMGridCtrl::OnEraseBkgnd(CDC* pDC)
{
// �� ��������� �������� �� ������� ���
	return false;
}

void CMGridCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(!m_IsSelectionLocked)
	{
		CListCtrl::OnLButtonDown(nFlags, point);
	}
}

void CMGridCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if(m_DoubleClickCommandID != 0)
	{
		GetParent()->PostMessage(WM_COMMAND, m_DoubleClickCommandID);
	}
}

void CMGridCtrl::OnRButtonDown(UINT nFlags, CPoint point)
{
	if(!m_IsSelectionLocked)
	{
		CListCtrl::OnRButtonDown(nFlags, point);
	}
}

void CMGridCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(m_IsSelectionLocked)
		return;
	
	CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
	if(m_InsertKeyCommandID != 0 && nChar == VK_INSERT)
	{
		GetParent()->PostMessage(WM_COMMAND, m_InsertKeyCommandID);
	}
	else if(m_DeleteKeyCommandID != 0 && nChar == VK_DELETE)
	{
		GetParent()->PostMessage(WM_COMMAND, m_DeleteKeyCommandID);
	}
}

/*
void CMGridCtrl::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
// �������������� ������ �������� ���������
	if(lpDrawItemStruct->CtlType != ODT_HEADER)
		return;

//	TRACE("OnDrawItem column %d\n", lpDrawItemStruct->itemID);
/*
// ������ ����� �������� ��������� ������� ���������� �������

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	RECT* itemRectangle = &lpDrawItemStruct->rcItem;

//	int delta = pDC->GetTextExtent(" ").cx;
	int delta = 3*::GetSystemMetrics(SM_CXEDGE);
	const int c_shift = 2;

// ������� ���������� ����
	if(HFONT(m_SymbolFont) == 0)
	{
		LOGFONT logFont = {0};
		pDC->GetCurrentFont()->GetLogFont(&logFont);
		logFont.lfCharSet = SYMBOL_CHARSET;
		logFont.lfHeight -= c_shift*2;
		lstrcpy(logFont.lfFaceName, "Symbol");

		m_SymbolFont.CreateFontIndirect(&logFont);
	}

// �������� ����� ���������
	HDITEM hdi;
	TCHAR  text[20];

	hdi.mask = HDI_TEXT;
	hdi.pszText = text;
	hdi.cchTextMax = 20;

	GetHeaderCtrl()->GetItem(lpDrawItemStruct->itemID, &hdi);

	CString string(text);
	int position = string.Find(' ');
	CString symbol = string.Left(position);

	itemRectangle->left += delta;
	itemRectangle->right -= delta;
	itemRectangle->top -= c_shift;

// ������ ������ ����� ���������� ������
	CFont* oldFont = pDC->SelectObject(&m_SymbolFont);
	pDC->DrawText(symbol, itemRectangle, DT_SINGLELINE | DT_VCENTER | DT_LEFT);

	itemRectangle->left += pDC->GetTextExtent(symbol).cx;
	itemRectangle->top += c_shift;

// ������ ������� ����������� ������
	pDC->SelectObject(oldFont);
	pDC->DrawText(string.Mid(position), itemRectangle, DT_SINGLELINE | DT_VCENTER | DT_LEFT);


	CWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
*/
// ����������� ������ ���������, ����������� �������� � ������ �������
void CMGridCtrl::SetRowContent(int row, CString& string, int startColumn)
{
	CString cellText;
	int begin = 0, end;
	int i = startColumn;
	bool notReady = true;

	while(notReady)
	{
		if((end = string.Find(c_Delimiter, begin)) >= 0)
			cellText = string.Mid(begin, end-begin);
		else
		{
			cellText = string.Mid(begin);
			notReady = false;
		}

// ���� ����������, ����������� ����� � ������
		if(m_ListStringID[i] && !cellText.IsEmpty())
		{
			cellText.LoadString(m_ListStringID[i][_tstoi(cellText)]);
		}

		SetItemText(row, i++, cellText);
		begin = end+1;

		if(i >= m_ListStringID.GetSize())
			break;
	}
}

void CMGridCtrl::GetRowContent(int row, CString& string, int startColumn)
{
	CString cellText;
	int numberOfColumns = GetHeaderCtrl()->GetItemCount()-1;

	string.Empty();
	for(int i = startColumn; i <= numberOfColumns; i++)
	{
		cellText = GetItemText(row, i);

// ���� ����������, ����������� ������ � �����
		if(m_ListStringID[i])
		{
			CString fromList;
			for(int* id = m_ListStringID[i]; *id != 0; id++)
			{
				fromList.LoadString(*id);
				if(cellText == fromList)
				{
					cellText.Format(_T("%d"), id-m_ListStringID[i]);
					break;
				}
			}
		}
		else
		{
			int pos = 0;
			while((pos = cellText.Find(c_Delimiter, pos)) >= 0)
				cellText.SetAt(pos, ' ');
		}

		string += cellText;
		if(i < numberOfColumns)
			string += c_Delimiter;
	}
}

// ����������� ������ ���������, ����������� �������� � ������� �������
void CMGridCtrl::SetColumnContent(int column, CString& string, int startRow)
{
	int numberOfRows = GetItemCount();
	CString cellText;
	int begin = 0, end;
	int i = startRow;
	bool notReady = true;

	while(notReady)
	{
		if((end = string.Find(c_Delimiter, begin)) >= 0)
			cellText = string.Mid(begin, end-begin);
		else
		{
			cellText = string.Mid(begin);
			notReady = false;
		}

// ���� ����������, ����������� ����� � ������
		if(m_ListStringID[column] && !cellText.IsEmpty())
		{
			cellText.LoadString(m_ListStringID[column][_tstoi(cellText)]);
		}

		SetItemText(i++, column, cellText);
		begin = end+1;

		if(i >= numberOfRows)
			break;
	}
}

void CMGridCtrl::GetColumnContent(int column, CString& string, int startRow)
{
	CString cellText;
	int numberOfRows = GetItemCount();

	string.Empty();
	for(int i = startRow; i <= numberOfRows; i++)
	{
		cellText = GetItemText(i, column);

// ���� ����������, ����������� ������ � �����
		if(m_ListStringID[column])
		{
			CString fromList;
			for(int* id = m_ListStringID[column]; *id != 0; id++)
			{
				fromList.LoadString(*id);
				if(cellText == fromList)
				{
					cellText.Format(_T("%d"), id-m_ListStringID[i]);
					break;
				}
			}
		}
		else
		{
			int pos = 0;
			while((pos = cellText.Find(c_Delimiter, pos)) >= 0)
				cellText.SetAt(pos, ' ');
		}

		string += cellText;
		if(i < numberOfRows)
			string += c_Delimiter;
	}
}

// �������������� ����������� ������ � ��������� ��������
int CMGridCtrl::GetIntegerValue(int row, int column)
{
	CString cellText = GetItemText(row, column);
	return _tstoi(cellText);
}

double CMGridCtrl::GetFloatValue(int row, int column)
{
	CString cellText = GetItemText(row, column);
	return _tstof(cellText);
}

// �������������� ������� �������� ������
int CMGridCtrl::InsertItem(int nItem, LPCTSTR lpszItem)
{
	int result = CListCtrl::InsertItem(nItem, lpszItem);
	if(m_AutoResizeColumns)
	{
// ��� ��������� ����� ����� ����� ��������� ������������ ������� ���������,
// ��� ������� ��������� ������ �������, ����� �� ���� �������������� �������
		ResizeColumns();
	}
	return result;
}

// �������������� ������� �������� ������ ��� ������ � ��� ������.
// ����� �������� ������������ ���������, �� ����������������� � ����,
// ����������� ���������� ������ (������). ������� �������������
// ��������� �� ����.
// ����������:
// ��������, ��� ���� �������� ��� ������� ������� ResizeColumns()
// ��������������� �� �����������  CMGridCtrl::OnSize(). ��� ����������,
// �� ���������� ���� �������� ������� ResizeColumns() �����.
BOOL CMGridCtrl::DeleteItem(int nItem)
{
//	if(GetItemCount() == GetCountPerPage()+1)
//		EnsureVisible(0, false);
	int result = CListCtrl::DeleteItem(nItem);
	if(m_AutoResizeColumns)
	{
// ��� ��������� ����� ����� ����� ��������� ������������ ������� ���������,
// ��� ������� ��������� ������ �������, ����� �� ���� �������������� �������
		ResizeColumns();
	}
	return result;
}
