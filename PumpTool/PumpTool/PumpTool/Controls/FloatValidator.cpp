// FloatValidator.cpp: implementation of the CGFloatValidator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FloatValidator.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGFloatValidator::CGFloatValidator(bool acceptMinus)
{
	m_AcceptMinus = acceptMinus;
	m_MinValue = -1.0E6;
	m_MaxValue =  1.0E6;
	m_FormatString = "%f";
}

CGFloatValidator::~CGFloatValidator()
{

}

bool CGFloatValidator::IsValidInput(const CString& string)
{
	LPCTSTR chr = LPCTSTR(string);
	if(m_AcceptMinus && *chr == '-')
		chr++;

	bool acceptPoint = true;
	for( ; *chr; chr++)
	{
		if(isdigit((BYTE)*chr))
			continue;
		if(acceptPoint && *chr == '.')
		{
			acceptPoint = false;
			continue;
		}
		return false;
	}
	return true;
}

// ���������� �������� ��������
UINT CGFloatValidator::ReplaceChar(UINT oldChar)
{
	if(oldChar == _T(','))
		return  _T('.');

	return oldChar;
}

bool CGFloatValidator::IsValidResult(const CString& string)
{
	double value = _tstof(string);
	return (value >= m_MinValue && value <= m_MaxValue);
}

void CGFloatValidator::GetValidString(CString& string)
{
	double value = _tstof(string);
	value = max(value, m_MinValue);
	value = min(value, m_MaxValue);
	string.Format(m_FormatString, value);
}

void CGFloatValidator::SetRange(double minValue, double maxValue, char format[])
{
	m_MinValue = minValue;
	m_MaxValue = maxValue;
	m_FormatString = format;
}
