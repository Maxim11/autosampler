#if !defined(AFX_GRIDCOMBOBOXCHILDEDIT_H__82729089_35CD_4C55_B45A_5C0069F4887A__INCLUDED_)
#define AFX_GRIDCOMBOBOXCHILDEDIT_H__82729089_35CD_4C55_B45A_5C0069F4887A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridComboBoxChildEdit.h : header file
//

//#include "GridComboBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBoxChildEdit window

// ����� ������������ ��� ����������� ����������� ��������� ��������������
// ����������� ������ � ������ ������������� �������

class CMGridComboBoxChildEdit : public CEdit
{
	friend class CMGridComboBox;

// Construction
public:
	CMGridComboBoxChildEdit();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMGridComboBoxChildEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMGridComboBoxChildEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMGridComboBoxChildEdit)
//	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* newFocusedWindow);
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG

protected:
	CMGridComboBox* m_ParentComboBox;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDCOMBOBOXCHILDEDIT_H__82729089_35CD_4C55_B45A_5C0069F4887A__INCLUDED_)
