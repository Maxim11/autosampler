// GridComboBox.cpp : implementation file
//

#include "stdafx.h"

#include "GridComboBox.h"

#include "EditableGridCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBox

CMGridComboBox::CMGridComboBox(int row, int column, const CString& init)
{
	m_Row = row;
	m_Column = column;

	m_InitialString = init;
	m_IsEscaped = false;
}

CMGridComboBox::~CMGridComboBox()
{
}


BEGIN_MESSAGE_MAP(CMGridComboBox, CComboBox)
	//{{AFX_MSG_MAP(CMGridComboBox)
	ON_WM_CREATE()
	ON_WM_NCDESTROY()
	ON_WM_KEYDOWN()
	ON_WM_CHAR()
	ON_WM_KILLFOCUS()
	ON_WM_GETDLGCODE()
//	ON_CONTROL_REFLECT(CBN_KILLFOCUS, OnKillFocusReflect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBox message handlers

int CMGridComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Set the proper font
	SetFont(GetParent()->GetFont());

// Send Notification to parent of ListView ctrl
	SendNotification(LVN_BEGINLABELEDIT);

	HWND hwndEdit = ::GetWindow(GetSafeHwnd(), GW_CHILD);
	if(hwndEdit)
	{
		m_ChildEdit.SubclassWindow(hwndEdit);

//		m_ChildEdit.SetValidator(m_Validator);
		m_ChildEdit.m_ParentComboBox = this;
	}

	return 0;
}

void CMGridComboBox::FillItems(int* stringIDs)
{
	ResetContent();

	CString string;
	while(*stringIDs)
	{
		string.LoadString(*stringIDs);
		AddString(string);
		stringIDs++;
	}

	SelectString(-1, m_InitialString);
	SetFocus();
}

void CMGridComboBox::FillItems(CStringList* stringList)
{
	ResetContent();

	CString string;
	POSITION pos = stringList->GetHeadPosition();
	while(pos)
	{
		AddString(stringList->GetNext(pos));
	}

	SelectString(-1, m_InitialString);
	SetFocus();
}

void CMGridComboBox::OnNcDestroy()
{
	CComboBox::OnNcDestroy();

	delete this;
}

void CMGridComboBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	TRACE("OnKeyDown %d\n", nChar);
//	CComboBox::OnKeyDown(nChar, nRepCnt, nFlags);

	CMEditableGridCtrl* gridCtrl = (CMEditableGridCtrl*)GetParent();
	bool isControlPressed = ::GetKeyState(VK_CONTROL) < 0;
    switch (nChar)
    {
		case VK_UP :
		{
			if(isControlPressed)
			{
				gridCtrl->EditCellText(m_Row, m_Column, -1, 0);
				return;
			}
		}
		case VK_DOWN :
		{
			if(isControlPressed)
			{
				gridCtrl->EditCellText(m_Row, m_Column, 1, 0);
				return;
			}
		}
		case VK_HOME :
		{
			if(isControlPressed)
			{
// ���� ��� ������ ���������� �����
				gridCtrl->EditCellText(gridCtrl->GetFirstEnabledRow()-1, m_Column, 1, 0);
				return;
			}
			break;
		}
		case VK_END :
		{
			if (isControlPressed)
			{
				gridCtrl->EditCellText(gridCtrl->GetLastEnabledRow()+1, m_Column, -1, 0);
				return;
			}
			break;
		}
   		case VK_NEXT :
		{
			int newRow = m_Row+gridCtrl->GetCountPerPage();
			gridCtrl->EditCellText(min(gridCtrl->GetLastEnabledRow(), newRow)+1, m_Column, -1, 0);
			return;
		}
		case VK_PRIOR :
		{
			int newRow = m_Row-gridCtrl->GetCountPerPage();
			gridCtrl->EditCellText(max(gridCtrl->GetFirstEnabledRow(), newRow)-1, m_Column, 1, 0);
			return;
		}
	}			// switch(nChar)

	CComboBox::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CMGridComboBox::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
//	TRACE("OnChar %d\n", nChar);
//	CComboBox::OnChar(nChar, nRepCnt, nFlags);

	CMEditableGridCtrl* gridCtrl = (CMEditableGridCtrl*)GetParent();
	bool isShiftPressed = GetKeyState(VK_SHIFT) < 0;
	switch(nChar)
	{
	case VK_ESCAPE:
		m_IsEscaped = true;
		GetParent()->SetFocus();
		return;
	case VK_RETURN:
		gridCtrl->EditCellText(m_Row, m_Column, 0, 1);
		return;
	case VK_TAB :
		if(isShiftPressed)
			gridCtrl->EditCellText(m_Row, m_Column, 0, -1);
		else
			gridCtrl->EditCellText(m_Row, m_Column, 0, 1);
		return;
	}

	CComboBox::OnChar(nChar, nRepCnt, nFlags);
}

/*
//void CMGridComboBox::OnKillFocus(CWnd* newFocusedWindow)
BOOL CMGridComboBox::OnKillFocusReflect()
{
	GetWindowText(m_InitialString);

// Send Notification to parent of ListView ctrl
	SendNotification(LVN_ENDLABELEDIT);
	DestroyWindow();

	return TRUE;
}
*/

void CMGridComboBox::OnKillFocus(CWnd* newFocusedWindow)
{
/*	CComboBox::OnKillFocus(newFocusedWindow);
	TRACE("CMGridComboBox::OnKillFocus %x %x %x %x\n",
		newFocusedWindow->GetParent()->GetSafeHwnd(), GetSafeHwnd(),
		newFocusedWindow->GetSafeHwnd(), GetParent()->GetSafeHwnd());
*/
	if(IsChild(newFocusedWindow))
		return;

	if(!m_IsEscaped)
		GetWindowText(m_InitialString);

// Send Notification to parent of ListView ctrl
	SendNotification(LVN_ENDLABELEDIT);
	DestroyWindow();
}


UINT CMGridComboBox::OnGetDlgCode()
{
	return DLGC_WANTMESSAGE;
}


void CMGridComboBox::SendNotification(int code)
{
// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = code;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_Row;
	dispinfo.item.iSubItem = m_Column;
	if(m_IsEscaped)
	{
		dispinfo.item.pszText = 0;
		dispinfo.item.cchTextMax = 0;
	}
	else
	{
		dispinfo.item.pszText = LPTSTR(LPCTSTR(m_InitialString));
		dispinfo.item.cchTextMax = m_InitialString.GetLength();
	}

	GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(),
					(LPARAM)&dispinfo );
}
