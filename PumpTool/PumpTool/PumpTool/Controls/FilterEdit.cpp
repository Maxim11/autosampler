// FilterEdit.cpp : implementation file
//

#include "stdafx.h"
#include "FilterEdit.h"

#include "SmartComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMFilterEdit

CMFilterEdit::CMFilterEdit() : CEdit()
{
	m_Validator = 0;
	m_ParentComboBox = 0;

	m_SelectionBegin = 0;
	m_SelectionEnd = 0;

	m_AcceptEnter = false;
}

CMFilterEdit::~CMFilterEdit()
{
}


BEGIN_MESSAGE_MAP(CMFilterEdit, CEdit)
	//{{AFX_MSG_MAP(CMFilterEdit)
	ON_WM_CHAR()
	ON_WM_KILLFOCUS()
	ON_WM_GETDLGCODE()
	ON_MESSAGE(WM_PASTE, OnEditPaste)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMFilterEdit message handlers

void CMFilterEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_RETURN)
	{
//		CEdit::OnChar(nChar, nRepCnt, nFlags);
		if(m_ParentComboBox)
		{
// ���������� ��� ComboBox
			m_ParentComboBox->DoOnEnterPressed();
		}
		else
		{
// �������� ���������, ������� �� ����� ���� ������� ������������ ����� ��������������
			GetParent()->SendMessage(WM_COMMAND,
				MAKEWPARAM(GetDlgCtrlID(), EN_VSCROLL), (LPARAM)m_hWnd);
		}
		return;
	}
	if(nChar == VK_TAB && m_ParentComboBox == 0)
	{
// ������� TAB �������������� �����, ������� ������������ ����������� �������
		CWnd* dialogWindow = GetParent();
		bool previous = ::GetKeyState(VK_SHIFT) < 0;
		dialogWindow->GetNextDlgTabItem(this, previous)->SetFocus();
		return;
	}

// ���� ������ Control, �� ���������� ��������
	if(!m_Validator || ::GetKeyState(VK_CONTROL) < 0)
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);
		return;
	}

	CString oldValue;
	CString newValue;
	int beginSelection, endSelection;

	GetWindowText(oldValue);
//	GetSel(beginSelection, endSelection);
	CEdit::OnChar(nChar, nRepCnt, nFlags);
	GetWindowText(newValue);

	if(newValue.IsEmpty() || newValue == oldValue)
		return;

// ������, ���� ����������, ������ ������� (������ � CEdit::OnChar() �� ��������)
	UINT newChar = m_Validator->ReplaceChar(nChar);
	if(newChar != nChar)
	{
		newValue.Replace(nChar, newChar);
	}

	if(!m_Validator->IsValidInput(newValue))
	{
// ��������������� ������ ��������
		GetSel(beginSelection, endSelection);
		SetSel(beginSelection-1, endSelection);
		ReplaceSel(_T(""));
//		SetWindowText(oldValue);
//		SetSel(beginSelection, endSelection);
	}
	else
	{
		if(newChar != nChar)
		{
// �������� ������ � �������� ����������
			GetSel(beginSelection, endSelection);
			SetSel(beginSelection-1, endSelection);
			ReplaceSel(CString(char(newChar)));
		}
	}
}

LRESULT CMFilterEdit::OnEditPaste(WPARAM wParam, LPARAM lParam)
{
	CString oldValue;
	CString newValue;
	int beginSelection, endSelection;

	GetWindowText(oldValue);
	GetSel(beginSelection, endSelection);

// �������� ����������� ���������� �������
	Default();

	GetWindowText(newValue);

	if(!m_Validator->IsValidInput(newValue))
	{
// ��������������� ������ ��������
		SetWindowText(oldValue);
		SetSel(beginSelection, endSelection);
	}

	return 0;
}

UINT CMFilterEdit::OnGetDlgCode()
{
	if(m_ParentComboBox && m_ParentComboBox->m_AcceptEnter || m_AcceptEnter)
		return DLGC_WANTMESSAGE;
	return CEdit::OnGetDlgCode();
}

bool CMFilterEdit::HaveEmptyString()
{
	CString text;
	GetWindowText(text);
	if(text.IsEmpty())
	{
		SetFocus();
		return true;
	}

	return false;
}

void CMFilterEdit::OnKillFocus(CWnd* newFocusedWindow)
{
	GetSel(m_SelectionBegin, m_SelectionEnd);

	CEdit::OnKillFocus(newFocusedWindow);
}

const CMFilterEdit& CMFilterEdit::operator =(const CMFilterEdit& src)
{
	m_Validator = src.m_Validator;

//	CString string;
	if(src.GetSafeHwnd())
		src.GetWindowText(m_EditString);
	else
		m_EditString = src.m_EditString;

	if(GetSafeHwnd())
		SetWindowText(m_EditString);

	return *this;
}

