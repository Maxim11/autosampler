// GridComboBoxChildEdit.cpp : implementation file
//

#include "stdafx.h"
//#include "..\Anaconda.h"
#include "GridComboBoxChildEdit.h"

#include "GridComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBoxChildEdit

CMGridComboBoxChildEdit::CMGridComboBoxChildEdit()
{
	m_ParentComboBox = 0;
}

CMGridComboBoxChildEdit::~CMGridComboBoxChildEdit()
{
}


BEGIN_MESSAGE_MAP(CMGridComboBoxChildEdit, CEdit)
	//{{AFX_MSG_MAP(CMGridComboBoxChildEdit)
	ON_WM_KEYDOWN()
	ON_WM_CHAR()
	ON_WM_KILLFOCUS()
	ON_WM_GETDLGCODE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBoxChildEdit message handlers
void CMGridComboBoxChildEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	bool isControlPressed = ::GetKeyState(VK_CONTROL) < 0;
    switch (nChar)
    {
		case VK_UP :
		case VK_DOWN :
		case VK_HOME :
		case VK_END :

			if (isControlPressed)
			{
				m_ParentComboBox->OnKeyDown(nChar, nRepCnt, nFlags);
				return;
			}
			break;
   		case VK_NEXT :
		case VK_PRIOR :
			m_ParentComboBox->OnKeyDown(nChar, nRepCnt, nFlags);
			return;
	}			// switch(nChar)

	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CMGridComboBoxChildEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch(nChar)
	{
	case VK_ESCAPE:
	case VK_RETURN:
	case VK_TAB:
		m_ParentComboBox->OnChar(nChar, nRepCnt, nFlags);
		return;
	}

	CEdit::OnChar(nChar, nRepCnt, nFlags);
}

void CMGridComboBoxChildEdit::OnKillFocus(CWnd* newFocusedWindow)
{
	m_ParentComboBox->OnKillFocus(newFocusedWindow);
}


UINT CMGridComboBoxChildEdit::OnGetDlgCode()
{
	return DLGC_WANTMESSAGE;
}

