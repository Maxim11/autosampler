#if !defined(AFX_EDITABLEGRIDCTRL_H__6B5CB995_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_)
#define AFX_EDITABLEGRIDCTRL_H__6B5CB995_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditableGridCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMEditableGridCtrl window

#include "GridCtrl.h"
#include "IntegerValidator.h"
#include "FloatValidator.h"

class CMEditableGridCtrl : public CMGridCtrl
{
friend class CMGridEdit;
friend class CMGridComboBox;

// Construction
public:
	CMEditableGridCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMEditableGridCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMEditableGridCtrl();

	virtual void CreateTable(const int numberOfColumns, const int numberOfRows);

	int InsertColumn(int column);
	BOOL DeleteColumn(int column);

	void SetValidator(const int column, CGValidator* validator);

	void EditCellText(int row, int column);
	void EditCellText(int row, int column, int deltaRow, int deltaColumn);

	int GetCurrentRow() { return m_CurrentRow; }
	int GetCurrentColumn() { return m_CurrentColumn; }
	void ResetCurrentCell() { m_CurrentRow = m_CurrentColumn = -1; }

	void SetLimitedEditing(bool enable);
	bool IsEditingFinished() { return m_IsEditingFinished; }
// ���������� ������ � ���� �������������� ������
	CString GetEditingText();
	void SetEditingText(const CString& text);

	void SetComboBoxColumn(int number, CStringList* list, DWORD style = CBS_DROPDOWNLIST);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMEditableGridCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelectComboBox();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnMouseWheel(UINT fFlags, short zDelta, CPoint point);
	//}}AFX_MSG

protected:
	int GetFirstEnabledRow();
	int GetLastEnabledRow();

protected:
// ������� ��� �������������� ����� (�� ����� �������)
	CArray<CGValidator*, CGValidator*> m_Validators;

	int m_CurrentRow;
	int m_CurrentColumn;

// ��������� �� ����, ����������� ��� �������������� ����������� ������
	CMGridEdit* m_EditCellWindow;
	CMGridComboBox* m_ComboBoxCellWindow;

// �������������� �������������� ������ �������� �������� EditCellText()
	bool m_LimitedEditing;
	bool m_IsEditingFinished;

// ����� �������, ��� ������� ������������ ComboBox �� ������� �����, � �� ����������������.
// (���� ����� ���� ������ ����)
	int m_ComboBoxColumnNumber;
	CStringList* m_ComboBoxStringList;
	DWORD m_ComboBoxStyle;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITABLEGRIDCTRL_H__6B5CB995_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_)
