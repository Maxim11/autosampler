// ImageComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "ImageComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMImageComboBox

CMImageComboBox::CMImageComboBox()
{
	m_ImageList = 0;
}

CMImageComboBox::~CMImageComboBox()
{
}


BEGIN_MESSAGE_MAP(CMImageComboBox, CComboBox)
	//{{AFX_MSG_MAP(CMImageComboBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMImageComboBox message handlers

void CMImageComboBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	RECT& itemRectangle = lpDrawItemStruct->rcItem;
	int itemID = lpDrawItemStruct->itemID;

	if( lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT) )
	{ 	
		COLORREF textColor;
		COLORREF backgroundColor;
		COLORREF defaultTextColor = pDC->GetTextColor();
		COLORREF defaultBackgroundColor = pDC->GetBkColor();

		if (!IsWindowEnabled())
		{
			textColor = ::GetSysColor(COLOR_GRAYTEXT);
			backgroundColor = ::GetSysColor(COLOR_BTNFACE);
		}
		else if(lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			textColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
			backgroundColor = ::GetSysColor(COLOR_HIGHLIGHT);
		}
		else
		{
			textColor = defaultTextColor;
			backgroundColor = defaultBackgroundColor;
		}

		pDC->FillSolidRect(&itemRectangle, backgroundColor);
		pDC->SetTextColor(textColor);

		if(m_ImageList && itemID >= 0)
		{
			m_ImageList->Draw(pDC, GetItemData(itemID), 
				CPoint(itemRectangle.left+2, itemRectangle.top+(itemRectangle.bottom-itemRectangle.top-16)/2),
				ILD_TRANSPARENT);
			itemRectangle.left += 16+2;			// 16 - ������ ������
		}

		CString text;
		CSize size = pDC->GetTextExtent(_T("0"));
		itemRectangle.left += size.cx;
		itemRectangle.right -= size.cx;
		if(itemID >= 0)
			GetLBText(itemID, text);
		pDC->DrawText(text, &itemRectangle, DT_SINGLELINE | DT_VCENTER | DT_LEFT);

		pDC->SetTextColor(defaultTextColor);
		pDC->SetBkColor(defaultBackgroundColor);
	}

    if (lpDrawItemStruct->itemAction & ODA_FOCUS) 
	{ 
        pDC->DrawFocusRect(&itemRectangle);
	}
}


void CMImageComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	CClientDC dc(this);
	CSize size = dc.GetTextExtent(_T("0"));
	lpMeasureItemStruct->itemHeight = (max(size.cy, 16+2));
}
