// IntegerValidator.h: interface for the CGIntegerValidator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTEGERVALIDATOR_H__CE6C2E26_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
#define AFX_INTEGERVALIDATOR_H__CE6C2E26_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Validator.h"

class CGIntegerValidator : public CGValidator  
{
public:
	CGIntegerValidator(bool acceptMinus = false);
	virtual ~CGIntegerValidator();

	virtual bool IsValidInput(const CString& string);
	virtual bool IsValidResult(const CString& string);
	virtual void GetValidString(CString& string);

	void SetRange(int minValue, int maxValue);

protected:
	bool m_AcceptMinus;

	bool m_CheckRange;
	int m_MinValue;
	int m_MaxValue;
};

#endif // !defined(AFX_INTEGERVALIDATOR_H__CE6C2E26_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
