#if !defined(AFX_GRIDEDIT_H__6B5CB992_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_)
#define AFX_GRIDEDIT_H__6B5CB992_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMGridEdit window
#include "FilterEdit.h"

class CMGridEdit : public CMFilterEdit
{
// Construction
public:
	CMGridEdit(int row, int column, const CString& init);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMGridEdit)
//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMGridEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMGridEdit)
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNcDestroy();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* newFocusedWindow);
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG

protected:
	void SendNotification(int code);

protected:
	int m_Row;
	int m_Column;

	bool m_IsEscaped;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDEDIT_H__6B5CB992_086F_11D7_9C0B_998C4EDF7A19__INCLUDED_)
