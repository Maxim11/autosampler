// GridEdit.cpp : implementation file
//

#include "stdafx.h"
//#include "GridTest.h"
#include "GridEdit.h"

#include "EditableGridCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMGridEdit

CMGridEdit::CMGridEdit(int row, int column, const CString& init)
{
	m_Row = row;
	m_Column = column;

	m_EditString = init;

	m_IsEscaped = false;
}

CMGridEdit::~CMGridEdit()
{
}


BEGIN_MESSAGE_MAP(CMGridEdit, CEdit)
	//{{AFX_MSG_MAP(CMGridEdit)
	ON_WM_CREATE()
	ON_WM_NCDESTROY()
	ON_WM_KEYDOWN()
	ON_WM_CHAR()
	ON_WM_KILLFOCUS()
	ON_WM_GETDLGCODE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMGridEdit message handlers

int CMGridEdit::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Set the proper font
	SetFont(GetParent()->GetFont());

	SetWindowText(m_EditString);
	SetFocus();
	SetSel(0, -1);

// Send Notification to parent of ListView ctrl
	SendNotification(LVN_BEGINLABELEDIT);

	return 0;
}

void CMGridEdit::OnNcDestroy()
{
	CEdit::OnNcDestroy();

	delete this;
}

void CMGridEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CMEditableGridCtrl* gridCtrl = (CMEditableGridCtrl*)GetParent();
	bool controlPressed = ::GetKeyState(VK_CONTROL) < 0;
	switch (nChar)
	{
		case VK_UP :
		{
			gridCtrl->EditCellText(m_Row, m_Column, -1, 0);
			return;
		}
		case VK_DOWN :
		{
			gridCtrl->EditCellText(m_Row, m_Column, 1, 0);
			return;
		}
		case VK_HOME :
		{
			if (controlPressed)
			{
// ���� ��� ������ ���������� �����
				gridCtrl->EditCellText(gridCtrl->GetFirstEnabledRow()-1, m_Column, 1, 0);
				return;
			}
			break;
		}
		case VK_END :
		{
			if (controlPressed)
			{
				gridCtrl->EditCellText(gridCtrl->GetLastEnabledRow()+1, m_Column, -1, 0);
				return;
			}
			break;
		}
   		case VK_NEXT :
		{
			int newRow = m_Row+gridCtrl->GetCountPerPage();
			gridCtrl->EditCellText(min(gridCtrl->GetLastEnabledRow(), newRow)+1, m_Column, -1, 0);
			return;
		}
		case VK_PRIOR :
		{
			int newRow = m_Row-gridCtrl->GetCountPerPage();
			gridCtrl->EditCellText(max(gridCtrl->GetFirstEnabledRow(), newRow)-1, m_Column, 1, 0);
			return;
		}
	}
	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CMGridEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CMEditableGridCtrl* gridCtrl = (CMEditableGridCtrl*)GetParent();
	bool shiftPressed = GetKeyState(VK_SHIFT) < 0;
	switch(nChar)
	{
	case VK_ESCAPE:
		m_IsEscaped = true;
		gridCtrl->m_CurrentRow = -1;
		gridCtrl->m_CurrentColumn = -1;
		GetParent()->SetFocus();
		return;
	case VK_RETURN:
		gridCtrl->EditCellText(m_Row, m_Column, 0, 1);
		return;
	case VK_TAB :
		if(shiftPressed)
			gridCtrl->EditCellText(m_Row, m_Column, 0, -1);
		else
			gridCtrl->EditCellText(m_Row, m_Column, 0, 1);
		return;
	}

	CMFilterEdit::OnChar(nChar, nRepCnt, nFlags);
}

void CMGridEdit::OnKillFocus(CWnd* newFocusedWindow)
{
	CEdit::OnKillFocus(newFocusedWindow);

	if(!m_IsEscaped)
		GetWindowText(m_EditString);

// Send Notification to parent of ListView ctrl
	SendNotification(LVN_ENDLABELEDIT);
	DestroyWindow();
}

UINT CMGridEdit::OnGetDlgCode()
{
	return DLGC_WANTMESSAGE;
}

void CMGridEdit::SendNotification(int code)
{
// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = code;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_Row;
	dispinfo.item.iSubItem = m_Column;
	if(m_IsEscaped)
	{
		dispinfo.item.pszText = 0;
		dispinfo.item.cchTextMax = 0;
	}
	else
	{
		dispinfo.item.pszText = LPTSTR(LPCTSTR(m_EditString));
		dispinfo.item.cchTextMax = m_EditString.GetLength();
	}

	GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(),
					(LPARAM)&dispinfo );
}

