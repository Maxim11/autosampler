#if !defined(AFX_IMAGECOMBOBOX_H__BFDFDB00_2F63_4B09_96C4_8AEF40FD9724__INCLUDED_)
#define AFX_IMAGECOMBOBOX_H__BFDFDB00_2F63_4B09_96C4_8AEF40FD9724__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMImageComboBox window

// ����� ������������ ��� �������� ���������� ������� ��
// �������� ����� �� ������

class CMImageComboBox : public CComboBox
{
// Construction
public:
	CMImageComboBox();

// Attributes
public:

// Operations
public:

	void SetImageList(CImageList* imageList) { m_ImageList = imageList; }
	void SetImage(int itemIndex, int imageIndex) { SetItemData(itemIndex, imageIndex); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMImageComboBox)
	public:
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMImageComboBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMImageComboBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

protected:
	CImageList* m_ImageList;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGECOMBOBOX_H__BFDFDB00_2F63_4B09_96C4_8AEF40FD9724__INCLUDED_)
