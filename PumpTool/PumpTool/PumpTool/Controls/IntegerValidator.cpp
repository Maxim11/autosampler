// IntegerValidator.cpp: implementation of the CGIntegerValidator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IntegerValidator.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGIntegerValidator::CGIntegerValidator(bool acceptMinus)
{
	m_AcceptMinus = acceptMinus;
	m_MinValue = INT_MIN;
	m_MaxValue = INT_MAX;

	m_CheckRange = false;
}

CGIntegerValidator::~CGIntegerValidator()
{

}

bool CGIntegerValidator::IsValidInput(const CString& string)
{
	LPCTSTR chr = LPCTSTR(string);
	if(m_AcceptMinus && *chr == '-')
		chr++;
	for( ; *chr; chr++)
		if(!isdigit((BYTE)*chr))
			return false;

	if(m_CheckRange)
	{
		int value = _tstoi(string);
		return (value >= m_MinValue && value <= m_MaxValue);
	}

	return true;
}

bool CGIntegerValidator::IsValidResult(const CString& string)
{
	int value = _tstoi(string);
	return (value >= m_MinValue && value <= m_MaxValue);
}

void CGIntegerValidator::GetValidString(CString& string)
{
	int value = _tstoi(string);
	value = max(value, m_MinValue);
	value = min(value, m_MaxValue);
	string.Format(_T("%d"), value);
}

void CGIntegerValidator::SetRange(int minValue, int maxValue)
{
	m_CheckRange = true;

	m_MinValue = minValue;
	m_MaxValue = maxValue;
}
