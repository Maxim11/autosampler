// HeaderCtrlExt.cpp : implementation file
//

#include "stdafx.h"
#include "HeaderCtrlExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// ��������� � ��������������� �������������
// 1. ���������� �������� ������� ������� �� �������� ������ ���������
// (��� ����������� ������ � �����������)
// 2. ��������� ��������� ����
//

/////////////////////////////////////////////////////////////////////////////
// CMHeaderCtrlExt

CMHeaderCtrlExt::CMHeaderCtrlExt()
{
}

CMHeaderCtrlExt::~CMHeaderCtrlExt()
{
}


BEGIN_MESSAGE_MAP(CMHeaderCtrlExt, CHeaderCtrl)
	//{{AFX_MSG_MAP(CMHeaderCtrlExt)
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMHeaderCtrlExt message handlers

void CMHeaderCtrlExt::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(m_IsResizingEnabled)
		CHeaderCtrl::OnLButtonDown(nFlags, point);
}

void CMHeaderCtrlExt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{	
	if(m_IsResizingEnabled)
		CHeaderCtrl::OnLButtonDblClk(nFlags, point);
}

BOOL CMHeaderCtrlExt::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_IsResizingEnabled)
		return CHeaderCtrl::OnSetCursor(pWnd, nHitTest, message);

	return TRUE;
}

void CMHeaderCtrlExt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
// ������ ����� �������� ��������� ������� ���������� �������

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	RECT* itemRectangle = &lpDrawItemStruct->rcItem;

//	int delta = pDC->GetTextExtent(" ").cx;
	int delta = 3*::GetSystemMetrics(SM_CXEDGE);
	const int c_shift = 2;

// ������� ���������� ����
	if(HFONT(m_SymbolFont) == 0)
	{
		LOGFONT logFont = {0};
		pDC->GetCurrentFont()->GetLogFont(&logFont);
		logFont.lfCharSet = SYMBOL_CHARSET;
		logFont.lfHeight -= c_shift*2;
		lstrcpy(logFont.lfFaceName, _T("Symbol"));

		m_SymbolFont.CreateFontIndirect(&logFont);
	}

// �������� ����� ���������
	HDITEM hdi;
	TCHAR  text[20];

	hdi.mask = HDI_TEXT;
	hdi.pszText = text;
	hdi.cchTextMax = 20;

	GetItem(lpDrawItemStruct->itemID, &hdi);

	CString string(text);
	int position = string.Find(' ');
	CString symbol = string.Left(position);

	itemRectangle->left += delta;
	itemRectangle->right -= delta;
	itemRectangle->top -= c_shift;

// ������ ������ ����� ���������� ������
	CFont* oldFont = pDC->SelectObject(&m_SymbolFont);
	pDC->DrawText(symbol, itemRectangle, DT_SINGLELINE | DT_VCENTER | DT_LEFT);

	itemRectangle->left += pDC->GetTextExtent(symbol).cx;
	itemRectangle->top += c_shift;

// ������ ������� ����������� ������
	pDC->SelectObject(oldFont);
	pDC->DrawText(string.Mid(position), itemRectangle, DT_SINGLELINE | DT_VCENTER | DT_LEFT);
}


