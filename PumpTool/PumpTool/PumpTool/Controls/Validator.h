// Validator.h: interface for the CGValidator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VALIDATOR_H__CE6C2E25_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
#define AFX_VALIDATOR_H__CE6C2E25_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGValidator  
{
public:
	CGValidator() { };
	virtual ~CGValidator() { };

	virtual bool IsValidInput(const CString& string) { return true; }
	virtual bool IsValidResult(const CString& string) { return true; }
	virtual void GetValidString(CString& string) { };

// ���������� �������� ��������
	virtual UINT ReplaceChar(UINT oldChar) { return oldChar; }
};

#endif // !defined(AFX_VALIDATOR_H__CE6C2E25_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
