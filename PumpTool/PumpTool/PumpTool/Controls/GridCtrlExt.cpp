// GridCtrlExt.cpp: implementation of the CMGridCtrlExt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PeakExpert.h"
#include "GridCtrlExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CMGridCtrlExt, CMGridCtrl)
	//{{AFX_MSG_MAP(CMCurveListBox)
	ON_WM_MOUSEMOVE()
//	ON_WM_LBUTTONUP()
//	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_NOTIFY_REFLECT_EX(LVN_ITEMCHANGED, OnSelectionChanged)
	ON_WM_CAPTURECHANGED()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMGridCtrlExt::CMGridCtrlExt()
{
	m_LastCheckedItemIndex = -1;
	m_ActiveItemIndex = -1;

//	m_IsLeftButtonPressed = false;

	m_ToolTipMargin = -1;
}

CMGridCtrlExt::~CMGridCtrlExt()
{

}

// ������� ����������� ���������
void CMGridCtrlExt::HideToolTip()
{
	if(!m_ToolTip.IsWindowVisible())
		return;

	m_ToolTip.Activate(this, 0, false);
	if(GetCapture() == this)// && !m_IsLeftButtonPressed)
		ReleaseCapture();

	m_ActiveItemIndex = -1;
}

// ���������� ����������� ���������
void CMGridCtrlExt::ShowToolTip(const int item, const CString& text)
{
	m_ActiveItemIndex = item;

	CRect itemRect;
	GetSubItemRect(m_ActiveItemIndex, m_ActiveColumn, LVIR_BOUNDS, itemRect);
	ClientToScreen(&itemRect);

	m_ToolTip.SetPosition(itemRect.left-1, itemRect.top-1);
	m_ToolTip.SetColors(GetSelectionMark() == m_ActiveItemIndex);
	itemRect.bottom++;
	itemRect.left = itemRect.right = m_ToolTipMargin-4;
	m_ToolTip.AdjustRect(&itemRect, true);

	m_ToolTip.Activate(this, text, true);

// ������ ���� ��������� ����������� ����� ������� �� ������� ���� �
// �������� ��������� ���� ��� ������ �� ����������� ���������
	if(GetCapture() != this)
		SetCapture();
}

// ��������� ������������� ���������� ��������� ��� ����� ��������� �������
void CMGridCtrlExt::CheckItemForTooltip(CPoint& point)
{
	LVHITTESTINFO info;
	int item = -1;

	info.pt = point;

	if(SubItemHitTest(&info) >= 0 && info.iSubItem == m_ActiveColumn)
	{
		item = info.iItem;
	}
	else
	{
		HideToolTip();
		m_LastCheckedItemIndex = -1;
		return;
	}

// ������ ������� ��� ��������������
	if(item == m_LastCheckedItemIndex)
		return;

	CRect cellRect;
	CString itemText = GetItemText(item, m_ActiveColumn);
	GetSubItemRect(item, m_ActiveColumn, LVIR_BOUNDS, cellRect);

	int width = GetStringWidth(itemText)+2*m_ToolTipMargin;

	if(width > cellRect.Width())
		ShowToolTip(item, itemText);
	else
		HideToolTip();

	m_LastCheckedItemIndex = item;
}

// ������� ���� ����������� ��������� � ������������� ��� ������� � �����
void CMGridCtrlExt::PreSubclassWindow()
{
	CMGridCtrl::PreSubclassWindow();

	m_ToolTip.Create(this);
	m_ToolTip.SetFont(GetFont(), false);

	CClientDC cdc(this);
	CSize size = cdc.GetTextExtent(_T("0"), 1);
	m_ToolTipMargin = size.cx;
}

// ���������� ���� ����������� ���������
void CMGridCtrlExt::OnDestroy()
{
	CMGridCtrl::OnDestroy();

	if(m_ToolTip.GetSafeHwnd() != 0)
		m_ToolTip.DestroyWindow();
}

void CMGridCtrlExt::OnMouseMove(UINT nFlags, CPoint point)
{
	CMGridCtrl::OnMouseMove(nFlags, point);

//	m_IsLeftButtonPressed = ((nFlags & MK_LBUTTON)!= 0) ;

// ���������, �� ����� �� ������ �� ������� ����
	CRect clientRect;
	CRect headerRect;
	GetClientRect(clientRect);
	GetHeaderCtrl()->GetClientRect(headerRect);
	clientRect.top += headerRect.bottom;

	if(!clientRect.PtInRect(point))
	{
		HideToolTip();
		m_LastCheckedItemIndex = -1;
		return;
	}

	CheckItemForTooltip(point);
}

/*
void CMGridCtrlExt::OnLButtonDown(UINT nFlags, CPoint point)
{
	TRACE("CMGridCtrlExt OnLButtonDown 1 %d\n", GetCapture() == this);
	CMGridCtrl::OnLButtonDown(nFlags, point);
	TRACE("CMGridCtrlExt OnLButtonDown 2 %d\n", GetCapture() == this);

	m_IsLeftButtonPressed = true;

// ��� ������� ������ ���� �� �������� ����������� � ����� ���������� ��������
	if(m_ToolTip.IsWindowVisible())
		m_ToolTip.SetColors(GetSelectionMark() == m_ActiveItemIndex);
}

void CMGridCtrlExt::OnLButtonUp(UINT nFlags, CPoint point)
{
	CMGridCtrl::OnLButtonUp(nFlags, point);
	TRACE("CMGridCtrlExt OnLButtonUp\n");

	m_IsLeftButtonPressed = false;
}
*/

BOOL CMGridCtrlExt::OnSelectionChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	if(pResult)
		*pResult = 0;

	NMLISTVIEW* changedRow = (NMLISTVIEW*)pNMHDR;
// �������� ����� ������������ ���� ��� ����� ����������� ��������
	if(changedRow->iItem == m_ActiveItemIndex && m_ToolTip.IsWindowVisible())
		m_ToolTip.SetColors(GetSelectionMark() == m_ActiveItemIndex);

// ���������� ��� ��������� ��������� ������������ �����!
	return FALSE;
}

void CMGridCtrlExt::OnCaptureChanged(CWnd* pWnd)
{
	TRACE("CMGridCtrlExt OnCaptureChanged %x this = %x f %x\n", pWnd, this, GetCapture());
	if(m_ToolTip.IsWindowVisible() && GetCapture() != this)
	{
		HideToolTip();
		m_LastCheckedItemIndex = -1;
	}
}

void CMGridCtrlExt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CMGridCtrl::OnKeyDown(nChar, nRepCnt, nFlags);

// ��� ��������� � ������� ���������� ���������� ��������, ��� � ��� �������� ����
	CPoint point;
	::GetCursorPos(&point);
	ScreenToClient(&point);

	if(nChar != VK_UP && nChar != VK_DOWN)
		return;

	CRect clientRect;
	CRect headerRect;
	GetClientRect(clientRect);
	GetHeaderCtrl()->GetClientRect(headerRect);
	clientRect.top += headerRect.bottom;
	if(!clientRect.PtInRect(point))
		return;

	CheckItemForTooltip(point);
}


