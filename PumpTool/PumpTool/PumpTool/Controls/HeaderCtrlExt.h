#if !defined(AFX_HEADERCTRLEXT_H__05A31EA8_D213_4027_9482_F9993C4872D9__INCLUDED_)
#define AFX_HEADERCTRLEXT_H__05A31EA8_D213_4027_9482_F9993C4872D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeaderCtrlExt.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMHeaderCtrlExt window

// ��������� � ��������������� �������������
class CMHeaderCtrlExt : public CHeaderCtrl
{
friend class CMGridCtrl;

// Construction
public:
	CMHeaderCtrlExt();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMHeaderCtrlExt)
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMHeaderCtrlExt();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMHeaderCtrlExt)
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

protected:
// ��������� �� ��������� �������� �������
	bool m_IsResizingEnabled;

// ���� ��� ��������� ��������� ���� � ���������
	CFont m_SymbolFont;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADERCTRLEXT_H__05A31EA8_D213_4027_9482_F9993C4872D9__INCLUDED_)
