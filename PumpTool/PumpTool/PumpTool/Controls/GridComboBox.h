#if !defined(AFX_GRIDCOMBOBOX_H__FC52D5AA_98DA_427A_85CE_6206C22D0FC4__INCLUDED_)
#define AFX_GRIDCOMBOBOX_H__FC52D5AA_98DA_427A_85CE_6206C22D0FC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridComboBox.h : header file
//
#include "GridComboBoxChildEdit.h"

/////////////////////////////////////////////////////////////////////////////
// CMGridComboBox window

class CMGridComboBox : public CComboBox
{
	friend class CMGridComboBoxChildEdit;

// Construction
public:
	CMGridComboBox(int row, int column, const CString& init);

	void SetInitialString(const CString& string) { m_InitialString = string; }

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMGridComboBox)
//	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMGridComboBox();

	void FillItems(int* stringIDs);
	void FillItems(CStringList* stringList);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMGridComboBox)
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNcDestroy();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* newFocusedWindow);
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG

protected:
	void SendNotification(int code);

protected:
	int m_Row;
	int m_Column;

	bool m_IsEscaped;

	CString m_InitialString;

// ��� ������������� ���������� ������� ���������� �������������� ������� ��������������.
	CMGridComboBoxChildEdit m_ChildEdit;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDCOMBOBOX_H__FC52D5AA_98DA_427A_85CE_6206C22D0FC4__INCLUDED_)
