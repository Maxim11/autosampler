// FloatValidator.h: interface for the CGFloatValidator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLOATVALIDATOR_H__CE6C2E27_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
#define AFX_FLOATVALIDATOR_H__CE6C2E27_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Validator.h"

class CGFloatValidator : public CGValidator  
{
public:
	CGFloatValidator(bool acceptMinus = false);
	virtual ~CGFloatValidator();

	virtual bool IsValidInput(const CString& string);
	virtual bool IsValidResult(const CString& string);
	virtual void GetValidString(CString& string);

// ���������� �������� ��������
	virtual UINT ReplaceChar(UINT oldChar);

	void SetRange(double minValue, double maxValue, char format[]);
	CString GetFormatString() { return m_FormatString; }

protected:
	bool m_AcceptMinus;

	double m_MinValue;
	double m_MaxValue;
	CString m_FormatString;

};

#endif // !defined(AFX_FLOATVALIDATOR_H__CE6C2E27_F57F_11D5_9B2C_CB2F0EF2C019__INCLUDED_)
