#if !defined(AFX_FILTEREDIT_H__77B30804_F536_11D5_9B2B_D66110B8C319__INCLUDED_)
#define AFX_FILTEREDIT_H__77B30804_F536_11D5_9B2B_D66110B8C319__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterEdit.h : header file
//

#include "Validator.h"

/////////////////////////////////////////////////////////////////////////////
// CMFilterEdit window


class CMFilterEdit : public CEdit
{
friend class CMSmartComboBox;

// Construction
public:
	CMFilterEdit();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMFilterEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMFilterEdit();
	void SetValidator(CGValidator* validator) { m_Validator = validator; }
	void SetAcceptEnter(bool accept) { m_AcceptEnter = accept; }
	void SetParentComboBox(CMSmartComboBox* comboBox) { m_ParentComboBox = comboBox; }

	CGValidator* GetValidator() const { return m_Validator;}
	CString GetEditString() const { return m_EditString; }

	bool HaveEmptyString();
	void EnableAcceptEnter(bool enable) { m_AcceptEnter = enable; }

	const CMFilterEdit& operator =(const CMFilterEdit& src);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMFilterEdit)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* newFocusedWindow);
	afx_msg UINT OnGetDlgCode();
	afx_msg LRESULT OnEditPaste(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	CGValidator* m_Validator;
	CMSmartComboBox* m_ParentComboBox;
	CString m_EditString;

// ��� ������ ������ ���������� ��������� ������
	int m_SelectionBegin;
	int m_SelectionEnd;

// ���� �������������� �� ������� Enter �������� ��������� EN_VSCROLL
	bool m_AcceptEnter;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTEREDIT_H__77B30804_F536_11D5_9B2B_D66110B8C319__INCLUDED_)
