#if !defined(AFX_SMARTCOMBOBOX_H__22E9A230_F5F8_11D5_9B2D_AEC7EB10C019__INCLUDED_)
#define AFX_SMARTCOMBOBOX_H__22E9A230_F5F8_11D5_9B2D_AEC7EB10C019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SmartComboBox.h : header file
//

#include "Validator.h"
#include "FilterEdit.h"

/////////////////////////////////////////////////////////////////////////////
// CMSmartComboBox window

//const int c_MaxComboBoxSize = 8;

class CMSmartComboBox : public CComboBox
{
friend class CMFilterEdit;

// Construction
public:
	CMSmartComboBox();
	virtual ~CMSmartComboBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMSmartComboBox)
protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// �������� �� ������� Enter � ���� ��������������
	void DoOnEnterPressed();

// Implementation
public:
	void SetMaxNumberOfStrings(int number) { m_MaxNumberOfStrings = number; }
	int GetIntValue();	
	double GetFloatValue();

	void SetIntValue(int value);
	void SetFloatValue(double value);

// ����� ��������� ������� ������
	void GetSelectedString(CString& text);
// �������� �� ������ � ���� ��������������
	void GetSelectedString();
//	int  GetIntFromSelectedItem();
//	double GetFloatFromSelectedItem();
	void GetEditSelection(int& begin, int& end);

	void SetValidator(CGValidator* validator) { m_Validator = validator; }
	void SetAcceptEnter(bool accept) { m_AcceptEnter = accept; }

	void AddString(const CString& string);
	void AddCurrentString();
	bool HaveEmptyString();

	void SetSpecialString(CString string);
	CString& GetSpecialString() { return m_SpecialString; }

	void SaveToString(CString& dst) const;
	void GetFromString(const CString& src);

	const CMSmartComboBox& operator =(const CMSmartComboBox& src);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMSmartComboBox)
	afx_msg void OnKillFocus(CWnd* newFocusedWindow);
	//}}AFX_MSG

	CGValidator* m_Validator;
	CMFilterEdit m_FilterEdit;

	CStringList  m_StringList;
	CString m_SpecialString;				// ������, ������� �� ��������� �� ������

	int m_MaxNumberOfStrings;
	bool m_AcceptEnter;					// ������� Enter ������������ ������ �� ������

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMARTCOMBOBOX_H__22E9A230_F5F8_11D5_9B2D_AEC7EB10C019__INCLUDED_)
