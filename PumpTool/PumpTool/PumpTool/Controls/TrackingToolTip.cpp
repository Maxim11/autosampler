// TrackingToolTip.cpp: implementation of the CMTrackingToolTip class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TrackingToolTip.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void CALLBACK EXPORT ToolTipTimerProc(HWND hWnd, UINT nMsg,
	UINT nIDEvent, DWORD dwTime);

CMTrackingToolTip::CMTrackingToolTip()
{

}

CMTrackingToolTip::~CMTrackingToolTip()
{

}

BOOL CMTrackingToolTip::Create(CWnd* pParentWnd, DWORD dwStyle)
{
	ASSERT(GetSafeHwnd() == 0);

	if(!CToolTipCtrl::Create(pParentWnd, dwStyle))
		return FALSE;

	TOOLINFO ti;
	FillInToolInfo(ti, pParentWnd, 0);
	ti.uFlags |= TTF_TRACK | TTF_ABSOLUTE;

	SendMessage(TTM_ADDTOOL, 0, (LPARAM)&ti);

	return TRUE;
}

/*
BOOL CMTrackingToolTip::PreTranslateMessage(MSG* pMsg) 
{

// ������� ����� ������� ���������� ����-���������
// �� ������ ������� ��������� ���� ���������
	switch(pMsg->message)
	{
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_LBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
		{
		CPoint point = pMsg->pt;
		LPARAM lParam;
		GetOwner()->ScreenToClient(&point);
		lParam = MAKELPARAM(short(point.x), short(point.y));
		GetOwner()->SendMessage(pMsg->message, pMsg->wParam, lParam);
		TRACE("CMTrackingToolTip %d %d\n", pMsg->pt.x, pMsg->pt.y);
		}
		break;
//	case WM_RBUTTONDOWN:
//		SendMessage(TTM_TRACKACTIVATE, (WPARAM)false, (LPARAM)0);
//		break;
	}

	return CToolTipCtrl::PreTranslateMessage(pMsg);
}
*/

// ������� ��������� ����������� ���������
void CMTrackingToolTip::SetPosition(int posX, int posY)
{
	SendMessage(TTM_TRACKPOSITION, 0, (LPARAM)(DWORD)MAKELONG(posX, posY));
}

// �������� ������� ����, ������������ ������� ������ ������
void CMTrackingToolTip::AdjustRect(CRect* rect, bool allMargins)
{
	CClientDC cdc(this);
	CRect margins;
	CSize size = cdc.GetTextExtent(_T("0"), 1);
	int delta = (rect->Height()-size.cy);

	SendMessage(TTM_GETMARGIN, 0, (LPARAM)(LPRECT)margins);
	margins.top = delta/2;
	margins.bottom = delta-margins.top;
	if(allMargins)
	{
		margins.left = rect->left;
		margins.right = rect->right;
	}

	SendMessage(TTM_SETMARGIN, 0, (LPARAM)(LPRECT)margins);
}

// ���������� ��� ������� ��������� � �������� �������
void CMTrackingToolTip::Activate(CWnd* pWnd, LPCTSTR text, bool activate, int interval)
{
	ASSERT(::IsWindow(m_hWnd));

	TOOLINFO ti;
	FillInToolInfo(ti, pWnd, 0);
	ti.lpszText = (LPTSTR)text;

	SendMessage(TTM_UPDATETIPTEXT, 0, (LPARAM)&ti);
	SendMessage(TTM_TRACKACTIVATE, (WPARAM)activate,
		(LPARAM)&ti);

// ����� �������� �������� ������� ��������� ��������
	if(interval)
		SetTimer(0, interval, ToolTipTimerProc);
}

// ������������� ����� ��� ��������� ��������� ������� � ������
void CMTrackingToolTip::SetColors(bool isItemSelected)
{
	COLORREF textColor;
	COLORREF backgroundColor;
	if(isItemSelected)
	{
		textColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
		backgroundColor = ::GetSysColor(COLOR_HIGHLIGHT);
	}
	else
	{
		CClientDC clientDC(this);
		textColor = clientDC.GetTextColor();
		backgroundColor = clientDC.GetBkColor();
	}
	SetTipTextColor(textColor);
	SetTipBkColor(backgroundColor);
}

void CALLBACK EXPORT ToolTipTimerProc(
		HWND hWnd,		// handle of CWnd that called SetTimer
		UINT nMsg,		// WM_TIMER
		UINT nIDEvent,	// timer identification
		DWORD dwTime)	// system time
{
	TOOLINFO ti;
	memset(&ti, 0, sizeof(TOOLINFO));
	ti.cbSize = sizeof(TOOLINFO);

	::SendMessage(hWnd, TTM_TRACKACTIVATE, (WPARAM)0,
		(LPARAM)&ti);
	::KillTimer(hWnd, nIDEvent);
}
