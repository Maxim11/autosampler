#pragma once

// ��������� �� ������ ���� ��������� �� 4 (��� �� ���������)
#pragma pack(2)

// ������������ ���������, � �� �����, ��������� ��������� ��
// �������� ������� ����� � ��������� �� ����� ����� ����
// �������� � ��������� �� ���������
struct CGCanFrame
{
	BYTE m_Flags;
	BYTE m_Dlc;
	UINT32 m_ExtendedId;
	union
	{
		BYTE Byte[8];
		UINT16 Uint16[4];
		INT16  Int16[4];
		UINT32 Uint32[2];
		INT32  Int32[2];
	} 
	m_Data;

	UINT32 m_TimeStamp;
	
	CGCanFrame() { m_Data.Uint32[0] = m_Data.Uint32[1] = 0; }
};

CString GetLogString(const CGCanFrame& canFrame);
bool SetFromString(CGCanFrame* canFrame, CString& string);

CString GetHeaderString();

// �������� ���������� � ��������� ����
CString GetIdString(const CGCanFrame& canFrame);
CString GetDlcString(const CGCanFrame& canFrame);
CString GetDataString(const CGCanFrame& canFrame);
CString GetTimeString(const CGCanFrame& canFrame);
CString GetFlagsString(const CGCanFrame& canFrame);

UINT HexStringToInt(CString& string);

#define CAN_FLAG_ECHO_FRAME   0x2

#define CAN_FLAG_EXTENDED_ID   (1 << 31)