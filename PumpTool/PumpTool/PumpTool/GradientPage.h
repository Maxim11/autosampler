#pragma once

#include "ToolPage.h"
#include "ProgramData.h"

#include "Controls\GridCtrl.h"
#include "Controls\ImageComboBox.h"

#include "GradientProgramStage.h"

/////////////////////////////////////////////////////////////////////////////
// CMGradientPage dialog

class CGProgramData;

class CMGradientPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMGradientPage)

// Construction
public:
	CMGradientPage();
	~CMGradientPage();

	//virtual void ProcessStatusFrame(const CGCanFrame* canFrame);
	void ShowValuesPumpA(bool show = true);
	void ShowValuesPumpB(bool show = true);
	void SetPumpA(CGPiton* pump) { m_PumpA = pump; }
	void SetPumpB(CGPiton* pump) { m_PumpB = pump; }

	void SetPumpASerialNumber(int serialNumber);
	void SetPumpBSerialNumber(int serialNumber);

	void EnableControls(bool enable);

	void CheckPumpsState();

	void SelectNextProgramStage();

	void SaveToRegistry();
	void LoadFromRegistry();
	void SaveEluentInfo();

// Dialog Data
	enum { IDD = IDD_PAGE_GRADIENT };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnSetActive();
 	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Implementation
public:
	afx_msg void OnBnClickedCheckPumpA();
	afx_msg void OnBnClickedCheckPumpB();
	afx_msg void OnBnClickedCheckStartFill();
	afx_msg void OnBnClickedCheckStartDischarge();
	afx_msg void OnBnClickedCheckStartWork();
	afx_msg void OnBnClickedCheckVolume();
	//afx_msg void OnBnClickedCheckStartStop();
	afx_msg void OnBnClickedRadioFill();
	afx_msg void OnBnClickedRadioDischarge();
	afx_msg void OnBnClickedRadioWork();
	afx_msg void OnBnClickedButtonEdit();
	afx_msg void OnComboBoxSelectProgram();

	afx_msg void OnCommandStart();
	afx_msg void OnBnClickedCheckRestart();

protected:
	bool StartFillMode();
	bool StartDischargeMode();
	bool StartWorkMode();
	void StopCurrentMode(bool isWorkMode);

	void ShowGradientWaitDialog();
	void CloseGradientWaitDialog();

	virtual void StartPressureReleasing(int flowRate);

	void EnableRestartAfterStop();
	bool CheckEluents();

protected:
	DECLARE_MESSAGE_MAP()

	void FillProgramNamesComboBox();
	void CreateProgramTable();
	void ShowProgramData();
	void FillFromTimeColumn();
	void InsertStartStages(CGProgramData& programData);

	bool GetGradientProgram();

	void ShowControls(bool forWorkMode);
	void SetNewMode(int mode);

	void CreateExeFolderName(CString& name);
	void LoadProgramFile();
	void SaveProgramFile();

protected:
	CMSmartComboBox m_VolumeComboBox;
	//CMSmartComboBox m_VolumeAComboBox;
	//CMSmartComboBox m_VolumeBComboBox;

	CMImageComboBox m_ProgramNamesComboBox;
	CMGridCtrl m_ProgramParametersGrid;
	CImageList m_ProgramNamesImageList;

	class CMGradientWaitDialog* m_GradientWaitDialog;

protected:
	int m_SelectedMode;
	int m_FlowRates[3];
	int m_Volumes[3];

	//class CGPiton* m_PumpB;

// ������ ���� �������� ��������� �� ������ �����
	CGProgramDataContainer m_MainProgramContainer;
	CString m_ProgramFileName;

// ��� ������� ��������� ���������
	CString m_CurrentProgramName;
// ��������� ��������� �� ����� ������������ ������
	bool m_CurrentProgramIsValid;

	CGGradientProgram m_GradientProgram;

	// ��������� �� � ��������� �������� ������� �� �����.
	bool m_IsInWaitState;

	// ������ ��������� ��� �������� ����������.
	CString m_HeaderInfoBegin;
	// ������������ ������������ ����� ��������� ��� ��������� ����������.
	int m_StageDuration;

	// ���� ������� ��� ���������� ���������� �� ������� � ���������� ������.
	CString m_EluentASerialNumberKey;
	CString m_EluentBSerialNumberKey;

	bool m_IsWorking;
public:
	afx_msg void OnEnChangeEditPressureA();
};
