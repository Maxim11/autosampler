#include "afxwin.h"
#if !defined(AFX_SELECTINSTRUMENTTYPEDIALOG_H__8E526497_CF06_4F66_BBDC_48F3CE8FE755__INCLUDED_)
#define AFX_SELECTINSTRUMENTTYPEDIALOG_H__8E526497_CF06_4F66_BBDC_48F3CE8FE755__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectInstrumentTypeDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMSelectPumpConnectionDialog dialog

class CMSelectPumpConnectionDialog : public CDialog
{
// Construction
public:
	CMSelectPumpConnectionDialog(CWnd* pParent = NULL);   // standard constructor

	void SetPortNames(CString* names);
	void EnablePortSelectionOnly(bool enable) { m_PortSelectionOnly = enable; }

// Dialog Data
	//{{AFX_DATA(CMSelectPumpConnectionDialog)
	enum { IDD = IDD_DIALOG_SELECT_INSTRUMENT };
	int		m_InstrumentIndex;
	CString m_ConnectionPort;
	CString m_UsbDevice;
	int		m_UsbIndex;
	CString m_ConnectionPortB;
	CString m_UsbDeviceB;
	int		m_UsbIndexB;
	BOOL    m_IsGradientOn;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMSelectPumpConnectionDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
// �������� ��� ������ ���������
	void DoOnSelectDetector(int index);

	// Generated message map functions
	//{{AFX_MSG(CMSelectPumpConnectionDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioPiton3();
	afx_msg void OnRadioPiton4();
	afx_msg void OnSelectComPort();
	afx_msg void OnBnClickedCheckGradientOn();
	afx_msg void OnSelectComPortB();
	//}}AFX_MSG

protected:
// ���������� ������ ������������ �� USB ���������
	void FillUsbDeviceList(int controlD);

protected:

	CString* m_PortNames;
// ��� �� ������ ���� ��� ����������� �������
	bool m_IsPortSelected;

// ��������� �������� ������ ���� ��� ����������� �������
	bool m_PortSelectionOnly;

	int m_NumberOfUsbDevices;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeComboSelectInstrument();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTINSTRUMENTTYPEDIALOG_H__8E526497_CF06_4F66_BBDC_48F3CE8FE755__INCLUDED_)
