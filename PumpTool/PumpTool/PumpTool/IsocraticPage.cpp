#include "stdafx.h"
#include "resource.h"
#include "IsocraticPage.h"

#include "Instrument/Piton.h"
//#include "ValveCloseDialog.h"
//#include "PressureReleasingDialog.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

static const TCHAR c_RegistrySection[] = _T("Isocratic");


IMPLEMENT_DYNCREATE(CMIsocraticPage, CMToolPage)

/////////////////////////////////////////////////////////////////////////////
// CMIsocraticPage property page

CMIsocraticPage::CMIsocraticPage() : CMToolPage(CMIsocraticPage::IDD)
{
	/*
	m_Volumes[0] = 35000;
	m_Volumes[1] = 35000;
	m_Volumes[2] = 0;
	m_FlowRates[0] = 20000;
	m_FlowRates[1] = 20000;
	m_FlowRates[2] = 1000;
*/
	m_Settings.m_AreChanged = false;
	m_IsWorking = false;
}

CMIsocraticPage::~CMIsocraticPage()
{
}

void CMIsocraticPage::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMIsocraticPage, CMToolPage)
	//ON_BN_CLICKED(IDC_RADIO_FILL, &CMIsocraticPage::OnBnClickedRadioFill)
	//ON_BN_CLICKED(IDC_RADIO_DISCHARGE, &CMIsocraticPage::OnBnClickedRadioDischarge)
	//ON_BN_CLICKED(IDC_RADIO_WORK, &CMIsocraticPage::OnBnClickedRadioWork)
	//ON_BN_CLICKED(IDC_BUTTON_START, &CMIsocraticPage::OnBnClickedButtonStart)
	//ON_BN_CLICKED(IDC_BUTTON_STOP, &CMIsocraticPage::OnBnClickedButtonStop)
	//ON_BN_CLICKED(IDC_CHECK_START_STOP, &CMIsocraticPage::OnBnClickedCheckStartStop)
	//ON_BN_CLICKED(IDC_BUTTON_SETTINGS, &CMIsocraticPage::OnBnClickedButtonSettings)
	ON_BN_CLICKED(IDC_CHECK_START_FILL_A, &CMIsocraticPage::OnBnClickedCheckStartFill)
	ON_BN_CLICKED(IDC_CHECK_START_DISCHARGE_A, &CMIsocraticPage::OnBnClickedCheckStartDischarge)
	ON_BN_CLICKED(IDC_CHECK_START_WORK_A, &CMIsocraticPage::OnBnClickedCheckStartWork)
	ON_BN_CLICKED(IDC_CHECK_VOLUME_FILL_A, &CMIsocraticPage::OnBnClickedCheckVolumeFill)
	ON_BN_CLICKED(IDC_CHECK_VOLUME_DISCHARGE_A, &CMIsocraticPage::OnBnClickedCheckVolumeDischarge)
	ON_EN_CHANGE(IDC_EDIT_VOLUME_A, &CMIsocraticPage::OnEnChangeEditVolumeA)
END_MESSAGE_MAP()

BOOL CMIsocraticPage::OnInitDialog()
{
	int result = CMToolPage::OnInitDialog();

	//CButton* fillButton = (CButton*)GetDlgItem(IDC_RADIO_FILL);
	//fillButton->SetCheck(true);
	//fillButton->SetFocus();

	m_FillVolumeComboBox.SetValidator(&m_IntegerValidator);
	m_FillVolumeComboBox.SubclassDlgItem(IDC_COMBO_VOLUME_FILL_A, this);
	m_FillVolumeComboBox.LimitText(5);

	m_DischargeVolumeComboBox.SetValidator(&m_IntegerValidator);
	m_DischargeVolumeComboBox.SubclassDlgItem(IDC_COMBO_VOLUME_DISCHARGE_A, this);
	m_DischargeVolumeComboBox.LimitText(5);

	m_FlowRateComboBox.SetValidator(&m_IntegerValidator);
	m_FlowRateComboBox.SubclassDlgItem(IDC_COMBO_FLOW_RATE_A, this);
	m_FlowRateComboBox.LimitText(4);
	
	m_StartPressureComboBox.SetValidator(&m_FloatValidator);
	m_StartPressureComboBox.SubclassDlgItem(IDC_COMBO_START_PRESSURE_A, this);
	m_StartPressureComboBox.LimitText(5);

	m_StartFlowRateComboBox.SetValidator(&m_IntegerValidator);
	m_StartFlowRateComboBox.SubclassDlgItem(IDC_COMBO_START_FLOW_RATE_A, this);
	m_StartFlowRateComboBox.LimitText(4);

	m_EluentsComboBox.SubclassDlgItem(IDC_COMBO_ELUENT_A, this);
	m_EluentsComboBox.SetMaxNumberOfStrings(15);

	LoadFromRegistry();

	//m_SelectedMode = PUMP_MODE_FILL;
	//m_FlowRateComboBox.SetWindowText(_T("20000"));
	//m_VolumeComboBox.SetWindowText(_T("35000"));
	//m_StartPressureComboBox.SetWindowText(_T("20"));
	//m_StartFlowRateComboBox.SetWindowText(_T("1000"));

	//m_FlowRateComboBox.EnableWindow(false);

	((CProgressCtrl*)GetDlgItem(IDC_PROGRESS_PUMP_A))->SetRange(0, 100);

	CString savedName = AfxGetApp()->GetProfileString(c_RegistrySection, _T("Eluent"), _T(""));
	// ������ �������� ��� ������ ���� �������� �� �������.
	if(m_EluentsComboBox.GetCount() == 0)
	{
		InitEluentsComboBox(&m_EluentsComboBox);
	}
	m_EluentsComboBox.SetWindowText(savedName);

	CWnd* timeField = GetDlgItem(IDC_STATIC_TIME_A);
	CreateLargeFont(timeField);
	timeField->SetFont(&m_LargeFont);
	timeField->SetWindowText(_T(""));

	//GetDlgItem(IDC_COMBO_FLOW_RATE)->SetFocus();

	return result;
}

BOOL CMIsocraticPage::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN && (pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_CANCEL))
	{
		StopCurrentMode();
		// ���������� ��� ����������� ��������� � ����������� � ������.
		UpdatePageHeader();
		return TRUE;
	}
	
	return CMToolPage::PreTranslateMessage(pMsg);
}

void CMIsocraticPage::SetPumpSerialNumber(int serialNumber)
{
	CMToolPage::SetPumpSerialNumber(serialNumber, IDS_CONTROL_PUMP, IDC_STATIC_PUMP_A);
	m_EluentASerialNumberKey.Format(_T("EluentA_%d"), serialNumber);

	CString eluent = AfxGetApp()->GetProfileString(_T("Settings"), m_EluentASerialNumberKey, _T(""));
	if(!eluent.IsEmpty())
	{
		m_EluentsComboBox.SetWindowText(eluent);
	}
}

void CMIsocraticPage::ShowValuesPumpA(bool show)
{
	const int c_PumpControls[] =
	{
		IDC_EDIT_FLOW_RATE_A,
		IDC_EDIT_VOLUME_A,
		IDC_EDIT_PRESSURE_A,
		IDC_PROGRESS_PUMP_A,
		IDC_STATIC_PERCENTS_A,
		IDC_STATIC_TIME_A
	};
	if(show)
	{
		ShowValuesForPump(m_PumpA, c_PumpControls);
	}
	else
	{
		ResetValuesForPump(c_PumpControls);
	}
}

void CMIsocraticPage::CheckPumpState()
{
	if(m_PumpA->IsReady() && m_IsWorking)
	{
		// TODO ��������� ������ ��� ��������� ���������.
		// ����� �������� ���������� ��������.
		CButton* startButton = 0;
		switch(sm_CurrentMode)
		{
		case PUMP_MODE_FILL:
			startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
			break;
		case PUMP_MODE_DISCHARGE:
			startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
			break;
		case PUMP_MODE_WORK:
			startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
			break;
		}
		if(startButton != 0)
		{
			startButton->SetCheck(false);
			EnableControls(true);
			EnableOtherPages(true);
			
			// ���������� �������� ������ � ������������� �������.
			((CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A))->SetCheck(false);
			((CButton*)GetDlgItem(IDC_CHECK_VOLUME_DISCHARGE_A))->SetCheck(false);
		}
		if(!m_HeaderInfoString.IsEmpty())
		{
			m_HeaderInfoString.Empty();
			UpdatePageHeader();
		}
	}
	m_IsWorking = !m_PumpA->IsReady();

#if 0	// ����� �������� ���������� ������������� � �� ����� ����. �����������.
	if(m_PumpA->IsInPressureReleasingMode())
	{
		// ���������� �������� ��� ��������� ������ ����� ��������� �����.
		if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
		{
			m_PressureReleasingDialog->SetProgressPosition(m_PumpA->GetCurrentPressure());
		}
		if(sm_CurrentMode == PUMP_MODE_DISCHARGE)
		{
			sm_CurrentMode = PUMP_MODE_PRESSURE_RELEASING;
			ShowPressureReleasingDialog(m_PumpA->GetCurrentPressure(), 0, IDS_HEADER_AUTO_RELEASING);
		}
	}

	if(m_PumpA->IsPressureReleased())
	{
		// ���������� "����������" ���������.
		m_PumpA->StopCurrentMode();

		//EnableModeButtons(true);
		if(m_PressureReleasingDialog != 0 && m_PressureReleasingDialog->GetSafeHwnd() != 0)
		{
			m_PressureReleasingDialog->EndDialog(IDOK);
			m_PressureReleasingDialog->DestroyWindow();
		}

#if !USE_MODAL_P_R_DIALOG

		// �������� ����� ��������� ����� ��������.
		if(m_SelectedMode == PUMP_MODE_FILL)
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
			startButton->SetCheck(true);
			OnBnClickedCheckStartFill();
		}
		if(m_SelectedMode == PUMP_MODE_DISCHARGE)
		{
			CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
			startButton->SetCheck(true);
			OnBnClickedCheckStartDischarge();
		}
#else
		// ��� ��������� ������� ����� ��� �������� �����������
		// ���������� ����������� ������� ������ ������ ������.
#endif
	}
#endif
}

void CMIsocraticPage::InitForRunningState()
{
	bool isRunning = false;
	CButton* startButton;

	if(m_PumpA->IsInFillMode())
	{
		//sm_IsRunning = true;
		sm_CurrentMode = PUMP_MODE_FILL;
		startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
		startButton->SetCheck(true);
		OnBnClickedCheckStartFill();
		EnableControls(false);
		startButton->EnableWindow(true);
		m_HeaderInfoString.LoadString(IDS_INFO_FILL);
	}
	if(m_PumpA->IsInDischargeMode())
	{
		//sm_IsRunning = true;
		sm_CurrentMode = PUMP_MODE_DISCHARGE;
		startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
		startButton->SetCheck(true);
		OnBnClickedCheckStartFill();
		EnableControls(false);
		startButton->EnableWindow(true);
		m_HeaderInfoString.LoadString(IDS_INFO_DISCHARGE);
	}
	if(m_PumpA->IsInWorkMode())
	{
		//sm_IsRunning = true;
		sm_CurrentMode = PUMP_MODE_WORK;
		startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
		startButton->SetCheck(true);
		EnableControls(false);
		startButton->EnableWindow(true);
		m_HeaderInfoString.LoadString(IDS_INFO_WORK);
	}
	UpdatePageHeader();
}
/*
void CMIsocraticPage::ShowControls(bool forWorkMode)
{
	m_FlowRateComboBox.EnableWindow(forWorkMode);

	GetDlgItem(IDC_STATIC_START_PRESSURE)->ShowWindow(forWorkMode);
	m_StartPressureComboBox.ShowWindow(forWorkMode);
	GetDlgItem(IDC_STATIC_START_PRESSURE_U)->ShowWindow(forWorkMode);
	GetDlgItem(IDC_STATIC_START_FLOW_RATE)->ShowWindow(forWorkMode);
	m_StartFlowRateComboBox.ShowWindow(forWorkMode);
	GetDlgItem(IDC_STATIC_START_FLOW_RATE_U)->ShowWindow(forWorkMode);

	//GetDlgItem(IDC_STATIC_MODE_VOLUME)->ShowWindow(!forWorkMode);
	//m_VolumeComboBox.ShowWindow(!forWorkMode);
	//GetDlgItem(IDC_STATIC_MODE_VOLUME_U)->ShowWindow(!forWorkMode);
}
*/

void CMIsocraticPage::EnableControls(bool enable)
{
//	GetDlgItem(IDC_RADIO_FILL)->EnableWindow(enable);
//	GetDlgItem(IDC_RADIO_DISCHARGE)->EnableWindow(enable);
//	GetDlgItem(IDC_RADIO_WORK)->EnableWindow(enable);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(enable);

	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
	if(button->GetCheck() != 0)
	{
		m_FillVolumeComboBox.EnableWindow(enable);
	}
	button->EnableWindow(enable);

	button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_DISCHARGE_A);
	if(button->GetCheck() != 0)
	{
		m_DischargeVolumeComboBox.EnableWindow(enable);
	}
	button->EnableWindow(enable);

	m_FlowRateComboBox.EnableWindow(enable);
	m_StartPressureComboBox.EnableWindow(enable);
	m_StartFlowRateComboBox.EnableWindow(enable);

	GetDlgItem(IDC_CHECK_START_FILL_A)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_START_DISCHARGE_A)->EnableWindow(enable);
	GetDlgItem(IDC_CHECK_START_WORK_A)->EnableWindow(enable);

	// ���������� ��� ���������� ��������� ������� Escape.
	SetFocus();
}

/*
void CMIsocraticPage::SetNewMode(int mode)
{
//	m_Volumes[m_SelectedMode] = m_VolumeComboBox.GetIntValue();
//	m_FlowRates[m_SelectedMode] = m_FlowRateComboBox.GetIntValue();

	m_SelectedMode = mode;
//	m_VolumeComboBox.SetIntValue(m_Volumes[m_SelectedMode]);
//	m_FlowRateComboBox.SetIntValue(m_FlowRates[m_SelectedMode]);
}

void CMIsocraticPage::OnBnClickedRadioFill()
{
	ShowControls(false);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(true);
	SetNewMode(PUMP_MODE_FILL);

//	ShowValuesPumpA();
}

void CMIsocraticPage::OnBnClickedRadioDischarge()
{
	ShowControls(false);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(false);
	SetNewMode(PUMP_MODE_DISCHARGE);
}

void CMIsocraticPage::OnBnClickedRadioWork()
{
	ShowControls(true);
	GetDlgItem(IDC_COMBO_ELUENT_A)->EnableWindow(false);
	SetNewMode(PUMP_MODE_WORK);

	CString text;
	GetDlgItem(IDC_COMBO_ELUENT_A)->GetWindowText(text);
	AfxGetApp()->WriteProfileString(c_RegistrySection, _T("Eluent"), text);
}
*/
#if 0
void CMIsocraticPage::OnBnClickedCheckStartStop()
{
	CString text;
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_STOP);
	
	if(startButton->GetCheck())
	{
/*
		int flowRate = m_FlowRateComboBox.GetIntValue();
		int targetVolume = m_VolumeComboBox.GetIntValue();
		int startPressure = m_StartPressureComboBox.GetIntValue();
		int startFlowRate = m_StartFlowRateComboBox.GetIntValue();

		// ��� ����� ������ ������� ����, ����e - �������.
		CMValveCloseDialog dialog(this, m_SelectedMode == PUMP_MODE_DISCHARGE);
		if(dialog.DoModal() == IDCANCEL)
		{
			startButton->SetCheck(false);
			return;
		}
*/
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		sm_CurrentMode = m_SelectedMode;

		int infoStringID = 0;
		switch(m_SelectedMode)
		{
		case PUMP_MODE_FILL:
			m_FillVolumeComboBox.AddCurrentString();
			StartFillMode();
			infoStringID = IDS_INFO_FILL;
			break;
		case PUMP_MODE_DISCHARGE:
			m_DischargeVolumeComboBox.AddCurrentString();
			StartDischargeMode();
			infoStringID = IDS_INFO_DISCHARGE;
			break;
		case PUMP_MODE_WORK:
			m_FlowRateComboBox.AddCurrentString();
			m_StartPressureComboBox.AddCurrentString();
			m_StartFlowRateComboBox.AddCurrentString();
			StartWorkMode();
			infoStringID = IDS_INFO_WORK;
			break;
		}

		if(sm_IsRunning)
		{
			m_HeaderInfoString.LoadString(infoStringID);
		}
		else
		{
			m_HeaderInfoString.Empty();
		}
	}
	else
	{
		m_PumpA->StopCurrentMode();
		sm_IsRunning = false;

//		SetStartButtonText(IDS_CONTROL_START);
		EnableControls(true);
		m_HeaderInfoString.Empty();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}
#endif

bool CMIsocraticPage::StartFillMode()
{
	// ��� ������ ��������� �������� � ������ ������� ����.
	bool canStart = StartPumpMode(PUMP_MODE_FILL, m_Settings.m_MaxPressureDischarge, m_Settings.m_ReleasingFlowRate);
	bool fillWithoutDischarge = false;
	if(canStart)
	{
		CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
		int targetVolume = c_MaxPumpVolume;
		if(button->GetCheck() != 0)
		{
			int value = m_FillVolumeComboBox.GetIntValue();
			if(value > 0)
			{
				targetVolume = value;
				fillWithoutDischarge = (targetVolume <= 2000);
			}
		}

		// ����� �� ������ �� ������ �������.
		m_PumpA->StartSmartFill(c_FillFlowRate, targetVolume, fillWithoutDischarge);
		sm_CurrentMode = PUMP_MODE_FILL;

		ShowInfoString(IDS_INFO_FILL);

//		SetStartButtonText(IDS_CONTROL_STOP);
	}
	return canStart;
}

bool CMIsocraticPage::StartDischargeMode()
{
	// ��� ����� ��������� �������� � ������ ������� ����.
	bool canStart = StartPumpMode(PUMP_MODE_DISCHARGE, m_Settings.m_MaxPressureDischarge, m_Settings.m_ReleasingFlowRate);
	if(canStart)
	{
		CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_DISCHARGE_A);
		int targetVolume = c_MaxPumpVolume;
		if(button->GetCheck() != 0)
		{
			int value = m_DischargeVolumeComboBox.GetIntValue();
			if(value > 0)
			{
				targetVolume = value;
			}
		}

		m_PumpA->StartDischargeMode(c_DischargeFlowRate, targetVolume);
		sm_CurrentMode = PUMP_MODE_DISCHARGE;

		ShowInfoString(IDS_INFO_DISCHARGE);
//		SetStartButtonText(IDS_CONTROL_STOP);
	}
	return canStart;
}

bool CMIsocraticPage::StartWorkMode()
{
	int flowRate = m_FlowRateComboBox.GetIntValue();
	int startPressure = int(m_StartPressureComboBox.GetFloatValue()*10.0+0.5);
	int startFlowRate = m_StartFlowRateComboBox.GetIntValue();
	// ��� ������ ������ ������� ����.
	//CMValveCloseDialog dialog(this, false);
	//if(dialog.DoModal() != IDCANCEL)
	{
#if 1
		m_PumpA->StartWorkMode(flowRate, startPressure, startFlowRate);
		sm_CurrentMode = PUMP_MODE_WORK;
#else
		// ������ ��� �����.
		m_PumpA->SetGradientStage(5, 1, flowRate, flowRate);
		m_PumpA->StartGradientStage(5);
#endif

//		SetStartButtonText(IDS_CONTROL_STOP);
		ShowInfoString(IDS_INFO_WORK);
		return true;
	}
	return false;
}

void CMIsocraticPage::StopCurrentMode()
{
	m_PumpA->StopCurrentMode();
//	sm_IsRunning = false;
	sm_CurrentMode = PUMP_MODE_WAIT;
	ShowInfoString(0);

	EnableControls(true);
	EnableOtherPages(true);

	// ������� �������� ����� ���� ����������, ������� ��������� ������ ����������.
	((CButton*)GetDlgItem(IDC_CHECK_START_FILL_A))->SetCheck(false);
	((CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A))->SetCheck(false);
	((CButton*)GetDlgItem(IDC_CHECK_START_WORK_A))->SetCheck(false);

	m_HeaderInfoString.Empty();
}

void CMIsocraticPage::StartPressureReleasing(int flowRate)
{
	m_PumpA->StartPressureReleasingMode(flowRate);
}

void CMIsocraticPage::OnBnClickedButtonSettings()
{
}

void CMIsocraticPage::SaveToRegistry()
{
	CWinApp* app = AfxGetApp();
	CString string;

	m_FillVolumeComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("FillVolume"), string);

	m_DischargeVolumeComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("DischargeVolume"), string);

	m_FlowRateComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("FlowRate"), string);

	m_StartPressureComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("StartPressure"), string);

	m_StartFlowRateComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("StartFlowRate"), string);

	m_EluentsComboBox.SaveToString(string);
	app->WriteProfileString(c_RegistrySection, _T("Eluents"), string);
}

void CMIsocraticPage::SaveEluentInfo()
{
	// ���������� �� ������� � ���������� ������.
	if(!m_EluentASerialNumberKey.IsEmpty())
	{
		CString string;
		m_EluentsComboBox.GetWindowText(string);
		AfxGetApp()->WriteProfileString(_T("Settings"), m_EluentASerialNumberKey, string);
	}
}

void CMIsocraticPage::ShowInfoString(int stringID)
{
	CString text;
	if(stringID != 0)
	{
		text.LoadString(stringID);
	}
	CWnd* info = GetDlgItem(IDC_EDIT_INFO);
	if(info != NULL)
	{
		info->SetWindowText(text);
	}
}

void CMIsocraticPage::LoadFromRegistry()
{
	CWinApp* app = AfxGetApp();
	CString string;

	string = app->GetProfileString(c_RegistrySection, _T("FillVolume"), _T(""));
	m_FillVolumeComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("DischargeVolume"), _T(""));
	m_DischargeVolumeComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("FlowRate"), _T("1000;"));
	m_FlowRateComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("StartPressure"), _T("2.0;"));
	m_StartPressureComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("StartFlowRate"), _T("1000;"));
	m_StartFlowRateComboBox.GetFromString(string);

	string = app->GetProfileString(c_RegistrySection, _T("Eluents"), _T(""));
	m_EluentsComboBox.GetFromString(string);
}


void CMIsocraticPage::OnBnClickedCheckStartFill()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_FILL_A);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		//m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);

		m_FillVolumeComboBox.AddCurrentString();

		if(StartFillMode())
		{
			m_HeaderInfoString.LoadString(IDS_INFO_FILL);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);

			m_EluentsComboBox.AddCurrentString();
			SaveEluentInfo();
		}
		else
		{
			startButton->SetCheck(false);
		}
	}
	else
	{
		StopCurrentMode();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

void CMIsocraticPage::OnBnClickedCheckStartDischarge()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_DISCHARGE_A);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);


		m_DischargeVolumeComboBox.AddCurrentString();

		if(StartDischargeMode())
		{
			m_HeaderInfoString.LoadString(IDS_INFO_DISCHARGE);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);
		}
		else
		{
			startButton->SetCheck(false);
		}
	}
	else
	{
		StopCurrentMode();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

void CMIsocraticPage::OnBnClickedCheckStartWork()
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_WORK_A);
	
	if(startButton->GetCheck())
	{
		m_PumpA->SetPressureLimits(m_Settings.m_MinPressureWork, m_Settings.m_MaxPressureWork, m_Settings.m_MaxPressureDischarge);
		m_PumpA->SetPressureReleasing(m_Settings.m_AutomaticPressureReleasing, m_Settings.m_ReleasingFlowRate);


		m_FlowRateComboBox.AddCurrentString();
		m_StartPressureComboBox.AddCurrentString();
		m_StartFlowRateComboBox.AddCurrentString();

		if(StartWorkMode())
		{
			m_HeaderInfoString.LoadString(IDS_INFO_WORK);
			// ����������� �������� ����������, � ��� ����� ������ �������.
			EnableControls(false);
			// ��������� ������ ��� �������� ���������� ������.
			startButton->EnableWindow(true);
			// ��������� �������� ���������� �� ��������� ���������.
			EnableOtherPages(false);

			m_EluentsComboBox.AddCurrentString();
			SaveEluentInfo();
		}
		else
		{
			startButton->SetCheck(false);
		}
	}
	else
	{
		StopCurrentMode();
	}

	// ���������� ��� ����������� ��������� � ����������� � ������.
	UpdatePageHeader();
}

void CMIsocraticPage::OnBnClickedCheckVolumeFill()
{
	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_FILL_A);
	m_FillVolumeComboBox.EnableWindow(button->GetCheck());
}

void CMIsocraticPage::OnBnClickedCheckVolumeDischarge()
{
	CButton* button =  (CButton*)GetDlgItem(IDC_CHECK_VOLUME_DISCHARGE_A);
	m_DischargeVolumeComboBox.EnableWindow(button->GetCheck());
}


void CMIsocraticPage::OnEnChangeEditVolumeA()
{
	// TODO:  ���� ��� ������� ���������� RICHEDIT, �� ������� ���������� �� �����
	// send this notification unless you override the CMToolPage::OnInitDialog()
	// ������� � ����� CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  �������� ��� �������� ����������
}
