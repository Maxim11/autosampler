// SelectInstrumentTypeDialog.cpp : implementation file
//

#include "stdafx.h"
//#include "stend.h"
#include "Resource.h"
#include "SelectPumpConnectionDialog.h"
#include "Instrument\UsbDevices.h"

//#include "Dialogs\DialogsResource.h"
#include "InstrumentBase\DChannel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//const int c_InstrumentWithUsb = 4;
const int c_UsbProductId = c_UsbProductId_Piton;

/////////////////////////////////////////////////////////////////////////////
// CMSelectPumpConnectionDialog dialog


CMSelectPumpConnectionDialog::CMSelectPumpConnectionDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMSelectPumpConnectionDialog::IDD, pParent)
	, m_IsGradientOn(FALSE)
{
	//{{AFX_DATA_INIT(CMSelectPumpConnectionDialog)
	m_InstrumentIndex = -1;
	//m_ConnectionPort = -1;
	m_UsbIndex = -1;
	//}}AFX_DATA_INIT

	//m_PortNames = 0;
	//m_IsPortSelected = false;
	//m_PortSelectionOnly = true;
}


void CMSelectPumpConnectionDialog::SetPortNames(CString* names)
{
	//m_PortNames = names;
}

void CMSelectPumpConnectionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMSelectPumpConnectionDialog)
	//DDX_Radio(pDX, IDC_RADIO_PITON_3, m_InstrumentIndex);
	//DDX_CBString(pDX, IDC_COMBO_SELECT_COM_PORT, m_ConnectionPort);
	DDX_CBString(pDX, IDC_COMBO_SELECT_INSTRUMENT, m_UsbDevice);
	//DDX_CBIndex(pDX, IDC_COMBO_SELECT_INSTRUMENT, m_UsbIndex);
	//DDX_CBString(pDX, IDC_COMBO_SELECT_COM_PORT_B, m_ConnectionPortB);
	DDX_CBString(pDX, IDC_COMBO_SELECT_INSTRUMENT_B, m_UsbDeviceB);
	//DDX_CBIndex(pDX, IDC_COMBO_SELECT_INSTRUMENT_B, m_UsbIndexB);
	//DDX_Check(pDX, IDC_CHECK_GRADIENT_ON, m_IsGradientOn);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMSelectPumpConnectionDialog, CDialog)
	//{{AFX_MSG_MAP(CMSelectPumpConnectionDialog)
	//ON_BN_CLICKED(IDC_RADIO_PITON_3, OnRadioPiton3)
	//ON_BN_CLICKED(IDC_RADIO_PITON_4, OnRadioPiton4)
	//ON_CBN_SELCHANGE(IDC_COMBO_SELECT_COM_PORT, OnSelectComPort)
	//ON_BN_CLICKED(IDC_CHECK_GRADIENT_ON, OnBnClickedCheckGradientOn)
	//ON_CBN_SELCHANGE(IDC_COMBO_SELECT_COM_PORT_B, OnSelectComPortB)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMSelectPumpConnectionDialog message handlers
#if 0
void CMSelectPumpConnectionDialog::OnRadioPiton3() 
{
	DoOnSelectDetector(0);
}

void CMSelectPumpConnectionDialog::OnRadioPiton4() 
{
	DoOnSelectDetector(1);
}

// �������� ��� ������ ���������
void CMSelectPumpConnectionDialog::DoOnSelectDetector(int index)
{
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT);

	m_InstrumentIndex = index;

// ���� ���� ��� �� ���������, �������� ����������� � �������
	if(!m_IsPortSelected)
	{
		comboBox->SelectString(-1, m_PortNames[index]);
	}

	GetDlgItem(IDOK)->EnableWindow(true);

	bool isUsbSelected = (comboBox->GetCurSel() == 0);
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT);
	comboBox->EnableWindow(isUsbSelected);
	if(isUsbSelected)
	{
		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT);
		if(!m_UsbDevice.IsEmpty())
		{
			comboBox->SelectString(-1, m_UsbDevice);
		}
		else
		{
			comboBox->SetCurSel(0);
		}
	}
	else
	{
		comboBox->SetCurSel(-1);
	}
}

void CMSelectPumpConnectionDialog::OnSelectComPort() 
{
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT);
	bool isUsbSelected = (comboBox->GetCurSel() == 0);
	m_IsPortSelected = true;

	// ���� �� ������� USB, ������ �������� ������
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT);
	comboBox->EnableWindow(isUsbSelected);
	if(isUsbSelected)
	{
		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT);
		if(!m_UsbDevice.IsEmpty())
		{
			comboBox->SelectString(-1, m_UsbDevice);
		}
		else
		{
			comboBox->SetCurSel(0);
		}
	}
	else
	{
		comboBox->SetCurSel(-1);
	}
}
#endif

BOOL CMSelectPumpConnectionDialog::OnInitDialog() 
{
	const int cMaxPort = 32;
	const CString c_NamePrefix = _T("\\\\.\\");
	CString name;
	CComboBox* comboBox;
/*
// ������� ������ ��������� ������
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT);

	for(int i = 1; i <= cMaxPort; i++)
	{
		name.Format(_T("COM%d"), i);
		HANDLE handle = ::CreateFile( c_NamePrefix+name, GENERIC_READ | GENERIC_WRITE,
				0,								// exclusive access
				NULL,							// no security attrs
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
				NULL );							// no template file

		if (handle == INVALID_HANDLE_VALUE)		// �� ������� ������� COM ����
			continue;

		CloseHandle(handle);
		comboBox->AddString(name);
	}
	if(comboBox->GetCount() == 0)
		m_ConnectionPort.Empty();
	

	comboBox->InsertString(0, CString(_T("USB")));

	if(m_ConnectionPort.IsEmpty() || !comboBox->FindString(-1, m_ConnectionPort) )
	{
		comboBox->SetCurSel(0);
	}
*/	
	/*
	// ���� �� ������� USB, ������ �������� ������
	if(comboBox->GetCurSel() != 0)
	{
		GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT)->EnableWindow(false);
	}
	else
*/	
	{
		comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT);
		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT);
		if(!m_UsbDevice.IsEmpty())
		{
			comboBox->SelectString(-1, m_UsbDevice);
		}
		else
		{
			comboBox->SetCurSel(0);
		}
	}

	if(m_NumberOfUsbDevices > 1)
	{
		CWnd* staticText = GetDlgItem(IDC_STATIC_PUMP_A);
		CString text;
		staticText->GetWindowText(text);
		text += _T(" A");
		staticText->SetWindowText(text);

		comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT_B);
		comboBox->EnableWindow(true);

		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT_B);
		comboBox->InsertString(0, CString(_T("")));
		comboBox->SetCurSel(0);
			
		if(!m_UsbDeviceB.IsEmpty())
		{
			comboBox->SelectString(-1, m_UsbDeviceB);
		}
	}
/*
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT_B);
	comboBox->AddString(CString(_T("USB")));
	comboBox->AddString(CString(_T("CAN")));
	comboBox->SetCurSel(-1);

// ���� ��� ��������� �� ������, ������ �������� �� ������
	if(m_InstrumentIndex < 0)
	{
		GetDlgItem(IDOK)->EnableWindow(false);
		GetDlgItem(IDCANCEL)->EnableWindow(false);
	}

// ��������� ��������� ���� ���������
	if(m_PortSelectionOnly)
	{
		GetDlgItem(IDC_RADIO_PITON_3)->EnableWindow(false);
		GetDlgItem(IDC_RADIO_PITON_4)->EnableWindow(false);
//		GetDlgItem()->EnableWindow(false);
	}
*/
	BOOL result = CDialog::OnInitDialog();

	return result;
}

// ���������� ������ ������������ �� USB ���������
void CMSelectPumpConnectionDialog::FillUsbDeviceList(int controlD)
{
	//IDC_COMBO_SELECT_INSTRUMENT
	CComboBox* instrumentComboBox = (CComboBox*)GetDlgItem(controlD);
	CGDataChannel dataChannel;

	dataChannel.Create("USB", DCT_USB_HID, 100, 0);
// ������������� ������� ���� � �������� ��������� ���������
// � ����������� ���������
	//dataChannel.SetDeviceNotificationWindow(GetSafeHwnd());

	instrumentComboBox->ResetContent();

// ��������� ���������� ������������ ���������
	m_NumberOfUsbDevices = dataChannel.EnumerateDevices(c_UsbVendorId_Lumex, c_UsbProductId);
	for(int i = 0; i < m_NumberOfUsbDevices; i++)
	{
		const int c_bufferLength = 256;
//		wchar_t serialNumber[c_bufferLength];
		wchar_t productName[c_bufferLength];
		CString string;
//		int beginSN;

		dataChannel.GetNextDeviceName((i == 0), productName, c_bufferLength);
		instrumentComboBox->AddString(productName);
		/*
		dataChannel.GetNextSerialNumber((i == 0), serialNumber, c_bufferLength);
		dataChannel.GetNextProductString(productName, c_bufferLength);
		string = CString(productName)+_T(" N");
		beginSN = string.GetLength();
		string += CString(serialNumber);

		int index = instrumentComboBox->AddString(string);
		// ���������� ������ ������ ����������.
		instrumentComboBox->SetItemData(index, beginSN);
		*/
	}
#if 0
	{
		int index = instrumentComboBox->AddString(CString(_T("Piton3 N1234")));
		instrumentComboBox->SetItemData(index, 8);
	}
#endif
	//instrumentComboBox->SetCurSel(
	//	(m_UsbIndex >= 0 && m_NumberOfUsbDevices >= m_UsbIndex) ? m_UsbIndex : 0);
}

#if 0
void CMSelectPumpConnectionDialog::OnBnClickedCheckGradientOn()
{
	bool isChecked = ((CButton*)GetDlgItem(IDC_CHECK_GRADIENT_ON))->GetCheck() != 0;
	GetDlgItem(IDC_COMBO_SELECT_COM_PORT_B)->EnableWindow(isChecked);
	GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT_B)->EnableWindow(isChecked);

	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT_B);
	int selected = comboBox->GetCurSel();

	// ������� USB.
	if(isChecked && comboBox->GetCurSel() == 0)
	{
		//comboBox->SetCurSel(0);
		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT_B);
		
		comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT_B);
		if(comboBox->GetCount() > 1)
		{
			if(!m_UsbDeviceB.IsEmpty())
			{
				comboBox->SelectString(-1, m_UsbDeviceB);
			}
			else
			{
				comboBox->SetCurSel(1);
			}
		}
	}
}

void CMSelectPumpConnectionDialog::OnSelectComPortB()
{
	CComboBox* comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_COM_PORT_B);
	bool isUsbSelected = (comboBox->GetCurSel() == 0);

	// ���� �� ������� USB, ������ �������� ������
	comboBox = (CComboBox*)GetDlgItem(IDC_COMBO_SELECT_INSTRUMENT_B);
	comboBox->EnableWindow(isUsbSelected);
	if(isUsbSelected)
	{
		FillUsbDeviceList(IDC_COMBO_SELECT_INSTRUMENT_B);
		if(!m_UsbDeviceB.IsEmpty())
		{
			comboBox->SelectString(-1, m_UsbDeviceB);
		}
		else
		{
			comboBox->SetCurSel(1);
		}
	}
	else
	{
		comboBox->SetCurSel(-1);
	}
}
#endif
