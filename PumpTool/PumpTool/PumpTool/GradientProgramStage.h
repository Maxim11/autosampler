// GradientProgramStage.h: interface for the CGGradientProgramStage class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "afxtempl.h"

//class CGProgramData;

// ������ �� ����� ����� ��������� ������������������ ���������
class CGGradientProgramStage
{
friend class CGProgramData;
friend class CGGradientProgram;
//friend class CMChromatographicProgramPanel;
//friend class CGChromatographicProgram;

public:
	CGGradientProgramStage() { };		// ���������� ��� ������ ������

	void SetDataFromString(CString string);

	int GetDuration() { return m_Duration; }

private:
	CGGradientProgramStage(double ft, double ewl, double rwl, int pmts);

private:
	int m_Duration;
	int m_PumpA_Begin;
	int m_PumpA_End;

// !!! ��� ���������� ����� ������ ������� ������� CGChromatographicProgram::AddStageToList() !!!

	// ��������������� �������� ��� ��������� �������� ���������.
	int m_FinishTime;
	int m_Number;		// ���������� ����� ����� (������� � ����)
};

// ������ � ���� ������ ��������� ������������������ ���������
class CGGradientProgram
{
	friend class CGPiton;

public:
	CGGradientProgram();
	virtual ~CGGradientProgram();

	bool GetFromProgramData(CGProgramData* programData, bool timeInMinutes);

	//CGGradientProgramStage& GetNextStage();
	void Start(CGPiton* pumpA, CGPiton* pumpB, bool fromConditioning = false);
	void Restart();
	void Stop();
	void StartAcceleration();
	void StartConditioning();
	bool StartNextStage();
	void StartFirstWorkStage();
	void EnableRestartAfterStop(bool enable) { m_RestartAfterStop = enable; }

	// �������� ������������� ����� �����.
	bool Check(int stageTime);
	bool IsRunning();
	bool IsStartStageActive();
	bool IsWaitingStageActive();

	int GetStageIndex() { return m_StageIndex; }
	int GetStageDuration();

protected:
	//void LoadPreliminaryStage(int n, int flowRateA, int flowRateB);
	//void StartNextStage();
	//void StartLoadedStage(int n);

protected:
	CList<CGGradientProgramStage, CGGradientProgramStage&> m_StagesList;
	POSITION m_Position;

// ������������� ��������� ���������, ����� ��� ���� ������
	int m_StartPressure;
	int m_StartFlowRate;
	int m_WorkFlowRate;
	int m_ConditioningTime;
	CString m_EluentA;
	CString m_EluentB;

	// ����� ������������ �����.
	int m_StageIndex;
	int m_CurrentStageDuration;

	bool m_RestartAfterStop;

	CGPiton* m_PumpA;
	CGPiton* m_PumpB;
};

enum
{
	c_StageSetValve     = -4,
	c_StageStart        = -3,
	c_StageConditioning = -2,
	c_StageWait         = -1,
	// ��� �� ���������� ���������.
	c_StageNone         = 0,
	// ����� ����� ��������� ����� ������������� ������.
};

/*
class CGGradientProgramStageList : 
	public CList<CGGradientProgramStage, CGGradientProgramStage&>  
{
public:
	CGGradientProgramStageList();
	virtual ~CGGradientProgramStageList();

	bool GetFromProgramData(CGProgramData* programData, bool timeInMinutes);

protected:
// ������������� ��������� ���������, ����� ��� ���� ������
	int m_StartPressure;
	int m_StartFlowRate;
	int m_WorkFlowRate;
	int m_ConditioningTime;
};
*/
