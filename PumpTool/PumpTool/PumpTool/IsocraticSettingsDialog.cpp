// IsocraticSettingsDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "IsocraticSettingsDialog.h"


// ���������� ���� CMIsocraticSettingsDialog

IMPLEMENT_DYNAMIC(CMIsocraticSettingsDialog, CDialog)

CMIsocraticSettingsDialog::CMIsocraticSettingsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMIsocraticSettingsDialog::IDD, pParent)
	, m_MaxPressureWork(_T(""))
	, m_MinPressureWork(_T(""))
	, m_MaxPressureDischarge(_T(""))
	, m_AutomaticPressureReleasing(FALSE)
	, m_ReleasingFlowRate(_T(""))
{

}

CMIsocraticSettingsDialog::~CMIsocraticSettingsDialog()
{
}

void CMIsocraticSettingsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MAX_PRESSURE_WORK, m_MaxPressureWork);
	DDX_Text(pDX, IDC_EDIT_MIN_PRESSURE_WORK, m_MinPressureWork);
	DDX_Text(pDX, IDC_EDIT_MAX_PRESSURE_DISCHARGE, m_MaxPressureDischarge);
	DDX_Check(pDX, IDC_CHECK_AUTOMATIC_RELEASING, m_AutomaticPressureReleasing);
	DDX_Text(pDX, IDC_EDIT_FLOW_RATE, m_ReleasingFlowRate);
}


BEGIN_MESSAGE_MAP(CMIsocraticSettingsDialog, CDialog)
END_MESSAGE_MAP()


// ����������� ��������� CMIsocraticSettingsDialog
