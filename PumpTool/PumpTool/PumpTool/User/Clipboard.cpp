// Clipboard.cpp: implementation of the CGClipboard class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Clipboard.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGClipboard::CGClipboard()
{
	m_DataSource = new COleDataSource;
	m_DataCached = false;
}

CGClipboard::~CGClipboard()
{
	if(m_DataCached)
	{
		try
		{
			m_DataSource->SetClipboard();
		}
		catch(...)
//		catch(SCODE sc)
		{
//			CString s = AfxGetScodeString(sc);
			delete m_DataSource;
		}
		return;
	}
}

// �������� � ����� ������ ���������� � �������� ����������
void CGClipboard::CopyLocaleInfo()
{
// �������� ���������� ���� ������
	HGLOBAL localeBlock = ::GlobalAlloc(GMEM_MOVEABLE, 4);
	if(localeBlock == 0)
		return;

	DWORD* localeData = (DWORD*)::GlobalLock(localeBlock);
	if(localeData == 0)
		return;

// �������� � ���� ����������� ������
	*localeData = GetUserDefaultLCID();

	::GlobalUnlock(localeBlock);
// ����������� ���� �� �������!

// ������� ������ � ����� ������, ��������� �������� Ole
	try
	{
		m_DataSource->CacheGlobalData(CF_LOCALE, localeBlock);
		m_DataCached = true;
	}
	catch(...)
	{
	}
}

// �������� � ����� ������ �����
void CGClipboard::CopyText(const CStringA& text)
{
	int length = text.GetLength()+1;
// �������� ���������� ���� ������
	HGLOBAL textBlock = ::GlobalAlloc(GMEM_MOVEABLE, length);
	if(textBlock == 0)
		return;

	char* textData = (char*)::GlobalLock(textBlock);
	if(textData == 0)
		return;

// �������� � ���� ����������� ������
	//strcpy(textData, text);
	strcpy_s(textData, length, text);

	::GlobalUnlock(textBlock);
// ����������� ���� �� �������!

// ������� ������ � ����� ������, ��������� �������� Ole
	try
	{
		m_DataSource->CacheGlobalData(CF_TEXT, textBlock);
		m_DataCached = true;
	}
	catch(...)
	{
	}
}

// �������� � ����� ������ �����
void CGClipboard::CopyText(const CString& text)
{
	int length = text.GetLength()+1;
// �������� ���������� ���� ������ (*2 - ��� Unicode)
	HGLOBAL textBlock = ::GlobalAlloc(GMEM_MOVEABLE, length*2);
	if(textBlock == 0)
		return;

	TCHAR* textData = (TCHAR*)::GlobalLock(textBlock);
	if(textData == 0)
		return;

// �������� � ���� ����������� ������
	//strcpy(textData, text);
	_tcscpy_s(textData, length, text);

	::GlobalUnlock(textBlock);
// ����������� ���� �� �������!

// ������� ������ � ����� ������, ��������� �������� Ole
	try
	{
		m_DataSource->CacheGlobalData(CF_UNICODETEXT, textBlock);
		m_DataCached = true;
	}
	catch(...)
	{
	}
}

void CGClipboard::CopyRtfText(const CStringA& text)
{
	int length = text.GetLength()+1;
// �������� ���������� ���� ������ (*2 - ��� Unicode)
	HGLOBAL textBlock = ::GlobalAlloc(GMEM_MOVEABLE, length);
	if(textBlock == 0)
		return;

	char* textData = (char*)::GlobalLock(textBlock);
	if(textData == 0)
		return;

// �������� � ���� ����������� ������
	//strcpy(textData, text);
	strcpy_s(textData, length, text);

	::GlobalUnlock(textBlock);
// ����������� ���� �� �������!

	CLIPFORMAT format = (CLIPFORMAT)::RegisterClipboardFormat(CF_RTF);

// ������� ������ � ����� ������, ��������� �������� Ole
	try
	{
		m_DataSource->CacheGlobalData(format, textBlock);

		m_DataCached = true;
	}
	catch(...)
	{
	}
}

void CGClipboard::CopyMetafile(HMETAFILEPICT hMetaFilePict)
{
/*
//copy to clipboard
	AfxGetMainWnd()->OpenClipboard();
	EmptyClipboard();
	::SetClipboardData(CF_ENHMETAFILE, hMetaFile);
	CloseClipboard();
*/
	STGMEDIUM data;
	data.tymed = TYMED_MFPICT;
	data.hMetaFilePict = hMetaFilePict;
	data.pUnkForRelease = 0;
	try
	{
		m_DataSource->CacheData(CF_METAFILEPICT, &data);
		m_DataCached = true;
	}
	catch(...)
	{
	}
}

void CGClipboard::CopyEnhMetafile(HENHMETAFILE hMetaFile)
{
#if 0

	AfxGetMainWnd()->OpenClipboard();
	EmptyClipboard();
	::SetClipboardData(CF_ENHMETAFILE, hMetaFile);
	CloseClipboard();

#else

	STGMEDIUM data;
	data.tymed = TYMED_ENHMF;
	data.hEnhMetaFile = hMetaFile;
	data.pUnkForRelease = 0;		// ���������� ��� ����������� ������������ ������� hMetaFile

	try
	{
		m_DataSource->CacheData(CF_ENHMETAFILE, &data);
		m_DataCached = true;
	}
	catch(...)
	{
	}
#endif
}

