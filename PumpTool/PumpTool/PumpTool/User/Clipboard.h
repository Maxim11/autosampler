// Clipboard.h: interface for the CGClipboard class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIPBOARD_H__D0B54BBA_BD38_4C89_A801_7888A0398A80__INCLUDED_)
#define AFX_CLIPBOARD_H__D0B54BBA_BD38_4C89_A801_7888A0398A80__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxole.h>

class CGClipboard  
{
public:
	CGClipboard();
	virtual ~CGClipboard();

	void CopyLocaleInfo();
	void CopyText(const CStringA& text);
	void CopyText(const CString& text);
	void CopyRtfText(const CStringA& text);
	void CopyMetafile(HMETAFILEPICT hMetaFilePict);
	void CopyEnhMetafile(HENHMETAFILE hMetaFile);

protected:
	COleDataSource* m_DataSource;

	bool m_DataCached;
};

#endif // !defined(AFX_CLIPBOARD_H__D0B54BBA_BD38_4C89_A801_7888A0398A80__INCLUDED_)
