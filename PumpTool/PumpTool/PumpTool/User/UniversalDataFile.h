// UniversalDataFile.h: interface for the CGUniversalDataFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UNIVERSALDATAFILE_H__8559CDDB_3F6C_45E7_9F04_0282268E348C__INCLUDED_)
#define AFX_UNIVERSALDATAFILE_H__8559CDDB_3F6C_45E7_9F04_0282268E348C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"

class CGSectionInfo
{
friend class CGIntervalInfo;
friend class CGArrayInfo;
friend class CGUniversalDataFile;

public:
	CGSectionInfo() { };
	CGSectionInfo(const CString& name);
	virtual ~CGSectionInfo() { };

	CString GetName() { return m_Name; }
	int GetCount() { return m_Strings.GetCount(); }
	int IsEmpty() { return m_Strings.IsEmpty(); }
    void Clear() { m_Strings.RemoveAll(); }
	void AddString(const CString& key, const CString& value);

// ����� ��������, ���������������� ��������� �����
	bool Lookup(const CString& key, CString& value);
// ����� � ������ ��������, ���������������� ��������� �����
	bool Replace(const CString& key, CString& value);
// ������ ��� ���������� ��������, ���������������� ��������� �����
	void SetString(const CString& key, CString& value);
// ����� � �������� ��������, ���������������� ��������� �����
	bool Remove(const CString& key);
// ����� ������ ������, ���������������� ��������� �����
	int  GetIndex(const CString& key);

// �������� � ������ ����� ��� ������������ �������
	void CopyToStringArray(CStringArray& stringArray);
// ���������� ������ �� ������
	CString GetAt(int index);
// ������� ������ �� ������
	void RemoveAt(int index);

// �������� ������ ������ ������ ��� ���������
	void CopyDataFrom(const CGSectionInfo& info);
// �������� ������ �������
	void operator =(const CGSectionInfo& info);

protected:
	void Write(CArchive& archive);

	void AddString(const CString& string);
// ��������� � ������������� ������
//	void AddSorted(CString& string);

protected:
	CString m_Name;
	CStringList m_Strings;
};

class CGIntervalInfo
{
friend class CGUniversalDataFile;

public:
	CGIntervalInfo() { };
	virtual ~CGIntervalInfo() { };

	double GetFrom() { return m_From; }
	double GetTo()   { return m_To; }
	double GetStep() { return m_Step; }

protected:
// ������ ������ �������� ���������
	void Write(CArchive& archive); //, int number);

// ������ ������ �������� ���������
	bool Parse(CGSectionInfo& sectionInfo);

protected:
	CString m_Name;
	double m_From;
	double m_To;
	double m_Step;
	CString m_Units;

protected:
	bool m_IsValid;
};

// ��������������� �����
class CGByteArray : public CArray<BYTE, BYTE>
{
public:
	CGByteArray(int size = 1024) { SetSize(0, size); }
	virtual ~CGByteArray() {};

	void AddBlock(void* block, const int blockSize);
	void AddBlock(CString string);
};


// ���������� � ������� ����� ���� ��������� ������������� ��������
// ������������ ���� ����� ������ ������ CGSectionInfo
class CGArrayInfo : public CGSectionInfo
{
friend class CGUniversalDataFile;

public:
	CGArrayInfo();
	virtual ~CGArrayInfo();

	int GetSize() { return m_SizeOfArray; }
	int GetType() { return m_TypeOfElement; }
	CString& GetName() { return m_Name; }
	CString& GetNameForX() { return m_NameForX; }
// ���������� ��������� �� ������ �������.
	BYTE* GetData();

	bool IsProcessed() { return m_IsProcessed; }
	void SetProcessed() { m_IsProcessed = true; }

	void operator =(CGArrayInfo& array);

// ������� ��� ������ � ��������, �������� ������ ������� ���������� �����
	void* GetNextBlock(int* blockSize);

protected:
// ������ ������ �������� �������
	void Write(CArchive& archive); //, int number);
// ������ �������� ������ �������
	void WriteData(CArchive& archive);
// ������ �������� ������ �������
	bool ReadData(CArchive& archive);

// ������ ������ �������� �������
	bool Parse(CGSectionInfo& sectionInfo);

// ����� ������� � ������
	int GetLength();

protected:
	CString m_Name;
	int m_TypeOfElement;
	int m_SizeOfArray;
	CString m_Units;
	CString m_NameForX;

// ��������� �� ������, ��������������� �������
	BYTE* m_ArrayData;
// ��������� �� ��������� ���� � ������ ������� ���������� �����
	BYTE* m_BlockPointer;

protected:
// ������ ������� ����������� �������
	bool m_IsValid;
// ����, ����������� ��� ������ ������ ��� ��������������� ��� ������
	bool m_IsProcessed;
};

enum
{
	c_Int8Type,
	c_Int16Type,
	c_Int32Type,
	c_Real32Type,
	c_Real64Type,
	c_ByteType,
};

class CGUniversalDataFile
{
public:
	CGUniversalDataFile();
	virtual ~CGUniversalDataFile();

	int  Read(CArchive& archive, bool firstSectionOnly = false);
	void Write(CArchive& archive);

	int  Read(const CString& fileName, bool firstSectionOnly = false, bool reportError = true);
	bool Write(const CString& fileName, bool reportError = true);

	bool IsAscii() { return m_IsAscii; }

public:
	void AddSectionInfo(CGSectionInfo& sectionInfo);
	void AddSectionInfo(CGSectionInfo& sectionInfo, const CString& newName);
	void AddIntervalInfo(CString name, double from, double to, double step, CString units = _T(""));

// ��������� ���������� � �������. ������������ ��������� �� ������� ������ �������.
	BYTE* AddArrayInfo(const CString& name, int type, int size,
		CString dataForX = _T(""), CString units = _T(""),
		bool headerOnly = false);
	void AddArrayInfo(const CString& name, const CGByteArray& buffer);

// ��������� �������� �������������� ������ � ������ �������� ������ ��� ������������ �������
	CGArrayInfo& GetLastArrayInfo();

// ����� ���������� �� �����
	CGSectionInfo* FindSectionInfo(CString name);
	CGIntervalInfo* FindIntervalInfo(CString name);
	CGArrayInfo* FindArrayInfo(CString name);

// �������� �� ������ ��������
	POSITION GetFirstArrayInfoPosition() const;
	CGArrayInfo& GetNextArrayInfo(POSITION& position);
	int GetNumberOfArrays();

// �������� �� ������ ������
	POSITION GetFirstSectionPosition() const;
	CGSectionInfo& GetSectionInfo(POSITION& position);

protected:
// ����� ������ � ���������� ������� � ������� ���������
	CList<CGSectionInfo, CGSectionInfo&> m_SectionList;
// ������ �������� ��������
	CList<CGIntervalInfo, CGIntervalInfo&> m_IntervalList;
// ������ �������� ��������
	CList<CGArrayInfo, CGArrayInfo&> m_ArrayList;

// ��������� ��� �������� ������, ��������� � ���������
//	bool m_IsComplete;

protected:
	void CalculateCRCValue();
	void CalculateCRCValue(BYTE* data, int length);
	void CalculateCRCTable();

protected:
// ���� ������������ � �������� ���������� (���� �������� ������ �� �������)
//	bool m_IsTemporary;
//
	bool m_IsAscii;

// ���������� �� ����������� ����� (�� ����� ��� ��������� ������)
	bool m_UseCRC;
// �������� ����������� ����� ����� �� CRC32
	unsigned int m_CRCValue;
// ������� ��� ���������� ����������� ����� ����� �� CRC32
	static unsigned int sm_CRCTable[256];
};

const CString c_SectionInterval = _T("Interval");
const CString c_SectionArray    = _T("Array");
const CString c_BeginMark       = _T("[Begin]");
const CString c_NewLine         = _T("\n");

// ���� �������� ������ Read()
enum
{
	c_NoError,
	c_OpenError,
	c_FormatError,
	c_ParseError,
	c_IncompleteError,
	c_CRCError
};

#endif // !defined(AFX_UNIVERSALDATAFILE_H__8559CDDB_3F6C_45E7_9F04_0282268E348C__INCLUDED_)
