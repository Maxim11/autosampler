// UniversalDataFile.cpp: implementation of the CGUniversalDataFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UniversalDataFile.h"

#include "Service.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////

CGSectionInfo::CGSectionInfo(const CString& name)
{
	m_Name = name;
}

// ������ ������ � ����
void CGSectionInfo::Write(CArchive& archive)
{
	archive.WriteString(_T("[")+m_Name+_T("]")+c_NewLine);
	POSITION pos = m_Strings.GetHeadPosition();
	while(pos)
	{
		archive.WriteString(m_Strings.GetNext(pos)+c_NewLine);
	}

	archive.WriteString(c_NewLine);
}

// ��������� ������ � ����� ������
void CGSectionInfo::AddString(const CString& key, const CString& value)
{
	CString string = key+'='+value;
	m_Strings.AddTail(string);
}

void CGSectionInfo::AddString(const CString& string)
{
	m_Strings.AddTail(string);
}

/*
// ��������� ������ � ������������� ������
void CGSectionInfo::AddSorted(CString& string)
{
	POSITION pos = m_Strings.GetTailPosition();
	while(pos)
	{
		int res = m_Strings.GetAt(pos).CollateNoCase(string);
		if(res < 0)
		{
			m_Strings.InsertAfter(pos, string);
			return;
		}
		if(res == 0)
			return;

		m_Strings.GetPrev(pos);
	}

	m_Strings.AddHead(string);
}
*/

// ����� ��������, ���������������� ��������� �����	
// ��������� ����� ����, ������ CMap �� ������������
bool CGSectionInfo::Lookup(const CString& key, CString& value)
{
	POSITION pos = m_Strings.GetHeadPosition();
	int end;
	while(pos)
	{
		CString& string = m_Strings.GetNext(pos);
		end = string.Find('=');
		if(end > 0 && string.Left(end) == key)
		{
			value = string.Mid(end+1);
			return true;
		}
	}
	return false;
}

// ����� � ������ ��������, ���������������� ��������� �����
bool CGSectionInfo::Replace(const CString& key, CString& value)
{
	POSITION pos = m_Strings.GetHeadPosition();
	int end;
	while(pos)
	{
		CString& string = m_Strings.GetNext(pos);
		end = string.Find('=');
		if(end > 0 && string.Left(end) == key)
		{
			string = key+'='+value;
			return true;
		}
	}

	return false;
}

// ������ ��� ���������� ��������, ���������������� ��������� �����
void CGSectionInfo::SetString(const CString& key, CString& value)
{
	if(!Replace(key, value))
		AddString(key, value);
}

// ����� � �������� ��������, ���������������� ��������� �����
bool CGSectionInfo::Remove(const CString& key)
{
	POSITION pos = m_Strings.GetHeadPosition();
	int end;
	while(pos)
	{
		CString& string = m_Strings.GetAt(pos);
		end = string.Find('=');
		if(end > 0 && string.Left(end) == key)
		{
			m_Strings.RemoveAt(pos);
			return true;
		}
		m_Strings.GetNext(pos);
	}

	return false;
}

// ����� ������ ������, ���������������� ��������� �����
int CGSectionInfo::GetIndex(const CString& key)
{
	POSITION pos = m_Strings.GetHeadPosition();
	int end;
	int counter = 0;
	while(pos)
	{
		CString& string = m_Strings.GetNext(pos);
		end = string.Find('=');
		if(end > 0 && string.Left(end) == key)
		{
			return counter;
		}
		counter++;
	}

	return -1;
}

// �������� � ������ ����� ��� ������������ �������
void CGSectionInfo::CopyToStringArray(CStringArray& stringArray)
{
	stringArray.SetSize(m_Strings.GetCount(), 0);
	POSITION pos = m_Strings.GetHeadPosition();
	int i = 0;
	while(pos)
	{
		stringArray[i++] = m_Strings.GetNext(pos);
	}
}

// ���������� ������ �� ������
CString CGSectionInfo::GetAt(int index)
{
	CString result;

	POSITION pos = m_Strings.GetHeadPosition();
	int i = 0;
	while(pos)
	{
		CString& next = m_Strings.GetNext(pos);
		if(i == index)
		{
			result = next;
			break;
		}
		i++;
	}

	return result;
}

// ������� ������ �� ������
void CGSectionInfo::RemoveAt(int index)
{
	POSITION pos = m_Strings.GetHeadPosition();
	int i = 0;
	while(pos)
	{
		if(i == index)
		{
			m_Strings.RemoveAt(pos);
			break;
		}
		m_Strings.GetNext(pos);
		i++;
	}
}

// �������� ������ ������ ������ ��� ���������
void CGSectionInfo::CopyDataFrom(const CGSectionInfo& info)
{
	m_Strings.RemoveAll();
	POSITION pos = info.m_Strings.GetHeadPosition();
	while(pos)
	{
		m_Strings.AddTail(info.m_Strings.GetNext(pos));
	}
}

// �������� ������ �������
void CGSectionInfo::operator =(const CGSectionInfo& info)
{
	m_Name = info.m_Name;
	CopyDataFrom(info);
}

//////////////////////////////////////////////////////////////////////

const CString c_NameKey  = _T("Name");
const CString c_UnitsKey = _T("Units");
const CString c_FromKey  = _T("From");
const CString c_ToKey    = _T("To");
const CString c_StepKey  = _T("Step");

// ������ ������ �������� ���������
void CGIntervalInfo::Write(CArchive& archive) //, int number)
{
	CString string;
	CGSectionInfo sectionInfo(c_SectionInterval+' '+m_Name);

	if(!m_Units.IsEmpty())
		sectionInfo.AddString(c_UnitsKey, m_Units);

	DoubleToString(m_From, string);
	sectionInfo.AddString(c_FromKey, string);
	DoubleToString(m_To, string);
	sectionInfo.AddString(c_ToKey, string);
	DoubleToString(m_Step, string);
	sectionInfo.AddString(c_StepKey, string);

	sectionInfo.Write(archive);
}

// ������ ������ �������� ���������
bool CGIntervalInfo::Parse(CGSectionInfo& sectionInfo)
{
	const int c_length = c_SectionInterval.GetLength();
	if(sectionInfo.m_Name.Left(c_length) != c_SectionInterval)
		return false;

	m_IsValid = true;
	CString value;
	if(sectionInfo.m_Name.GetAt(c_length) != ' ')
		m_IsValid = false;
	m_Name = sectionInfo.m_Name.Mid(c_length+1);
	if(m_Name.IsEmpty())
		m_IsValid = false;

	sectionInfo.Lookup(c_UnitsKey, m_Units);

	if(sectionInfo.Lookup(c_FromKey, value))
		m_From = _tstof(value);
	else
		m_IsValid = false;

	if(sectionInfo.Lookup(c_ToKey, value))
		m_To = _tstof(value);
	else
		m_IsValid = false;

	if(sectionInfo.Lookup(c_StepKey, value))
		m_Step = _tstof(value);
	else
		m_IsValid = false;

	return true;
}

//////////////////////////////////////////////////////////////////////
// ������������ ���� ��������� ������� � �� ����� � ������
const CString c_TypeNames[] = {
	_T("int8"),
	_T("int16"),
	_T("int32"),
	_T("real32"),
	_T("real64"),
	_T("byte")
};

const int c_TypeLength[] = { 1, 2, 4, 4, 8, 1 };
const int c_NumberOfTypes = sizeof(c_TypeLength)/sizeof(c_TypeLength[0]);

const CString c_TypeKey = _T("Type");
const CString c_SizeKey = _T("Size");
const CString c_XKey    = _T("X");

CGArrayInfo::CGArrayInfo()
{
	m_SizeOfArray = 0;
	m_ArrayData = 0;
	m_BlockPointer = 0;

	m_IsProcessed = false;
}

CGArrayInfo::~CGArrayInfo()
{
	if(m_ArrayData)
		delete [] m_ArrayData;
}

// ������ ������ �������� �������
void CGArrayInfo::Write(CArchive& archive) //, int number)
{
	CString string;
	CGSectionInfo sectionInfo(c_SectionArray+' '+m_Name);

	if(!m_Units.IsEmpty())
		sectionInfo.AddString(c_UnitsKey, m_Units);

// Real32 - ��� �� ���������
	if(m_TypeOfElement != c_Real32Type)
	{
		string = c_TypeNames[m_TypeOfElement];
		sectionInfo.AddString(c_TypeKey, string);
	}

	string.Format(_T("%d"), m_SizeOfArray);
	sectionInfo.AddString(c_SizeKey, string);
	if(!m_NameForX.IsEmpty())
		sectionInfo.AddString(c_XKey, m_NameForX);

// ��������� ������ �� ������
	POSITION pos = m_Strings.GetHeadPosition();
	while(pos)
		sectionInfo.AddString(m_Strings.GetNext(pos));

	sectionInfo.Write(archive);
}

// ������ �������� ������ �������
void CGArrayInfo::WriteData(CArchive& archive)
{
// ��� ������ ��������� ������ ������ ����� �� ����� ������
	if(m_ArrayData != 0)
		archive.Write(m_ArrayData, GetLength());
}

// ������ �������� ������ �������
bool CGArrayInfo::ReadData(CArchive& archive)
{
// �������� ������ ��� ������
	GetData();
	ASSERT(m_ArrayData != 0);

	if(!m_IsValid)
		return false;
	
	unsigned int length = GetLength();
	unsigned int dataLength = archive.Read(m_ArrayData, length);
// ��������� ������, ��� ������ ���� �������� ���������
	if(dataLength != length)
	{
		m_SizeOfArray = dataLength/c_TypeLength[m_TypeOfElement];
		return false;
	}
	return true;
}

// ������ ������ �������� �������
bool CGArrayInfo::Parse(CGSectionInfo& sectionInfo)
{
	const int c_length = c_SectionArray.GetLength();
	if(sectionInfo.m_Name.Left(c_length) != c_SectionArray)
		return false;

// �������� ������, ���������� ��������� �������� ������ CGSectionInfo
	m_Strings.RemoveAll();
	m_Strings.AddTail(&(sectionInfo.m_Strings));

	m_IsValid = true;
	CString value;
	if(sectionInfo.m_Name.GetAt(c_length) != ' ')
		m_IsValid = false;
	m_Name = sectionInfo.m_Name.Mid(c_length+1);
	if(m_Name.IsEmpty())
		m_Name = _T("Unknown");

	m_Units.Empty();
	sectionInfo.Lookup(c_UnitsKey, m_Units);
	m_NameForX.Empty();
	sectionInfo.Lookup(c_XKey, m_NameForX);

	m_SizeOfArray = -1;
	if(sectionInfo.Lookup(c_SizeKey, value))
		m_SizeOfArray = _tstoi(value);

	m_TypeOfElement = -1;
	if(sectionInfo.Lookup(c_TypeKey, value))
	{
		for(int i = 0; i < c_NumberOfTypes; i++)
		{
			if(value == c_TypeNames[i])
			{
				m_TypeOfElement = i;
				break;
			}
		}
	}
	else
	{
		m_TypeOfElement = c_Real32Type;
	}

	if(m_SizeOfArray <= 0 || m_TypeOfElement < 0)
		m_IsValid = false;

	return true;
}

// ����� ������� � ������
int CGArrayInfo::GetLength()
{
	return c_TypeLength[m_TypeOfElement]*m_SizeOfArray;
}

// ���������� ��������� �� ������ �������.
BYTE* CGArrayInfo::GetData()
{
// �������� � ������� ����� ������ ����� ���������� ���������
	ASSERT(m_SizeOfArray > 0);

//	int length = GetLength();
	if(!m_ArrayData)
		m_ArrayData = new BYTE [GetLength()];

	return m_ArrayData;
}

void CGArrayInfo::operator =(CGArrayInfo& array)
{
// ������ ���������� ��������, ���������� ������ �������
	ASSERT(array.m_ArrayData == 0);

// �������� ������, ���������� ��������� �������� ������ CGSectionInfo
	m_Strings.RemoveAll();
	m_Strings.AddTail(&(array.m_Strings));

	m_Name = array.m_Name;
	m_TypeOfElement = array.m_TypeOfElement;
	m_SizeOfArray = array.m_SizeOfArray;
	m_Units = array.m_Units;
	m_NameForX = array.m_NameForX;

	m_IsValid = array.m_IsValid;
}

// ������� ��� ������ � ��������, �������� ������ ������� ���������� �����
void* CGArrayInfo::GetNextBlock(int* blockSize)
{
	ASSERT(m_TypeOfElement == c_ByteType);
// ������ ����� �������
	if(m_BlockPointer == 0)
		m_BlockPointer = m_ArrayData;
	if(m_BlockPointer >= m_ArrayData+m_SizeOfArray)
		return 0;

// ��������� ������ �����
	short size;
	BYTE* header = (BYTE*)&size;
	*header++ = *m_BlockPointer++;
	*header++ = *m_BlockPointer++;

// ���������� ��������� �� ������ ����� � �������� ���������� ���������
	void* dataPointer = (void*)(m_BlockPointer);
	m_BlockPointer += size;

	*blockSize = size;
	return dataPointer;
}

void CGByteArray::AddBlock(void* block, const int blockSize)
{
// ����������� ������
	int oldSize = GetSize();
	int newSize = oldSize+sizeof(short)+blockSize;
	SetSize(newSize);

// ��������� ����� �������� ����� � ������� short
	int i = oldSize;
	short size = short(blockSize);
	BYTE* header = (BYTE*)&size;
	SetAt(i++, *header++);
	SetAt(i++, *header);

// �������� ������	
	BYTE* data = (BYTE*)block;
	for( ; i < newSize; i++, data++)
		SetAt(i, *data);
}

void CGByteArray::AddBlock(CString string)
{
	AddBlock((void*)(LPCTSTR)string, (string.GetLength()+1)*sizeof(TCHAR));
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// MFC �� ��������� ��������� ASCII ������ ��� ������ ��� UNICODE,
// ��� ���������� ��� ������ ������ ������
bool ReadAsciiString(CArchive& archive, CStringA& string)
{
	char ch;
	string = "";    // empty string without deallocating
	try
	{
		do
		{
			archive >> ch;

			if (ch != '\r' && ch != '\n')
				string += ch;
		}
		while (ch != '\n');
	}
	catch(CArchiveException* e)
	{
		if (e && e->m_cause == CArchiveException::endOfFile)
		{
			e->Delete();
			return (string.GetLength() > 0);
		}
		else
		{
			throw(e);
		}
	}

	return true;
}

bool ReadChar(CArchive& archive, TCHAR& ch)
{
	try
	{
		archive >> ch;
	}
	catch(CArchiveException* e)
	{
		if (e && e->m_cause == CArchiveException::endOfFile)
		{
			e->Delete();
			return false;
		}
		else
		{
			throw(e);
		}
	}

	return true;
}

bool ReadString(CArchive& archive, CString& string, bool useAscii)
{
	if(useAscii)
	{
		CStringA asciiString;
		bool result = ReadAsciiString(archive, asciiString);
		if(result)
		{
			string = CString(asciiString);
		}
		return result;
	}
	else
	{
		return archive.ReadString(string) != 0;
	}
}

unsigned int CGUniversalDataFile::sm_CRCTable[256] = { 0 };

CGUniversalDataFile::CGUniversalDataFile()
{
	m_UseCRC = true;
	m_IsAscii = false;
}

CGUniversalDataFile::~CGUniversalDataFile()
{

}

int CGUniversalDataFile::Read(const CString& fileName, bool firstSectionOnly, bool reportError)
{
	CFile file;
	CFileException fileException;
	if(!file.Open(fileName, CFile::modeRead, &fileException))
	{
		if(reportError)
			fileException.ReportError();
		return c_OpenError;
	}

	int result = c_NoError;
	try
	{
		CArchive archive(&file, CArchive::load);
		result = Read(archive, firstSectionOnly);
		
		archive.Close();
		file.Close();
	}
	catch(CException* exception)
	{
		if(reportError)
			exception->ReportError();
		exception->Delete();
	}

	return result;
}

int CGUniversalDataFile::Read(CArchive& archive, bool firstSectionOnly)
{
	CString string;
	CString asciiString;

	CGSectionInfo sectionInfo;
	CGIntervalInfo intervalInfo;
	CGArrayInfo arrayInfo;

	TCHAR ch;
	if(ReadChar(archive, ch))
	{
		m_IsAscii = (ch >= 0x100);
		// ������������ � ������ �����. � ������������ ���� ������� �� ������.
		archive.Flush();
		archive.GetFile()->SeekToBegin();
	}
	else
	{
		return c_FormatError;
	}

	POSITION pos;
	//while(archive.ReadString(string))
	while(ReadString(archive, string, m_IsAscii))
	{
		if(string.IsEmpty())
			continue;
		 
// ������� ������
		if(string[0] != '[')
		{
// ��������� �������� ������ �� ����������� ������ - ��� �� mdf-����		
			if(sectionInfo.m_Name.IsEmpty() && m_SectionList.IsEmpty())
				return c_FormatError;

			if(string.Find('=', 0) >= 0)
				sectionInfo.AddString(string);
			continue;
		}
		
// ���������

// ������ ��������� ��������
		if(intervalInfo.Parse(sectionInfo))
		{
			m_IntervalList.AddTail(intervalInfo);
		}
// ������ ��������� ������
		else if(arrayInfo.Parse(sectionInfo))
		{
			m_ArrayList.AddTail(arrayInfo);
		}
// ������ ��������� ���������
		else
		{
			if(!sectionInfo.m_Name.IsEmpty())
			{
				m_SectionList.AddTail(sectionInfo);
				sectionInfo.m_Name.Empty();
				if(firstSectionOnly)
					return c_NoError;
			}
		}
		
// ������ ������
		if(string == c_BeginMark)
		{
// ������ �������� ������
			pos = m_ArrayList.GetHeadPosition();
//			m_IsComplete = true;
			bool success = true;
			while(pos) // && m_IsComplete)
			{
				success = m_ArrayList.GetNext(pos).ReadData(archive);
			}
// ��������� �� ��� ������, ��������� � ���������
			if(!success)
				return c_IncompleteError;

			if(m_UseCRC)
			{
				unsigned int dataCRCValue;
				if(archive.Read(&dataCRCValue, 4) != 4)
					return c_CRCError;
				CalculateCRCValue();
				if(dataCRCValue != m_CRCValue)
					return c_CRCError;
			}
			
			return c_NoError;
		}
		
// �������� ��������� ����� ������			
		int end = string.ReverseFind(']');
		if(end < 0)
			return c_ParseError;
		string = string.Mid(1, end-1);
		sectionInfo = CGSectionInfo(string);
	}

// ���� ���� ������ �����������, ��������� ������ ��� �� ���������
	if(!sectionInfo.m_Name.IsEmpty())
	{
		m_SectionList.AddTail(sectionInfo);
	}

	return c_NoError;
}

bool CGUniversalDataFile::Write(const CString& fileName, bool reportError)
{
	try
	{
// ������ �������� ����� � ������������ �������� ����������
		CFile file(fileName, CFile::modeCreate | CFile::modeWrite);

		CArchive archive(&file, CArchive::store);
		
		Write(archive);

		archive.Close();
		file.Close();
	}
	catch(CException* exception)
	{
		if(reportError)
		{
			exception->ReportError();
		}
		exception->Delete();
		return false;
	}

	return true;
}

void CGUniversalDataFile::Write(CArchive& archive)
{
//	int arraySize = 0;
	POSITION pos;

// ��������� ����������� �����
	if(m_UseCRC)
		CalculateCRCValue();
	
// ���������� � ��������� ���� �������� ������� � ������� ���������
	pos = m_SectionList.GetHeadPosition();
	while(pos)
	{
		m_SectionList.GetNext(pos).Write(archive);
	}
	
// ���������� � ��������� ���� �������� ����������
	pos = m_IntervalList.GetHeadPosition();
	while(pos)
	{
		m_IntervalList.GetNext(pos).Write(archive);
	}
	
// ���������� � ��������� ���� �������� ��������
	pos = m_ArrayList.GetHeadPosition();
	while(pos)
	{
		m_ArrayList.GetNext(pos).Write(archive);
	}
	
	pos = m_ArrayList.GetHeadPosition();
	bool haveBinaryData = (pos != 0);
// ���������� ������ ������ ������
	if(pos)
		archive.WriteString(c_BeginMark+c_NewLine);
	
//	if(m_IsTemporary)
//		return;

// ���������� �������� ������
	while(pos)
	{
		m_ArrayList.GetNext(pos).WriteData(archive);
	}
// ���������� ����������� �����
	if(m_UseCRC && haveBinaryData)
		archive.Write(&m_CRCValue, 4);
}

// ��������� ������
void CGUniversalDataFile::AddSectionInfo(CGSectionInfo& sectionInfo)
{
	m_SectionList.AddTail(sectionInfo);
}

// ��������� ������ � ������ �� ���
void CGUniversalDataFile::AddSectionInfo(CGSectionInfo& sectionInfo, const CString& newName)
{
	m_SectionList.AddTail(sectionInfo);
	if(!newName.IsEmpty())
		m_SectionList.GetTail().m_Name = newName;
}

void CGUniversalDataFile::AddIntervalInfo(CString name, double from, double to, double step, CString units)
{
	CGIntervalInfo intervalInfo;
	intervalInfo.m_Name = name;
	intervalInfo.m_Units = units;
	intervalInfo.m_From = from;
	intervalInfo.m_To = to;
	intervalInfo.m_Step = step;

	m_IntervalList.AddTail(intervalInfo);
}

// ��������� ���������� � �������. ������������ ��������� �� ������� ������ �������.
BYTE* CGUniversalDataFile::AddArrayInfo(const CString& name, int type, int size,
										CString dataForX, CString units,
										bool headerOnly)
{
	if(size <= 0)
		return 0;

	CGArrayInfo arrayInfo;
	arrayInfo.m_Name = name;
	arrayInfo.m_Units = units;
	arrayInfo.m_TypeOfElement = type;
	arrayInfo.m_SizeOfArray = size;
	arrayInfo.m_NameForX = dataForX;

	m_ArrayList.AddTail(arrayInfo);

// ��� ������ ���������� ����� ������������ ������ ��������� �������
	if(headerOnly)
	{
		m_UseCRC = false;
		return 0;
	}

// �������� ������� ������ ��� ������ � ���������� ��������� �� ���
	return m_ArrayList.GetTail().GetData();
}

void CGUniversalDataFile::AddArrayInfo(const CString& name, const CGByteArray& buffer)
{
	CGArrayInfo arrayInfo;
	arrayInfo.m_Name = name;
	arrayInfo.m_Units.Empty();
	arrayInfo.m_TypeOfElement = c_ByteType;
	arrayInfo.m_SizeOfArray = buffer.GetSize();
	ASSERT(arrayInfo.m_SizeOfArray > 0);

	m_ArrayList.AddTail(arrayInfo);
	memcpy(m_ArrayList.GetTail().GetData(), buffer.GetData(),buffer.GetSize());
}

CGArrayInfo& CGUniversalDataFile::GetLastArrayInfo()
{
	return m_ArrayList.GetTail();
}

// ����� ������ �� ���������
CGSectionInfo* CGUniversalDataFile::FindSectionInfo(CString name)
{
	POSITION pos = m_SectionList.GetHeadPosition();
	while(pos)
	{
		if(m_SectionList.GetAt(pos).m_Name == name)
			return &(m_SectionList.GetAt(pos));
		m_SectionList.GetNext(pos);
	}

// �� �����
	return 0;
}

// ����� ��������� �� �����
CGIntervalInfo* CGUniversalDataFile::FindIntervalInfo(CString name)
{
	POSITION pos = m_IntervalList.GetHeadPosition();
	while(pos)
	{
		if(m_IntervalList.GetAt(pos).m_Name == name)
			return &(m_IntervalList.GetAt(pos));
		m_IntervalList.GetNext(pos);
	}

// �� �����
	return 0;
}

// ����� ������� �� �����
CGArrayInfo* CGUniversalDataFile::FindArrayInfo(CString name)
{
	POSITION pos = m_ArrayList.GetHeadPosition();
	while(pos)
	{
//		CString tmp = m_ArrayList.GetAt(pos).m_Name;
		if(m_ArrayList.GetAt(pos).m_Name == name)
			return &(m_ArrayList.GetAt(pos));
		m_ArrayList.GetNext(pos);
	}

// �� �����
	return 0;
}


// �������� �� ������ ��������
POSITION CGUniversalDataFile::GetFirstArrayInfoPosition() const
{
	return m_ArrayList.GetHeadPosition();
}

CGArrayInfo& CGUniversalDataFile::GetNextArrayInfo(POSITION& position)
{
	return m_ArrayList.GetNext(position);
}

int CGUniversalDataFile::GetNumberOfArrays()
{
	return m_ArrayList.GetCount();
}

// �������� �� ������ ������
POSITION CGUniversalDataFile::GetFirstSectionPosition() const
{
	return m_SectionList.GetHeadPosition();
}

CGSectionInfo& CGUniversalDataFile::GetSectionInfo(POSITION& position)
{
	return m_SectionList.GetNext(position);
}

void CGUniversalDataFile::CalculateCRCValue()
{
	m_CRCValue = 0xFFFFFFFF;
	if(sm_CRCTable[1] == 0)
		CalculateCRCTable();

/*
	char test[] = "123456789";
	CalculateCRCValue((BYTE*)test, 9);
	TRACE("CRC32 table 0x%8x\n", sm_CRCTable[1]);
	TRACE("CRC32 check 0x%8x 0x%8x\n", m_CRCValue, m_CRCValue ^ 0xFFFFFFFF);
*/

	POSITION pos;
// ����������� ����� �������� ������

	pos = m_ArrayList.GetHeadPosition();
	while(pos)
	{
		CGArrayInfo& arrayInfo = m_ArrayList.GetNext(pos);
		CalculateCRCValue(arrayInfo.GetData(), arrayInfo.GetLength());
	}

	m_CRCValue ^= 0xFFFFFFFF;
}

void CGUniversalDataFile::CalculateCRCValue(BYTE* data, int length)
{
	while(length--)
	{
		m_CRCValue = sm_CRCTable[BYTE((m_CRCValue)^(*data++))]
			^ (m_CRCValue >> 8);
	}
}

void CGUniversalDataFile::CalculateCRCTable()
{
	unsigned long i, reg;
    const unsigned long c_crcMask = 0xedb88320;
	
	for (i = 0; i < 256; i++)
	{
		reg = i;
		for (int k = 0; k < 8; k++)
			reg = (reg & 1) ? c_crcMask ^ (reg >> 1) : reg >> 1;
		
		sm_CRCTable[i] = reg;
	}
}

///////////////////////////////////////////////////////////

