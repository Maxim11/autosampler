#if !defined(AFX_COMMONPROGRAMDIALOG_H__4B8041A7_A0C8_4238_AB1F_22BAA6780D50__INCLUDED_)
#define AFX_COMMONPROGRAMDIALOG_H__4B8041A7_A0C8_4238_AB1F_22BAA6780D50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommonProgramsDialog.h : header file
//

#include "Controls\EditableGridCtrl.h"
#include "Controls\SmartComboBox.h"
#include "Controls\TrackingToolTip.h"
#include "ProgramData.h"

/////////////////////////////////////////////////////////////////////////////
// CMCommonProgramDialog dialog

class CGProgramDataContainer;

class CMCommonProgramDialog : public CDialog
{
// Construction
public:
	CMCommonProgramDialog(UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor

public:
	void SetContainer(CGProgramDataContainer* mainContainer);
	void CreateEmptyTable();
	void SetActiveProgramName(CString& string) { m_ActiveProgramName = string; }
	CString& GetActiveProgramName() { return m_ActiveProgramName; }

	void SetExternalLightMeasurement() { m_IsExternalLightMeasurement = true; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMCommonProgramDialog)
	public:
//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMCommonProgramDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnProgramSelected(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginNameEditing(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndNameEditing(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginCellEditing(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndCellEditing(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnProgramItemSelected(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonAddProgram();
	afx_msg void OnButtonDeleteProgram();
	afx_msg void OnButtonCopyProgram();
	afx_msg void OnButtonRenameProgram();
	afx_msg void OnButtonAddRow();
	afx_msg void OnButtonDeleteRow();
	afx_msg void OnButtonCopyRow();
	afx_msg void OnButtonPasteRow();
	afx_msg void OnDoubleClickedButtonAddRow();
	afx_msg void OnOK();
	afx_msg void OnStartEditProgramName();
	afx_msg void OnStartEditProgramParameters();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG

protected:
// ���������� ������ � ���������
	bool AddRow(bool emptyRow = true, bool editItem = true, bool afterLast = false);

// ��������� ������� ���������� �� ���������� ��������
	virtual void ProgramToTable(int programIndex) { };
// ���������� ���������� ������� � ����������
	virtual void TableToProgram(int programIndex) { };

// ����� ��������� ��� �������� ����������
	virtual void GetTipText(CString& tipText, int controlID) const;
// ����� ��� �������� ���������� �����
	virtual CString GetCopyStageBuffer() const { ASSERT(false); return _T(""); }
	virtual void SetCopyStageBuffer(const CString& string) { };
	virtual bool CopyStageBufferIsValid() const;


// �������� ��������� ������ ������ ��� ������ �� ������� ���� ��������
	void EnableNameButtons();
// �������� ��������� ������ ������ ��� ������ � ����������� ���������
	void EnableParametersButtons(bool itemSelected = false);
// ��������� ����� ������������ ���� �������
	void EnablePopupMenuItem(CMenu* popupMenu, int itemID);

// ���������� ��������� � ������������ �������� ���������
	void ShowErrorMessage(CGProgramData& programData, bool editItem);
// ���������� ��������� � ������������ ����� ���������
	void ShowErrorMessage(int nameIndex, int messageID);
// �� �� ��� ������������� ���������
	virtual void ShowErrorMessage(int number, const CString& errorText, bool editItem) { };

// ���������� ��� ���������� ������ � �������
	virtual void DoOnAddRow(int row, bool emptyRow) { };
// ���������� ��� �������� ������ �� �������
	virtual void DoOnDeleteRow(int row) { };
// ���������� ��� ��������� �������� �������
	virtual void DoOnEndCellEditing(int row, int column, const CString& cellText) { };
// ���������� �� ���������� �������������� �������� ��������� �������� ESC
	virtual void DoOnEscapeCellEditing(int row, int column);

// ���������� ���������
	void ShowToolTipMessage(const CString& message, CPoint& point, int interval = 5000);
// ������� ���������
	void HideToolTipMessage();

protected:
// ������ ������������� ��������
	CMEditableGridCtrl m_ProgramNamesGrid;
// ������� ���������� ���������
	CMEditableGridCtrl m_ProgramParametersGrid;

//	CMSmartComboBox m_StrobeDelayComboBox;
//	CMSmartComboBox m_StrobeDurationComboBox;

	CImageList m_ProgramNamesImageList;

// ���� ��� ����������� ���������� � ����������� ��������� ���������
	CMTrackingToolTip m_TrackingToolTip;

// �������, ������������ � �������� �������� � �������� �������
	CGIntegerValidator m_IntegerValidator;
	CGFloatValidator m_FloatValidator;

// ����������� ��������� ��� ������
//	CToolTipCtrl m_ToolTipCtrl;

protected:
// ��������� ���� �������� �������������
	bool m_IsProgramModified;
// ��������� �� ���������� ��������� �� ������ ��� ������ ���������
//	bool m_IsAutoErrorMessageEnabled;

// ��� ��������� �� ��������������
	CString m_OldProgramName;
// ��� �������������� �������� ������� ���������� ������ ����������, ����� ������, ���� �� ���������
	CString m_OldCellText;

// ������� ������ � ����� �������� ��� ������
	int m_OldProgramIndex;
	int m_NewProgramIndex;

// ��� ���������, ��������� � ������ ������ �������
	CString m_ActiveProgramName;

// ������ �� ���������� ������������ ���������� ���������
	CString m_FixedString;
	CString m_EluentA;
	CString m_EluentB;
// ��� ������������
	int m_ScanningType;

// ��������� ������������ ��� ��������� ��� ������� � ���������� �������� � ��������
	bool m_IsExternalLightMeasurement;

// ����� ������ ������� � ������� ���������, ������� ����� �������������
	int m_FirstEnabledColumn;

// ������ ���� ��������� ������������� (�� ������� Enter � ��������� �������)
	bool m_RowIsAutoAdded;

// ��������� ��� ������ ���������� ������
//	CString m_StrobeDelayTip;
//	CString m_StrobeDurationTip;

protected:
// ������ ������� ��������� ���������
//	CGChromatographicProgramData m_ProgramData;
// ������ ���� ��������� �������� ���������
	CGProgramDataContainer* m_MainProgramContainer;
// ������ �������� ���������, ���������� ��� ��������������
	CGProgramDataContainer m_ChangedProgramContainer;
// ����� ��������� ��������
	CStringList m_RemovedProgramNames;

protected:
// �������������� ����������� ����
	int m_ProgramNamesMenuID;
	int m_ProgramParameteresMenuID;

	int m_ProgramDeletePromptID;

protected:
// ������ ��� ������
	static HICON sm_AddProgramIcon;
	static HICON sm_DeleteProgramIcon;
	static HICON sm_CopyProgramIcon;
	static HICON sm_RenameProgramIcon;

	static HICON sm_AddRowIcon;
	static HICON sm_DeleteRowIcon;
	static HICON sm_CopyRowIcon;
	static HICON sm_PasteRowIcon;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMONPROGRAMDIALOG_H__4B8041A7_A0C8_4238_AB1F_22BAA6780D50__INCLUDED_)
