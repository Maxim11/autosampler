// ValveCloseDialog.cpp: ���� ����������
//
#include "stdafx.h"
#include "resource.h"
#include "PumpTool.h"
#include "ValveCloseDialog.h"


// ���������� ���� CMValveCloseDialog

IMPLEMENT_DYNAMIC(CMValveCloseDialog, CDialog)

CMValveCloseDialog::CMValveCloseDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMValveCloseDialog::IDD, pParent)
{
	m_OpenValve = false;
}

CMValveCloseDialog::CMValveCloseDialog(CWnd* pParent, bool openValve)
	: CDialog(CMValveCloseDialog::IDD, pParent)
{
	m_OpenValve = openValve;
}

CMValveCloseDialog::~CMValveCloseDialog()
{
}

void CMValveCloseDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMValveCloseDialog::OnInitDialog()
{
	BOOL result = CDialog::OnInitDialog();
	CenterWindow();

	CWnd* textField = GetDlgItem(IDC_STATIC_TEXT);
	static CFont font;
	if(font.GetSafeHandle() == 0)
	{
		LOGFONT logFont = {0};
		textField->GetFont()->GetLogFont(&logFont);
		logFont.lfHeight += logFont.lfHeight/2;

		font.CreateFontIndirect(&logFont);
	}
	textField->SetFont(&font);
	
	if(m_OpenValve)
	{
		CString text;
		text.LoadString(IDS_CONTROL_VALVE_OPEN);
		textField->SetWindowText(text);
	}

	return result;
}


BEGIN_MESSAGE_MAP(CMValveCloseDialog, CDialog)
END_MESSAGE_MAP()


// ����������� ��������� CMValveCloseDialog
