#if !defined(_FSTREAM_MFC_H__INCLUDED_)
#define _FSTREAM_MFC_H__INCLUDED_

#include <fstream>

// ��� ������ �������� ���������� ������� ���������� ����� ��������������
// ��� ������ CString, ����� ��������� �� ���������� ������, � ��������
// ���������
inline std::ostream& operator <<(std::ostream& os, const CString& string)
{
	os << static_cast<const TCHAR*>((LPCTSTR)string);
	return os;
}

#endif		// _FSTREAM_MFC_H__INCLUDED_