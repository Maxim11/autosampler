#pragma once


// ����� ����������� ������
const COLORREF c_ColorBlack 	= RGB(0, 0, 0);
const COLORREF c_ColorWhite 	= RGB(255, 255, 255);
const COLORREF c_ColorRed		= RGB(255, 0, 0);
const COLORREF c_ColorBlue		= RGB(0, 0, 255);
const COLORREF c_ColorMagenta	= RGB(235, 0, 235);
const COLORREF c_ColorGreen 	= RGB(0, 192, 0);
const COLORREF c_ColorDarkRed	= RGB(255, 127, 0);
const COLORREF c_ColorCyan	= RGB(0, 235, 235);

// ����� � �� �������� ������������ �� ���������� wxWindows

const COLORREF c_ColorDarkOrchid	   = RGB(153, 50, 204);
const COLORREF c_ColorCoral 		   = RGB(255, 127, 0);
const COLORREF c_ColorBrown 		   = RGB(165, 42, 42);
const COLORREF c_ColorCadetBlue 	   = RGB(95, 159, 159);
const COLORREF c_ColorDarkTurquoise    = RGB(112, 47, 219);
const COLORREF c_ColorForestGreen	   = RGB(35, 142, 35);
const COLORREF c_ColorGold			   = RGB(204, 127, 50);
const COLORREF c_ColorKhaki 		   = RGB(159, 159, 95);
const COLORREF c_ColorMaroon		   = RGB(142, 35, 107);
const COLORREF c_ColorMediumAquamarine = RGB(50, 204, 153);
const COLORREF c_ColorMediumBlue	   = RGB(50, 50, 204);
const COLORREF c_ColorSeaGreen		   = RGB(35, 142, 107);
const COLORREF c_ColorMediumVioletRed  = RGB(219, 112, 147);
const COLORREF c_ColorOrange		   = RGB(204, 50, 50);
const COLORREF c_ColorOrangeRed 	   = RGB(255, 0, 127);
const COLORREF c_ColorPink			   = RGB(188, 143, 234);
const COLORREF c_ColorPaleGreen 	   = RGB(143, 188, 143);
const COLORREF c_ColorSlateBlue 	   = RGB(0, 127, 255);
const COLORREF c_ColorSpringGreen	   = RGB(0, 255, 127);
const COLORREF c_ColorLightGold 	   = RGB(255, 204, 25);
//const COLORREF c_Color	= RGB(, , );

const COLORREF c_ColorGray	           = RGB(128, 128, 128);
const COLORREF c_ColorLightGray        = RGB(192, 192, 192);
const COLORREF c_ColorPlum             = RGB(234, 173, 234);
const COLORREF c_ColorThistle          = RGB(216, 191, 216);

// ������-������� ����� ��� ����
const COLORREF c_ColorLightBlue    = RGB(191, 216, 216);
const COLORREF c_ColorTurquoise    = RGB(173, 234, 234);
