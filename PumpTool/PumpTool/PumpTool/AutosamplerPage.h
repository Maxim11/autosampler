﻿#pragma once
#include "ToolPage.h"

// Диалоговое окно CMAutosamplerPage

//class CMAutosamplerPage : public CDialogEx
const int LOWEST_SPIN_VALUE = 300;
const int  HIGHEST_SPIN_VALUE = 40000;

class CMAutosamplerPage : public CMToolPage
{
	DECLARE_DYNAMIC(CMAutosamplerPage)

public:
	//CMAutosamplerPage(CWnd* pParent = nullptr);   // стандартный конструктор
	CMAutosamplerPage();   // стандартный конструктор
	virtual ~CMAutosamplerPage();

// Данные диалогового окна
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PAGE_AUTOSAMPLER};
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV
	virtual BOOL OnInitDialog();

protected:

	bool m_IsAutosamplerOn;
	CMSmartComboBox m_VialNumberComboBox;
	class CGAutosampler* m_Autosampler;
	CMSmartComboBox m_WasteVialComboBox;
	CMSmartComboBox m_WashOn;



	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedStartwash();
	afx_msg void OnCbnSelchangeSolventvial();
	afx_msg void OnDeltaposSpinWashvolume(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEnChangeEditWashvolume();
private:
	CSpinButtonCtrl m_spin_WashVolume;
};
