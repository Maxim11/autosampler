#pragma once


// ���������� ���� CMIsocraticSettingsDialog

class CMIsocraticSettingsDialog : public CDialog
{
	DECLARE_DYNAMIC(CMIsocraticSettingsDialog)

public:
	CMIsocraticSettingsDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~CMIsocraticSettingsDialog();

// ������ ����������� ����
	enum { IDD = IDD_DIALOG_ISOCRATIC_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_MaxPressureWork;
	CString m_MinPressureWork;
	CString m_MaxPressureDischarge;
	CString m_ReleasingFlowRate;
	BOOL m_AutomaticPressureReleasing;
};
