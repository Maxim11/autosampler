// Piton3.h: interface for the CGPiton class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "../InstrumentBase\MasterInstrument.h"

// ���������� � ������ ������ ����
/*
struct CSVersionInfo
{
	BYTE Software;
	BYTE Hardware;
	BYTE Subversion;
	BYTE Year;
	BYTE Month;
	BYTE Day;
};
*/
class CGGradientProgram;

//const int c_MaxDetectorParameterNumber = 50;

class CGPiton : public CGMasterInstrument
{
public:
	CGPiton();
	virtual ~CGPiton();

// �������, ���������������� ������ �������� ������ CGMasterDevice
//	virtual bool Create();
//	virtual void Initialize();

// �������, ������� ������ ���������������� ��� ����������� �������� �������
	virtual bool ProcessIncomingMessage(CGIncomingMessage& message);
//	virtual void DoOnSystemMessage();

// �������� ������� ������ ���������� ������
	virtual void AskSerialNumber();
// ������ ������� �������� ���������� �������
	virtual void AskParameters();
// ����������� ������ ������ ������������ ������������
//	virtual void AskNodesVersionInfo();

// ������ ��������� ��������� ������� �� ��������������
	void AskParameter(UINT16 parameterID);
	void SetParameter(UINT16 parameterID, INT32 value);

// �������� ��������� ������ �� ������ ���������
	int GetParameterStatus() { return m_AskParameterStatus; }

// �������� ��������� ������ �� ������ ���������
	INT32 GetParameterValue() { return m_AskedParameterValue; }

// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
//	virtual const CString GetShortName() const { return c_ShortName; }

	//double GetCurrentVolume();

// ������ ��������� ��� ������ �������� ��� �������� ��������� �������������
//	virtual bool IsInstrumentReady() { return IsMonochromatorReady(); }

// �������� ������� ������� ������ ��������� ������
	virtual bool ResetErrorState();


// ���������� ������������� ������ ��������� �� ������ � ������ �����
//	virtual int GetErrorMessageID(int errorCode);

// ������/���������� ���������� �� ���������� �������
	//virtual void LockKeyboard(bool enable);

// ������ �� ��������� ���������� ������
//	void AskErrorStatistic();
// ������ ������� ���������.
	void AskResourceCounter();
// ������ ������ ���������.
	int GetResourceCounter();
// ����� �� ������  ���������� ������
//	BYTE* GetErrorStatistic() { return m_ErrorStatistic; }

// ����������������� �������
//	void ResetInstrument();

public:

	// ��������� ����� ������� � �������� �������� �� ���������.
	void StartSmartFill(const int flowRate, const int targetVolume, bool fillOnly);

	// ��������� ����� ������ �������.
	void StartFillMode(const int flowRate, const int targetVolume);
	// ��������� ����� ����� �������.
	void StartDischargeMode(const int flowRate, const int targetVolume);
	// ��������� ����� ������ �������.
	void StartWorkMode(const int targetFlowRate, const int initialPressure, int initialFlowRate);

	void StartPressureReleasingMode(int flowRate);

	void StopCurrentMode();

	void SetInternalValveState(BYTE targetState);
	
	void SetFlowRate(const int flowRate);
	void SetPressureLimits(const int minPressureWork, const int maxPressureWork, const int maxPressureDischarge);
	void SetPressureReleasing(const bool isAutomatic, const int releasingFlowRate);

	void AssignRoleB();
	bool IsCanSlave();

	void ConnectSlavePump(bool connect);
	void ConnectSlavePump(CGPiton* pump);
	void DisconnectSlavePump(CGPiton* pump);

	void LoadGradientStage(int stage, int time, int beginFlowRate, int endFlowRate);
	void StartGradientStage(int stage);
	void StartGradientStage(int time, int beginFlowRate, int endFlowRate);

	bool IsReady();
	bool IsInWaitMode();
	bool IsInFillMode();
	bool IsInDischargeMode();
	bool IsInWorkMode();
	bool IsInPressureReleasingMode();
	bool IsPressureReleased();

	bool IsSmartFillFill();
	bool IsSmartFillWait();
	bool IsSmartFillWork();
	bool IsSmartFillAirFound();
	bool IsSmartFillStopped();

	// �������� ��������� ��������� ������� ��� �������.
	void SetValues(int volume, int pressure);
	bool GenerateCurrentValues();
public:

// ��������� �����-��������.	
	bool IsExternalValveOn() { return m_IsExternalValveOn; }
	// ���������, ��� �������� �����������������.
	bool IsFlowRateOk();

	int GetTargetMode()     { return m_Manager.TargetMode;   }

	int GetCurrentState()     { return m_CurrentState;   }
	int GetCurrentVolume()   { return m_CurrentVolume;   }
	int GetCurrentFlowRate() { return m_CurrentFlowRate; }
	int GetCurrentPressure() { return m_CurrentPressure; }
	int GetCurrentModeTime() { return m_Manager.Time;   }
	int GetInternalValveState() { return m_InternalValveState;   }

	void SetGradientProgram(CGGradientProgram* program);
	void StartGradientProgram();
	void CheckProgramState();

//	CSVersionInfo* GetLampVersion() { return &m_LampVersion; }

protected:
// ��������� ������������ ���������� ���������
	virtual void DoOnSystemMessage(CGIncomingMessage& message);

// ������� ��������� ���������� ���������
	//void DoOnParameterValue(CGIncomingMessage& message);

	void DoOnInstrumentInfo(CGIncomingMessage& message);
	void DoOnSetWorkParameter(CGIncomingMessage& message);
	void DoOnGetWorkParameter(CGIncomingMessage& message);
	void DoOnMeasuredDataMessage(CGIncomingMessage& message);
	void DoOnErrorMessage(CGIncomingMessage& message);
	void DoOnInstrumentStateInfo(CGIncomingMessage& message);

	void DoOnCommInfoMessage(CGIncomingMessage& message);
	void DoOnGetCanNodeInfoMessage(CGIncomingMessage& message);
	void DoOnCanNodePacket(CGIncomingMessage& message);

	void DoOnGetParameter(CGIncomingMessage& message);
	void DoOnGetDiagnosticInfo(CGIncomingMessage& message);

// ������� ��������� ���������, ����������� ��� ����������� � ��
// �������� �� ���������� ��������
	bool DoOnSysemStatisticInfoMessage(CGIncomingMessage& message);
// ����� ���� ���� ���� ���������
	void DumpMessage(CGIncomingMessage& message, int start = 0);

	void AskWorkParameter(BYTE logicalUnit, BYTE parameterID);
	void SetWorkParameter(BYTE logicalUnit, BYTE parameterID, INT16 value);

	// ������ ����������, ����������� � ������������� ���� �������
//	void AskCanNodeInfo(BYTE nodeAddress, BYTE infoID);
//	void SendCanNodeCommand(BYTE nodeAddress, BYTE *canData);
//	void EnableCanTracing(BYTE nodeAddress, BYTE flag);

	// ������ ���������������� ������ ������
	void StartDiagnosticMode(BYTE mode);
	// ���������� ������� ������ � ����������� ��������.
	void CheckSmartFillState();
	// �������� ������� ���������� ������.
	void CheckFilledVolume();

public:
	// ����������� ������ ������� �������.
// ��������� ���������� � ������ � ������������ �������
	virtual int GetSerialNumber(); 
	virtual int GetSoftVersionNumber();
	virtual int GetHardVersionNumber();
// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
	virtual const CString GetShortName() const { return _T("Piton"); } ;

// ��������� ����������� ���������
	virtual bool GetMeasuredValues(double values[]) { return false; } 

private:
// ��������������, ��������� ����� ����� ���� � ������������ �����������
	virtual void Create(CGDataChannel* dataChannel) { CGInstrument::Create(dataChannel); }

protected:

	struct
	{
		// ����� ��������������� ��������.
		int TargetMode;
		// �����, ���������� �� �������.
		int Mode;
		// ����� ������ � ������� ������, ���������� �� �������.
		int Time;
	}
	m_Manager;

// ��������� ����� ��������
	bool m_IsExternalValveOn;
// ����� �������� ������� (��������, �������������� � ��)
	BYTE m_InstrumentStateMask;
	// ����� ����������� ��������� �� ���� CAN.
	bool m_IsCanSlave;

// ��������� ����� (� ������� �� ��������� ��������� ����� ����� ����!)
	INT32 m_SerialNumber;
// ���������� � ���� � ������ ������ �������� �������
	BYTE m_Type;
	CSVersionInfo m_MainControllerVersion;

// ��������� � ������ ��������� ������
	bool m_IsInErrorState;
// ���������� �� ��������� ��������� ��������� ��������������� ���������
	bool m_IsDiagnosticEnabled;

// ������, ���������� ��������� ������������� ����������� ���������
	CString m_IncomingMessageLogString;

protected:

// ������������� ������������ ���������
	UINT16 m_AskedParameter;
// ���������� �����
	INT32  m_AskedParameterValue;
// �������� ������� ���������
	int m_AskParameterStatus;

// ��������� ������� ���������� ������
//	BYTE m_ErrorStatistic[24];

// ���������� � ������ ������ D2M
//	CSVersionInfo m_LampVersion;

// ������ ������ ���������.
	int m_ResourceCounter;

	int m_TargetFlowRate;

// ���������� �� ������� ��������� ������.
	int m_CurrentState;
	int m_InternalValveState;
	// ��������� ������� ����.
	int m_FoamSensorState;
	int m_CurrentVolume;
	int m_CurrentFlowRate;
	int m_CurrentPressure;

	// ������ ��� ������ �������� ����� ��������� �����.
	int m_MaxAllowedPressure;

	CGGradientProgram* m_GradientProgram;

	// ���������� ������� � ��������� ���������� �� ���������� ��������� �������.
	struct
	{
		int State;
		int TimeCounter;
		bool FillOnly;
	}
	m_SmartFill;

public:
	static const BYTE c_NetAddress;
// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
	static const TCHAR c_ShortName[6];

	// �������� ����, ��� ������ instrument ����� ��� CGPiton
	static const bool IsInstrumentModelEqual(CGInstrument* instrument);
};

/*
// ���������� �� ������ � ����������� �������.
struct CSErrorInfo
{
	UINT16 ErrorCode;
	BYTE   Type;
	BYTE   Length;
	UINT32 Value;
};
*/

// �������� ��� ��������� ������� ���������
enum
{
	c_ParameterWaitAnswer,
	c_ParameterIsValid,
	c_ParameterIsInvalid,
};

enum
{
	c_PumpStateReceived = c_FirstUnusedInstrumentState,
	c_PumpModeReceived,
	c_PumpProgramStageChanged,
	c_PumpSmartFillWait,
	c_PumpSmartFillWork,
	c_PumpSmartFillAirInCamera,
	c_PumpSmartFillStopped,
	c_FirstUnusedPumpState,
};

enum
{
	c_PumpMaster,
	c_PumpSlaveB,
	c_PumpSlaveC,
};

// ��� ����������� ���������� ��������.
const int c_MaxPumpVolume = 40000;
// ��� ���������� �������� ����������.
const int c_WorkPumpVolume = 35000;
const int c_FillFlowRate = 0; //20000;
const int c_DischargeFlowRate = 0; // 20000;
const int c_MaxFlowRate = 20000;

// ��������� �����.
const BYTE VALVE_STATE_WORK            = 0;
const BYTE VALVE_STATE_FILL            = 1;
const BYTE VALVE_STATE_DISCHARGE       = 2;
const BYTE VALVE_STATE_RUN             = 4;
const BYTE VALVE_ERROR_JAM             = 5;
const BYTE VALVE_ERROR_OVERFLOW        = 6;
const BYTE VALVE_ERROR_SUPPLY          = 7;

const BYTE MANAGER_COMMAND = 0x80;
const BYTE MANAGER_EXECUTE = 0x81;
