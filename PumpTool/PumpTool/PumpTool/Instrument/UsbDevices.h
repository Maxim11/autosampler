#if !defined(USB_H_INCLUDED)
#define USB_H_INCLUDED

// ��������� USB-���������, ������������� � ����.

const unsigned short c_UsbVendorId_Lumex         = 0x16E0;

const unsigned short c_UsbProductId_Capel205_     = 0x0301;
const unsigned short c_UsbProductId_Capel115     = 0x0302;

const unsigned short c_UsbProductId_Panorama     = 0x030A;

const unsigned short c_UsbProductId_SphDetector  = 0x0311;
const unsigned short c_UsbProductId_Piton        = 0x0315;
const unsigned short c_UsbProductId_Thermostat   = 0x0319;

const unsigned short c_UsbProductId_Bootloader   = 0x0321;
const unsigned short c_UsbProductId_CanAdapter   = 0x0380;

// �������������� ������� (����� NetAddress), ������������ ���������� 
// ����������� ������.

const unsigned short c_InstrumentId_Panorama     = 150;
const unsigned short c_InstrumentId_Capel        = 190;
const unsigned short c_InstrumentId_SphStend     = 210;
const unsigned short c_InstrumentId_SphDetector  = 230;
const unsigned short c_InstrumentId_Piton        = 130;
const unsigned short c_InstrumentId_Thermostat   = 120;

#endif // USB_H_INCLUDED
