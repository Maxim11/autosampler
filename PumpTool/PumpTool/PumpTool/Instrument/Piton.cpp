// Piton.cpp: implementation of the CGPiton class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Piton.h"
#include "PitonConstants.h"
#include "UsbDevices.h"

#include "GradientProgramStage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define USE_P_R_MODE   0

// ��������� ������ �� ������� �������.
enum
{
	c_SmartFillNone,
	c_SmartFillFill,
	c_SmartFillWait,
	c_SmartFillWorkFast,
	c_SmartFillWorkSlow1,
	c_SmartFillWorkSlow2,
	c_SmartFillWorkSlow3,
	c_SmartFillWait2,
	c_SmartFillWait3,
	c_SmartFillWorkMaxSpeed,
	c_SmartFillFoamFast,
	c_SmartFillFoamFast1,
	c_SmartFillFoamSlow,
	c_SmartFillStop,
	c_SmartFillFinish,
	// ��� �������� ���������� ������������ ������, ����� ��������� ���� ����� �����.
	c_SmartFillDischarge,
};

// �������� ����, ��� ������ instrument ����� ��� CGPiton
const bool CGPiton::IsInstrumentModelEqual(CGInstrument* instrument)
{
	return (instrument->GetInstrumentModel() == (NET_ADDR_PITON << 8));
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
const BYTE CGPiton::c_NetAddress = NET_ADDR_PITON;
const TCHAR CGPiton::c_ShortName[6] = _T("Piton");

CGPiton::CGPiton()
{
	m_NetAddress = NET_ADDR_PITON;
	m_InstrumentId = NET_ADDR_PITON;
	m_InstrumentModel = (NET_ADDR_PITON << 8);
	m_SerialNumber = -1;

	m_UsbVendorID = c_UsbVendorId_Lumex;
	m_UsbProductID = c_UsbProductId_Piton;
	// ��������� ����� �������� � �������������
	m_InternalTimeUnitToSec = 0.001;

	m_IsDiagnosticEnabled = true;
	m_IsInErrorState = false;

	// �������� ���� ������� �� ������������.
	m_Manager.TargetMode = -1;
	m_Manager.Mode = -1;
	m_Manager.Time = 0;
	m_TargetFlowRate = -1;
	m_SmartFill.State = c_SmartFillNone;

	// 0.5 ����.
	m_MaxAllowedPressure = 5;

	m_GradientProgram = 0;
	//m_SlavePump = 0;
}

CGPiton::~CGPiton()
{

}

int CGPiton::GetSerialNumber()
{
	return m_SerialNumber;
}

int CGPiton::GetSoftVersionNumber()
{
	return m_MainControllerVersion.Software;
}

int CGPiton::GetHardVersionNumber()
{
	return m_MainControllerVersion.Hardware;
}

// �������� ������� ������� ������ ��������� ������
bool CGPiton::ResetErrorState()
{
// !!! ��������� ������ ���� �� ����������� � ������ ������
	return true;
}

bool CGPiton::IsReady()
{
	// ��������� �������� ����� ��������������� � �������� ������ �� ������� ����.
	// ��� "������" �������� ����������.
	return (IsInWaitMode() && m_SmartFill.State == c_SmartFillNone);
}

bool CGPiton::IsInWaitMode()
{
	return (m_Manager.Mode == MANAGER_WAIT && m_Manager.TargetMode == MANAGER_WAIT);
}

bool CGPiton::IsInFillMode()
{
	return m_Manager.Mode == MANAGER_FILL;
}

bool CGPiton::IsInDischargeMode()
{
	return m_Manager.Mode == MANAGER_DISCHARGE;
}

bool CGPiton::IsInWorkMode()
{
	return m_Manager.Mode == MANAGER_ISOCRATIC_WORK || m_Manager.Mode == MANAGER_SET_INITIAL_PRESSURE;
}

bool CGPiton::IsInPressureReleasingMode()
{
	return m_Manager.TargetMode == MANAGER_PRESSURE_RELEASING;
}
bool CGPiton::IsPressureReleased()
{
#if USE_P_R_MODE
	return m_Manager.Mode == MANAGER_PRESSURE_RELEASED;
#else
	return (m_Manager.TargetMode == MANAGER_PRESSURE_RELEASING
			&& m_CurrentPressure <= m_MaxAllowedPressure);
#endif
}

// �������� ��������� ��������� ������� ��� �������.
void CGPiton::SetValues(int volume, int pressure)
{
	m_CurrentVolume = volume;
	m_CurrentPressure = pressure;
}

bool CGPiton::GenerateCurrentValues()
{
	if(!IsConnectionEstablished())
	{
		if(IsInPressureReleasingMode())
		{
			m_CurrentPressure--;
			m_CurrentVolume += 2;

			return true;
		}
	}
	return false;
}

// ��������� ����� ������� � �������� �������� �� ���������.
void CGPiton::StartSmartFill(int flowRate, int targetVolume, bool fillOnly)
{
	StartFillMode(flowRate, targetVolume);
#if 1
	m_SmartFill.State = c_SmartFillFill;
	// 
	m_SmartFill.FillOnly = fillOnly;
#endif
}

// ��������� ����� ������ �������.
void CGPiton::StartFillMode(int flowRate, int targetVolume)
{
	ASSERT(targetVolume > 0);

	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_FILL << BYTE(0);
	message << short(flowRate) << short(targetVolume) << short(0);

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_FILL;
	m_TargetFlowRate = flowRate;

	CString string;
	string.Format(_T("SET_WORK_MODE FILL V=%d"), targetVolume);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

// ��������� ����� ����� �������.
void CGPiton::StartDischargeMode(int flowRate, int targetVolume)
{
	ASSERT(targetVolume > 0);

	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_DISCHARGE << BYTE(0);
	message << short(flowRate) << short(targetVolume) << short(0);

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_DISCHARGE;
	m_TargetFlowRate = flowRate;

	CString string;
	string.Format(_T("SET_WORK_MODE DISCHARGE V=%d"), targetVolume);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

// ��������� ����� ������ �������.
void CGPiton::StartWorkMode(int targetFlowRate, int initialPressure, int initialFlowRate)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_ISOCRATIC_WORK << BYTE(0);
	message << short(targetFlowRate);
	message << short(initialPressure) << short(initialFlowRate);

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_ISOCRATIC_WORK;
	m_TargetFlowRate = targetFlowRate;

	CString string;
	string.Format(_T("SET_WORK_MODE WORK FR=%d IP=%d IFR=%d"),
		targetFlowRate, initialPressure, initialFlowRate);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::StartPressureReleasingMode(int flowRate)
{
#if USE_P_R_MODE
	// ������ ������� ����� ��������, �� ������� ������� � ��������.
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_PRESSURE_RELEASING << BYTE(0);
	message << short(flowRate) << short(c_MaxPumpVolume) << short(0);
#else
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_FILL << BYTE(0);
	message << short(flowRate) << short(c_MaxPumpVolume) << short(0);
#endif

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_PRESSURE_RELEASING;
	m_TargetFlowRate = flowRate;

	CString string;
	string.Format(_T("SET_WORK_MODE P Release FR=%d"), flowRate);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::StopCurrentMode()
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_WAIT << BYTE(0);
	message << short(0) << short(0) << short(0);

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_WAIT;
	m_TargetFlowRate = 0;
	m_SmartFill.State = c_SmartFillNone;

	CString string;
	string.Format(_T("SET_WORK_MODE STOP"));
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

/*
void CGPiton::StartGradientStage(int stage)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_GRADIENT_WORK << BYTE(0);
	message << short(stage);
	message << short(0) << short(0);

	m_Manager.TargetMode = MANAGER_GRADIENT_WORK;

	CString string;
	string.Format(_T("SET_WORK_MODE GRAD St=%d"), stage);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}
*/

// ������ ��������� ����� ����������� ���������.
// ����� - � �������. ������������� ��� ��������������� �����.
void CGPiton::LoadGradientStage(int stage, int time, int beginFlowRate, int endFlowRate)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_PARAMETERS);
	message << MOTOR_UNIT << MU_SET_STAGE_DATA;
	message << BYTE(stage) << BYTE(time);
	message << short(beginFlowRate) << short(endFlowRate);

	CString string;
	string.Format(_T("SET_WORK_PARAMETERS GRAD St=%d t=%d BFR=%d EFR=%d"),
		stage, time, beginFlowRate, endFlowRate);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::StartGradientStage(int time, int beginFlowRate, int endFlowRate)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_GRADIENT_WORK << BYTE(time);
	message << short(beginFlowRate) << short(endFlowRate);

	CString string;
	string.Format(_T("SET_WORK_MODE GRAD t=%d BFR=%d EFR=%d"),
		time, beginFlowRate, endFlowRate);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::SetInternalValveState(BYTE targetState)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << MANAGER_SET_VALVE << targetState;
	message << short(0) << short(0) << short(0);

	m_Manager.Mode = MANAGER_COMMAND;
	m_Manager.TargetMode = MANAGER_SET_VALVE;

	CString string;
	string.Format(_T("SET_WORK_MODE VALVE %d"), targetState);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

// ���������, ��� �������� �����������������. ��� ����� ��� ������ �
// �����, ��� �������� ��������. � ������ ��������� �������, ��� ��� ������,
// ��������� ������� �������� ����������.
bool CGPiton::IsFlowRateOk()
{
	return m_Manager.TargetMode == MANAGER_GRADIENT_WORK
		|| (m_TargetFlowRate > 0 && abs(m_CurrentFlowRate)*4 > m_TargetFlowRate*3
		&& m_SmartFill.State == c_SmartFillNone);
}

void CGPiton::SetFlowRate(const int flowRate)
{
	SetWorkParameter(MOTOR_UNIT, MU_SET_FLOW, INT16(flowRate));
}

void CGPiton::SetPressureLimits(const int minPressureWork, const int maxPressureWork, const int maxPressureDischarge)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_PARAMETERS);
	message << MOTOR_UNIT << MU_SET_MIN_MAX_PRESSURE;
	message << short(minPressureWork) << short(maxPressureWork);
	message << short(maxPressureDischarge);

	m_MaxAllowedPressure = maxPressureDischarge;

	CString string;
	string.Format(_T("SET_WORK_PARAMETERS LIM Wmax=%d Wmin=%d Dmax=%d"),
		maxPressureWork, minPressureWork, maxPressureDischarge);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::SetPressureReleasing(const bool isAutomatic, const int releasingFlowRate)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_PARAMETERS);
	message << MOTOR_UNIT << MU_SET_FLOW_RATE_FOR_RELEASE_PRESSURE;
	message << short(isAutomatic) << short(releasingFlowRate);

	CString string;
	string.Format(_T("SET_WORK_PARAMETERS PR auto=%d FR=%d"),
		isAutomatic, releasingFlowRate);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

void CGPiton::AssignRoleB()
{
	// ��� ������ B ������������� ����������. �� ������������
	// ��� �������������� �������� ���� � ��������� ������ �� �������.
	m_InstrumentId = NET_ADDR_PITON+c_PumpSlaveB;
}

bool CGPiton::IsCanSlave()
{
	return m_IsCanSlave;
}

// ������������� ��� ��������� ����� � ������� ������� �� ���� CAN.
void CGPiton::ConnectSlavePump(bool connect)
{
	// ��������� ��������� � ��������������� 1.
	CGOutgoingMessage message(0, 1);
	message << BYTE(connect) << BYTE(0);

	CString string;
	string.Format(_T("CONNECT_SLAVE_PUMP %d "), connect);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

// ���������� ������������� �������� ������.
void CGPiton::ConnectSlavePump(CGPiton* pump)
{
	AddSlaveInstrument(pump);
	pump->m_IsCanSlave = true;
	pump->m_NetAddress = NET_ADDR_PITON+c_PumpSlaveB;

	// ������� ������.
	if(m_DataChannel->IsOpened())
	{
		ConnectSlavePump(true);
	}
}

void CGPiton::DisconnectSlavePump(CGPiton* pump)
{
	pump->StopCurrentMode();
	pump->m_IsCanSlave = false;
	pump->m_NetAddress = NET_ADDR_PITON+c_PumpMaster;

	if(m_DataChannel->IsOpened())
	{
		ConnectSlavePump(false);
	}
	RemoveSlaveInstruments();
}

#if defined PROJECT_STEND
void CGPiton::LockKeyboard(bool enable)
{
}

//#else

// ������/���������� ���������� �� ���������� �������
void CGPiton::LockKeyboard(bool enable)
{
	// ������ ���������� ��� ������� � �����
	// �������� ����������.
	BYTE mode = (enable ? EXTERNAL_CONTROL_MODE : INTERNAL_CONTROL_MODE);
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	
	message << mode;

	CString string;
	string.Format(_T("SET_WORK_MODE %d"), mode);
	SendOutgoingMessage(message, &string);

	TRACE(_T("<- ")+string+'\n');
}
#endif

// �������� ������� ������ ���������� ������
void CGPiton::AskSerialNumber()
{
// �������� �� ������ ����� �������, �� � ������ ������
	CGOutgoingMessage message(GetNetAddress(), GET_INSTRUMENT_INFO);
	message << BYTE(0);

	CString string = _T("GET_INSTRUMENT_INFO");
	SendOutgoingMessage(message, &string);

	TRACE(_T("<- ")+string+'\n');
}

// �������� ������� ������ ����������������.
void CGPiton::AskResourceCounter()
{
	AskParameter('MR');
}

void CGPiton::AskParameters()
{
	AskSerialNumber();
}

void CGPiton::AskParameter(UINT16 parameterID)
{
	CGOutgoingMessage message(GetNetAddress(), GET_PARAMETER);
	message << parameterID;
	m_AskedParameter = parameterID;
	m_AskParameterStatus = c_ParameterWaitAnswer;

	CString string;
	string.Format(_T("GET_PARAMETER %c%c"), BYTE(parameterID>>8), BYTE(parameterID));

	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

void CGPiton::SetParameter(UINT16 parameterID, INT32 value)
{
	CGOutgoingMessage message(GetNetAddress(), SET_PARAMETER);
	message << UINT32(parameterID) << value;
	//m_AskedParameter = parameterID;

	CString string;
	string.Format(_T("SET_PARAMETER %c%c Value=%d"), 
		BYTE(parameterID>>8), BYTE(parameterID), value);

	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

/*
// ����������� ������ ������ ������������ ������������
void CGPiton::AskNodesVersionInfo()
{
//	AskCanNodeInfo(D2M_NODE_ADDR, 'V');		// Version
}
*/

void CGPiton::AskWorkParameter(BYTE logicalUnit, BYTE parameterID)
{
	CGOutgoingMessage message(GetNetAddress(), GET_WORK_PARAMETER);
	message << logicalUnit << parameterID;

	CString string;
	string.Format(_T("GET_WORK_PARAMETER %d %d"), logicalUnit, parameterID);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

void CGPiton::SetWorkParameter(BYTE logicalUnit, BYTE parameterID, INT16 value)
{
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_PARAMETER);
	message << logicalUnit << parameterID << value;

	CString string;
	string.Format(_T("SET_WORK_PARAMETER %d %d v=%d"), logicalUnit, parameterID, value);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}
/*
// ������ ����������, ����������� � ������������� ���� �������
void CGPiton::AskCanNodeInfo(BYTE nodeAddress, BYTE infoID)
{
	CGOutgoingMessage message(GetNetAddress(), GET_CAN_NODE_INFO);
	message << nodeAddress << infoID;

	CString string;
	string.Format(_T("GET_CAN_NODE_INFO Addr=%d Page=%c"), nodeAddress, infoID);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

// ������ ����������, ����������� � ������������� ���� �������
void CGPiton::SendCanNodeCommand(BYTE nodeAddress, BYTE *canData)
{
	CGOutgoingMessage message(GetNetAddress(), SET_CAN_NODE_INFO);
	message << nodeAddress << BYTE(0);

	for(int i = 0; i < 8; i++)
	{
		message << canData[i];
	}

	CString string;
	string.Format(_T("SET_CAN_NODE_INFO Addr=%d Page=%c"), nodeAddress, canData[0]);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

// ���������� ������ ������������ ������� ������� �� ������������ �����
void CGPiton::EnableCanTracing(BYTE nodeAddress, BYTE flag)
{
	CGOutgoingMessage message(GetNetAddress(), ENABLE_CAN_TRACING);
	message << nodeAddress << flag;

	CString string;
	string.Format(_T("ENABLE_CAN_TRACING Addr=%d 0x%x"), nodeAddress, flag);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}
*/

/*
// ���������� ������������� ������ ��������� �� ������ � ������ �����
int CGPiton::GetErrorMessageID(int errorCode)
{
	switch(errorCode)
	{
	case c_StepperLowLimit:
	case c_StepperHighLimit:
		return IDS_ERROR_INVALID_WAVE_LENGTH;
// "����� ����� ��������� �� ��������� ��������� �������������"
	}
	return 0;
}
*/

// ������ ���������������� ������ ������
void CGPiton::StartDiagnosticMode(BYTE mode)
{
	CGOutgoingMessage message(GetNetAddress(), SET_DIAGNOSTIC_MODE);
	message << mode;

	CString string;
	string.Format(_T("SET_DIAGNOSTIC_MODE Mode=%d"), mode);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+'\n');
}

// ������ ������ �����
int CGPiton::GetResourceCounter()
{
	return m_ResourceCounter;
}

bool CGPiton::ProcessIncomingMessage(CGIncomingMessage& message)
{
	BYTE instrumentID = message.GetInstrumentID();
	BYTE messageID = message.GetMessageID();

	ASSERT(instrumentID == m_NetAddress);

// ��������� �� ����������, ������������ ������ ��������� � ��������� �������
	if(m_MeasurementState == c_PassiveState)
	{
	}

	m_IncomingMessageLogString.Empty();

	switch(messageID)
	{
	case GET_INSTRUMENT_INFO:
		DoOnInstrumentInfo(message);
		break;
	case SET_WORK_MODE:
		m_IncomingMessageLogString.Format(_T("SET_WORK_MODE(%d)"), message.GetMessageID());
		break;
	case SET_WORK_PARAMETER:
		DoOnSetWorkParameter(message);
		break;
	case GET_WORK_PARAMETER:
		DoOnGetWorkParameter(message);
		break;
	case MEASUREMENT_RESULT:
		DoOnMeasuredDataMessage(message);
		break;
	case INSTRUMENT_STATE_INFO:
		DoOnInstrumentStateInfo(message);
		break;
	case ERROR_INFO:
		DoOnErrorMessage(message);
		break;
	case EVENT_INFO:
		/*
		{
			// ������� ������ ESCAPE �� �������
			// �������� ������� ���������
			BYTE scanCode = *(message.GetData()+1);
			if(scanCode == 5 && m_Manager.TargetMode != MANAGER_WAIT)
			{
				StopCurrentMode();
			}
		}
		*/
		break;
	case CAN_NODE_PACKET:
//		DoOnCanNodePacket(message);
		break;
	case GET_PARAMETER:
		DoOnGetParameter(message);
		break;

	case GET_DIAGNOSTIC_INFO:
		DoOnGetDiagnosticInfo(message);
		break;

	case GET_CAN_NODE_INFO:
		DoOnGetCanNodeInfoMessage(message);
		break;
	case SYSTEM_STATISTIC_INFO:
		DoOnSysemStatisticInfoMessage(message);
		break;

	default:
//		if(m_IsDiagnosticEnabled)
//			ProcessDiagnosticMessage(message);
		break;
	}

// ����� ������ �������� � m_IncomingMessageLogString �� ��������,
// ���������� ���������� �� �� ��� ��������� ������
	TRACE(_T("-> %s\n"), m_IncomingMessageLogString);

// ���������� ������ � ���� ��������� (���� ��� ������)
	WriteDebugInfo(_T("-> ")+m_IncomingMessageLogString+_T("\n"));
	return true;
}

// ���������� ���������� ���������
void CGPiton::DoOnSystemMessage(CGIncomingMessage& message)
{
/*
	if(message.GetDataSize() > sizeof(m_InstrumentCommInfo))
	{
		message >> m_MasterNetAddress;
		message >> m_InstrumentCommInfo;
	}
*/
	// TODO !!!! USB !!!
	UINT16* data = (UINT16*)message.GetData();

	m_InstrumentCommInfo.m_DataPacketFormatError = data[1];
	m_InstrumentCommInfo.m_DataPacketCrcError = data[0];
	m_InstrumentCommInfo.m_WriteBufferOverflowError = data[9];


	m_IncomingMessageLogString.Format(_T("COMM_STATISTIC_INFO D_CRC %d D_L %d A_CRC %d BF %d RTx %d RBF %d Ovf %d Sync %d TxQ %d FN %d"),
		data[0], data[1], data[2], data[5],
		data[7], data[8],
		data[3], data[4],
		data[6], data[9]
		);
	// data[5] - ���������� ������
	// data[7] - ������� ��������� �������
	// data[8] - ���������� ������ ��� ��������� ��������

// ���������� ������ � ���� ��������� (���� ��� ������)
	WriteDebugInfo(_T("-> ")+m_IncomingMessageLogString+_T("\n"));
}

void CGPiton::DoOnInstrumentInfo(CGIncomingMessage& message)
{
	int oldSerialNumber = m_SerialNumber;
	BYTE hardwareMask;

	// ��� �������
	message >> m_Type;
	// ����� ������ �������� � ������
	message >> m_MainControllerVersion.Software;
	message >> m_MainControllerVersion.Subversion;
	message >> m_MainControllerVersion.Hardware;
	message >> m_MainControllerVersion.Year;
	message >> m_MainControllerVersion.Month;
	message >> m_MainControllerVersion.Day;
	message >> hardwareMask;

	// ��������� �����
	//message >> m_SerialNumber;

	m_InstrumentState = c_InstrumentInfoChanged;
		
	m_IncomingMessageLogString.Format(_T("GET_INSTRUMENT_INFO(%d) Type=%d Ver=%d.%d.%d %02d-%02d-%02d N=%d"),
		message.GetMessageID(),
		m_Type,
		m_MainControllerVersion.Software,
		m_MainControllerVersion.Subversion,
		m_MainControllerVersion.Hardware,
		m_MainControllerVersion.Year,
		m_MainControllerVersion.Month,
		m_MainControllerVersion.Day,
		m_SerialNumber
		);
}

void CGPiton::DoOnGetWorkParameter(CGIncomingMessage& message)
{
	BYTE logicalUnit;
	BYTE parameterID;
	INT16 value;

	message >> logicalUnit >> parameterID >> value;
	switch(logicalUnit)
	{
	case MOTOR_UNIT:
	/*
		if(parameterID == PU_FREQUENCY)
		{
			m_MeasurementFrequency = value;
		}
	*/
		break;
	case MAIN_CONTROLLER_UNIT:
		break;
	}
	

	m_IncomingMessageLogString.Format(_T("GET_WORK_PARAMETER(%d) Unit= %d Param=%d Value=%d"),
		message.GetMessageID(),
		logicalUnit, parameterID, value);
}

void CGPiton::DoOnSetWorkParameter(CGIncomingMessage& message)
{
	BYTE logicalUnit;
	BYTE parameterID;
	INT16 value;

	message >> logicalUnit >> parameterID >> value;
	if(logicalUnit == MOTOR_UNIT && parameterID == MU_ACTIVATE_STAGE)
	{
//		m_MeasurementFrequency = value;
	}

	m_IncomingMessageLogString.Format(_T("SET_WORK_PARAMETER(%d) Unit= %d Param=%d Value=%d"),
		message.GetMessageID(),
		logicalUnit, parameterID, value);
}

void CGPiton::DoOnMeasuredDataMessage(CGIncomingMessage& message)
{
//	TRACE("Instrument state = %d\n", m_MeasurementState);
//	long photoChannel, refChannel;
	struct CSPumpData
	{
		BYTE MotorState;
		BYTE ValveState;
		UINT16 CurrentVolume;
		INT16 CurrentFlowRate;
		UINT16 CurrentPressure;
	};

	CSPumpData* pumpData = (CSPumpData*)message.GetData();

	m_CurrentState = pumpData->MotorState;
	m_FoamSensorState = (m_CurrentState >> 4);
	m_CurrentState &= 0x0F;
	m_InternalValveState = pumpData->ValveState;
	m_CurrentVolume = pumpData->CurrentVolume;
	m_CurrentFlowRate = pumpData->CurrentFlowRate;
	m_CurrentPressure = pumpData->CurrentPressure;

	m_InstrumentState = c_PumpStateReceived;
	if(m_TargetFlowRate < 0)
	{
		// m_TargetFlowRate ������������ ������ � ������ ������� ���������.
		m_TargetFlowRate = abs(m_CurrentFlowRate);
	}

	m_IncomingMessageLogString.Format(_T("MEASUREMENT_RESULT(%d) MState=%02x VState=%02x V=%d FR=%d P=%d"),
		message.GetMessageID(),
		pumpData->MotorState,
		pumpData->ValveState,
		pumpData->CurrentVolume,
		pumpData->CurrentFlowRate, pumpData->CurrentPressure);
}

void CGPiton::DoOnInstrumentStateInfo(CGIncomingMessage& message)
{
/*	BYTE logicalUnit;
	BYTE unitState;
	CString string;
*/
	struct CSPumpState
	{
		BYTE  Unit;
		BYTE  UnitMode;
		BYTE  IsExternalValveOn;
		BYTE  GradientStage;
		INT32 ModeTime;
	};
	
	CSPumpState* pumpState = (CSPumpState*)message.GetData();

	switch(pumpState->Unit)
	{
	case MAIN_CONTROLLER_UNIT:
		m_Manager.Time = pumpState->ModeTime;
		if(m_Manager.Mode < MANAGER_WAIT)
		{
			// ��������� ������ ��� ��������.
			if(m_TargetFlowRate >= 0)
			{
				// ��������� ������ � ������� �������� ������� ��� ��������.
				m_Manager.Mode = pumpState->UnitMode;
			}
		}
		else
		{
			if(pumpState->UnitMode == MANAGER_WAIT)
			{
				// ����� ����������� ������ �������������� �������������� ��������.
				if(m_Manager.Mode == m_Manager.TargetMode)
				{
					// ������������ ��� �������� ��������� ��������.
					m_Manager.TargetMode = MANAGER_WAIT;
				}
				// ����� � ���������� MANAGER_WAIT, ���������� � ������ ���������� �������.
				if(	m_Manager.Mode == MANAGER_COMMAND)
				{
					pumpState->UnitMode = MANAGER_EXECUTE;
				}
				// �������� �������� �����.
				if(	m_Manager.Mode == MANAGER_DISCHARGE)
				{
					// �� ��� ����� ������ �� ������� �������.
					if(m_SmartFill.State == c_SmartFillNone)
					{
						// � ���� ��������� ����� 5 ������ ���� ����� �������� � ��������� "������"
						m_SmartFill.FillOnly = true;
						m_SmartFill.State = c_SmartFillWait;
					}
			}
			}

			m_Manager.Mode = pumpState->UnitMode;
			m_InstrumentState = c_PumpModeReceived;
		}
		if(m_SmartFill.State != c_SmartFillNone)
		{
			CheckSmartFillState();
		}

		if(m_GradientProgram != 0)
		{
			if(m_GradientProgram->Check(pumpState->ModeTime))
			{
				m_InstrumentState = c_PumpProgramStageChanged;
			}
		}

		// ������� ����� �����-��������
		if(pumpState->IsExternalValveOn && !m_IsExternalValveOn)
		{
			// ���������� ����� ������� �������� ������������������ ���������
			// � CMChromatographicFrame::OnCommandExternalStart()
			m_InstrumentState = c_ExternalMeasurementStart;
		}
		// �������� ������� ����� �����-��������
		if(m_IsExternalValveOn && !pumpState->IsExternalValveOn)
		{
			// ������� �� ��� ������� ������������ �
			// � CMChromatographicFrame::OnCommandExternalStop()
			m_InstrumentState = c_ExternalMeasurementStop;
		}
		m_IsExternalValveOn = (pumpState->IsExternalValveOn != 0);

		m_IncomingMessageLogString.Format(_T("INSTRUMENT_STATE_INFO(%d) Unit= %d Mode=%d v = %d, T=%d"),
			message.GetMessageID(),
			pumpState->Unit, pumpState->UnitMode,
			pumpState->IsExternalValveOn, pumpState->ModeTime);
		break;
	case MOTOR_UNIT:
		break;
	}
}

// ������� ��������� ���������, ����������� ��� ����������� � ��
// �������� �� ���������� ��������
bool CGPiton::DoOnSysemStatisticInfoMessage(CGIncomingMessage& message)
{
	BYTE type;

	message >> type;
#if 0
	switch(type)
	{
	case SYS_STAT_CPU_USAGE:
		{
			BYTE cpuUsage, skip;
			UINT32 taskSwitchsPerSecond;
			message >> cpuUsage >> skip >> taskSwitchsPerSecond;
			m_IncomingMessageLogString.Format(_T("SYSTEM_STATISTIC_INFO(%d) type=C CPU=%d%% TS=%d"),
				message.GetMessageID(),
				cpuUsage, taskSwitchsPerSecond);
		}
		break;
	case SYS_STAT_QUEUE_USAGE:
		m_IncomingMessageLogString.Format(_T("SYSTEM_STATISTIC_INFO(%d) type=Q"),
			message.GetMessageID());
		DumpMessage(message, 1);
		break;
	case SYS_STAT_STACK_USAGE:
		{
			BYTE taskIndex, taskPriority, skip;
			UINT16 stackUsed;
			message >> taskIndex >> taskPriority >> skip >> stackUsed;
			m_IncomingMessageLogString.Format(_T("SYSTEM_STATISTIC_INFO(%d) type=S Task=%d Prio=%d Free=%d"),
				message.GetMessageID(),
				taskIndex, taskPriority, stackUsed);
		}
		break;
	}
#endif
		m_IncomingMessageLogString.Format(_T("SYSTEM_STATISTIC_INFO(%d)"),
			message.GetMessageID()
			);

	return true;
}

// ����� ���� ���� ���� ���������
void CGPiton::DumpMessage(CGIncomingMessage& message, int start)
{
	CString string;
	BYTE value;
	for(int i = start; i < message.GetDataSize(); i++)
	{
		message >> value;
		string.Format(_T(" %d"), value);
		m_IncomingMessageLogString += string;
	}
}

void CGPiton::DoOnErrorMessage(CGIncomingMessage& message)
{
/*
	BYTE errorCode, flag = 0;
	short value = 0;
	message >> errorCode;
	message >> flag >> value;
	m_ErrorCode = errorCode;
	if((flag & ERROR_FLAG_EXTENDED) != 0)
	{
		m_ErrorCode |= (1 << 8) | (value << 16);
	}

	m_IncomingMessageLogString.Format(_T("ERROR(%d) N=%d f %d v %d"),
		message.GetMessageID(),
		errorCode, flag, value);
*/
	CSErrorInfo* errorInfo = (CSErrorInfo*)message.GetData();

	// �������������� �������� ���� �� ������������.
	m_ErrorCode = errorInfo->ErrorCode;
	//m_LastErrorInfo = *errorInfo;

	m_IncomingMessageLogString.Format(_T("ERROR(%d) N=%d.%d Type=%d Length=%d Value=%d"),
		message.GetMessageID(),
		errorInfo->ErrorCode >> 8, 
		errorInfo->ErrorCode & 0xFF, 
		errorInfo->Type,
		errorInfo->Length,
		errorInfo->Value);
}

void CGPiton::DoOnGetCanNodeInfoMessage(CGIncomingMessage& message)
{
	//BYTE nodeAddress = *message.GetData();
	//BYTE result = *(message.GetData()+1);
	const BYTE* canData = message.GetData()+2;

	//ASSERT(result != 0);
	//DoOnD2MNodeInfoMessage(canData);
}
/*
void CGPiton::DoOnCanNodePacket(CGIncomingMessage& message)
{
	struct CSCanFrame
	{
		UINT16 StandardID;
		BYTE Type;
		BYTE Dlc;
		BYTE Data[8];
		UINT32 TimeStamp;
	};

	CSCanFrame* canFrame = (CSCanFrame*)message.GetData();
	const BYTE* canData = canFrame->Data;

	if(canFrame->Type == CAN_TRACE_STATUS_FLAG)
	{
		if(canData[1] == 'D')
		{
	// ��������� �����
			DoOnD2LampStateInfoMessage(canData);
		}
		else if(canData[1] == 'M')
		{
	// ��������� �������������
			DoOnMonochomatorStateInfoMessage(canData);
		}
	}

	m_IncomingMessageLogString.Format(_T("TRACE_CAN_FRAME(%d) Id=%02x Type=%d DLC=%d Data: %02x %02x %02x %02x %02x %02x %02x %02x Time=%d"),
		message.GetMessageID(),
		canFrame->StandardID,
		canFrame->Type,
		canFrame->Dlc,
		canFrame->Data[0],
		canFrame->Data[1],
		canFrame->Data[2],
		canFrame->Data[3],
		canFrame->Data[4],
		canFrame->Data[5],
		canFrame->Data[6],
		canFrame->Data[7],
		canFrame->TimeStamp
		);
}
*/

void CGPiton::DoOnGetParameter(CGIncomingMessage& message)
{
	UINT16 requestID, resultID;
	INT32 value;

	message >> requestID;
	message >> resultID;
	message >> value;

	switch(resultID)
	{
	case 'MR':
		m_ResourceCounter = value;
		break;
	}

// ���� �������� ������� � ���������� �������, ������������� ������� ����������.
	if(requestID == m_AskedParameter)
	{
		m_AskedParameter = -1;
		m_AskedParameterValue = value;

		m_AskParameterStatus = (resultID != 0 ? c_ParameterIsValid : c_ParameterIsInvalid); 
	}

	m_IncomingMessageLogString.Format(_T("GET_PARAMETER(%d) Param=%c%c Value=%d"),
		message.GetMessageID(),
		BYTE(requestID>>8), BYTE(requestID), value);
}

/*
// ������ �� ��������� ���������� ������
void CGPiton::AskErrorStatistic()
{
	CGOutgoingMessage message(GetNetAddress(), GET_DIAGNOSTIC_INFO);
	
	message << DIAGNOSTIC_GET_ERROR_COUNTERS;
	// ������������ ��� ������� �������������� �������
	m_ErrorStatistic[0] = 0;

	CString string;
	string.Format(_T("GET_DIAGNOSTIC_INFO %d"), DIAGNOSTIC_GET_ERROR_COUNTERS);
	SendOutgoingMessage(message, &string);

	TRACE(_T("<- ")+string+'\n');
}
*/

void CGPiton::DoOnGetDiagnosticInfo(CGIncomingMessage& message)
{
/*
	memcpy(m_ErrorStatistic, message.GetData(), message.GetDataSize());

	m_IncomingMessageLogString.Format(_T("GET_DIAGNOSTIC_INFO(%d) id=%d"),
		message.GetMessageID(),
		m_ErrorStatistic[0]);
*/
}
void CGPiton::SetGradientProgram(CGGradientProgram* program)
{
	m_GradientProgram = program;
	//m_SlavePump = slavePump;
}

void CGPiton::StartGradientProgram()
{
	ASSERT(m_GradientProgram != 0);
	//ASSERT(m_SlavePump != 0);
}

void CGPiton::CheckProgramState()
{
}

// ���������� ������� ������ � ����������� ��������.
void CGPiton::CheckSmartFillState()
{
	const int c_NonZeroPressure  = 1;
	const int c_NonZeroFlowRate  = 10;
	const int c_WorkModeFlowRateFast = 2000;
	const int c_WorkModeFlowRateSlow = 400;
	const int c_WorkModeFlowRateMiddle = 1000;
	const int c_WorkModeTimeFast     = 3;
	const int c_WorkModeTimeSlow     = 15;
	const int c_WaitAfterFillTime    = 5;
	const int c_WaitAfterWorkTime    = 2;

	TRACE(_T("SmartFill %d %d %d\n"), m_SmartFill.State, m_Manager.Mode, m_Manager.Time);
	switch(m_SmartFill.State)
	{
	case c_SmartFillNone:
		return;
	case c_SmartFillFill:
		// ���������� �������� ����, ��� ����� �������� ���������� �� ��������� �������� ������.
		if(IsInWaitMode())
		{
			m_SmartFill.State = c_SmartFillWait;
			m_InstrumentState = c_PumpSmartFillWait;
		}
		break;
	case c_SmartFillWait:
		if(m_Manager.Time >= c_WaitAfterFillTime)		// 5 ��� �� ������������ �������� � �������
		{
			if(m_SmartFill.FillOnly)
			{
				m_SmartFill.State = c_SmartFillStop;
				// ������� ����� � ��������� ������.
				//SetInternalValveState(VALVE_STATE_WORK);
			}
			else
			{
				// ��� ����� � ��������� ������ ��������� �� �������� 2000 ���/��� �� ���������� �������� 0.1 ����.
				StartWorkMode(c_NonZeroFlowRate, c_NonZeroPressure, c_WorkModeFlowRateFast);
				m_SmartFill.State = c_SmartFillWorkFast;
				m_InstrumentState = c_PumpSmartFillWork;
			}
		}
		break;
	case c_SmartFillWorkFast:
		// ����� �������� �����.
		//if(m_Manager.Mode == MANAGER_SET_INITIAL_PRESSURE)
		{
			if(m_Manager.Time > c_WorkModeTimeFast)
			{
				StartWorkMode(c_NonZeroFlowRate, c_NonZeroPressure, c_WorkModeFlowRateMiddle);
				m_SmartFill.State = c_SmartFillWorkSlow2;
				break;
			}
		}
		// break �����������.
	case c_SmartFillWorkSlow1:
	case c_SmartFillWorkSlow2:
	case c_SmartFillWorkSlow3:
	case c_SmartFillWorkMaxSpeed:
		// ���������� ����� ������ �������� � ������� � ����� ������. ������ �������� �������.
		if(m_Manager.Mode == MANAGER_ISOCRATIC_WORK)
		{
			StopCurrentMode();
			m_SmartFill.State = c_SmartFillWait2;
		}
		else
		{
			if(m_SmartFill.State == c_SmartFillWorkSlow1)
			{
				if(m_Manager.Time > c_WorkModeTimeSlow)
				{
					// ��� ����� � ��������� ������ ����������� ��������. 
					StartWorkMode(c_NonZeroFlowRate, c_NonZeroPressure, c_WorkModeFlowRateMiddle);
					m_SmartFill.State = c_SmartFillWorkSlow2;
				}
			}
			if(m_SmartFill.State == c_SmartFillWorkSlow2)
			{
				if(m_Manager.Time > c_WorkModeTimeSlow)
				{
					// ��� ����� � ��������� ������ ����������� ��������. 
					StartWorkMode(c_NonZeroFlowRate, c_NonZeroPressure, c_WorkModeFlowRateFast);
					m_SmartFill.State = c_SmartFillWorkSlow3;
				}
			}
			if(m_SmartFill.State == c_SmartFillWorkSlow3)
			{
				if(m_Manager.Time > c_WorkModeTimeSlow*3)
				{
					/*
					// ��� ����� � ��������� ������ ����������� �������� �� ������������. 
					StartWorkMode(c_NonZeroFlowRate, c_NonZeroPressure, c_MaxFlowRate);
					m_SmartFill.State = c_SmartFillWorkMaxSpeed;
					*/
					// �������� ��� � �� ������ ����� - �������� � �����
					StopCurrentMode();
					m_SmartFill.State = c_SmartFillWait3;
					// ����������� ������������ � �������� � �������.
					// ��������, ������� ������������ ����. ��� ������.
					m_InstrumentState = c_PumpSmartFillAirInCamera;
				}
			}
			// ������� ����������� ��� ���������� �������� "�����".
			if(m_Manager.Mode == MANAGER_WAIT)
			{
				m_SmartFill.State = c_SmartFillStop;
			}
		}
		break;
	case c_SmartFillWait2:
		if(IsInWaitMode() && m_Manager.Time >= c_WaitAfterWorkTime)		// 2 ��� �� �������� ��������� ��������
		{
			m_SmartFill.State = c_SmartFillFoamFast;
			StartDischargeMode(c_DischargeFlowRate, c_MaxPumpVolume);
		}
		break;
	case c_SmartFillWait3:
		if(IsInWaitMode() && m_Manager.Time >= c_WaitAfterWorkTime)		// 2 ��� �� �������� ��������� ��������
		{
			// ������� ���� � ��������� �������.
			m_SmartFill.State = c_SmartFillFoamFast1;
			StartDischargeMode(c_DischargeFlowRate, c_MaxPumpVolume);
		}
		break;
	case c_SmartFillFoamFast:
		if(m_Manager.Mode == MANAGER_DISCHARGE)
		{
			if(m_Manager.Time > 2)		// 2 ��� �� ���� �� ������������ ��������.
			{
				m_SmartFill.State = c_SmartFillFoamSlow;
				m_SmartFill.TimeCounter = 0;
				StartDischargeMode(2000, c_MaxPumpVolume);
			}
		}
		break;
	case c_SmartFillFoamSlow:
	case c_SmartFillFoamFast1:
		if(m_Manager.Mode == MANAGER_DISCHARGE)
		{
			// ������������ ������� �������.
			if(m_FoamSensorState == 0)
			{
				if(m_SmartFill.TimeCounter > 0)
				{
					StopCurrentMode();
					m_SmartFill.State = c_SmartFillStop;
				}
				m_SmartFill.TimeCounter++;
			}
			else
			{
				m_SmartFill.TimeCounter = 0;
			}

			// ������� ����������� ��� ���������� �������� "�����".
			if(m_Manager.Mode == MANAGER_WAIT)
			{
				m_SmartFill.State = c_SmartFillStop;
			}
		}
		break;
	case c_SmartFillStop:
		if(IsInWaitMode())
		{
			m_SmartFill.State = c_SmartFillFinish;
			// ������� ����� � ��������� ������.
			SetInternalValveState(VALVE_STATE_WORK);
		}
		break;
	case c_SmartFillFinish:
		if(IsInWaitMode())
		{
			m_SmartFill.State = c_SmartFillNone;
			// ����������� � ���������� ��������� ������.
			m_InstrumentState = c_PumpSmartFillStopped;
		}
		break;

	// ��� �������� ���������� ������������ ������, ����� ��������� ���� ����� �����.
	case c_SmartFillDischarge:
		if(IsInWaitMode())
		{
			// � ���� ��������� ����� 5 ������ ���� ����� �������� � ��������� "������"
			m_SmartFill.FillOnly = true;
			m_SmartFill.State = c_SmartFillWait;
		}
		break;
	}
}

bool CGPiton::IsSmartFillFill()
{
	return (m_SmartFill.State == c_SmartFillFill);
}

bool CGPiton::IsSmartFillWait()
{
	return (m_SmartFill.State == c_SmartFillWait);
}

bool CGPiton::IsSmartFillWork()
{
	return (m_SmartFill.State == c_SmartFillWorkFast);
}

bool CGPiton::IsSmartFillAirFound()
{
	return (m_SmartFill.State == c_SmartFillFoamFast1);
}

bool CGPiton::IsSmartFillStopped()
{
	return (m_SmartFill.State == c_SmartFillNone);
}


// �������� ������� ���������� ������.
void CGPiton::CheckFilledVolume()
{
	// TODO �������� ���������, ���� ������� ������ ������ ����������.
}
