#include "StdAfx.h"
#include "Thermostat.h"
#include "UsbDevices.h"


CGThermostat::CGThermostat(void)
{
	m_NetAddress = NET_ADDR_THERMO;
	m_InstrumentId = NET_ADDR_THERMO;
	m_InstrumentModel = (NET_ADDR_THERMO << 8);
	m_SerialNumber = -1;
	m_InternalTimeUnitToSec = 1;

	m_UsbVendorID = c_UsbVendorId_Lumex;
	m_UsbProductID = c_UsbProductId_Thermostat;

	m_TargetTemperature = 0;
}

CGThermostat::~CGThermostat(void)
{
}

void CGThermostat::AskSerialNumber()
{
	// �������� �� /*������*/ ����� �������, /*�� �*/ a ������ ������
	CGOutgoingMessage message(GetNetAddress(), GET_INSTRUMENT_INFO);
	// ��� ����������� ������� ����, ��� ������ �������� ���������������
	// ���������� � �� ���� ���������� ��������������� ��������� ����������.
	//message << BYTE(1);

	CString string = _T("GET_INSTRUMENT_INFO");
	SendOutgoingMessage(message, &string);

	TRACE(_T("<- ")+string+'\n');
}


void CGThermostat::SetWorkTemperature(int workTemperature)
{
	bool switchOn = (workTemperature > 0);
	CGOutgoingMessage message(GetNetAddress(), SET_WORK_MODE);
	message << short(switchOn);
	message << short(workTemperature);

	CString string;
	string.Format(_T("SET_WORK_MODE T=%d"), workTemperature);
	SendOutgoingMessage(message, &string);
	TRACE(_T("<- ")+string+_T("\n"));
}

bool CGThermostat::ProcessIncomingMessage(CGIncomingMessage& message)
{
	BYTE instrumentID = message.GetInstrumentID();
	BYTE messageID = message.GetMessageID();

	ASSERT(instrumentID == m_NetAddress);

	m_IncomingMessageLogString.Empty();

	switch(messageID)
	{
	case GET_INSTRUMENT_INFO:
		DoOnInstrumentInfo(message);
		break;
	case SET_WORK_MODE:
		m_IncomingMessageLogString.Format(_T("SET_WORK_MODE(%d)"), message.GetMessageID());
		break;
	case INSTRUMENT_STATE_INFO:
		DoOnInstrumentStateInfo(message);
		break;
	case ERROR_INFO:
		DoOnErrorMessage(message);
		break;
	default:
//		if(m_IsDiagnosticEnabled)
//			ProcessDiagnosticMessage(message);
		break;
	}

// ����� ������ �������� � m_IncomingMessageLogString �� ��������,
// ���������� ���������� �� �� ��� ��������� ������
	TRACE(_T("-> %s\n"), m_IncomingMessageLogString);

// ���������� ������ � ���� ��������� (���� ��� ������)
	WriteDebugInfo(_T("-> ")+m_IncomingMessageLogString+_T("\n"));
	return true;
}

void CGThermostat::DoOnInstrumentStateInfo(CGIncomingMessage& message)
{
/*	BYTE logicalUnit;
	BYTE unitState;
	CString string;
*/
	struct CSThermoStateInfo
	{
		BYTE  Mode;
		BYTE  State;
		short TargetTemperature;
		short WorkTemperature;
		short RoomTemperature;
	};

	CSThermoStateInfo* thermoStateInfo = (CSThermoStateInfo*)message.GetData();
	m_TargetTemperature = thermoStateInfo->TargetTemperature;
	m_WorkTemperature = thermoStateInfo->WorkTemperature;
	m_RoomTemperature = thermoStateInfo->RoomTemperature;

	// ���������� UI.
	m_InstrumentState = c_ThermostatStateReceived;

	m_IncomingMessageLogString.Format(_T("THERMO_STATE(%d) DevMode=0x%02x DevState=0x%02x TargetT=%d WorkT=%d AirT=%d"),
		message.GetMessageID(),
		thermoStateInfo->Mode,
		thermoStateInfo->State,
		thermoStateInfo->TargetTemperature,
		thermoStateInfo->WorkTemperature,
		thermoStateInfo->RoomTemperature
		);
}

void CGThermostat::DoOnInstrumentInfo(CGIncomingMessage& message)
{
	int oldSerialNumber = m_SerialNumber;
	BYTE hardwareMask;
	BYTE type;

	// ��� �������
	message >> type;
	// ����� ������ �������� � ������
	message >> m_Version.Software;
	message >> m_Version.Subversion;
	message >> m_Version.Hardware;
	message >> m_Version.Year;
	message >> m_Version.Month;
	message >> m_Version.Day;
	message >> hardwareMask;

	// � ��������� �������� ����� ���������� �����. ��� �������� ������ ������ ��� ��.
	m_SerialNumber = GetUsbSerialNumber();
	if(m_SerialNumber != oldSerialNumber)
		m_InstrumentState = c_InstrumentInfoChanged;

	m_IncomingMessageLogString.Format(_T("GET_INSTRUMENT_INFO(%d) Type=%d Ver=%d.%d.%d %02d-%02d-%02d N=%d"),
		message.GetMessageID(),
		type,
		m_Version.Software,
		m_Version.Subversion,
		m_Version.Hardware,
		m_Version.Year,
		m_Version.Month,
		m_Version.Day,
		m_SerialNumber
		);
}
void CGThermostat::DoOnErrorMessage(CGIncomingMessage& message)
{
	CSErrorInfo* errorInfo = (CSErrorInfo*)message.GetData();

	// �������������� �������� ���� �� ������������.
	m_ErrorCode = errorInfo->ErrorCode;

	m_IncomingMessageLogString.Format(_T("ERROR(%d) N=%d.%d Type=%d Length=%d Value=%d"),
		message.GetMessageID(),
		errorInfo->ErrorCode >> 8, 
		errorInfo->ErrorCode & 0xFF, 
		errorInfo->Type,
		errorInfo->Length,
		errorInfo->Value);
}

int CGThermostat::GetSerialNumber()
{
	return 1;
}

int CGThermostat::GetSoftVersionNumber()
{
	return 1;
}

int CGThermostat::GetHardVersionNumber()
{
	return 0;
}

