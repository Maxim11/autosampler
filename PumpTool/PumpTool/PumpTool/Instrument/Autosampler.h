#pragma once
#include "InstrumentBase\MasterInstrument.h"
#include <string>
#include <map>
#include <functional>

// ������������� �������.
const BYTE NET_ADDR_AUTOSAMPLER = '$';

// �������, ���������� �� ���������� � ������
const BYTE GET_INSTRUMENT_INFORM = 1;
const BYTE SET_WORKING_MODE = 2;

// C��������, ���������� ��������
const BYTE INSTRUMENT_STATE_INFORM = 16;
const BYTE ERROR_INFORM = 17;

using namespace std;

class CGAutosampler :
	public CGMasterInstrument
{
public:
	CGAutosampler(void);
	~CGAutosampler(void);

	bool IsWorking() { return (m_targetVial > 0); }
	void chkCalc(char* ps, char* sumar);
	void StartWashExecution(int targetVial, int wasteVial, int washVolume, char* washOn);
	int StatusWashExecution();

	void AskSerialNumber();

	// �������, ������� ������ ���������������� ��� ����������� �������� �������
	virtual bool ProcessIncomingMessage(CGIncomingMessage& message);
	typedef void (CGAutosampler::* funcPointer)(CGIncomingMessage&);
	map <string, funcPointer> opCode;

public:
	// ����������� ������ ������� �������.
// ��������� ���������� � ������ � ������������ �������
	virtual int GetSerialNumber();
	virtual int GetSoftVersionNumber();
	virtual int GetHardVersionNumber();
	// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
	virtual const CString GetShortName() const { return _T("Autosampler"); };

	// ��������� ����������� ���������
	virtual bool GetMeasuredValues(double values[]) { return false; }
	// �������� ������� ������� ������ ��������� ������
	virtual bool ResetErrorState() { return true; };

protected:
	// ��������� ������������ ���������� ���������
		//virtual void DoOnSystemMessage(CGIncomingMessage& message);

	// ������� ��������� ���������� ���������
		//void DoOnParameterValue(CGIncomingMessage& message);

	void DoOnInstrumentInfo(CGIncomingMessage& message);
	void DoOnErrorMessage(CGIncomingMessage& message);
	void DoOnInstrumentStateInfo(CGIncomingMessage& message);

protected:
	// ��������� ����� (� ������� �� ��������� ��������� ����� ����� ����!)
	INT32 m_SerialNumber;
	// ������, ���������� ��������� ������������� ����������� ���������
	CString m_IncomingMessageLogString;

	short m_targetVial;

	struct CSVersionInfo
	{
		BYTE Software;
		BYTE Hardware;
		BYTE Subversion;
		BYTE Year;
		BYTE Month;
		BYTE Day;
	}
	m_Version;
					 
	string READ_SERIAL_NUMBER = "FF";
	string READ_SAMPLER_STATUS = "1D";

	//void (CGAutosampler::* samplerStatus)(CGIncomingMessage&) = &CGAutosampler::DoOnInstrumentStateInfo;	

	//m = {
	//	{ READ_SAMPLER_STATUS, &CGAutosampler::DoOnInstrumentStateInfo }
	//	//{ READ_SAMPLER_STATUS, std::bind(&CGAutosampler::DoOnInstrumentStateInfo, this)}
	//};

	


};

enum
{
	// ��������, �� �������������� � M����������������� ��������.
	c_AutosamplerStateReceived = 101,
};