#pragma once
#include "InstrumentBase\MasterInstrument.h"

class CGThermostat :
	public CGMasterInstrument
{
public:
	CGThermostat(void);
	~CGThermostat(void);

	bool IsWorking(){ return (m_TargetTemperature > 0); }
	int GetTargetTemperature() { return m_TargetTemperature; }
	double GetWorkTemperature() { return m_WorkTemperature*0.01; }
	double GetRoomTemperature() { return m_RoomTemperature*0.01; }

	void SetWorkTemperature(int workTemperature);
	void AskSerialNumber();

	// �������, ������� ������ ���������������� ��� ����������� �������� �������
	virtual bool ProcessIncomingMessage(CGIncomingMessage& message);

public:
	// ����������� ������ ������� �������.
// ��������� ���������� � ������ � ������������ �������
	virtual int GetSerialNumber(); 
	virtual int GetSoftVersionNumber();
	virtual int GetHardVersionNumber();
// ������� ��� �������, ������������ ��� ������ � ������� ����������� ���������
	virtual const CString GetShortName() const { return _T("Thermo"); } ;

// ��������� ����������� ���������
	virtual bool GetMeasuredValues(double values[]) { return false; } 
// �������� ������� ������� ������ ��������� ������
	virtual bool ResetErrorState() { return true; };

protected:
// ��������� ������������ ���������� ���������
	//virtual void DoOnSystemMessage(CGIncomingMessage& message);

// ������� ��������� ���������� ���������
	//void DoOnParameterValue(CGIncomingMessage& message);

	void DoOnInstrumentInfo(CGIncomingMessage& message);
	void DoOnErrorMessage(CGIncomingMessage& message);
	void DoOnInstrumentStateInfo(CGIncomingMessage& message);

protected:
// ��������� ����� (� ������� �� ��������� ��������� ����� ����� ����!)
	INT32 m_SerialNumber;
// ������, ���������� ��������� ������������� ����������� ���������
	CString m_IncomingMessageLogString;

	short m_TargetTemperature;
	short m_WorkTemperature;
	short m_RoomTemperature;

	struct CSVersionInfo
	{
		BYTE Software;
		BYTE Hardware;
		BYTE Subversion;
		BYTE Year;
		BYTE Month;
		BYTE Day;
	}
	m_Version;

};

enum
{
	// ��������, �� �������������� � ����������������� ��������.
	c_ThermostatStateReceived = 100,
};


// ������������� �������.
const BYTE NET_ADDR_THERMO     = 120;	

// �������, ���������� �� ���������� � ������
const BYTE GET_INSTRUMENT_INFO        = 1;
const BYTE SET_WORK_MODE              = 2;

// C��������, ���������� ��������
const BYTE INSTRUMENT_STATE_INFO      = 16;
const BYTE ERROR_INFO                 = 17;
