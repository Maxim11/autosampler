#ifndef INSTRUMENT_CONSTANTS_PITON3
#define INSTRUMENT_CONSTANTS_PITON3

// ������ ���� �������� ����������� ��������, �������������� ��
// ��������������� ���������. �� ��� �� ��� ������������ ��� ����������
// �� ����������

// ������ ��������� � ��������� ����
const BYTE NET_ADDR_PITON     = 130;	

// �������, ���������� �� ���������� � ������
const BYTE GET_INSTRUMENT_INFO        = 1;
const BYTE SET_WORK_MODE              = 2;
const BYTE SET_WORK_PARAMETER         = 3;
const BYTE GET_WORK_PARAMETER         = 4;
const BYTE SET_WORK_PARAMETERS        = 5;

// C��������, ���������� ��������
const BYTE MEASUREMENT_RESULT         = 8;
const BYTE FAST_SCAN_RESULT           = 9;
const BYTE INSTRUMENT_STATE_INFO      = 16;
const BYTE ERROR_INFO                 = 17;
const BYTE EVENT_INFO                 = 18;

// ������� � ���������, ������������ � ��������������� �����.
const BYTE GET_CAN_NODE_INFO          = 32;
const BYTE SET_CAN_NODE_INFO          = 33;
const BYTE ENABLE_CAN_TRACING         = 34;
const BYTE GET_PARAMETER              = 35;
const BYTE SET_PARAMETER              = 36;
const BYTE GET_DIAGNOSTIC_INFO        = 37;
const BYTE SET_DIAGNOSTIC_MODE        = 38;

const BYTE CAN_NODE_PACKET            = 48;
const BYTE SYSTEM_STATISTIC_INFO      = 49;

/////////////////////////////////////////////////////////

// ������ ����������.
const BYTE MANAGER_WAIT                  = 0;
const BYTE MANAGER_FILL                  = 1;
const BYTE MANAGER_DISCHARGE             = 2;
const BYTE MANAGER_ISOCRATIC_WORK        = 3;
const BYTE MANAGER_GRADIENT_WORK         = 4;
const BYTE MANAGER_SET_VALVE             = 5;

// ��������� ������.
const BYTE MANAGER_ERROR                 = 10;
const BYTE MANAGER_INIT                  = 11;
const BYTE MANAGER_SET_INITIAL_PRESSURE  = 12;
const BYTE MANAGER_PRESSURE_RELEASING    = 13;
const BYTE MANAGER_PRESSURE_RELEASED     = 14;
const BYTE MANAGER_PRESSURE_MAXIMUM      = 15; // �������� ������������� �������� � ������ ������ ������������.
const BYTE MANAGER_PRESSURE_MINIMUM      = 16; // �������� ������������ �������� � ������ ������ ������������.
const BYTE MANAGER_PRESSURE_OVERFLOW     = 17; // ��������� �������� ��� �����.

// ��������� ������.
//const BYTE INTERNAL_CONTROL_MODE       = 1;
//const BYTE EXTERNAL_CONTROL_MODE       = 2;

// ���������� ������ �����
const BYTE MAIN_CONTROLLER_UNIT       = 1;
const BYTE MOTOR_UNIT                 = 2;
const BYTE KEYBOARD_UNIT              = 3;

// �������������� ������� ����������


const BYTE MU_SET_FLOW               = 1;
const BYTE MU_SET_STAGE_DATA         = 2;
const BYTE MU_ACTIVATE_STAGE         = 3;
const BYTE MU_SET_MIN_MAX_PRESSURE   = 4;
const BYTE MU_SET_FLOW_RATE_FOR_RELEASE_PRESSURE         = 5;


// ����� ������� ������, ���������� � INSTRUMENT_STATE_INFO
const BYTE MODE_MASK_EXTERNAL          = 0x01;
const BYTE MODE_MASK_ANALYSIS          = 0x02;
const BYTE MODE_MASK_AUTOTEST          = 0x04;
const BYTE MODE_MASK_AUTOCALIBRATION   = 0x08;
const BYTE MODE_MASK_VALVE_ON          = 0x80;

const BYTE ERROR_TYPE_WARNING		 = 0;
const BYTE ERROR_TYPE_NON_FATAL	 = 1;
const BYTE ERROR_TYPE_FATAL  		 = 2;
const BYTE ERROR_FLAG_EXTENDED 	 = 0x80;

const BYTE CAN_TRACE_STATUS_FLAG       = 0x01;
const BYTE CAN_TRACE_SET_FLAG          = 0x02;
const BYTE CAN_TRACE_GET_FLAG          = 0x04;
const BYTE CAN_TRACE_RESULT_FLAG       = 0x08;
const BYTE CAN_DISABLE_TRACING_FLAG    = 0x80;

const BYTE SYS_STAT_CPU_USAGE           =   1;
const BYTE SYS_STAT_QUEUE_USAGE         =   2;
const BYTE SYS_STAT_STACK_USAGE         =   3;

const BYTE DIAGNOSTIC_GET_ERROR_COUNTERS  =   1;

/////////////////////////////////////////////////////////
// ������ ����� �� ���� CAN
const BYTE   MOTOR_NODE_ADDR           = 7;

#endif // INSTRUMENT_CONSTANTS_PITON3
