#include "StdAfx.h"
#include "Autosampler.h"
#include "UsbDevices.h"
#include "InstrumentBase/DChannel.h"
#include <map>

CGAutosampler::CGAutosampler(void)
{
	
	opCode[READ_SAMPLER_STATUS] = &CGAutosampler::DoOnInstrumentStateInfo;
	//funcPointer p = (funcPointer)opCode[READ_SAMPLER_STATUS];

	opCode = {
		{READ_SAMPLER_STATUS, (funcPointer)opCode[READ_SAMPLER_STATUS]},
		//{ READ_SAMPLER_STATUS, (void (*)(CGIncomingMessage&))DoOnInstrumentStateInfo }
		//{ READ_SAMPLER_STATUS, std::bind(&CGAutosampler::DoOnInstrumentStateInfo, this)}
	};
	m_NetAddress = NET_ADDR_AUTOSAMPLER;
	m_InstrumentId = NET_ADDR_AUTOSAMPLER;
	m_InstrumentModel = (NET_ADDR_AUTOSAMPLER << 8);
	m_SerialNumber = -1;
	m_InternalTimeUnitToSec = 1;

	m_UsbVendorID = c_UsbVendorId_Lumex;
	m_UsbProductID = c_UsbProductId_Thermostat;

	m_targetVial = 0;
}

CGAutosampler::~CGAutosampler(void)
{
}

void CGAutosampler::chkCalc(char* ps, char* sumar)
{
	char itemer;
	char summ = 0;
	char mapp[] = "0123456789ABCDEF";
	//���������� ����� ������ ��� ���������� xor ���� ��������, �������� ������������� 0
	while (*ps != '\0')
	{
		summ ^= *ps;
		ps++;
	}

	/*�������� ����� ���� � ASCII ������*/
	itemer = ((unsigned char)summ >> 4) & 0x0f; //0x0f;
	sumar[0] = mapp[itemer];

	itemer = summ & 0x0f;
	sumar[1] = mapp[itemer];
}

void CGAutosampler::AskSerialNumber()
{
	// �������� �� /*������*/ ����� �������, /*�� �*/ a ������ ������
	//CGOutgoingMessage message(GetNetAddress(), GET_INSTRUMENT_INFORM);

	// ��� ����������� ������� ����, ��� ������ �������� ���������������
	// ���������� � �� ���� ���������� ��������������� ��������� ����������.
	//message << BYTE(1);

	//CString string = _T("GET_INSTRUMENT_INFO");
	//SendOutgoingMessage(message, &string);

	//TRACE(_T("<- ") + string + '\n');
	char dataForCommand[60] = "$FF00000000001001\r";

	SendOutgoingMessage(dataForCommand);
}


void CGAutosampler::StartWashExecution(int targetVial, int wasteVial, int washVolume, char* washOn)
{
	char dataForCommand[60] = "$0B000000000020";
	char cycles[] = "01";
	char buffer[8];
	bool switchOn = (targetVial > 0);

	if (targetVial == 0) strcpy(dataForCommand, "$0A00000000001070\r");

	else
	{
		itoa(targetVial, buffer, 16);
		_strupr_s(buffer);
		strcat(dataForCommand, buffer);

		itoa(washVolume, buffer, 16);
		int repeat = 8 - strlen(buffer);

		for (int i = 0; i < repeat; ++i)
		{
			strcat(dataForCommand, "0");
		}

		_strupr_s(buffer);

		strcat(dataForCommand, buffer);
		strcat(dataForCommand, cycles);
		strcat(dataForCommand, washOn);

		itoa(wasteVial, buffer, 16);
		_strupr_s(buffer);
		strcat(dataForCommand, buffer);

		int buferDataSize = strlen(dataForCommand);

		chkCalc(&dataForCommand[1], &dataForCommand[buferDataSize]);
		strcat(dataForCommand, "\r");
	}

	SendOutgoingMessage(dataForCommand);
	//CGOutgoingMessage message(GetNetAddress(), SET_WORKING_MODE);
	//message << short(switchOn);
	//message << short(targetVial);

	//CString string;
	//string.Format(_T("SET_WORK_MODE T=%d"), targetVial);
	//SendOutgoingMessage(message, &string);
	//TRACE(_T("<- ") + string + _T("\n"));
}

int CGAutosampler::StatusWashExecution()
{
	char* buferData = new char[100];
	char dataForCommand[] = "$1D00000000001074\r";
	CGIncomingMessage message;

	for (int i = 0, n = 350; i < n; i++)
	{
		BYTE dataBufer[1024];
		Sleep(1000);
		SendOutgoingMessage(dataForCommand);
		//InstrumentMessageProcessingLoop();
		//ProcessIncomingMessage(message);
		//ReplyFromComPort(dataBufer);
		//CString a = DumpMessage(message, 0);		

		if (buferData[7] == '0') break;
	}

	return 0;
}

bool CGAutosampler::ProcessIncomingMessage(CGIncomingMessage& message)
{
	//int  d = message.GetDataSize_ASCII();
	const BYTE* data = message.GetData_ASCII();
	//BYTE *messageID = message.GetMessageID_HTA();
	BYTE* messageID = { message.GetMessageID_HTA() };
	string e = "d";

	(this->*(funcPointer)opCode[e])(message);
	funcPointer p = (funcPointer)opCode[e];
	(this->*p)(message);
	//char* opCode = ["sad" => "FF"];


	//BYTE* d = (BYTE*)"FF";
	//BYTE* c = messageID[0];
	//int s = 1; 
	//bool j = (messageID[0] == d);
	//if (messageID[0] == d) s = 2;

	//const BYTE* dat = (BYTE*)"$03000000000010";
	//BYTE dat2[] = "$03";
	//int g = memcmp(dat, dat2, 3);
	//struct Op_Code
	//{
	//	BYTE READ[4] = "$03";
	//} comand;

	//for (auto& el : comand)
	//{

	//}
	////if (memcmp(dat, dat2, 3)) cout << "OK"; exit;
	//int g2 = memcmp(dat, comand.READ, 3);
	//switch (memcmp(dat, dat2, 3))
	//{
	//case 0:
	//	cout << "OK";
	//	break;
	//}



	//BYTE messageID = data[1];// +data[2];

	//ASSERT(instrumentID == m_NetAddress);

	//m_IncomingMessageLogString.Empty();

	/*switch (messageID[0])
	{*/
	/*case GET_INSTRUMENT_INFORM:
		DoOnInstrumentInfo(message);
		break;
	case SET_WORKING_MODE:
		m_IncomingMessageLogString.Format(_T("SET_WORK_MODE(%d)"), message.GetMessageID());
		break;
	case INSTRUMENT_STATE_INFORM:
		DoOnInstrumentStateInfo(message);
		break;
	case ERROR_INFORM:
		DoOnErrorMessage(message);
		break;*/
		//default:
		//	//		if(m_IsDiagnosticEnabled)
		//	//			ProcessDiagnosticMessage(message);
		//	break;
		//}

		//// ����� ������ �������� � m_IncomingMessageLogString �� ��������,
		//// ���������� ���������� �� �� ��� ��������� ������
		//TRACE(_T("-> %s\n"), m_IncomingMessageLogString);

		//// ���������� ������ � ���� ��������� (���� ��� ������)
		//WriteDebugInfo(_T("-> ") + m_IncomingMessageLogString + _T("\n"));
	return true;
}

void CGAutosampler::DoOnInstrumentStateInfo(CGIncomingMessage& message)
{
	/*	BYTE logicalUnit;
		BYTE unitState;
		CString string;
	*/
	struct CSThermoStateInfo
	{
		BYTE  Mode;
		BYTE  State;
		short TargetTemperature;
		short WorkTemperature;
		short RoomTemperature;
	};

	CSThermoStateInfo* thermoStateInfo = (CSThermoStateInfo*)message.GetData();

	//m_TargetTemperature = thermoStateInfo->TargetTemperature;
	//m_WorkTemperature = thermoStateInfo->WorkTemperature;
	//m_RoomTemperature = thermoStateInfo->RoomTemperature;

	// ���������� UI.
	m_InstrumentState = c_AutosamplerStateReceived;

	m_IncomingMessageLogString.Format(_T("THERMO_STATE(%d) DevMode=0x%02x DevState=0x%02x TargetT=%d WorkT=%d AirT=%d"),
		message.GetMessageID(),
		thermoStateInfo->Mode,
		thermoStateInfo->State,
		thermoStateInfo->TargetTemperature,
		thermoStateInfo->WorkTemperature,
		thermoStateInfo->RoomTemperature
	);
}

void CGAutosampler::DoOnInstrumentInfo(CGIncomingMessage& message)
{
	int oldSerialNumber = m_SerialNumber;
	BYTE hardwareMask;
	BYTE type;

	// ��� �������
	message >> type;
	// ����� ������ �������� � ������
	message >> m_Version.Software;
	message >> m_Version.Subversion;
	message >> m_Version.Hardware;
	message >> m_Version.Year;
	message >> m_Version.Month;
	message >> m_Version.Day;
	message >> hardwareMask;

	// � ��������� �������� ����� ���������� �����. ��� �������� ������ ������ ��� ��.
	m_SerialNumber = GetUsbSerialNumber();
	if (m_SerialNumber != oldSerialNumber)
		m_InstrumentState = c_InstrumentInfoChanged;

	m_IncomingMessageLogString.Format(_T("GET_INSTRUMENT_INFO(%d) Type=%d Ver=%d.%d.%d %02d-%02d-%02d N=%d"),
		message.GetMessageID(),
		type,
		m_Version.Software,
		m_Version.Subversion,
		m_Version.Hardware,
		m_Version.Year,
		m_Version.Month,
		m_Version.Day,
		m_SerialNumber
	);
}
void CGAutosampler::DoOnErrorMessage(CGIncomingMessage& message)
{
	CSErrorInfo* errorInfo = (CSErrorInfo*)message.GetData();

	// �������������� �������� ���� �� ������������.
	m_ErrorCode = errorInfo->ErrorCode;

	m_IncomingMessageLogString.Format(_T("ERROR(%d) N=%d.%d Type=%d Length=%d Value=%d"),
		message.GetMessageID(),
		errorInfo->ErrorCode >> 8,
		errorInfo->ErrorCode & 0xFF,
		errorInfo->Type,
		errorInfo->Length,
		errorInfo->Value);
}

int CGAutosampler::GetSerialNumber()
{
	return 1;
}

int CGAutosampler::GetSoftVersionNumber()
{
	return 1;
}

int CGAutosampler::GetHardVersionNumber()
{
	return 0;
}

