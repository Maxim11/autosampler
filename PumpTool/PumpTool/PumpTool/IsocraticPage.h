#pragma once

#include "ToolPage.h"
//#include "Controls/BitmaskListBox.h"

/////////////////////////////////////////////////////////////////////////////
// CMIsocraticPage dialog

class CGPiton;

class CMIsocraticPage : public CMToolPage
{
	DECLARE_DYNCREATE(CMIsocraticPage)

// Construction
public:
	CMIsocraticPage();
	~CMIsocraticPage();

	//virtual void ProcessStatusFrame(const CGCanFrame* canFrame);
	void ShowValuesPumpA(bool show = true);
	void CheckPumpState();

	void SetPump(CGPiton* pump) { m_PumpA = pump; }
	void SetPumpSerialNumber(int serialNumber);

	void EnableControls(bool enable);

	void SaveToRegistry();
	void LoadFromRegistry();
	void SaveEluentInfo();

	void ShowInfoString(int stringID);


// Dialog Data
	enum { IDD = IDD_PAGE_ISOCRATIC };

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Implementation
public:
	afx_msg void OnBnClickedRadioFill();
	afx_msg void OnBnClickedRadioDischarge();
	afx_msg void OnBnClickedRadioWork();
	//afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedCheckStartStop();
	afx_msg void OnBnClickedButtonSettings();

	afx_msg void OnBnClickedCheckStartFill();
	afx_msg void OnBnClickedCheckStartDischarge();
	afx_msg void OnBnClickedCheckStartWork();
	afx_msg void OnBnClickedCheckVolumeFill();
	afx_msg void OnBnClickedCheckVolumeDischarge();

protected:
	DECLARE_MESSAGE_MAP()

	void ShowControls(bool forWorkMode);
	void SetNewMode(int mode);

	bool StartFillMode();
	bool StartDischargeMode();
	bool StartWorkMode();
	void StopCurrentMode();

	virtual void InitForRunningState();
	virtual void StartPressureReleasing(int flowRate);

protected:
	//int m_FlowRates[3];
	//int m_Volumes[3];

protected:
	CMSmartComboBox m_FillVolumeComboBox;
	CMSmartComboBox m_DischargeVolumeComboBox;
	CMSmartComboBox m_FlowRateComboBox;
	CMSmartComboBox m_StartPressureComboBox;
	CMSmartComboBox m_StartFlowRateComboBox;

	CMSmartComboBox m_EluentsComboBox;

	// ���� ������� ��� ���������� ���������� �� ������� � ���������� ������.
	CString m_EluentASerialNumberKey;

	bool m_IsWorking;
public:
	afx_msg void OnCbnSelchangeComboEluentA();
	afx_msg void OnCbnSelchangeComboFlowRateA();
	afx_msg void OnEnChangeEditVolumeA();
};
