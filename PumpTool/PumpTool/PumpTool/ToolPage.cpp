// ToolPage.cpp: ���� ����������
//

#include "stdafx.h"
#include "PumpTool.h"
#include "ToolPage.h"

#include "MainPropertySheet.h"
#include "CanFrame.h"
#include "Instrument/Piton.h"

#include "ErrorDialog.h"
#include "ValveCloseDialog.h"
//#include "PressureReleasingDialog.h"

// CMToolPage
CSPumpSettings CMToolPage::m_Settings;
int  CMToolPage::sm_CurrentMode = PUMP_MODE_UNKNOWN;
//bool CMToolPage::sm_IsRunning = false;

IMPLEMENT_DYNAMIC(CMToolPage, CMFCPropertyPage)

CMToolPage::CMToolPage(UINT nIDTemplate, UINT nIDCaption)
	: CMFCPropertyPage(nIDTemplate, nIDCaption)
{
	//m_IsErrorShowing = false;
	m_ErrorDialog = 0;
	//m_PressureReleasingDialog = 0;
	m_PumpA = 0;
	m_PumpB = 0;
	m_SelectedMode = PUMP_MODE_UNKNOWN;
}

CMToolPage::~CMToolPage()
{
	if(m_ErrorDialog != 0)
	{
		delete m_ErrorDialog;
	}
	/*
	if(m_PressureReleasingDialog != 0)
	{
		delete m_PressureReleasingDialog;
	}
	*/
}

// ����� ���� �� ����������� �� ������ ENTER.
BOOL CMToolPage::OnApply()
{
	return FALSE;
}

// ����� ���� �� ����������� �� ������ ESCAPE, ������� ��������������
// CPropertyPage::OnNotify() pNMHDR->code == PSN_RESET.
/*
void CMToolPage::OnReset()
{
	return;
}
*/

void CMToolPage::CreateTimeString(CString& text, const int volume, const int flowRate)
{
	if(sm_CurrentMode == PUMP_MODE_WAIT || sm_CurrentMode == PUMP_MODE_UNKNOWN
		|| flowRate == 0)
	{
		text.Empty();
		return;
	}

    // ����� �� ������� ���������� �������� ��� ������� �������� ������.
    int seconds = (volume > 0 ? (60*volume+flowRate/2)/flowRate : 0);
	int minutes = seconds/60;
	int hours = minutes/60;

	seconds -= minutes*60;
	minutes -= hours*60;

	text.Format(_T("%2d:%02d:%02d"), hours, minutes, seconds);
}

#if 0
bool CMToolPage::ShowPressureReleasingDialog(int from, int to, int headerTextId)
{
	if(m_PressureReleasingDialog == 0)
	{
		m_PressureReleasingDialog = new CMPressureReleasingDialog;
	}
#if !USE_MODAL_P_R_DIALOG
	if(m_PressureReleasingDialog->GetSafeHwnd() == 0)
	{
		m_PressureReleasingDialog->Create(IDD_DIALOG_PRESSURE_RELEASING, this);
	}
	m_PressureReleasingDialog->SetProgressLimits(from, to);
	m_PressureReleasingDialog->ShowWindow(true);
	return true;
#else
	m_PressureReleasingDialog->SetProgressLimits(from, to);
	m_PressureReleasingDialog->SetPumps(m_PumpA, m_PumpB);
	return m_PressureReleasingDialog->DoModal() == IDOK;
#endif

	if(headerTextId != 0)
	{
		CString text;
		text.LoadString(headerTextId);
		m_PressureReleasingDialog->SetWindowText(text);
	}

}
#endif

/*
void CMToolPage::SendWriteCommand(CGCanFrame* canFrame)
{
}

void CMToolPage::SendReadCommand(int page)
{
}

void CMToolPage::SendReadCommand105M(int page)
{
}

void CMToolPage::CreateVersionString(const CGCanFrame* canFrame)
{
	CString string;
	string.Format(_T("v. %d.%d (%d/%d/%d)"),
		canFrame->m_Data.Byte[1],
		canFrame->m_Data.Byte[2],
		canFrame->m_Data.Byte[5],
		canFrame->m_Data.Byte[4],
		canFrame->m_Data.Byte[3]+2000
	);

	if(string != m_VersionString)
	{
		m_VersionString = string;
		// ����� ������������ ��������� ���������� ���������� �� ��������� ����.
		Invalidate(true);
	}
}

void CMToolPage::ClearVersionString()
{
	if(!m_VersionString.IsEmpty())
	{
		m_IsFirmwareFor105M = false;
		m_VersionString.Empty();
		Invalidate(true);
	}
}
*/


int CMToolPage::GetIntValue(int controlID)
{
	const int MAX_SIZE = 6;
	TCHAR text[MAX_SIZE];

	GetDlgItem(controlID)->GetWindowText(text, MAX_SIZE);
	return _ttoi(text);
}

void CMToolPage::SetIntValue(int controlID, int value)
{
	CString text;
	text.Format(_T("%d"), value);
	GetDlgItem(controlID)->SetWindowText(text);
}

int CMToolPage::GetCheckedRadioButton(const int buttonID[])
{
	for(int i = 0; buttonID[i] != 0; i++)
	{
		CButton* radioButton = (CButton*)GetDlgItem(buttonID[i]);
		if(radioButton->GetCheck() != 0)
		{
			return i;
		}
	}
	return 0;
}

void CMToolPage::CreateLargeFont(const CWnd* control)
{
	if(m_LargeFont.GetSafeHandle() == 0)
	{
		LOGFONT logFont = {0};
		control->GetFont()->GetLogFont(&logFont);
		logFont.lfHeight += logFont.lfHeight/2;

		m_LargeFont.CreateFontIndirect(&logFont);
	}
}

void CMToolPage::EnableControls(const int controlID[], const int enable)
{
	for(int i = 0; controlID[i] != 0; i++)
	{
		CWnd* control = GetDlgItem(controlID[i]);
		control->EnableWindow(enable);
	}
}

void CMToolPage::EnableModeButtons(const int enable)
{
	const int buttonsID[] = 
	{
		IDC_CHECK_START_FILL_A,
		IDC_CHECK_START_DISCHARGE_A,
		IDC_CHECK_START_WORK_A,
		0
	};
	EnableControls(buttonsID, enable);
}

void CMToolPage::InitEluentsComboBox(const int comboBoxID, const CString& savedName, const int select)
{
	CString text;
	CComboBox* eluentsComboBox = (CComboBox*)GetDlgItem(comboBoxID);
	for(int i = 0; i < 10; i++)
	{
		text.LoadString(IDS_TEXT_ELUENT_0+i);
		eluentsComboBox->AddString(text);
	}
	if(savedName.IsEmpty())
	{
		eluentsComboBox->SetCurSel(select);
	}
	else
	{
		if(eluentsComboBox->SelectString(0, savedName) == CB_ERR)
		{
			eluentsComboBox->AddString(savedName);
			eluentsComboBox->SelectString(0, savedName);
		}
	}
}

void CMToolPage::InitEluentsComboBox(class CMSmartComboBox* eluentsComboBox)
{
	CString text;
	for(int i = 9; i >= 0; i--)
	{
		text.LoadString(IDS_TEXT_ELUENT_0+i);
		eluentsComboBox->AddString(text);
	}

	eluentsComboBox->SetCurSel(0);
}

void CMToolPage::EnableOtherPages(bool enable)
{
	CMToolPage* sourcePage = (enable ? 0 : this);
	((CMMainPropertySheet*)GetParent())->EnableAllPages(sourcePage, enable);
}

void CMToolPage::ShowValuesForPump(CGPiton* pump, const int controlID[])
{
	enum
	{
		c_ControlFlowRate,
		c_ControlVolume,
		c_ControlPressure,
		c_ControlProgress,
		c_ControlPercents,
		c_ControlTime,
	};

	CString text;
	int volume = pump->GetCurrentVolume();
	int flowRate = pump->GetCurrentFlowRate();

	if(volume < 0)
	{
		if(volume > -c_MaxPumpVolume/10)
		{
			volume = 0;
		}
		else
		{
			// ������� �� ������� ���������� 16-������ ��������, � ������������
			// ����� �� ���������� � �������� ����������.
			volume = UINT16(volume);

			if(volume > c_MaxPumpVolume)
			{
				volume = c_MaxPumpVolume;
			}
		}
	}
	if(flowRate < 0)
	{
		flowRate = -flowRate;
	}

	text.Format(_T("%d"), flowRate);
	GetDlgItem(controlID[c_ControlFlowRate])->SetWindowText(text);

	text.Format(_T("%d"), volume);
	GetDlgItem(controlID[c_ControlVolume])->SetWindowText(text);

	text.Format(_T("%.1f"), pump->GetCurrentPressure()/10.0);
	GetDlgItem(controlID[c_ControlPressure])->SetWindowText(text);

	if(volume > c_WorkPumpVolume)
	{
		volume = c_WorkPumpVolume;
	}

	int percents = (volume*100 + c_WorkPumpVolume/2)/c_WorkPumpVolume;
	CProgressCtrl* progressCtrl = ((CProgressCtrl*)GetDlgItem(controlID[c_ControlProgress]));
	if(progressCtrl->GetPos() != percents)
	{
		progressCtrl->SetPos(percents);
	}
	text.Format(_T("%d"), percents);
	GetDlgItem(controlID[c_ControlPercents])->SetWindowText(text+CString(_T('%')));

	bool showTimeString = pump->IsFlowRateOk();

	if(sm_CurrentMode == PUMP_MODE_UNKNOWN && !pump->IsInWaitMode())
	{
		// ��������� �������� ��� ���������� ������.
		InitForRunningState();
		if(sm_CurrentMode != PUMP_MODE_UNKNOWN)
		{
			showTimeString = true;
		}
	}

	if(sm_CurrentMode == PUMP_MODE_FILL)
	{
		volume = c_WorkPumpVolume-volume;
	}

	if(showTimeString)
	{
		CreateTimeString(text, volume, flowRate);
	}
	else
	{
		text.Empty();
	}
	GetDlgItem(controlID[c_ControlTime])->SetWindowText(text);
}

void CMToolPage::ResetValuesForPump(const int controlID[])
{
	enum
	{
		c_ControlFlowRate,
		c_ControlVolume,
		c_ControlPressure,
		c_ControlProgress,
		c_ControlPercents,
		c_ControlTime,
	};

	CString text;
	GetDlgItem(controlID[c_ControlFlowRate])->SetWindowText(text);
	GetDlgItem(controlID[c_ControlVolume])->SetWindowText(text);
	GetDlgItem(controlID[c_ControlPressure])->SetWindowText(text);

	((CProgressCtrl*)GetDlgItem(controlID[c_ControlProgress]))->SetPos(0);
	GetDlgItem(controlID[c_ControlPercents])->SetWindowText(text);
	//GetDlgItem(controlID[c_ControlPercents])->SetWindowText(text+CString(_T('%')));

	GetDlgItem(controlID[c_ControlTime])->SetWindowText(text);
}

bool CMToolPage::StartPumpMode(int mode, int maxPressure, int flowRate)
{
#if 0
	// ���������� ����� ��� ���������� ������ ����� ������ ��������.
	m_SelectedMode = mode;
	// ���������� ���������� �� ������ ������ ��������.
	((CMMainPropertySheet*)GetParent())->SetMasterPage(this); 
	if(maxPressure > 0)
	{
		//m_PumpA->SetValues(10000, 15);
		if(m_PumpA->GetCurrentPressure() > maxPressure)
		{
			StartPressureReleasing(flowRate);
#if !USE_MODAL_P_R_DIALOG
			ShowPressureReleasingDialog(m_PumpA->GetCurrentPressure(), maxPressure);
			EnableModeButtons(false);
			return false;
#else
			if(!ShowPressureReleasingDialog(m_PumpA->GetCurrentPressure(), maxPressure))
			{
				// ������ ������ ������ � �������.
				((CMMainPropertySheet*)GetParent())->SetMasterPage(0); 
				return false;
			}
#endif
		}
	}

#endif
/*
	bool valveOpen = (mode == PUMP_MODE_DISCHARGE);

	// ��� ������ ������ ������� ����.
	CMValveCloseDialog dialog(this, valveOpen);
	if(dialog.DoModal() == IDCANCEL)
	{
		m_SelectedMode = PUMP_MODE_UNKNOWN;
		return false;
	}
*/
	return true;
}

void CMToolPage::SetStartButtonText(const int textID)
{
	CButton* startButton = (CButton*)GetDlgItem(IDC_CHECK_START_STOP);
	CString text;
	ASSERT(startButton != 0);

	text.LoadString(textID);
	startButton->SetWindowText(text);
}

void CMToolPage::ShowErrorMessage(const int textID, const int errorTextID, const int errorCode)
{
	if(m_ErrorDialog == 0)
		m_ErrorDialog = new CMErrorDialog;

	if(!m_ErrorDialog->SetErrorCode(textID, errorTextID, errorCode))
	{
		// ������������ ����� ������������ ��������� �� ������
		return;
	}

	if(m_ErrorDialog->GetSafeHwnd() == 0)
	{
		m_ErrorDialog->Create(IDD_DIALOG_ERROR_MESSAGE, this);
	}
	else
	{
		m_ErrorDialog->Update();
	}


/*
	if(!m_IsErrorShowing)
	{
		CString text;
		CString errorText;
		text.Format(textID, errorCode);
		errorText.LoadString(errorTextID+errorCode);
		text += CString(_T('\n')) + errorText;

		m_IsErrorShowing = true;
		AfxMessageBox(text);
		m_IsErrorShowing = false;
	}
*/
}

void CMToolPage::SetPumpSerialNumber(int serialNumber, int stringID, int controlID)
{
	if(GetSafeHwnd() != 0)
	{
		CString textPump;
		CString textNumber;
		textPump.LoadString(stringID);
		if(serialNumber > 0)
		{
			CString textNumber;
			textNumber.Format(_T(" N%d"), serialNumber);
			textPump += textNumber;
		}
		GetDlgItem(controlID)->SetWindowText(textPump);
	}
}


// �������������� ��������� ��������.
void CMToolPage::UpdatePageHeader()
{
	// ��� ������ ����������� �������� ��������� ��������� �������
	// ����������� ���������. ������� �������������� ���� �����.
	CRect rect;
	int headerHeight = ((CMFCPropertySheet*)GetParent())->GetHeaderHeight();
	GetClientRect(&rect);
	rect.bottom = rect.top + headerHeight;
	InvalidateRect(&rect, true);
}

// ���������� ��������� � ����������� ��������� ��������
void CMToolPage::ShowToolTipMessage(CString& message, CWnd* control, int interval)
{
	if(m_TrackingToolTip.GetSafeHwnd() == 0)
	{
		if(!m_TrackingToolTip.Create(this))
			return;
		m_TrackingToolTip.SetMaxTipWidth(300);
	}

	ASSERT(control != 0);

	CRect rect;
	control->GetWindowRect(&rect);
	m_TrackingToolTip.SetPosition(rect.right+10, rect.top);
	m_TrackingToolTip.Activate(this, message, true, interval);
}

// ������� ��������� � ����������� ��������� ��������
void CMToolPage::HideToolTipMessage()
{
	if(m_TrackingToolTip.GetSafeHwnd() != 0)
	{
		m_TrackingToolTip.Activate(this, 0, false, 0);
	}
}


BEGIN_MESSAGE_MAP(CMToolPage, CMFCPropertyPage)
	ON_WM_CLOSE()
END_MESSAGE_MAP()



// ����������� ��������� CMToolPage


